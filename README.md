# Bureau Mobile Android

Online Android application client of bureau web portal where employees can search and book desks.

# Instructions to add the google-services to the proyect  
## Please, be sure to replace app/google-services.json with the one of Firebase console,
## the one that is in the project is invalid

* Go the firebase console, [Google Firebase](https://console.firebase.google.com)
* Select the Proyect (Bureau-Production), the one with **com.jalasoft.bureau.production**
* Select Bureau for android, and select Setting page
* On the general tab, download google-services.json 
* Now, go to your android studio and paste the google-services.json on app/ and run the app
* Remember to verify that the auto-generated values contain the new values (app/build/generated/res/google-services/demo/debug/values/values.xml)