package com.jalasoft.bureau.ui.favorites.repository

import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.favorites.UserFav
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

open class MainFavoritesRepository
@Inject
constructor(private val favoritesProductionService: FavoritesProductionService): FavoritesRepository {

    override suspend fun getUserInfo(
    ): Response<UserFav>? {
        return withContext(Dispatchers.IO) {
            favoritesProductionService.getUserInfo()
        }
    }

    override suspend fun getFavoritesAssetsProduction(
        listFav: String?
    ): Response<List<Asset.AssetItem>>? {
        return withContext(Dispatchers.IO) {
            favoritesProductionService.getFavoritesAssets(listFav)
        }
    }

    override suspend fun deleteFavoriteAsset(
        idAsset: String
    ): Response<UserFav>? {
        return withContext(Dispatchers.IO) {
            favoritesProductionService.deleteFavoritesAsset(idAsset)
        }
    }

    override suspend fun getBookingsByDate(
        startTime: String,
        endTime: String,
        assetId: String
    ): Response<ArrayList<BookingResponse.Booking>>? {
        return  favoritesProductionService.getBookingsByDate(startTime, endTime, assetId)
    }
}