package com.jalasoft.bureau.ui.workstations.repository

import com.jalasoft.bureau.data.interfaces.FavoriteService
import com.jalasoft.bureau.data.model.favorites.UserFav
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class AssetDetailProductionService
@Inject
constructor(private val retrofit: Retrofit) {
    suspend fun getUserInfo(
    ): Response<UserFav>? {
        return retrofit.create(FavoriteService::class.java)?.getUserInfo()
    }

    suspend fun postFavoritesAsset(
        idAsset: String
    ): Response<UserFav>? {
        return retrofit.create((FavoriteService::class.java))?.postFavoriteAsset(
                idAsset
            )
    }

    suspend fun deleteFavoritesAsset(
        idAsset: String
    ): Response<UserFav>? {
        return retrofit.create((FavoriteService::class.java))?.deleteFavoriteAsset(
                idAsset
            )
    }
}