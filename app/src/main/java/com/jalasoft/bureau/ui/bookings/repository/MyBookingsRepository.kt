package com.jalasoft.bureau.ui.bookings.repository

import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.building.Building
import retrofit2.Response
import java.time.ZonedDateTime
import javax.inject.Inject

interface MyBookingsRepository {
    suspend fun getBookingsProduction(
        date: String,
        endTime: String,
        ownerEmail: String
    ): Response<List<BookingResponse.Booking>>?

    suspend fun getUpcommingBookings(
        date: String,
        ownerEmail: String
    ): Response<List<BookingResponse.Booking>>?

    suspend fun getPermanentBookings(
        ownerEmail: String
    ): Response<List<Asset.AssetItem>>?

    suspend fun getAssetDetailsProduction(
        assetId: String
    ): Response<Asset.AssetItem>?

    suspend fun getBuildingDetails(
        buildingId: String
    ): Response<Building.BuildingItem>?

    suspend fun deleteBookingProduction(
        idBooking: String
    ): Response<BookingResponse.Booking>?

    suspend fun getEventsProduction(
        workstationId: String,
        date: ZonedDateTime
    ): Response<ArrayList<BookingResponse.Booking>>?

    suspend fun getAreaDetailByAreaId(
        areaId: String
    ): Response<Area.AreaItem>?
}