package com.jalasoft.bureau.ui.buildings.view

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.adapter.BuildingAdapter
import com.jalasoft.bureau.data.interfaces.BuildingListener
import com.jalasoft.bureau.databinding.FragmentBuildingBinding
import com.jalasoft.bureau.ui.main.view.MainActivity
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.workstations.viewmodel.AssetResultFragmentViewModel
import com.jalasoft.bureau.utils.*
import com.jalasoft.bureau.utils.login.AuthStateManager
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BuildingFragment : Fragment(), BuildingListener {

    private lateinit var buildingBinding: FragmentBuildingBinding
    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()
    private val assetViewModel: AssetResultFragmentViewModel by activityViewModels()
    private lateinit var buildingRecycler: RecyclerView
    lateinit var adapter: BuildingAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_building, container, false)
        buildingBinding = FragmentBuildingBinding.bind(view)
        return buildingBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(!resources.getBoolean(R.bool.isTablet)){
            setupToolbar()
        }
        initRecycler()
        initSubscriptions()
        refreshLayoutBuilding()
    }

    private fun setupToolbar() {
        (activity as MainActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
        (activity as MainActivity).binding.appBar.toolbar.visibility = View.VISIBLE
        (activity as MainActivity).binding.appBar.title.visibility = View.GONE
        (activity as MainActivity).binding.appBar.logo.visibility = View.VISIBLE
    }

    private fun initRecycler() {
        buildingRecycler = buildingBinding.rvBuildings
        buildingRecycler.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
    }

    override fun didSelectBuildingItem(buildingId: String, buildingName: String, buildingTimeZone: String?) {
        FirebaseCrashlyticsUtils.setCrashlyticsKey(KEY_BUILDING_ID, buildingId)
        if (buildingId == adapter.selected) {
            (activity as MainActivity).binding.drawerLayout?.closeDrawer(Gravity.LEFT)
            return
        }
        mainActivityViewModel.prefs.saveData(buildingId, SHARED_BUILDING_ID)
        mainActivityViewModel.prefs.saveData(buildingName, SHARED_BUILDING_NAME)
        mainActivityViewModel.prefs.saveData(buildingTimeZone, SHARED_BUILDING_ZONE_ID)
        mainActivityViewModel.buildingName.value = buildingName
        mainActivityViewModel.buildingId.value = buildingId
        mainActivityViewModel.buildingTimeZone = buildingTimeZone
        assetViewModel.isAreaMapLoading.postValue(true)
        if(!resources.getBoolean(R.bool.isTablet)){
            val action: NavDirections =
                BuildingFragmentDirections.actionBuildingFragmentToNewAreaFragment(
                    buildingId,
                    buildingName
                )
            findNavController().navigate(action)
        } else {
            adapter.selected = buildingId
            adapter.notifyItemRangeChanged(0, adapter.buildings.count())
            (activity as MainActivity).binding.drawerLayout?.closeDrawer(Gravity.LEFT)
        }
    }

    private fun getBookingsAreasTotal() {
        mainActivityViewModel.getBuildingsAreas(adapter.buildings)
        mainActivityViewModel.buildingsAreas.observe(viewLifecycleOwner) {
            adapter.buildings = it
            adapter.notifyItemRangeChanged(0, adapter.buildings.count())
        }
    }

    private fun initSubscriptions() {

        mainActivityViewModel.userInfo.observe(viewLifecycleOwner) {
            val token = AuthStateManager.getInstance(requireContext()).current.accessToken ?: ""
            if(token.isNotEmpty()){
                mainActivityViewModel.getBuildings()
            }
        }

        mainActivityViewModel.myBuildings.observe(viewLifecycleOwner) { buildings ->
            buildingBinding.tvCount.text = String.format("(%s)", buildings.size.toString())
            adapter = BuildingAdapter(buildings, this, "")
            buildingRecycler.adapter = adapter
            buildingBinding.rvBuildings.scheduleLayoutAnimation()
            if(resources.getBoolean(R.bool.isTablet)){
                getBookingsAreasTotal()
            }
        }
        mainActivityViewModel.workstationIsLoading.observe(viewLifecycleOwner) { isLoading ->
            if (isLoading) {
                buildingBinding.progressCircular.visibility = View.VISIBLE
            } else {
                buildingBinding.progressCircular.visibility = View.INVISIBLE
            }
        }
    }

    private fun refreshLayoutBuilding() {
        val refreshLayout = buildingBinding.swipeRefreshLayout
        refreshLayout.setOnRefreshListener {
            mainActivityViewModel.getBuildings()
            refreshLayout.isRefreshing = false
        }
    }
}