package com.jalasoft.bureau.ui.profile.view

import android.content.Context
import android.content.res.TypedArray
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.jalasoft.bureau.BuildConfig
import com.jalasoft.bureau.R
import com.jalasoft.bureau.databinding.FragmentProfileBinding
import com.jalasoft.bureau.ui.bookings.viewmodel.MyBookingsViewModel
import com.jalasoft.bureau.ui.main.view.MainActivity
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.profile.viewmodel.ProfileViewModel
import com.jalasoft.bureau.utils.*
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProfileFragment : Fragment(), AdapterView.OnItemSelectedListener {

    lateinit var binding: FragmentProfileBinding
    val viewModel: ProfileViewModel by activityViewModels()
    private lateinit var appContext: Context
    private val mainViewModel: MainActivityViewModel by activityViewModels()
    val myBookingsViewModel: MyBookingsViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appContext = this.requireContext()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if(!resources.getBoolean(R.bool.isTablet)){
            setupToolbar()
        }
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        initButtonListeners()
        initAppearanceSpinner()
        initBuildingSwitch()
        setUserData()
        binding.versionLabel.text =
            if (BuildConfig.DEMO) String.format(getString(R.string.version), BuildConfig.VERSION_NAME) + getString(R.string.versionStaging)
            else String.format(getString(R.string.version), BuildConfig.VERSION_NAME)
        return binding.root
    }

    private fun setUserData() {
        binding.userName.text = mainViewModel.userInfo.value?.name ?: ""
        binding.userEmail.text = mainViewModel.userInfo.value?.email ?: ""
        val role = mainViewModel.userInfo.value?.role ?: ""
        if (role == ADMINISTRATOR_ROLE){
            binding.btnAdminMenu.visibility = View.VISIBLE
            binding.btnAdminMenu.isEnabled = true
        }
        // Necessary to check if string pic is null/empty due to picasso "IllegalArgumentException"
        Picasso.get().load(
            if ( !mainViewModel.userInfo.value?.picture.isNullOrEmpty()) mainViewModel.userInfo.value?.picture
            else "empty url")
            .placeholder(R.drawable.ic_profile)
            .error(R.drawable.ic_profile)
            .fit().into(binding.userImage)
    }

    private fun initButtonListeners() {
        binding.btnLogOut.setOnClickListener {
            showSignOutPopUp()
        }
        binding.btnAdminMenu.setOnClickListener{
            //will show admin page when created
        }
    }

    private fun initAppearanceSpinner() {
        var prefsFlag = DarkModeUtils.returnAppearanceFlag(mainViewModel.prefs.getData(
            SHARED_USER_APPEARANCE))
        if(prefsFlag!=1 && prefsFlag!=2) prefsFlag = 0
        val spinner = binding.appearanceSpinner
        spinner.onItemSelectedListener = this
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.appearance_options_array,
            R.layout.spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
            spinner.adapter = adapter
            spinner.setSelection(prefsFlag)
        }
    }

    private fun initBuildingSwitch() {
        val switchStatus = mainViewModel.prefs.getData(
            SHARED_USER_SAVE_BUILDING)
        val switch = binding.saveBuildingSwitch

        switch.isChecked = switchStatus.isNullOrEmpty() || switchStatus == "ON"

        switch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                mainViewModel.prefs.saveData("ON", SHARED_USER_SAVE_BUILDING)
            } else {
                mainViewModel.prefs.saveData("OFF", SHARED_USER_SAVE_BUILDING)
            }
        }
    }

    private fun showSignOutPopUp() {
        val builder = androidx.appcompat.app.AlertDialog.Builder(appContext)
        builder.setMessage(R.string.logout_confirm)
            .setCancelable(false)
            .setPositiveButton(R.string.yes) { _, _ ->
                mainViewModel.userInfo.postValue(null)
                mainViewModel.userBookFor.postValue(null)
                FirebaseCrashlyticsUtils.cleanUserId()
                FirebaseCrashlyticsUtils.cleanAllKeys()
                myBookingsViewModel.clearAllNotifications(requireContext()) { logOutAction() }
            }
            .setNegativeButton(R.string.no) { dialog, _ -> dialog.dismiss() }
        val alert = builder.create()
        alert.show()
    }

    private fun logOutAction() {
        viewModel.endSession(requireContext()){
            activity?.finish()
        }
        mainViewModel.prefs.clearData()
        viewModel.logOut(appContext)
    }

    private fun setupToolbar() {
        (activity as MainActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
        (activity as MainActivity).binding.appBar.toolbar.visibility = View.VISIBLE
        (activity as MainActivity).binding.appBar.title.visibility = View.GONE
        (activity as MainActivity).binding.appBar.logo.visibility = View.VISIBLE
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
        val options: TypedArray = resources.obtainTypedArray(R.array.appearance_options_array)
        val selected = options.getString(pos)
        mainViewModel.prefs.saveData(selected ?: "", SHARED_USER_APPEARANCE)
        DarkModeUtils.darkMode(selected)
        options.recycle()
    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // Do nothing
    }
}