package com.jalasoft.bureau.ui.workstations.repository

import com.jalasoft.bureau.data.interfaces.WorkstationService
import com.jalasoft.bureau.data.model.workstation.Workstation
import com.jalasoft.bureau.utils.timeFilter.TimeFilter
import org.joda.time.DateTime
import retrofit2.Response
import retrofit2.Retrofit
import java.util.*
import javax.inject.Inject

class WorkstationResultsProductionService
@Inject
constructor(private val retrofit: Retrofit) {
    suspend fun getWorkstationsByAreaIdProduction(
        areaId: String,
        timeFilter: TimeFilter
    ): Response<List<Workstation.WorkstationItem>>? {
        val startCal = Calendar.getInstance() as Calendar
        val startTime = DateTime(timeFilter.start)
        val endTime = DateTime(timeFilter.end)
        timeFilter.date.let {
        }
        startCal.set(Calendar.HOUR, startTime.hourOfDay)
        startCal.set(Calendar.MINUTE, startTime.minuteOfHour)
        val endCal = startCal.clone() as Calendar
        endCal.set(Calendar.HOUR, endTime.hourOfDay)
        endCal.set(Calendar.MINUTE, endTime.minuteOfHour)
        val start = DateTime(startCal).toString()
        val end = DateTime(endCal).toString()
        return retrofit.create(WorkstationService::class.java)
            ?.getWorkstations(areaId, start, end)
    }

    suspend fun getWorkstationsLayoutProduction(
        areaId: String
    ): Response<Workstation.WorkstationLayout>? {
        return retrofit.create(WorkstationService::class.java)
            ?.getWorkstationsLayout(areaId)
    }
}