package com.jalasoft.bureau.ui.workstations.repository

import com.jalasoft.bureau.data.interfaces.IBookingAvailability
import com.jalasoft.bureau.data.model.booking.BookingAvailability
import com.jalasoft.bureau.data.model.booking.BookingBody
import com.jalasoft.bureau.data.model.booking.BookingConfiguration
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.common.RecurrenceRule
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class BookingAvailabilityService
@Inject
constructor(private val retrofit: Retrofit) {

    suspend fun getBookingAvailabilityProduction(
        workstationId: String,
        startTime: String,
        endTime: String
    ): Response<BookingAvailability>? {
        return retrofit.create(IBookingAvailability::class.java)
            .getBookingAvailability(
                workstationId,
                startTime,
                endTime
            )
    }

    suspend fun postBooking(
        workstationId: String,
        startTime: String, endTime: String
    ): Response<BookingResponse.Booking>? {
        val bookingBody = BookingBody(endTime, false, RecurrenceRule(), startTime)
        return retrofit.create(IBookingAvailability::class.java)
            .postBooking(
                workstationId,
                bookingBody
            )
    }

    suspend fun getBookingsConfiguration(): Response<BookingConfiguration>? {
        return retrofit.create(IBookingAvailability::class.java)
            .getBookingConfiguration()
    }
}