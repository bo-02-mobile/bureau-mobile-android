package com.jalasoft.bureau.ui.login.viewmodel

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.lifecycle.ViewModel
import com.jalasoft.bureau.BuildConfig
import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.ui.login.view.LoginActivity
import com.jalasoft.bureau.ui.main.view.MainActivity
import com.jalasoft.bureau.utils.ERROR_TAG
import com.jalasoft.bureau.utils.ErrorMessage
import com.jalasoft.bureau.utils.login.AuthStateManager
import net.openid.appauth.*

class LoginViewModel : ViewModel() {
    lateinit var listener: BadRequest

    fun login(context: Context, onLoginCompletion: () -> Unit) {
        AuthorizationServiceConfiguration.fetchFromUrl(Uri.parse(BuildConfig.LOGIN_CONFIG_URI)) { config, ex ->
            if (config != null) {
                AuthStateManager.getInstance(context).replace(AuthState(config))
                val authRequestBuilder = AuthorizationRequest.Builder(
                    config,
                    BuildConfig.LOGIN_CLIENT_ID,
                    ResponseTypeValues.CODE,
                    Uri.parse(BuildConfig.LOGIN_REDIRECT_URI)
                ).setScope(BuildConfig.LOGIN_SCOPES)
                val authRequest = authRequestBuilder.build()
                val authService = AuthorizationService(context)
                authService.performAuthorizationRequest(
                    authRequest,
                    PendingIntent.getActivity(
                        context,
                        0,
                        Intent(context, MainActivity::class.java),
                        0 or PendingIntent.FLAG_MUTABLE
                    ),
                    PendingIntent.getActivity(
                        context,
                        0,
                        Intent(context, LoginActivity::class.java),
                        0 or PendingIntent.FLAG_MUTABLE
                    )
                )
                onLoginCompletion()
            } else if (ex != null) {
                listener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                Log.d(ERROR_TAG, ex.error.toString())
            }
        }
    }
}