package com.jalasoft.bureau.ui.bookings.repository

import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.ui.workstations.repository.EventService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.time.ZonedDateTime
import javax.inject.Inject

open class MainMyBookingsRepository
@Inject
constructor(
    private val myBookingsProductionService: MyBookingsProductionService,
    private val eventService: EventService
    ) : MyBookingsRepository {

    override suspend fun getBookingsProduction(
        date: String,
        endTime: String,
        ownerEmail: String
    ): Response<List<BookingResponse.Booking>>? {
        return withContext(Dispatchers.IO) {
            myBookingsProductionService.getBookingsProduction(date, endTime, ownerEmail)
        }
    }

    override suspend fun getUpcommingBookings(
        date: String,
        ownerEmail: String
    ): Response<List<BookingResponse.Booking>>? {
        return withContext(Dispatchers.IO) {
            myBookingsProductionService.getUpcommingBookings(date, ownerEmail)
        }
    }

    override suspend fun getPermanentBookings(
        ownerEmail: String
    ): Response<List<Asset.AssetItem>>? {
        return withContext(Dispatchers.IO) {
            myBookingsProductionService.getPermanentBookings(ownerEmail)
        }
    }

    override suspend fun getAssetDetailsProduction(
        assetId: String
    ): Response<Asset.AssetItem>? {
        return myBookingsProductionService.getAssetDetails(assetId)
    }

    override suspend fun getBuildingDetails(
        buildingId: String,
    ): Response<Building.BuildingItem>? {
        return withContext(Dispatchers.IO) {
            myBookingsProductionService.getBuildingDetails(buildingId)
        }
    }

    override suspend fun deleteBookingProduction(
        idBooking: String
    ): Response<BookingResponse.Booking>? {
        return myBookingsProductionService.deleteBookingProduction(idBooking)
    }

    override suspend fun getEventsProduction(
        workstationId: String,
        date: ZonedDateTime
    ): Response<ArrayList<BookingResponse.Booking>>? {
        return eventService.getEventsProduction(
            workstationId,
            date
        )
    }

    override suspend fun getAreaDetailByAreaId(
        areaId: String
    ): Response<Area.AreaItem>? {
        return myBookingsProductionService.getAreaDetailByAreaId(areaId)
    }


}