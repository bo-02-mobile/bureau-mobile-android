package com.jalasoft.bureau.ui.favorites.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.data.model.favorites.UserFav
import com.jalasoft.bureau.ui.favorites.repository.MainFavoritesRepository
import com.jalasoft.bureau.utils.DateUtils
import com.jalasoft.bureau.utils.ErrorMessage
import com.jalasoft.bureau.utils.TIMEZONE_UTC
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.time.LocalDate
import java.time.LocalTime
import java.time.Period
import java.time.ZoneId
import javax.inject.Inject

@HiltViewModel
class FavoritesViewModel
@Inject
constructor(var repository: MainFavoritesRepository) : ViewModel() {
    var liveListFav = MutableLiveData<List<String?>>()
    val listCount = MutableLiveData<Int>()
    var assetList = MutableLiveData<List<Asset.AssetItem>>(emptyList())
    var favoritesActivityListener: BadRequest? = null
    val isLoading = MutableLiveData<Boolean>()
    val assetDetails = MutableLiveData<Asset.AssetItem>()
    var assetListAvailable = MutableLiveData<List<Asset.AssetItem>>(emptyList())

    fun getUserInfo() {
        isLoading.postValue(true)
        viewModelScope.launch {
            try {
                val response = repository.getUserInfo()
                response?.let { response ->
                    if (response.isSuccessful) {
                        val body = response.body()
                        body?.let { user ->
                            withContext(Dispatchers.Main) {
                                if (user.favoriteAssets.isNotEmpty()) {
                                    liveListFav.postValue(user.favoriteAssets)
                                } else {
                                    liveListFav.postValue(listOf())
                                    listCount.postValue(0)
                                    isLoading.postValue(false)
                                }
                            }
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            favoritesActivityListener?.fetchBadRequest(ErrorMessage.REQUEST_FAILED)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    favoritesActivityListener?.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getFavoritesAssets(
        listFav: List<String?>,
        buildings: List<Building.BuildingItem>?
    ) {
        var userFavAssets = listFav.joinToString(separator = ",") { "id($it)" }
        viewModelScope.launch {
            try {
                val response = repository.getFavoritesAssetsProduction(userFavAssets)
                response?.let { response ->
                    if (response.isSuccessful) {
                        val body = response.body()
                        body?.let { assets ->
                            withContext(Dispatchers.Main) {
                                val filteredAssets = mutableListOf<Asset.AssetItem>()
                                buildings?.forEach { buildingItem ->
                                    filteredAssets.addAll(assets.filter { it.buildingId == buildingItem.id })
                                }
                                assetList.postValue(filteredAssets.sortedWith(compareBy { it.properties?.owner != null }))
                                listCount.postValue(filteredAssets.size)
                                if (filteredAssets.isEmpty()) {
                                    isLoading.postValue(false)
                                }
                            }
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            favoritesActivityListener?.fetchBadRequest(ErrorMessage.REQUEST_FAILED)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    favoritesActivityListener?.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun deleteFavoriteAsset(idAsset: String, onDelete: () -> (Unit)) {
        isLoading.postValue(true)
        viewModelScope.launch {
            try {
                val service: Response<UserFav>? =
                    repository.deleteFavoriteAsset(idAsset)
                if (service == null || !service.isSuccessful) {
                    withContext(Dispatchers.Main) {
                        favoritesActivityListener?.fetchBadRequest(ErrorMessage.REQUEST_FAILED)
                    }
                    return@launch
                }
                val response = service.body()
                response?.let {
                    withContext(Dispatchers.Main) {
                        if (it.favoriteAssets.isNotEmpty()) {
                            liveListFav.postValue(it.favoriteAssets)
                        } else {
                            assetList.postValue(emptyList())
                            listCount.postValue(0)
                            isLoading.postValue(false)
                        }
                        onDelete()
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    favoritesActivityListener?.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun checkOverlappingEvent(
        startTime: String,
        endTime: String,
        adapterAssetList: List<Asset.AssetItem>,
        mainStartTime: LocalTime,
        mainEndTime: LocalTime,
        dateSelected: LocalDate,
        buildings: List<Building.BuildingItem>?
    ) {
        viewModelScope.launch {
            try {
                adapterAssetList.forEach {
                    val response =
                        repository.getBookingsByDate(startTime, endTime, it.id ?: "")
                    response?.let { response ->
                        if (response.isSuccessful) {
                            val body = response.body()
                            body?.let { assetBookings ->
                                val len = assetBookings.size
                                it.available = checkIfAssetIsAvailable(it, len, mainStartTime, mainEndTime, dateSelected, buildings)
                            }
                        }
                    }
                }
                assetListAvailable.postValue(adapterAssetList)
                isLoading.postValue(false)
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    favoritesActivityListener?.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun checkIfAssetIsAvailable(
        asset: Asset.AssetItem,
        bookingsSize: Int,
        startTime: LocalTime,
        endTime: LocalTime,
        dateSelected: LocalDate?,
        buildings: List<Building.BuildingItem>?): Boolean {
        val bookingWindow = asset.policies?.bookingWindow ?: 0
        val dayDiff = Period.between(LocalDate.now(), dateSelected).days
        if ((dayDiff >= bookingWindow && bookingWindow != 0) || bookingsSize > 0) {
            return false
        }
        val buildingTimezone =
            buildings?.first { it.id == asset.buildingId }?.timezone ?: TIMEZONE_UTC
        val timezone = ZoneId.of(buildingTimezone).normalized()
        asset.policies?.schedule?.forEach { schedule ->
            val scheduleStartTime = DateUtils.stringToLocalDateTime(
                LocalDate.now(),
                schedule.startTime,
                timezone
            ).toLocalTime()
            val scheduleEndTime = DateUtils.stringToLocalDateTime(
                LocalDate.now(),
                schedule.endTime,
                timezone
            ).toLocalTime()
            val isInValidRange = startTime >= scheduleStartTime && endTime <= scheduleEndTime
            if (isInValidRange) {
                return true
            }
        }
        return false
    }
}
