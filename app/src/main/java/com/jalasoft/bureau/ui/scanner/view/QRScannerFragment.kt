package com.jalasoft.bureau.ui.scanner.view

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts.RequestPermission
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialog
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.budiyev.android.codescanner.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.zxing.BarcodeFormat
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.data.interfaces.BottomSheetDialogListener
import com.jalasoft.bureau.data.interfaces.BottomSheetFromQRDialogListener
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.scanner.BookingConfirmResponse
import com.jalasoft.bureau.databinding.*
import com.jalasoft.bureau.ui.bookings.viewmodel.MyBookingsViewModel
import com.jalasoft.bureau.ui.main.view.MainActivity
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.scanner.viewmodel.QRScannerViewModel
import com.jalasoft.bureau.ui.workstations.view.AssetDetailFragment
import com.jalasoft.bureau.utils.*
import com.jalasoft.bureau.utils.extensions.formatDateToTimezone
import dagger.hilt.android.AndroidEntryPoint
import org.joda.time.DateTime
import retrofit2.Response
import java.time.*
import java.time.format.DateTimeFormatter

@AndroidEntryPoint
class QRScannerFragment : DialogFragment(), BadRequest, BottomSheetDialogListener,
    BottomSheetFromQRDialogListener {

    lateinit var binding: FragmentQrScannerBinding
    val viewModel: QRScannerViewModel by activityViewModels()
    val mainViewModel: MainActivityViewModel by activityViewModels()
    val myBookingsViewModel: MyBookingsViewModel by activityViewModels()
    private lateinit var codeScanner: CodeScanner
    private lateinit var appContext: Context
    var isBottomSheetOpen: Boolean = false
    private lateinit var bottomSheetAssetDetail: AssetDetailFragment
    private var prefs: Prefs? = null
    private var dialogList: MutableList<AppCompatDialog> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(!resources.getBoolean(R.bool.isTablet)){
            (activity as AppCompatActivity?)?.supportActionBar?.hide()
        }
        appContext = this.requireContext()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentQrScannerBinding.inflate(inflater, container, false)
        prefs = Prefs(requireContext())
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkPermissions()
        initViewModel()
        initSubscriptions()
        initButtonListeners()
        validateBookingFromWebLinkIfNeeded(requireContext())
        if (!resources.getBoolean(R.bool.isTablet)) {
            binding.cancelButton.isVisible = false
        }
    }

    private val requestPermissionLauncher =
        registerForActivityResult(RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
                startScanning()
            } else {
                Toast.makeText(
                    appContext,
                    resources.getString(R.string.qr_warning_permission_toast),
                    Toast.LENGTH_LONG
                ).show()
            }
        }

    private fun initViewModel() {
        viewModel.badRequestListener = this
    }

    private fun initSubscriptions() {
        viewModel.bookingConfirmResponse.observe(viewLifecycleOwner) { response ->
            if (isBottomSheetOpen) showBottomSheetBookingConfirmedDialog(response)
            else showBottomSheetDialog(response)
        }
        viewModel.newAssetDetails.observe(viewLifecycleOwner) { response ->
            noReservationDialog(response)
        }
        viewModel.validatedBooking.observe(viewLifecycleOwner) { response ->
            alreadyValidatedDialog(response)
        }
        viewModel.notValidatedBooking.observe(viewLifecycleOwner) { response ->
            notValidatedDialog(response)
        }
    }

    private fun initButtonListeners() {
        binding.manualBtn.setOnClickListener {
            dialog()
        }

        binding.cancelButton.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.cancelButtonIcon?.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun checkPermissions() {
        when (PackageManager.PERMISSION_GRANTED) {
            ContextCompat.checkSelfPermission(appContext, Manifest.permission.CAMERA) -> {
                startScanning()
            }
            else -> {
                requestPermissionLauncher.launch(Manifest.permission.CAMERA)
            }
        }
    }

    private fun startScanning() {
        val scannerView = binding.codeScannerView
        codeScanner = CodeScanner(appContext, scannerView)
        codeScanner.camera = CodeScanner.CAMERA_BACK
        codeScanner.formats = listOf(BarcodeFormat.QR_CODE)
        codeScanner.autoFocusMode = AutoFocusMode.SAFE
        codeScanner.scanMode = ScanMode.SINGLE

        codeScanner.decodeCallback = DecodeCallback {
            activity?.runOnUiThread {
                val assetID = StringUtils.getAssetIdFromQRUrl(it.text)
                val booking = mainViewModel.bookingQR.value
                val onSuccess: (response: Response<BookingConfirmResponse>, bookingId: String) -> Unit = { response: Response<BookingConfirmResponse>, bookingId: String ->
                    mainViewModel.highlightedBookId = response.body()?.id
                    mainViewModel.bookingQR.postValue(null)
                    myBookingsViewModel.deleteNotificationByBookingId(bookingId, requireContext())
                }
                val userInfo = mainViewModel.userInfo.value
                val myDate = mainViewModel.myDate.value
                val myStartTime = mainViewModel.userStartTime.value
                val myEndTime = mainViewModel.userEndTime.value
                viewModel.checkIfTheBookingExists(
                    appContext,
                    booking,
                    assetID,
                    userInfo,
                    myDate,
                    myStartTime,
                    myEndTime,
                    onSuccess,
                    ::resetTimeFiltersQRNewBooking)
            }
        }
        codeScanner.errorCallback = ErrorCallback {
            activity?.runOnUiThread {
                Log.d(ERROR_TAG, it.message.toString())
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (::codeScanner.isInitialized) {
            codeScanner.startPreview()
        }
    }

    override fun onPause() {
        if (::codeScanner.isInitialized) {
            codeScanner.releaseResources()
        }
        dialogList.forEach { dialog ->
            dialog.dismiss()
        }
        super.onPause()
    }

    fun dialog() {
        val builder = AlertDialog.Builder(appContext)
        val inflater = layoutInflater
        val dialogBinding = QrscannerDialogBinding.inflate(inflater)
        builder.setView(dialogBinding.root)
            .setPositiveButton(resources.getString(R.string.qr_dialog_booking_validate)) { _, _ ->
                val assetID = dialogBinding.code.text.toString()
                val booking = mainViewModel.bookingQR.value
                val onSuccess: (response: Response<BookingConfirmResponse>, bookingId: String) -> Unit = { response: Response<BookingConfirmResponse>, bookingId: String ->
                    mainViewModel.highlightedBookId = response.body()?.id
                    mainViewModel.bookingQR.postValue(null)
                    myBookingsViewModel.deleteNotificationByBookingId(bookingId, requireContext())
                }
                val userInfo = mainViewModel.userInfo.value
                val myDate = mainViewModel.myDate.value
                val myStartTime = mainViewModel.userStartTime.value
                val myEndTime = mainViewModel.userEndTime.value
                viewModel.checkIfTheBookingExists(
                    appContext,
                    booking,
                    assetID,
                    userInfo,
                    myDate,
                    myStartTime,
                    myEndTime,
                    onSuccess,
                    ::resetTimeFiltersQRNewBooking)
            }
            .setNegativeButton(resources.getString(R.string.qr_dialog_insert_cancel)) { dialog, _ ->
                dialog.cancel()
            }
        val dialog = builder.create()
        dialog.show()
        dialogList.add(dialog)
    }

    private fun noReservationDialog(asset: Asset.AssetItem) {
        val builder = AlertDialog.Builder(appContext)
        val inflater = layoutInflater
        val dialogBinding = QrNewBookingDialogBinding.inflate(inflater)

        builder.setView(dialogBinding.root)
            .setPositiveButton(resources.getString(R.string.book_now)) { dialog, _ ->
                openBottomSheetForNewBooking(asset)
                dialog.dismiss()
            }
            .setNegativeButton(resources.getString(R.string.cancel)) { dialog, _ ->
                viewModel.resetDateTimeFilters(::setMyDateTime)
                dialog.dismiss()
                if (::codeScanner.isInitialized) {
                    codeScanner.startPreview()
                }
            }
        val dialog = builder.create()
        dialog.show()
        dialogList.add(dialog)
    }

    private fun alreadyValidatedDialog(booking: BookingResponse.Booking) {
        val date = DateUtils.dateFormat(booking.startTime, API_DATE_FORMAT, LOCAL_DATE_FORMAT)
        val startTime = DateUtils.dateFormat(booking.startTime, API_DATE_FORMAT, LOCAL_TIME_FORMAT)
        val endTime = DateUtils.dateFormat(booking.endTime, API_DATE_FORMAT, LOCAL_TIME_FORMAT)
        val myBuildings = mainViewModel.myBuildings.value
        val builder = MaterialAlertDialogBuilder(appContext).setOnDismissListener {
            if (::codeScanner.isInitialized) {
                codeScanner.startPreview()
            }
        }
            .setTitle(resources.getString(R.string.booking_already_validated_title))
            .setMessage(String.format(getString(R.string.booking_already_validated_body),
                booking.assetCode, booking.areaName, date, startTime, endTime,
                viewModel.getTimezoneFromBuildingId(booking.buildingId, myBuildings)))
            .setPositiveButton(resources.getString(R.string.i_understand)) { dialog, _ ->
                dialog.dismiss()
            }
        val dialog = builder.create()
        dialog.show()
        dialogList.add(dialog)
    }

    private fun notValidatedDialog(booking: BookingResponse.Booking) {
        val myBuildings = mainViewModel.myBuildings.value
        val buildingTimezone = viewModel.getTimezoneFromBuildingId(booking.buildingId, myBuildings)
        val date = DateUtils.dateFormat(booking.startTime, API_DATE_FORMAT, LOCAL_DATE_FORMAT)
        val formatter = DateTimeFormatter.ofPattern(LOCAL_TIME_FORMAT)
        val responseDate = LocalDateTime.ofInstant(
            Instant.parse(booking.startTime),
            ZoneId.of(buildingTimezone)
        )
        val startTime = booking.startTime.formatDateToTimezone(buildingTimezone)
        val endTime = booking.endTime.formatDateToTimezone(buildingTimezone)

        val limitBeforeDeadLine = prefs?.getData(SHARED_BOOKING_LIMIT_BEFORE_DEADLINE)
            ?: NOTIFICATION_DEFAULT_RANGE
        val startValidationTime = responseDate.minusMinutes(limitBeforeDeadLine.toLong()).format(formatter)
        val builder = MaterialAlertDialogBuilder(appContext).setOnDismissListener {
            if (::codeScanner.isInitialized) {
                codeScanner.startPreview()
            }
        }
            .setTitle(resources.getString(R.string.booking_not_validated_title))
            .setMessage(String.format(getString(R.string.booking_not_validated_body),
                booking.assetCode, booking.areaName, date, startTime, endTime, startValidationTime,buildingTimezone))
            .setPositiveButton(resources.getString(R.string.i_understand)) { dialog, _ ->
                dialog.dismiss()
            }
        val dialog = builder.create()
        dialog.show()
        dialogList.add(dialog)
    }

    private fun showBottomSheetDialog(response: BookingConfirmResponse) {
        val dialog = BottomSheetDialog(appContext)
        val dialogBinding = BottomSheetBookingValidateBinding.inflate(layoutInflater)
        DateTime.parse(response.startTime)
        dialogBinding.title.text = getString(R.string.qr_bottom_sheet_dialog_title)
        val date = DateUtils.dateFormat(response.startTime, API_DATE_FORMAT, QR_DATE_PATTERN)
        val myBuildings = mainViewModel.myBuildings.value
        val buildingTimezone = viewModel.getTimezoneFromBuildingId(response.buildingId, myBuildings)
        val startHour = response.startTime.formatDateToTimezone(buildingTimezone)
        val finishHour = response.endTime.formatDateToTimezone(buildingTimezone)
        val timeRange = requireContext().getString(R.string.booking_validation_success, startHour, finishHour)
        val assetType = StringUtils.formatAssetType(response.assetType)
        dialogBinding.tvBookingSuccessTitle.text =
            String.format(DATE_TIME_PATTERN, assetType, response.assetCode)

        dialogBinding.tvBookingSuccessBody.text = String.format(DATE_TIME_PATTERN, date, timeRange)
        dialogBinding.actionModeCloseButton.setOnClickListener {
            dialog.dismiss()
        }
        dialog.setContentView(dialogBinding.root)
        dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        dialog.show()
        dialog.setOnDismissListener {
            moveToBookingListFragment()
        }
        dialogList.add(dialog)
    }

    private fun showBottomSheetBookingConfirmedDialog(response: BookingConfirmResponse) {
        var assetType = viewModel.newAssetDetails.value?.properties?.type?.let {
            StringUtils.capitalize(it)
        }
        val assetCode = viewModel.newAssetDetails.value?.code
        val date = mainViewModel.myDate.value
        val startTime = mainViewModel.userStartTime.value
        val endTime = DateUtils.dateFormat(response.endTime, API_DATE_FORMAT, LOCAL_TIME_FORMAT)
        val formatterDate = DateTimeFormatter.ofPattern(LOCAL_DATE_FORMAT_WITHOUT_HOUR)
        val formatterTime = DateTimeFormatter.ofPattern(LOCAL_TIME_FORMAT)
        val tempDate =
            ZonedDateTime.of(date, startTime, ZoneId.systemDefault()).format(formatterDate)
        val tempStartTime =
            ZonedDateTime.of(date, startTime, ZoneId.systemDefault()).format(formatterTime)
        val dialog = BottomSheetDialog(appContext)
        val view = BottomSheetBookingSuccessBinding.inflate(layoutInflater)
        val btnCloseModal = view.actionModeCloseButton
        val tvTitle = view.tvBookingSuccessTitle
        val tvDescription = view.tvBookingSuccessBody
        if (assetType == null) assetType = WORKSTATION_LABEL
        tvTitle.text = String.format(getString(R.string.asset_title), assetType, assetCode)
        tvDescription.text =
            String.format(getString(R.string.asset_body), tempDate, tempStartTime, endTime)
        val tvQRMessage = view.textView
        tvQRMessage.text = resources.getString(R.string.booking_confirm_bottom_sheet_qr_suggestion)
        btnCloseModal.setOnClickListener {
            dialog.dismiss()
        }
        dialog.setContentView(view.root)
        dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        dialog.show()
        dialog.setOnDismissListener {
            bottomSheetAssetDetail.dismiss()
            val bottomNav = (activity as MainActivity).binding.bottomNavView
            val item: MenuItem = bottomNav.menu.getItem(MY_BOOKING_TAB)
            bottomNav.selectedItemId = item.itemId
        }
        dialogList.add(dialog)
    }

    override fun fetchBadRequest(errorType: ErrorMessage) {
        val builder = MaterialAlertDialogBuilder(appContext).setOnDismissListener {
            if (::codeScanner.isInitialized) {
                codeScanner.startPreview()
            }
        }
            .setTitle(resources.getString(errorType.message().first))
            .setMessage(resources.getString(errorType.message().second))
            .setPositiveButton(resources.getString(R.string.i_understand)) { dialog, _ ->
                dialog.dismiss()
            }
        val dialog = builder.create()
        dialog.show()
        dialogList.add(dialog)
    }

    private fun openBottomSheetForNewBooking(asset: Asset.AssetItem){
        val id = asset.id ?: ""
        val buildingId = asset.buildingId ?: ""
        val areaId = asset.areaId ?: ""
        val code = asset.code ?: ""
        if(id.isEmpty() or buildingId.isEmpty() or areaId.isEmpty() or code.isEmpty()) {
            fetchBadRequest(ErrorMessage.BOOKING_NOT_VALIDATED)
            return
        }
        bottomSheetAssetDetail = AssetDetailFragment(
            id,
            buildingId,
            areaId
        )
        bottomSheetAssetDetail.bottomSheetDialogListener =
            this@QRScannerFragment
        bottomSheetAssetDetail.bottomSheetFromQRDialogListener =
            this@QRScannerFragment
        if (!bottomSheetAssetDetail.isAdded && !isBottomSheetOpen) {
            bottomSheetAssetDetail.show(
                parentFragmentManager,
                BOTTOM_SHEET_ASSET_DETAIL
            )
            isBottomSheetOpen = !isBottomSheetOpen
            bottomSheetAssetDetail.newBookingFromQR = true
            bottomSheetAssetDetail.isBottomSheetExpanded = true
        }
    }

    override fun didSelectBottomSheetDialog() {
        isBottomSheetOpen = !isBottomSheetOpen
        viewModel.resetDateTimeFilters(::setMyDateTime)
    }

    override fun didConfirmBooking(assetId: String) {
        val userInfo = mainViewModel.userInfo.value
        val onSuccess: (response: Response<BookingConfirmResponse>, bookingId: String) -> Unit = { response: Response<BookingConfirmResponse>, bookingId: String ->
            mainViewModel.highlightedBookId = response.body()?.id
            mainViewModel.bookingQR.postValue(null)
            myBookingsViewModel.deleteNotificationByBookingId(bookingId, requireContext())
        }
        val myDate = mainViewModel.myDate.value
        val myStartTime = mainViewModel.userStartTime.value
        val myEndTime = mainViewModel.userEndTime.value
        viewModel.findBookingWithAssetId(
            appContext,
            assetId,
            userInfo,
            myDate,
            myStartTime,
            myEndTime,
            onSuccess,
            ::resetTimeFiltersQRNewBooking)
    }

    private fun moveToBookingListFragment() {
        val item: MenuItem = (activity as MainActivity).binding.bottomNavView.menu.getItem(MY_BOOKING_TAB)
        (activity as MainActivity).binding.bottomNavView.selectedItemId = item.itemId
    }

    private fun validateBookingFromWebLinkIfNeeded(context: Context) {
        if(mainViewModel.webLinkType == WebLinkType.WEB_LINK_BOOKING_CONFIRMATION) {
            val booking = mainViewModel.bookingQR.value
            val onSuccess: (response: Response<BookingConfirmResponse>, bookingId: String) -> Unit = { response: Response<BookingConfirmResponse>, bookingId: String ->
                mainViewModel.highlightedBookId = response.body()?.id
                mainViewModel.bookingQR.postValue(null)
                myBookingsViewModel.deleteNotificationByBookingId(bookingId, requireContext())
            }
            val userInfo = mainViewModel.userInfo.value
            val myDate = mainViewModel.myDate.value
            val myStartTime = mainViewModel.userStartTime.value
            val myEndTime = mainViewModel.userEndTime.value
            viewModel.checkIfTheBookingExists(
                context,
                booking,
                mainViewModel.webLinkType.assetId,
                userInfo,
                myDate,
                myStartTime,
                myEndTime,
                onSuccess,
                ::resetTimeFiltersQRNewBooking)
        }
        mainViewModel.webLinkType = WebLinkType.WEB_LINK_DEFAULT
    }

    private fun resetTimeFiltersQRNewBooking() {
        mainViewModel.myDate.value = LocalDate.now()
        mainViewModel.userStartTime.value = LocalTime.now()
        mainViewModel.userEndTime.value = LocalTime.of(18, 30)
    }

    private fun setMyDateTime(myDate: LocalDate?, myStartTime: LocalTime?, myEndTime: LocalTime?) {
        mainViewModel.myDate.value = myDate
        mainViewModel.userStartTime.value = myStartTime
        mainViewModel.userEndTime.value = myEndTime
    }
}