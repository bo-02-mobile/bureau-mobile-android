package com.jalasoft.bureau.ui.bookings.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.adapter.MyBookingsAdapter
import com.jalasoft.bureau.data.interfaces.MyBookingsListener
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.databinding.FragmentMyBookingListBinding
import com.jalasoft.bureau.ui.bookings.viewmodel.MyBookingsViewModel
import com.jalasoft.bureau.ui.main.view.MainActivity
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.workstations.viewmodel.AssetDetailFragmentViewModel
import com.jalasoft.bureau.utils.WORKSTATION_LABEL
import com.jalasoft.bureau.widgets.BookingWidget
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MyBookingListFragment : Fragment(), MyBookingsListener {

    private val myBookingsViewModel: MyBookingsViewModel by activityViewModels()
    private val mainViewModel: MainActivityViewModel by activityViewModels()
    private lateinit var recycler: RecyclerView
    private lateinit var todayBookingsRecycler: RecyclerView
    private lateinit var myBookingListBinding: FragmentMyBookingListBinding
    private val assetDetailViewModel: AssetDetailFragmentViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!resources.getBoolean(R.bool.isTablet)) {
            setupToolbar()
        } else {
            observeBookingsCreatedOrDeleted()
        }
        val view = inflater.inflate(R.layout.fragment_my_booking_list, container, false)
        myBookingListBinding = FragmentMyBookingListBinding.bind(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Firstly we call all the building from the api
        myBookingsViewModel.bookingActivityListener = (activity as MainActivity)
        sendRequests()
        initRecycler()
        initSubscriptions()
        initBookingSubscription()
        refreshLayoutBooking()
    }

    override fun onResume() {
        super.onResume()
        BookingWidget.refreshWidget(requireContext())
    }

    override fun onDestroy() {
        super.onDestroy()
        myBookingsViewModel.todayBookings.postValue(null)
        myBookingsViewModel.upcomingBookings.postValue(null)
    }

    private fun refreshLayoutBooking() {
        val refreshLayout = myBookingListBinding.swipeRefreshLayout
        refreshLayout.setOnRefreshListener {
            sendRequests()
            refreshLayout.isRefreshing = false
        }
    }

    private fun sendRequests() {
        val ownerEmail = mainViewModel.userInfo.value?.email.toString()
        val userBookingId = mainViewModel.userBookingId.value
        val buildings = mainViewModel.myBuildings.value
        myBookingsViewModel.getAllBookings(
            ownerEmail,
            userBookingId,
            false,
            ::goToBooking,
            buildings
        )
    }

    private fun goToBooking(booking: BookingResponse.Booking) {
        val assetType = booking.assetType ?: WORKSTATION_LABEL
        val bookingBundle: BookingResponse.BookingBundle = BookingResponse.BookingBundle(
            booking.assetCode,
            booking.assetId,
            booking.confirmedAt,
            booking.startTime,
            booking.endTime,
            booking.id,
            booking.buildingId,
            assetType,
            true,
            booking.reservationMessage,
            booking.isPermanent,
            booking.areaName
        )
        didBookingSelected(bookingBundle)
        mainViewModel.userBookingId.postValue("")
    }

    private fun setupToolbar() {
        (activity as MainActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
        (activity as MainActivity).binding.appBar.toolbar.visibility = View.VISIBLE
        (activity as MainActivity).binding.appBar.title.visibility = View.GONE
        (activity as MainActivity).binding.appBar.logo.visibility = View.VISIBLE
    }

    private fun initSubscriptions() {
        myBookingsViewModel.listCount.observe(viewLifecycleOwner) { listCount ->
            if (listCount == null) {
                return@observe
            }
            myBookingListBinding.tvCount.text = String.format("(%s)", listCount.toString())
        }

        myBookingsViewModel.isLoading.observe(viewLifecycleOwner) { isLoading ->
            if (isLoading) {
                myBookingListBinding.progressCircular.visibility = View.VISIBLE
            } else {
                if (myBookingsViewModel.todayBookings.value?.isEmpty() == true && myBookingsViewModel.upcomingBookings.value?.isEmpty() == true) {
                    myBookingListBinding.emptyMsg.visibility = View.VISIBLE
                }
                myBookingListBinding.progressCircular.visibility = View.INVISIBLE
            }
        }

    }

    private fun initBookingSubscription() {
        myBookingsViewModel.upcomingBookings.observe(viewLifecycleOwner) { upcomingBookings ->
            if (upcomingBookings == null) {
                return@observe
            }
            recycler.adapter =
                MyBookingsAdapter(upcomingBookings, myBookingsViewModel, this, false, mainViewModel.highlightedBookId)
            when {
                myBookingsViewModel.bookingListIsEmpty() -> {
                    myBookingListBinding.upcomingTitle.visibility = View.GONE
                }
                upcomingBookings.isEmpty() -> {
                    myBookingListBinding.myBookingsRV.visibility = View.GONE
                    myBookingListBinding.upcomingTitle.visibility = View.GONE

                }
                else -> {
                    myBookingListBinding.myBookingsRV.visibility = View.VISIBLE
                    myBookingListBinding.emptyMsg.visibility = View.GONE
                    myBookingListBinding.upcomingTitle.visibility = View.VISIBLE
                }
            }
        }

        myBookingsViewModel.todayBookings.observe(viewLifecycleOwner) { todayBookings ->
            if (todayBookings == null) {
                return@observe
            }
            todayBookingsRecycler.adapter =
                MyBookingsAdapter(todayBookings, myBookingsViewModel, this, true, mainViewModel.highlightedBookId)
            when {
                myBookingsViewModel.bookingListIsEmpty() -> {
                    myBookingListBinding.todayTitle.visibility = View.GONE
                }
                todayBookings.isEmpty() -> {
                    myBookingListBinding.todayTitle.visibility = View.GONE
                    myBookingListBinding.myBookingsRV.visibility = View.GONE
                }
                else -> {
                    myBookingListBinding.myBookingsRV.visibility = View.VISIBLE
                    myBookingListBinding.emptyMsg.visibility = View.GONE
                    myBookingListBinding.todayTitle.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun initRecycler() {
        recycler = myBookingListBinding.myBookingsRV
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.setHasFixedSize(true)
        recycler.isNestedScrollingEnabled = false
        todayBookingsRecycler = myBookingListBinding.todayBookingsRV
        todayBookingsRecycler.layoutManager = LinearLayoutManager(context)
        todayBookingsRecycler.setHasFixedSize(true)
        todayBookingsRecycler.isNestedScrollingEnabled = false

    }

    override fun didBookingSelected(booking: BookingResponse.BookingBundle) {
        val assetType = booking.assetType ?: WORKSTATION_LABEL
        val action =
            MyBookingListFragmentDirections.actionMyBookingListFragmentToMyBookingDetailFragment(
                booking.assetCode,
                booking.assetId,
                booking.confirmedAt,
                booking.startTime,
                booking.endTime,
                booking.bookingId,
                booking.buildingId,
                assetType,
                booking.isToday,
                booking.reservationMessage,
                booking.isPermanent ?: false,
                booking.areaName,
                booking.isBookingValidationInRange,
                booking.bookingRangeHourText
            )
        findNavController().navigate(action)
    }

    override fun didQrButtonPressed() {
        (activity as MainActivity).moveToQrScreen()
    }

    override fun didAnimationCompleted() {
        mainViewModel.highlightedBookId = null
    }

    private fun observeBookingsCreatedOrDeleted() {
        myBookingsViewModel.updateBookingsList.observe(viewLifecycleOwner) { updateList ->
            if (updateList) {
                sendRequests()
            }
        }
        assetDetailViewModel.updateBookingsList.observe(viewLifecycleOwner) { updateList ->
            if (updateList) {
                sendRequests()
            }
        }
    }
}