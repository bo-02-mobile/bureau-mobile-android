package com.jalasoft.bureau.ui.favorites.repository

import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.favorites.UserFav
import retrofit2.Response

interface FavoritesRepository {
    suspend fun getUserInfo(): Response<UserFav>?

    suspend fun getFavoritesAssetsProduction(
        listFav: String?
    ): Response<List<Asset.AssetItem>>?

    suspend fun deleteFavoriteAsset(
        idAsset: String
    ): Response<UserFav>?

    suspend fun getBookingsByDate(
        startTime: String,
        endTime: String,
        assetId: String
    ): Response<ArrayList<BookingResponse.Booking>>?
}