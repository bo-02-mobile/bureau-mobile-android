package com.jalasoft.bureau.ui.bookings.repository

import com.jalasoft.bureau.data.interfaces.AreaService
import com.jalasoft.bureau.data.interfaces.AssetService
import com.jalasoft.bureau.data.interfaces.BuildingService
import com.jalasoft.bureau.data.interfaces.MyBookingsService
import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.utils.IS_PUBLIC
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class MyBookingsProductionService
@Inject
constructor(private val retrofit: Retrofit) {

    suspend fun getBookingsProduction(
        date: String,
        endTime: String,
        ownerEmail: String,
    ): Response<List<BookingResponse.Booking>>? {
        return retrofit.create(MyBookingsService::class.java)?.getMyBookings(
                String.format("startTime gte date(%s)", date),
                String.format("startTime lte date(%s)", endTime),
                String.format("owner.email eq string(%s)", ownerEmail),
                "status.state ne string(UserCancelled)",
                "status.state ne string(SystemCancelled)"
            )
    }

    suspend fun getUpcommingBookings(date: String, userEmail: String): Response<List<BookingResponse.Booking>>? {
        return retrofit.create(MyBookingsService::class.java)?.getUpcomingBookings(
                String.format("startTime gte date(%s)", date),
                String.format("owner.email eq string(%s)", userEmail),
                "status.state ne string(UserCancelled)",
                "status.state ne string(SystemCancelled)"
            )
    }

    suspend fun getPermanentBookings(
        ownerEmail: String,
    ): Response<List<Asset.AssetItem>>? {
        return retrofit.create(MyBookingsService::class.java)?.getMyPermanentBookings(
                String.format("properties.owner.email eq string(%s)", ownerEmail)
            )
    }

    suspend fun getAssetDetails(
        assetId: String
    ): Response<Asset.AssetItem>? {
        return retrofit.create(AssetService::class.java)?.getAssetByIdProduction(
                assetId
            )
    }

    suspend fun deleteBookingProduction(
        idBooking: String
    ): Response<BookingResponse.Booking>? {
        return retrofit.create((MyBookingsService::class.java))?.deleteBookingProduction(
                idBooking
            )
    }

    suspend fun getBuildingDetails(
        buildingId: String
    ): Response<Building.BuildingItem>? {
        return retrofit.create(BuildingService::class.java)
            ?.getBuildingById(
                buildingId,
            )
    }

    suspend fun getAreaDetailByAreaId(
        areaId: String
    ): Response<Area.AreaItem>? {
        return retrofit.create(AreaService::class.java)
            ?.getAreaDetail(
                areaId,
                IS_PUBLIC
            )
    }

}