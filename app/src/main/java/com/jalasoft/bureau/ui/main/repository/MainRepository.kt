package com.jalasoft.bureau.ui.main.repository

import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.booking.BookingAvailability
import com.jalasoft.bureau.data.model.booking.BookingConfiguration
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.data.model.favorites.UserFav
import com.jalasoft.bureau.data.model.token.User
import com.jalasoft.bureau.data.model.user.Groups
import retrofit2.Response

interface MainRepository {
    suspend fun getBuildingsProduction(): Response<List<Building.BuildingItem>>?
    suspend fun getAreaByBuildingIdProduction(
        buildingId: String
    ): Response<List<Area.AreaItem>>?

    //Event Service
    suspend fun getEventsProduction(
        workstationId: String,
        date: String
    ): Response<ArrayList<BookingResponse.Booking>>?

    //Booking availability
    suspend fun getBookingAvailabilityProduction(
        workstationId: String,
        startTime: String,
        endTime: String
    ): Response<BookingAvailability>?

    suspend fun postBooking(
        workstationId: String,
        startTime: String,
        endTime: String
    ): Response<BookingResponse.Booking>?

    suspend fun fetchUser(): Response<User>?
    suspend fun fetchUsers(): Response<List<User>>?
    suspend fun getCurrentUser(): Response<UserFav>?
    suspend fun getGroups(userGroups: String?): Response<List<Groups.Group>>?

    //Booking configurations
    suspend fun getBookingsConfiguration(
    ): Response<BookingConfiguration>?
}

interface MainRepositoryMocking {
    fun getBuildingsProduction(): List<Building.BuildingItem>
    fun getBuildingsDemo(): List<Building.BuildingItem>
    fun getAreaByBuildingIdProduction(buildingId: String): List<Area.AreaItem>
    fun getAreaByBuildingIdDemo(buildingId: String): List<Area.AreaItem>
}