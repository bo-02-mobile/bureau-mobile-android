package com.jalasoft.bureau.ui.scanner.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.data.model.scanner.BookingConfirmRequestBody
import com.jalasoft.bureau.data.model.scanner.BookingConfirmResponse
import com.jalasoft.bureau.data.model.token.User
import com.jalasoft.bureau.notifications.BNotificationManager
import com.jalasoft.bureau.ui.bookings.repository.MainMyBookingsRepository
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.scanner.repository.IQRScannerRepository
import com.jalasoft.bureau.utils.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.time.*
import java.time.format.DateTimeFormatter
import javax.inject.Inject

@HiltViewModel
class QRScannerViewModel
@Inject
constructor(var repository: IQRScannerRepository, var repositoryBookings: MainMyBookingsRepository) : ViewModel() {

    val bookingConfirmResponse = MutableLiveData<BookingConfirmResponse>()
    lateinit var badRequestListener: BadRequest
    val newAssetDetails = MutableLiveData<Asset.AssetItem>()
    val validatedBooking = MutableLiveData<BookingResponse.Booking>()
    val notValidatedBooking = MutableLiveData<BookingResponse.Booking>()
    var tempMyDate: LocalDate? = null
    var tempMyStartTime: LocalTime? = null
    var tempMyEndTime: LocalTime? = null
    var prefs: Prefs? = null

    fun confirmBooking(bookingId: String, onSuccess: ((response: Response<BookingConfirmResponse>, bookingId: String)-> Unit)) {
        viewModelScope.launch {
            try {
                val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
                val date = ZonedDateTime.now().format(formatter)
                val requestBody = BookingConfirmRequestBody(REQ_BODY_OP, REQ_BODY_PATH, date)
                val response = repository.confirmBookingByCode(bookingId, requestBody)
                if (response == null || !response.isSuccessful) {
                    badRequestListener.fetchBadRequest(ErrorMessage.BOOKING_NOT_VALIDATED)
                } else {
                    bookingConfirmResponse.postValue(response.body())
                    onSuccess(response, bookingId)
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    badRequestListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun checkIfTheBookingExists(
        context: Context,
        booking: BookingResponse.Booking?,
        assetId: String,
        userInfo: User?,
        myDate: LocalDate?,
        myStartTime: LocalTime?,
        myEndTime: LocalTime?,
        onSuccess: ((response: Response<BookingConfirmResponse>,bookingId: String) -> Unit),
        resetTimeFiltersQRNewBooking: (() -> Unit)) {
        if (assetId == booking?.assetId) {
            confirmBooking(booking.id, onSuccess)
        } else {
            findBookingWithAssetId(context, assetId, userInfo, myDate, myStartTime, myEndTime, onSuccess, resetTimeFiltersQRNewBooking)
        }
    }

    fun findBookingWithAssetId(
        context: Context,
        assetId: String,
        userInfo: User?,
        myDate: LocalDate?,
        myStartTime: LocalTime?,
        myEndTime: LocalTime?,
        onSuccess: ((response: Response<BookingConfirmResponse>,bookingId: String) -> Unit),
        resetTimeFiltersQRNewBooking: (() -> Unit)
    ) {
        prefs = Prefs(context)
        viewModelScope.launch {
            try {
                val date = LocalDate.now()
                val time = LocalTime.of(0, 0, 0)
                val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
                val dateTime = ZonedDateTime.of(date, time, ZoneId.systemDefault()).format(formatter)
                val endTime = ZonedDateTime.of(date.plusDays(1), time, ZoneId.systemDefault()).format(formatter)
                val response =
                    userInfo?.email?.let { userEmail ->
                        repositoryBookings.getBookingsProduction(
                            dateTime,
                            endTime,
                            userEmail
                        )
                    }
                if (response == null || !response.isSuccessful) {
                    badRequestListener.fetchBadRequest(ErrorMessage.BOOKING_NOT_VALIDATED)
                } else {
                    val bookings = response.body()?.sortedBy { it.startTime }
                    bookings?.forEach { currBooking ->
                        if (currBooking.assetId != assetId) return@forEach
                        val bookingDate = LocalDateTime.ofInstant(
                            Instant.parse(currBooking.startTime),
                            ZoneId.systemDefault()
                        )
                        if (!isBookingToday(bookingDate)) return@forEach

                        if (!isBookingNotValidated(booking = currBooking)) {
                            validatedBooking.postValue(currBooking)
                            return@launch
                        }
                        if (isBookingInValidationRange(bookingDate)) {
                            confirmBooking(currBooking.id, onSuccess)
                            return@launch
                        } else {
                            notValidatedBooking.postValue(currBooking)
                            return@launch
                        }
                    }
                    getAssetDetails(assetId, resetTimeFiltersQRNewBooking, myDate, myStartTime, myEndTime)
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    badRequestListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun isBookingToday(bookingDate: LocalDateTime): Boolean {
        val tomorrow = LocalDateTime.now().withHour(0).withMinute(0).plusDays(1)
        val yesterday = LocalDateTime.now().withHour(23).withMinute(59).minusDays(1)
        return bookingDate.isBefore(tomorrow) and
                bookingDate.isAfter(yesterday)
    }

    fun isBookingInValidationRange(bookingDate: LocalDateTime): Boolean {
        val limitBeforeDeadLine = prefs?.getData(SHARED_BOOKING_LIMIT_BEFORE_DEADLINE) ?: NOTIFICATION_DEFAULT_RANGE
        val limitAfterDeadLine = prefs?.getData(SHARED_BOOKING_LIMIT_AFTER_DEADLINE) ?: NOTIFICATION_DEFAULT_RANGE
        val validationStartTime = bookingDate.minusMinutes(limitBeforeDeadLine.toLong())
        val validationEndTime = bookingDate.plusMinutes(limitAfterDeadLine.toLong())
        return LocalDateTime.now().isAfter(validationStartTime) and
                LocalDateTime.now().isBefore(validationEndTime)
    }

    fun isBookingNotValidated(booking: BookingResponse.Booking): Boolean {
        return booking.confirmedAt.isNullOrEmpty()
    }

    fun getTimezoneFromBuildingId(buildingId: String, myBuildings: List<Building.BuildingItem>?): String {
        val building = myBuildings?.find { it.id == buildingId }
        building?.let { safeBuilding ->
            return safeBuilding.timezone ?: UNDEFINED_TIMEZONE
        }
        return UNDEFINED_TIMEZONE
    }

    fun getAssetDetails(
        assetId: String,
        resetTimeFiltersQRNewBooking: (() -> Unit),
        myDate: LocalDate?,
        myStartTime: LocalTime?,
        myEndTime: LocalTime?
    ) {
        viewModelScope.launch {
            try {
                val service: Response<Asset.AssetItem>? =
                    repositoryBookings.getAssetDetailsProduction(assetId)
                if (service == null || !service.isSuccessful) {
                    withContext(Dispatchers.Main) {
                        badRequestListener.fetchBadRequest(ErrorMessage.BOOKING_NOT_VALIDATED)
                    }
                    return@launch
                } else {
                    val response = service.body()
                    response?.let {
                        withContext(Dispatchers.Main) {
                            saveCurrentDateTimeFilters(myDate, myStartTime, myEndTime)
                            setDateTimeFiltersQRNewBooking(resetTimeFiltersQRNewBooking)
                            newAssetDetails.postValue(it)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    badRequestListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun setDateTimeFiltersQRNewBooking(resetTimeFiltersQRNewBooking: () -> Unit) {
        resetTimeFiltersQRNewBooking()
    }

    fun saveCurrentDateTimeFilters(
        myDate: LocalDate?,
        myStartTime: LocalTime?,
        myEndTime: LocalTime?
    ) {
        tempMyDate = myDate
        tempMyStartTime = myStartTime
        tempMyEndTime = myEndTime
    }

    fun resetDateTimeFilters(
        setMyDateTime: (myDate: LocalDate?, myStartTime: LocalTime?, myEndTime: LocalTime?) -> Unit
    ) {
        setMyDateTime(tempMyDate, tempMyStartTime, tempMyEndTime)
        tempMyDate = null
        tempMyStartTime = null
        tempMyEndTime = null
    }
}