package com.jalasoft.bureau.ui.areas.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.ui.areas.repository.AreaProductionService
import com.jalasoft.bureau.utils.DateUtils
import com.jalasoft.bureau.utils.ErrorMessage
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.time.LocalDate
import java.time.LocalTime
import java.time.Period
import java.time.ZoneId
import javax.inject.Inject

@HiltViewModel
class AreaFragmentViewModel
    @Inject
    constructor(
    private val repository: AreaProductionService
) : ViewModel() {

    val myAreas = MutableLiveData<List<Area.AreaItem>>()
    val myAssets = MutableLiveData<List<Asset.AssetItem>>()
    val overLappedBookings = MutableLiveData<List<BookingResponse.Booking>>(null)
    val isAreasLoading = MutableLiveData<Boolean>()
    lateinit var mainActivityListener: BadRequest

    fun getAreaByBuildingId(buildingId: String) {
        isAreasLoading.postValue(true)
        viewModelScope.launch {
            try {
                val service: Response<List<Area.AreaItem>>? =
                    repository.getAreaByBuildingIdProduction(buildingId)
                if (service != null) {
                    if (service.isSuccessful) {
                        val response = service.body()
                        response?.let {
                            withContext(Dispatchers.Main) {
                                myAreas.postValue(it)
                            }
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            mainActivityListener.fetchBadRequest(ErrorMessage.REQUEST_FAILED_AREAS)
                        }
                    }
                    isAreasLoading.postValue(false)
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    mainActivityListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getAssetOverlapped(
        buildingId: String,
        overlappedBookings: List<String>,
        spaceType: String,
        arrLabels: List<String>
    ) {
        var tempOverlappedBookings = ""
        overlappedBookings.forEachIndexed { index, assetId ->
            if (index == 0) tempOverlappedBookings += "id(${assetId})"
            else tempOverlappedBookings += ",id(${assetId})"
        }
        viewModelScope.launch {
            try {
                val response =
                    repository.getAssetsByBuildingId(
                        buildingId,
                        tempOverlappedBookings,
                        spaceType,
                        arrLabels
                    )
                response?.let { response ->
                    if (response.isSuccessful) {
                        val body = response.body()
                        body?.let { assets ->
                            withContext(Dispatchers.Main) {
                                myAssets.postValue(assets.filter { a -> a.properties?.owner == null })
                            }
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            mainActivityListener.fetchBadRequest(ErrorMessage.REQUEST_FAILED_AREAS)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    mainActivityListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }

    }

    fun getOverlappedBookings(
        startTime: String,
        endTime: String,
    ) {
        viewModelScope.launch {
            try {
                val response = repository.getBookingsByDate(startTime, endTime)
                response?.let { response ->
                    if (response.isSuccessful) {
                        val body = response.body()
                        body?.let { bookings ->
                            withContext(Dispatchers.Main) {
                                overLappedBookings.postValue(bookings)
                            }
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    mainActivityListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun checkWorkstationSchedules(
        assets: List<Asset.AssetItem>,
        startTime: LocalTime,
        endTime: LocalTime,
        dateSelected: LocalDate,
        buildingZoneId: ZoneId
    ): List<Asset.AssetItem> {
        val filteredAssets = assets.filter { assetItem ->
            var isInValidRange = true
            run breaking@{
                assetItem.policies?.schedule?.forEach { schedule ->
                    val scheduleStartTime = DateUtils.stringToLocalDateTime(
                        LocalDate.now(),
                        schedule.startTime,
                        buildingZoneId
                    ).toLocalTime()
                    val scheduleEndTime = DateUtils.stringToLocalDateTime(
                        LocalDate.now(),
                        schedule.endTime,
                        buildingZoneId
                    ).toLocalTime()
                    isInValidRange = startTime >= scheduleStartTime && endTime <= scheduleEndTime
                    if (isInValidRange) {
                        return@breaking
                    }
                }
            }
            val bookingWindow = assetItem.policies?.bookingWindow ?: 0
            val dayDiff = Period.between(LocalDate.now(), dateSelected).days
            if (dayDiff >= bookingWindow && bookingWindow != 0) {
                isInValidRange = false
            }
            return@filter isInValidRange
        }
        return filteredAssets
    }

    fun clearAreas() {
        this.myAreas.value = emptyList()
    }

}