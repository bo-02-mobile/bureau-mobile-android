package com.jalasoft.bureau.ui.areas.repository

import com.jalasoft.bureau.data.interfaces.AssetService
import com.jalasoft.bureau.data.interfaces.BookingService
import com.jalasoft.bureau.data.interfaces.BuildingService
import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.di.RetrofitModule
import com.jalasoft.bureau.utils.ApiFilterUtils
import com.jalasoft.bureau.utils.IS_PUBLIC
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

open class AreaProductionService(private val retrofit: Retrofit) {

    open suspend fun getAreaByBuildingIdProduction(
        buildingId: String
    ): Response<List<Area.AreaItem>>? {
        return retrofit
            .create(BuildingService::class.java)?.getAreasFromABuildingAuthorization(
                "buildingId eq id(${buildingId})",
                IS_PUBLIC
            )
    }

    open suspend fun getAssetsProduction(
        buildingId: String,
        types: String?,
        arrLabels: List<String>
    ): Response<List<Asset.AssetItem>>? {
        val labelsSize: Int = arrLabels.size
        val labels: MutableList<String> = mutableListOf()
        arrLabels.forEach { label -> labels.add("properties.labels in array(id(${label}))") }
        if (types == "all" && labelsSize == 0) {
            return retrofit.create(AssetService::class.java)?.getAssetsProduction(
                    "buildingId eq id(${buildingId})",
                    null,
                    labels,
                    IS_PUBLIC,
                )
        }
        if (types == "all" && labelsSize > 0) {
            return retrofit.create(AssetService::class.java)?.getAssetsProduction(
                    "buildingId eq id(${buildingId})",
                    null,
                    labels,
                    IS_PUBLIC,
                )
        }
        if (types != "all" && labelsSize == 0) {
            return retrofit.create(AssetService::class.java)?.getAssetsProduction(
                    "buildingId eq id(${buildingId})",
                    "properties.type eq id(${types})",
                    labels,
                    IS_PUBLIC,
                )
        }
        return retrofit.create(AssetService::class.java)?.getAssetsProduction(
                "buildingId eq id(${buildingId})",
                "properties.type eq id(${types})",
                labels,
                IS_PUBLIC,
            )
    }

    open suspend fun getBookingsByDate(
        startTime: String,
        endTime: String,
    ): Response<ArrayList<BookingResponse.Booking>>? {
        return retrofit.create(BookingService::class.java)
            ?.getOverlappedBookings(
                startTime,
                endTime,
                "status.state ne string(UserCancelled)",
                "status.state ne string(SystemCancelled)",
            )
    }

    open suspend fun getAssetsByBuildingId(
        buildingId: String,
        overlappedBookings: String,
        types: String?,
        arrLabels: List<String>
    ): Response<List<Asset.AssetItem>>? {

        val labelsSize: Int = arrLabels.size
        val labels: MutableList<String> = mutableListOf()
        arrLabels.forEach { label -> labels.add("properties.labels in array(id(${label}))") }
        val overlappedFilter: String? = ApiFilterUtils.checkEmptyOverlappedIds(overlappedBookings)

        if ((types == "all" && labelsSize == 0) || (types == "all" && labelsSize > 0)) {
            return retrofit.create(AssetService::class.java)
                ?.getAssetsByBuildingId(
                    "buildingId eq id(${buildingId})",
                    overlappedFilter,
                    null,
                    labels,
                    IS_PUBLIC,
                    "asc(name)",
                )
        }
        return retrofit.create(AssetService::class.java)
            ?.getAssetsByBuildingId(
                "buildingId eq id(${buildingId})",
                overlappedFilter,
                "properties.type eq id(${types})",
                labels,
                IS_PUBLIC,
                "asc(name)",
            )
    }
}