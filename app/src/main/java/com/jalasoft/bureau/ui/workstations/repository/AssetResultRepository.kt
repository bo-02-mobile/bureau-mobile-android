package com.jalasoft.bureau.ui.workstations.repository

import com.jalasoft.bureau.data.interfaces.AreaService
import com.jalasoft.bureau.data.interfaces.AssetService
import com.jalasoft.bureau.data.interfaces.BookingService
import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.utils.ApiFilterUtils
import com.jalasoft.bureau.utils.IS_PUBLIC
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

open class AssetResultRepository
@Inject
constructor(private val retrofit: Retrofit) {

    open suspend fun getAreaDetailProduction(
        areaId: String
    ): Response<Area.AreaItem>? {
        return retrofit.create(AreaService::class.java)
            ?.getAreaDetail(areaId, IS_PUBLIC)
    }

    open suspend fun getBookingsByDate(
        startTime: String,
        endTime: String,
    ): Response<ArrayList<BookingResponse.Booking>>? {
        return retrofit.create(BookingService::class.java)
            ?.getOverlappedBookings(
                startTime,
                endTime,
                "status.state ne string(UserCancelled)",
                "status.state ne string(SystemCancelled)"
            )
    }

    fun getAreaDetailMock(): Area.AreaItem {
        return Area.AreaItem(
            null,
            "60a807c5dc0ba843b1ac4063",
            "3A",
            "2021-07-29T18:54:24.688Z", "", "40a807c5dc0ba843b1ac4060",
            null, "3A", true, null, "2020-12-19T18:54:24.688Z", 1, null
        )
    }

    open suspend fun getAssetsFromArea(
        buildingId: String,
        overlappedBookings: String,
    ): Response<List<Asset.AssetItem>>? {
        val overlappedFilter: String? = ApiFilterUtils.checkEmptyOverlappedIds(overlappedBookings)

        return retrofit.create(AssetService::class.java)
            ?.getAssetsFromArea(
                "buildingId eq id(${buildingId})",
                overlappedFilter,
                IS_PUBLIC
            )
    }
}