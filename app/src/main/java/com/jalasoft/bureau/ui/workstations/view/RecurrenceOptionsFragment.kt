package com.jalasoft.bureau.ui.workstations.view

import android.app.Dialog
import android.content.res.TypedArray
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.FrameLayout
import androidx.fragment.app.activityViewModels
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import com.jalasoft.bureau.R
import com.jalasoft.bureau.databinding.FragmentRecurrenceOptionsBinding
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.workstations.viewmodel.BookingOptionsViewModel
import com.jalasoft.bureau.utils.*
import dagger.hilt.android.AndroidEntryPoint
import java.time.format.DateTimeFormatter
import java.util.*
import javax.inject.Inject
import kotlin.math.roundToInt

@AndroidEntryPoint
class RecurrenceOptionsFragment : BottomSheetDialogFragment(), AdapterView.OnItemSelectedListener {

    private lateinit var recurrenceOptionsBinding: FragmentRecurrenceOptionsBinding

    private val mainViewModel: MainActivityViewModel by activityViewModels()

    @Inject
    lateinit var optionsViewModelFactory: BookingOptionsViewModel.AssistedFactory
    private val optionsViewModel: BookingOptionsViewModel by activityViewModels{
        BookingOptionsViewModel.provideFactory(optionsViewModelFactory, mainViewModel.myDate.value)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_recurrence_options, container, false)
        recurrenceOptionsBinding = FragmentRecurrenceOptionsBinding.bind(view)
        return recurrenceOptionsBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(!resources.getBoolean(R.bool.isTablet))  dialog?.window?.attributes?.windowAnimations = -1
        dialog?.setCanceledOnTouchOutside(false)

        initBottomSheet()
        initObservers()
        initEverySpinner()
        initMonthSpinner()
        initDaySpinner()
        initWeekNumberSpinner()
        initWeekDaySpinner()
        initButtonListeners()
        initRecurrenceSpinner()
        checkVisibilityWeekdayMarks()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        if(!resources.getBoolean(R.bool.isTablet)) return super.onCreateDialog(savedInstanceState)
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener {
                val bottomSheet = findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
                bottomSheet.setBackgroundResource(android.R.color.transparent)
            }
        }
    }

    private fun initBottomSheet() {
        (dialog as BottomSheetDialog).behavior.isDraggable = false
        val displayMetrics = requireContext().resources.displayMetrics
        (dialog as BottomSheetDialog).behavior.peekHeight =
            (185 * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
        dialog?.let {
            val bottomSheet = it.findViewById<View>(R.id.design_bottom_sheet)
            bottomSheet.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
        }
        (dialog as BottomSheetDialog).behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun initObservers() {
        mainViewModel.myDate.observe(viewLifecycleOwner) { startDate ->
            val formatter = DateTimeFormatter.ofPattern(DATETIME_WITH_YEAR_FORMAT, Locale.ENGLISH)
            recurrenceOptionsBinding.btnStartDate.text = startDate.format(formatter)
        }
        optionsViewModel.endDate.observe(viewLifecycleOwner) { endDate ->
            val formatter = DateTimeFormatter.ofPattern(DATETIME_WITH_YEAR_FORMAT, Locale.ENGLISH)
            recurrenceOptionsBinding.btnEndDate.text = endDate.format(formatter)
        }
    }

    private fun initRecurrenceSpinner() {
        val spinner = recurrenceOptionsBinding.recurrenceSpinner
        spinner.onItemSelectedListener = this
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.recurrence_options_array,
            R.layout.spinner_item_no_bg
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_item_no_bg)
            spinner.adapter = adapter
            spinner.setSelection(optionsViewModel.getRecurrenceOptionPos())
        }
    }

    private fun initEverySpinner() {
        val list: MutableList<Int> = mutableListOf()
        for (i in 1..optionsViewModel.getLimitForEveryOption()) {
            list.add(i)
        }
        val spinner = recurrenceOptionsBinding.everySpinner
        spinner.onItemSelectedListener = this
        ArrayAdapter(
            requireContext(),
            R.layout.spinner_item_no_bg,
            list
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_item_no_bg)
            spinner.adapter = adapter
            spinner.setSelection(optionsViewModel.everySelection - 1)
        }
    }

    private fun initMonthSpinner() {
        val spinner = recurrenceOptionsBinding.monthSpinner
        spinner.onItemSelectedListener = this
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.month_options_array,
            R.layout.spinner_item_no_bg
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_item_no_bg)
            spinner.adapter = adapter
            spinner.setSelection(optionsViewModel.monthSelected - 1)
        }
    }

    private fun initDaySpinner() {
        val list: MutableList<String> = mutableListOf()
        for (i in 1..DateUtils.daysOfMonth(optionsViewModel.monthSelected)) {
            list.add(DateUtils.ordinalFormOfNumber(i))
        }
        val spinner = recurrenceOptionsBinding.daySpinner
        spinner.onItemSelectedListener = this
        ArrayAdapter(
            requireContext(),
            R.layout.spinner_item_no_bg,
            list
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_item_no_bg)
            spinner.adapter = adapter
            spinner.setSelection(optionsViewModel.daySelected - 1)
        }
    }

    private fun initWeekNumberSpinner() {
        val spinner = recurrenceOptionsBinding.weekNumberSpinner
        spinner.onItemSelectedListener = this
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.weeknumber_options_array,
            R.layout.spinner_item_no_bg
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_item_no_bg)
            spinner.adapter = adapter
            spinner.setSelection(optionsViewModel.weekNumberSelected - 1)
        }
    }

    private fun initWeekDaySpinner() {
        val spinner = recurrenceOptionsBinding.weekdaySpinner
        spinner.onItemSelectedListener = this
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.weekday_options_array,
            R.layout.spinner_item_no_bg
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_item_no_bg)
            spinner.adapter = adapter
            spinner.setSelection(optionsViewModel.weekDaySelected - 1)
        }
    }

    private fun initButtonListeners() {
        recurrenceOptionsBinding.btnBack.setOnClickListener {
            this.dismiss()
        }

        recurrenceOptionsBinding.sundayLabel.setOnClickListener(onSelectWeekDayListener)
        recurrenceOptionsBinding.sundayCheck.setOnClickListener(onSelectWeekDayListener)
        recurrenceOptionsBinding.mondayLabel.setOnClickListener(onSelectWeekDayListener)
        recurrenceOptionsBinding.mondayCheck.setOnClickListener(onSelectWeekDayListener)
        recurrenceOptionsBinding.tuesdayLabel.setOnClickListener(onSelectWeekDayListener)
        recurrenceOptionsBinding.tuesdayCheck.setOnClickListener(onSelectWeekDayListener)
        recurrenceOptionsBinding.wednesdayLabel.setOnClickListener(onSelectWeekDayListener)
        recurrenceOptionsBinding.wednesdayCheck.setOnClickListener(onSelectWeekDayListener)
        recurrenceOptionsBinding.thursdayLabel.setOnClickListener(onSelectWeekDayListener)
        recurrenceOptionsBinding.thursdayCheck.setOnClickListener(onSelectWeekDayListener)
        recurrenceOptionsBinding.fridayLabel.setOnClickListener(onSelectWeekDayListener)
        recurrenceOptionsBinding.fridayCheck.setOnClickListener(onSelectWeekDayListener)
        recurrenceOptionsBinding.saturdayLabel.setOnClickListener(onSelectWeekDayListener)
        recurrenceOptionsBinding.saturdayCheck.setOnClickListener(onSelectWeekDayListener)

        recurrenceOptionsBinding.dayCheckbox.setOnClickListener {
            enableDayOption()
            optionsViewModel.onDaySelected = true
        }

        recurrenceOptionsBinding.weekNumberCheckbox.setOnClickListener {
            recurrenceOptionsBinding.weekNumberSpinner.isEnabled = true
            recurrenceOptionsBinding.weekdaySpinner.isEnabled = true
            recurrenceOptionsBinding.daySpinner.isEnabled = false
            recurrenceOptionsBinding.dayCheckbox.isChecked = false
            optionsViewModel.onDaySelected = false
        }

        recurrenceOptionsBinding.btnStartDate.setOnClickListener {
            val millis = mainViewModel.myDate.value?.atStartOfDay(mainViewModel.getBuildingZoneId())?.toInstant()
                ?.toEpochMilli()
            val calendarPicker = MaterialDatePicker.Builder.datePicker()
                .setTitleText(getString(R.string.day_booking))
                .setSelection(millis)
                .build()
            calendarPicker.show(parentFragmentManager, DATE_PICKER)
            calendarPicker.addOnPositiveButtonClickListener { timeMillisecond ->
                val showMessage = mainViewModel.postDate(timeMillisecond)
                recurrenceOptionsBinding.recurrenceSpinner.setSelection(0)
                recurrenceOptionsBinding.recurrenceSpinner.setSelection(optionsViewModel.getRecurrenceOptionPos())
                if (showMessage) {
                    val snack = dialog?.window?.decorView?.let { it1 ->
                        Snackbar.make(
                            it1,
                            getString(R.string.validateDate),
                            Snackbar.LENGTH_LONG
                        )
                    }
                    snack?.anchorView = recurrenceOptionsBinding.spaceSnack
                    snack?.show()
                }
            }
        }

        recurrenceOptionsBinding.btnEndDate.setOnClickListener {
            val millis = optionsViewModel.endDate.value?.atStartOfDay(mainViewModel.getBuildingZoneId())?.toInstant()
                ?.toEpochMilli()
            val calendarPicker = MaterialDatePicker.Builder.datePicker()
                .setTitleText(getString(R.string.day_booking))
                .setSelection(millis)
                .build()
            calendarPicker.show(parentFragmentManager, DATE_PICKER)
            calendarPicker.addOnPositiveButtonClickListener { timeMillisecond ->
                val showMessage = optionsViewModel.postEndDate(timeMillisecond, mainViewModel.myDate.value!!)
                if (showMessage) {
                    val snack = dialog?.window?.decorView?.let { it1 ->
                        Snackbar.make(
                            it1,
                            VALIDATE_END_DATE,
                            Snackbar.LENGTH_LONG
                        )
                    }
                    snack?.anchorView = recurrenceOptionsBinding.spaceSnack
                    snack?.show()
                }
            }
        }
    }

    private fun checkVisibilityWeekdayMarks() {
        recurrenceOptionsBinding.sundayCheck.alpha =
            optionsViewModel.returnAlphaForWeekday(SUNDAY_VALUE)
        recurrenceOptionsBinding.mondayCheck.alpha =
            optionsViewModel.returnAlphaForWeekday(MONDAY_VALUE)
        recurrenceOptionsBinding.tuesdayCheck.alpha =
            optionsViewModel.returnAlphaForWeekday(TUESDAY_VALUE)
        recurrenceOptionsBinding.wednesdayCheck.alpha =
            optionsViewModel.returnAlphaForWeekday(WEDNESDAY_VALUE)
        recurrenceOptionsBinding.thursdayCheck.alpha =
            optionsViewModel.returnAlphaForWeekday(THURSDAY_VALUE)
        recurrenceOptionsBinding.fridayCheck.alpha =
            optionsViewModel.returnAlphaForWeekday(FRIDAY_VALUE)
        recurrenceOptionsBinding.saturdayCheck.alpha =
            optionsViewModel.returnAlphaForWeekday(SATURDAY_VALUE)
    }

    private val onSelectWeekDayListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.sundayCheck, R.id.sundayLabel -> {
                optionsViewModel.checkWeekday(SUNDAY_VALUE)
            }
            R.id.mondayCheck, R.id.mondayLabel -> {
                optionsViewModel.checkWeekday(MONDAY_VALUE)
            }
            R.id.tuesdayCheck, R.id.tuesdayLabel -> {
                optionsViewModel.checkWeekday(TUESDAY_VALUE)
            }
            R.id.wednesdayCheck, R.id.wednesdayLabel -> {
                optionsViewModel.checkWeekday(WEDNESDAY_VALUE)
            }
            R.id.thursdayCheck, R.id.thursdayLabel -> {
                optionsViewModel.checkWeekday(THURSDAY_VALUE)
            }
            R.id.fridayCheck, R.id.fridayLabel -> {
                optionsViewModel.checkWeekday(FRIDAY_VALUE)
            }
            R.id.saturdayCheck, R.id.saturdayLabel -> {
                optionsViewModel.checkWeekday(SATURDAY_VALUE)
            }
        }
        checkVisibilityWeekdayMarks()
    }

    private fun enableDayOption() {
        recurrenceOptionsBinding.weekNumberSpinner.isEnabled = false
        recurrenceOptionsBinding.weekdaySpinner.isEnabled = false
        recurrenceOptionsBinding.weekNumberCheckbox.isChecked = false
        recurrenceOptionsBinding.daySpinner.isEnabled = true
        recurrenceOptionsBinding.dayCheckbox.isChecked = true
    }

    private fun onRecurrenceOptionSelected(option: String) {
        if (optionsViewModel.selection.value != option) {
            optionsViewModel.resetValues()
        }
        checkVisibilityWeekdayMarks()
        optionsViewModel.selection.value = option
        recurrenceOptionsBinding.everySpinner.isEnabled = true
        recurrenceOptionsBinding.containerOptions.visibility = View.VISIBLE
        when (option) {
            RECURRENCE_OPTION_NEVER -> {
                recurrenceOptionsBinding.containerOptions.visibility = View.GONE
            }
            RECURRENCE_OPTION_DAILY -> {
                recurrenceOptionsBinding.everyLabel.text = String.format(
                    "${getString(R.string.every)} (${getString(R.string.days)})")
                initEverySpinner()
                recurrenceOptionsBinding.containerDatesOptions.visibility = View.GONE
                recurrenceOptionsBinding.containerWeekDays.visibility = View.GONE
            }
            RECURRENCE_OPTION_WEEKLY -> {
                recurrenceOptionsBinding.everyLabel.text = String.format(
                    "${getString(R.string.every)} (${getString(R.string.weeks)})")
                initEverySpinner()
                recurrenceOptionsBinding.containerWeekDays.visibility = View.VISIBLE
                recurrenceOptionsBinding.containerDatesOptions.visibility = View.GONE
            }
            RECURRENCE_OPTION_MONTHLY -> {
                recurrenceOptionsBinding.everyLabel.text = String.format(
                    "${getString(R.string.every)} (${getString(R.string.months)})")
                initEverySpinner()
                initDaySpinner()
                recurrenceOptionsBinding.weekdaySpinner.setSelection(optionsViewModel.weekDaySelected - 1)
                recurrenceOptionsBinding.weekNumberSpinner.setSelection(optionsViewModel.weekNumberSelected - 1)

                recurrenceOptionsBinding.containerDatesOptions.visibility = View.VISIBLE
                recurrenceOptionsBinding.monthSpinner.visibility = View.GONE
                recurrenceOptionsBinding.monthSpinnerLabel.visibility = View.GONE
                recurrenceOptionsBinding.dividerMonth.visibility = View.GONE
                recurrenceOptionsBinding.containerWeekDays.visibility = View.GONE
                enableDayOption()
            }
            RECURRENCE_OPTION_YEARLY -> {
                recurrenceOptionsBinding.everyLabel.text = String.format(
                    "${getString(R.string.every)} (${getString(R.string.years)})")
                initEverySpinner()
                recurrenceOptionsBinding.monthSpinner.setSelection(optionsViewModel.monthSelected - 1)
                initDaySpinner()
                recurrenceOptionsBinding.weekdaySpinner.setSelection(optionsViewModel.weekDaySelected - 1)
                recurrenceOptionsBinding.weekNumberSpinner.setSelection(optionsViewModel.weekNumberSelected - 1)

                recurrenceOptionsBinding.everySpinner.isEnabled = false
                recurrenceOptionsBinding.containerDatesOptions.visibility = View.VISIBLE
                recurrenceOptionsBinding.monthSpinner.visibility = View.VISIBLE
                recurrenceOptionsBinding.monthSpinnerLabel.visibility = View.VISIBLE
                recurrenceOptionsBinding.dividerMonth.visibility = View.VISIBLE
                recurrenceOptionsBinding.containerWeekDays.visibility = View.GONE
                enableDayOption()
            }
            else -> {
                // do nothing
            }
        }
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
        when (parent.id) {
            recurrenceOptionsBinding.recurrenceSpinner.id -> {
                val options: TypedArray = resources.obtainTypedArray(R.array.recurrence_options_array)
                val selected = options.getString(pos) ?: ""
                onRecurrenceOptionSelected(selected)
                options.recycle()
            }
            recurrenceOptionsBinding.everySpinner.id -> {
                optionsViewModel.everySelection = pos + 1
            }
            recurrenceOptionsBinding.monthSpinner.id -> {
                if (optionsViewModel.monthSelected != pos + 1) {
                    optionsViewModel.daySelected = 1
                }
                optionsViewModel.monthSelected = pos + 1
                initDaySpinner()
            }
            recurrenceOptionsBinding.daySpinner.id -> {
                optionsViewModel.daySelected = pos + 1
            }
            recurrenceOptionsBinding.weekNumberSpinner.id -> {
                optionsViewModel.weekNumberSelected = pos + 1
            }
            recurrenceOptionsBinding.weekdaySpinner.id -> {
                optionsViewModel.weekDaySelected = pos + 1
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        // do nothing
    }
}