package com.jalasoft.bureau.ui.login.repository

import com.jalasoft.bureau.data.interfaces.FavoriteService
import com.jalasoft.bureau.data.interfaces.LoginService
import com.jalasoft.bureau.data.model.favorites.UserFav
import com.jalasoft.bureau.data.model.token.User
import com.jalasoft.bureau.data.model.user.Groups
import com.jalasoft.bureau.di.RetrofitModule
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class UserService
@Inject
constructor(
    private val retrofitAuth: Retrofit,
    private val retrofit: Retrofit,
) {
    suspend fun getUserInfo(): Response<User> {
        return retrofitAuth.create(LoginService::class.java).getUserInfo()
    }

    suspend fun getUsersInfo(): Response<List<User>> {
        return retrofit.create(LoginService::class.java).getUsersInfo()
    }

    suspend fun getCurrentUser(): Response<UserFav> {
        return retrofit.create(FavoriteService::class.java).getUserInfo()
    }

    suspend fun getGroups(userGroups: String?): Response<List<Groups.Group>> {
        return retrofit.create(LoginService::class.java).getGroups(
                "_id in array(${userGroups})"
            )
    }
}