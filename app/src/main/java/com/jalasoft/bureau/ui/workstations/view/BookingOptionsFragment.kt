package com.jalasoft.bureau.ui.workstations.view

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.app.Dialog
import android.database.MatrixCursor
import android.graphics.Color
import android.os.Bundle
import android.provider.BaseColumns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FilterQueryProvider
import android.widget.FrameLayout
import android.widget.SimpleCursorAdapter
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.activityViewModels
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.interfaces.BookingOptionsListener
import com.jalasoft.bureau.data.model.token.User
import com.jalasoft.bureau.databinding.DialogSelectUserBinding
import com.jalasoft.bureau.databinding.FragmentBookingOptionsBinding
import com.jalasoft.bureau.ui.main.view.MainActivity
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.workstations.viewmodel.BookingOptionsViewModel
import com.jalasoft.bureau.utils.*
import com.jalasoft.bureau.utils.extensions.formatCalendar
import dagger.hilt.android.AndroidEntryPoint
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*
import javax.inject.Inject


@AndroidEntryPoint
class BookingOptionsFragment : BottomSheetDialogFragment() {

    lateinit var bookingOptionsBinding: FragmentBookingOptionsBinding
    lateinit var bookingOptionsListener: BookingOptionsListener

    private val mainViewModel: MainActivityViewModel by activityViewModels()

    @Inject
    lateinit var optionsViewModelFactory: BookingOptionsViewModel.AssistedFactory
    private val optionsViewModel: BookingOptionsViewModel by activityViewModels{
        BookingOptionsViewModel.provideFactory(optionsViewModelFactory, mainViewModel.myDate.value)
    }
    var selectedDone = false
    var usersLoaded = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_booking_options, container, false)
        bookingOptionsBinding = FragmentBookingOptionsBinding.bind(view)
        return bookingOptionsBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)

        optionsViewModel.willPostRecurrentBooking = false
        val myDate = mainViewModel.myDate.value
        val userStartTime = mainViewModel.userStartTime.value
        val userEndTime = mainViewModel.userEndTime.value
        optionsViewModel.initDateTimeValues(
            myDate,
            userStartTime,
            userEndTime
        )
        val onSuccess = { usersList: List<User>? ->
            optionsViewModel.usersList.postValue(usersList)
        }
        mainViewModel.fetchUsers(onSuccess)
        initSubscribers()
        initBottomSheet()
        initButtonListeners()
        bookingPermissions()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        if(!resources.getBoolean(R.bool.isTablet)) return super.onCreateDialog(savedInstanceState)
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener {
                val bottomSheet = findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
                bottomSheet.setBackgroundResource(android.R.color.transparent)
            }
        }
    }

    private fun initSubscribers() {
        mainViewModel.myDate.observe(viewLifecycleOwner) { currentTime ->
            bookingOptionsBinding.btnDate.text = currentTime.formatCalendar(requireContext())
            optionsViewModel.updateEndDate(mainViewModel.myDate.value!!)
        }

        mainViewModel.userStartTime.observe(viewLifecycleOwner) {
            val formatter = DateTimeFormatter.ofPattern(LOCAL_TIME_FORMAT, Locale.ENGLISH)
            bookingOptionsBinding.btnStarts.text = it.format(formatter)
        }

        mainViewModel.userEndTime.observe(viewLifecycleOwner) {
            val formatter = DateTimeFormatter.ofPattern(LOCAL_TIME_FORMAT, Locale.ENGLISH)
            bookingOptionsBinding.btnEnds.text = it.format(formatter)
        }

        mainViewModel.userBookFor.observe(viewLifecycleOwner) {
            bookingOptionsBinding.btnBookFor.text = if (it.name.isNullOrEmpty()) it.email else it.name
        }

        optionsViewModel.usersList.observe(viewLifecycleOwner) {
            usersLoaded = true
        }

        optionsViewModel.selection.observe(viewLifecycleOwner) { selection ->
            bookingOptionsBinding.btnRepeat.text = selection.toString()
        }
    }

    private fun initBottomSheet() {
        (dialog as BottomSheetDialog).behavior.isDraggable = false
        dialog?.let {
            val bottomSheet = it.findViewById<View>(R.id.design_bottom_sheet)
            bottomSheet.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
        }
        (dialog as BottomSheetDialog).behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun initButtonListeners() {
        bookingOptionsBinding.btnCancel.setOnClickListener {
            this.dismiss()
        }

        bookingOptionsBinding.btnDone.setOnClickListener {
            selectedDone = true
            optionsViewModel.willPostRecurrentBooking = optionsViewModel.selection.value != getString(R.string.never)
            this.dismiss()
        }

        bookingOptionsBinding.btnDate.setOnClickListener {
            val millis = mainViewModel.myDate.value?.atStartOfDay(mainViewModel.getBuildingZoneId())?.toInstant()
                ?.toEpochMilli()
            val calendarPicker = MaterialDatePicker.Builder.datePicker()
                .setTitleText(getString(R.string.day_booking))
                .setSelection(millis)
                .build()
            calendarPicker.show(parentFragmentManager, DATE_PICKER)
            calendarPicker.addOnPositiveButtonClickListener { timeMillisecond ->
                val showMessage = mainViewModel.postDate(timeMillisecond)
                if (showMessage) {
                    val snack = dialog?.window?.decorView?.let { it1 ->
                        Snackbar.make(
                            it1,
                            getString(R.string.validateDate),
                            Snackbar.LENGTH_LONG
                        )
                    }
                    snack?.anchorView = bookingOptionsBinding.spaceSnack
                    snack?.show()
                }
            }
        }

        bookingOptionsBinding.btnStarts.setOnClickListener {
            val localDate = mainViewModel.userStartTime.value
            val timePicker = localDate?.let { date ->
                MaterialTimePicker.Builder()
                    .setInputMode(MaterialTimePicker.INPUT_MODE_KEYBOARD)
                    .setTimeFormat(TimeFormat.CLOCK_12H)
                    .setHour(date.hour)
                    .setMinute(date.minute)
                    .setTitleText(getString(R.string.select_start_time))
                    .setPositiveButtonText(getString(R.string.ok))
                    .build()
            }
            timePicker?.show(parentFragmentManager, START_TIME_PICKER_TAG)
            timePicker?.addOnPositiveButtonClickListener {
                mainViewModel.postStartTime(timePicker.hour, timePicker.minute)
            }
        }

        bookingOptionsBinding.btnEnds.setOnClickListener {
            val localDate = mainViewModel.userEndTime.value
            val timePicker = localDate?.let { date ->
                MaterialTimePicker.Builder()
                    .setInputMode(MaterialTimePicker.INPUT_MODE_KEYBOARD)
                    .setTimeFormat(TimeFormat.CLOCK_12H)
                    .setHour(date.hour)
                    .setMinute(date.minute)
                    .setTitleText(getString(R.string.select_ending_time))
                    .setPositiveButtonText(getString(R.string.ok))
                    .build()
            }
            timePicker?.show(parentFragmentManager, END_TIME_PICKER_TAG)
            timePicker?.addOnPositiveButtonClickListener {
                val showMessage = mainViewModel.postEndTime(timePicker.hour, timePicker.minute)
                if (showMessage) {
                    val snack = dialog?.window?.decorView?.let { it1 ->
                        Snackbar.make(
                            it1,
                            VALIDATE_END_TIME, Snackbar.LENGTH_LONG
                        )
                    }
                    snack?.anchorView = (activity as MainActivity).binding.bottomNavView
                    snack?.show()
                }
            }
        }

        bookingOptionsBinding.btnNow.setOnClickListener {
            val currentTime = LocalDateTime.now()
            mainViewModel.postStartTime( currentTime.hour, currentTime.minute)
            mainViewModel.postEndTime( currentTime.hour + 1, currentTime.minute, true)

            // Short animation
            val colorFrom = Color.YELLOW
            val colorTo = it.context.getColor(R.color.date_time_picker_button)
            val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), colorFrom, colorTo)
            colorAnimation.duration = 1000

            colorAnimation.addUpdateListener {
                    animator -> bookingOptionsBinding.btnEnds.setBackgroundColor(animator.animatedValue as Int)
            }
            colorAnimation.start()
        }

        bookingOptionsBinding.btnRepeat.setOnClickListener {
            val recurrenceOptionsBottomSheet = RecurrenceOptionsFragment()
            if (!recurrenceOptionsBottomSheet.isAdded) {
                recurrenceOptionsBottomSheet.show(
                    parentFragmentManager,
                    RECURRENCE_OPTIONS_BOTTOM_SHEET
                )
            }
        }

        bookingOptionsBinding.btnBookFor.setOnClickListener {
            if (usersLoaded) showInputDialog()
        }
    }

    private fun showInputDialog() {
        val users = optionsViewModel.usersList.value
        var id: String? = null
        var email: String? = null
        var name: String? = null
        // Set custom layout as an alert dialog
        val view = DialogSelectUserBinding.inflate(layoutInflater)
        val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
        alertDialogBuilder.setView(view.root)
        view.validationLabel.visibility = View.GONE
        // Configure adapter and filter provider
        val from = arrayOf(COL1, COL2)
        val to = intArrayOf(android.R.id.text1, android.R.id.text2)
        val adapter = object: SimpleCursorAdapter(requireActivity(), android.R.layout.simple_list_item_2, null, from, to, 0) {
            override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
                val view = super.getView(position, convertView, parent)
                val text1 = view.findViewById<View>(android.R.id.text1) as TextView
                val text2 = view.findViewById<View>(android.R.id.text2) as TextView
                text1.setTextColor(resources.getColor(R.color.text_medium, null))
                text2.setTextColor(resources.getColor(R.color.text_medium, null))
                return super.getView(position, convertView, parent)
            }
        }
        adapter.stringConversionColumn = 1
        val provider = FilterQueryProvider { constraint ->
            if (constraint.isNullOrEmpty() ) return@FilterQueryProvider null
            val suggestions = users?.filter { user ->
                user.email?.isBlank() == false &&
                        (user.name?.contains(constraint, true) == true ||
                                user.email?.contains(constraint, true) == true)
            }
            if (suggestions.isNullOrEmpty()) return@FilterQueryProvider null
            val matrixCursor = MatrixCursor(arrayOf(BaseColumns._ID, COL1, COL2, COL0))
            for (i in suggestions.indices) {
                matrixCursor.newRow().add(i).add(suggestions[i].name).add(suggestions[i].email).add(suggestions[i].id)
            }
            matrixCursor
        }
        adapter.filterQueryProvider = provider
        // Create dialog then set textView adapter and listeners
        val textView = view.enterUser
        textView.setAdapter(adapter)
        alertDialogBuilder.setCancelable(false)
            .setPositiveButton(R.string.ok) { _, _ ->
                email = email ?: textView.text.toString()
                val userBookFor = User(id, email, null, name)
                mainViewModel.userBookFor.postValue(userBookFor)
            }
        alertDialogBuilder.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
        val dialog: AlertDialog = alertDialogBuilder.create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = false
        textView.addTextChangedListener { editable ->
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = StringUtils.isValidEmail(editable)
            view.validationLabel.visibility =
                if (dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled) View.GONE
                else View.VISIBLE
        }
        textView.setOnItemClickListener { adapterView, _, i, _ ->
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).isEnabled = true
            val matrix = adapterView.getItemAtPosition(i) as MatrixCursor
            name = matrix.getString(1)
            email = matrix.getString(2)
            id = matrix.getString(3)
            textView.setText(if (email.isNullOrBlank()) name ?: "" else email)
        }

    }

    override fun dismiss() {
        super.dismiss()
        if (!selectedDone) {
            val resetDateTime = { myDate: LocalDate?, userStartTime: LocalTime?, userEndTime: LocalTime? ->
                mainViewModel.myDate.postValue(myDate)
                mainViewModel.userStartTime.postValue(userStartTime)
                mainViewModel.userEndTime.postValue(userEndTime)
            }
            optionsViewModel.resetDateTimeFilters(resetDateTime)
            optionsViewModel.resetValues()
            optionsViewModel.endDate.postValue(mainViewModel.myDate.value?.plusMonths(1))
            optionsViewModel.selection.postValue(getString(R.string.never))
        }
        bookingOptionsListener.didDoneButtonPressed()
    }

    private fun bookingPermissions() {
        val prefs = Prefs(requireContext())
        val recurrencePermission = prefs.getBooleanData(RECURRENCE_BOOKING_PERMISSION)
        val bookForOthersPermission = prefs.getBooleanData(BOOK_FOR_OTHERS_PERMISSION)
        if (recurrencePermission == true) {
            bookingOptionsBinding.repeatContainer.visibility = View.VISIBLE
        } else {
            bookingOptionsBinding.repeatContainer.visibility = View.GONE
        }
        if (bookForOthersPermission == true) {
            bookingOptionsBinding.bookForContainer.visibility = View.VISIBLE
        } else {
            bookingOptionsBinding.bookForContainer.visibility = View.GONE
        }
    }
}