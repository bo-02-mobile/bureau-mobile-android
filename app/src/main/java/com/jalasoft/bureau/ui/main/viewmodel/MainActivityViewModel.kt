package com.jalasoft.bureau.ui.main.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jalasoft.bureau.BuildConfig
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.data.interfaces.BookingAvailabilityListener
import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.booking.BookingConfiguration
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.data.model.common.Owner
import com.jalasoft.bureau.data.model.favorites.UserFav
import com.jalasoft.bureau.data.model.token.AccessTokenResponse
import com.jalasoft.bureau.data.model.token.User
import com.jalasoft.bureau.data.model.user.Groups
import com.jalasoft.bureau.ui.bookings.repository.MainMyBookingsRepository
import com.jalasoft.bureau.ui.main.repository.MainActivityRepository
import com.jalasoft.bureau.utils.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.openid.appauth.AuthorizationResponse
import net.openid.appauth.AuthorizationService
import net.openid.appauth.ClientSecretBasic
import okhttp3.internal.UTC
import retrofit2.Response
import java.lang.RuntimeException
import java.time.*
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel
@Inject
constructor(
    var repository: MainActivityRepository,
    var repositoryBookings: MainMyBookingsRepository,
    var prefs: Prefs
) : ViewModel() {

    val myBuildings = MutableLiveData<List<Building.BuildingItem>>()
    val myDate = MutableLiveData<LocalDate>()
    val userStartTime = MutableLiveData<LocalTime>()
    val userEndTime = MutableLiveData<LocalTime>()
    val userBookingId = MutableLiveData<String>()
    val tokenRequestResponse = MutableLiveData<AccessTokenResponse>()
    val userInfo = MutableLiveData<User>()
    val userBookFor = MutableLiveData<User>()
    lateinit var mainActivityListener: BadRequest
    lateinit var bookingAvailabilityListener: BookingAvailabilityListener
    val bookingQR = MutableLiveData<BookingResponse.Booking>()
    val workstationIsLoading = MutableLiveData<Boolean>()
    var highlightedBookId: String? = null
    val areaId = MutableLiveData<String>()
    val areaCode = MutableLiveData<String>()
    val buildingName = MutableLiveData<String>()
    val buildingId = MutableLiveData<String>()
    val buildingsAreas = MutableLiveData<List<Building.BuildingItem>>()
    var buildingTimeZone: String? = null
    var currentUser: UserFav? = null
    var groups: List<Groups.Group> = listOf(Groups.Group())
    var webLinkType: WebLinkType = WebLinkType.WEB_LINK_DEFAULT

    fun performTokenRequest(context: Context, response: AuthorizationResponse) {
        val authService = AuthorizationService(context)
        val clientAuth = ClientSecretBasic(BuildConfig.LOGIN_CLIENT_SECRET)
        authService.performTokenRequest(
            response.createTokenExchangeRequest(),
            clientAuth
        ) { resp, ex ->
            tokenRequestResponse.postValue(AccessTokenResponse(resp, ex))
        }
    }

    fun fetchUser() {
        val userId: String? = prefs.getData(SHARED_USER_ID)
        val userName: String? = prefs.getData(SHARED_USER_NAME)
        val userEmail: String? = prefs.getData(SHARED_USER_EMAIL)
        val userImage: String? = prefs.getData(SHARED_USER_IMAGE)
        val userRole: String? = prefs.getData(SHARED_USER_ROLE)
        if (userId != null && userName != null && userEmail != null && userImage != null && userEmail.isNotEmpty()) {
            val user = User(userId, userEmail, userImage, userName, userRole)
            userInfo.postValue(user)
            userBookFor.postValue(user)
            FirebaseCrashlyticsUtils.setUserEmailId(userEmail)
        } else {
            viewModelScope.launch {
                try {
                    val response = repository.fetchUser()
                    if (response?.isSuccessful == true) {
                        userInfo.postValue(response.body())
                        userBookFor.postValue(response.body())
                        val user: User = response.body() as User
                        FirebaseCrashlyticsUtils.setUserEmailId(user.email)
                        prefs.saveData(user.id, SHARED_USER_ID)
                        prefs.saveData(user.name, SHARED_USER_NAME)
                        prefs.saveData(user.email, SHARED_USER_EMAIL)
                        prefs.saveData(user.picture, SHARED_USER_IMAGE)
                    } else {
                        FirebaseCrashlyticsUtils.setCrashlyticsKey(KEY_NO_EMAIL, PREFS_ERROR)
                    }
                } catch (ex: Exception) {
                    withContext(Dispatchers.Main) {
                        mainActivityListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                    }
                }
            }
        }
    }

    fun initDateTimeFiltersDefault(useCrashlytics: Boolean = false) {
        userStartTime.value = LocalTime.of(8, 30)
        userEndTime.value = LocalTime.of(18, 30)
        myDate.value = LocalDate.now()

        if (!useCrashlytics) return
        FirebaseCrashlyticsUtils.setCrashlyticsKey(KEY_DATE_VALUE, myDate.value.toString())
        FirebaseCrashlyticsUtils.setCrashlyticsKey(
            KEY_START_TIME_VALUE,
            userStartTime.value.toString()
        )
        FirebaseCrashlyticsUtils.setCrashlyticsKey(KEY_END_TIME_VALUE, userEndTime.value.toString())
        val timeZone = Calendar.getInstance().timeZone.getDisplayName(false, TimeZone.SHORT)
        FirebaseCrashlyticsUtils.setCrashlyticsKey(KEY_TIMEZONE_VALUE, timeZone)
    }

    fun postDate(millis: Long): Boolean {
        val showMessage: Boolean
        val localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), UTC.toZoneId())
        val nowDate = LocalDate.now()
        val localTime = LocalTime.now()
        val userTime = userStartTime.value?.hour ?: 0
        if (nowDate.isAfter(localDateTime.toLocalDate())) {
            myDate.postValue(nowDate)
            showMessage = true
        } else {
            showMessage = false
            myDate.postValue(localDateTime.toLocalDate())
        }
        if (localDateTime.toLocalDate()
                .isBefore(nowDate.plusDays(1)) && localTime.hour >= userTime
        ) {
            userStartTime.postValue(localTime.truncatedTo(ChronoUnit.HOURS).plusHours(1))
            userEndTime.postValue(localTime.truncatedTo(ChronoUnit.HOURS).plusHours(3))
        }
        return showMessage
    }

    fun postStartTime(hour: Int, minute: Int){
        val endTimeHour = userEndTime.value?.hour ?: return
        val endTimeMinute = userEndTime.value?.minute ?: return
        if(hour > endTimeHour) {
            userEndTime.postValue(LocalTime.of(hour, minute))
            userStartTime.postValue(LocalTime.of(hour, minute))
        } else if(hour == endTimeHour && hour >= 22){ // almost midnight
            userStartTime.postValue(LocalTime.of(hour,minute))
            userEndTime.postValue(userEndTime.value?.plusMinutes(1))
        } else if (hour == endTimeHour && minute == endTimeMinute){
            userStartTime.postValue(LocalTime.of(hour,minute))
            userEndTime.postValue(userEndTime.value?.plusHours(1))
        } else {
            userStartTime.postValue(LocalTime.of(hour, minute))
        }
    }

    fun postEndTime(hour: Int, minute: Int, force: Boolean = false): Boolean {
        var showMessage = false
        try {
            val startTimeHour = userStartTime.value?.hour!!
            val startTimeMinute = userStartTime.value?.minute!!
            if (startTimeHour >= 22) { // almost midnight
                userEndTime.postValue(userStartTime.value?.plusMinutes(1))
            } else if(startTimeHour > hour && !force) {
                userStartTime.postValue(LocalTime.of(hour, minute))
                userEndTime.postValue(LocalTime.of(startTimeHour, startTimeMinute))
                showMessage = true
            } else if(startTimeHour == hour && startTimeMinute == minute){
                userEndTime.postValue(userStartTime.value?.plusHours(1))
            } else {
                userEndTime.postValue(LocalTime.of(hour, minute))
            }
            return showMessage
        } catch (e: RuntimeException) {
            return showMessage
        }
    }

    fun getBuildings() {
        workstationIsLoading.postValue(false)
        viewModelScope.launch {
            try {
                val service: Response<List<Building.BuildingItem>>? =
                    repository.getBuildingsProduction()
                if (service != null) {
                    workstationIsLoading.postValue(false)
                    if (service.isSuccessful) {
                        val response = service.body()
                        response?.let {
                            withContext(Dispatchers.Main) {
                                val publicBuildings = it.filter { it.public == true }
                                myBuildings.postValue(publicBuildings)
                            }
                        }
                    } else {
                        if (service.code() != 401) {
                            withContext(Dispatchers.Main) {
                                mainActivityListener.fetchBadRequest(ErrorMessage.REQUEST_FAILED)
                            }
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    mainActivityListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getBookings(context: Context, createNotifications: ((context: Context, booking: BookingResponse.Booking)->Unit)) {
        viewModelScope.launch {
            try {
                val date = LocalDate.now()
                val time = LocalTime.of(0, 0, 0)
                val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
                val dateTime = ZonedDateTime.of(date, time, ZoneId.systemDefault()).format(formatter)
                val endTime = ZonedDateTime.of(date.plusDays(1), time, ZoneId.systemDefault()).format(formatter)
                val response =
                    userInfo.value?.email?.let { userEmail ->
                        repositoryBookings.getBookingsProduction(
                            dateTime,
                            endTime,
                            userEmail
                        )
                    }
                if (response != null && response.isSuccessful) {
                    manageBookingData(response.body(), context, createNotifications)
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    mainActivityListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun resetUserBookFor() {
        userBookFor.postValue(userInfo.value)
    }

    private fun manageBookingData(bookings: List<BookingResponse.Booking>?, context: Context, createNotifications: ((context: Context, booking: BookingResponse.Booking)->Unit)) {
        val booking = BookingResponse.Booking(
            "", "", "", "", "", "",
            "", "", "" + "", "", false, false,
            Owner("", "", ""), null, "", "", 0, "", null, null
        )
        val nowDate = LocalDateTime.now()
        var temporalDate = nowDate.plusDays(1)
        var temporalBooking = booking
        val limitBeforeDeadLine = prefs.getData(SHARED_BOOKING_LIMIT_BEFORE_DEADLINE) ?: NOTIFICATION_DEFAULT_RANGE
        val limitAfterDeadLine = prefs.getData(SHARED_BOOKING_LIMIT_AFTER_DEADLINE) ?: NOTIFICATION_DEFAULT_RANGE
        bookings?.forEach { currBooking ->
            val bookingDate = LocalDateTime.ofInstant(
                Instant.parse(currBooking.startTime),
                ZoneId.systemDefault()
            )
            if (bookingDate.isBefore(temporalDate) && currBooking.confirmedAt == null &&
                nowDate.isAfter(bookingDate.minusMinutes(limitBeforeDeadLine.toLong())) && nowDate.isBefore(
                    bookingDate.plusMinutes(limitAfterDeadLine.toLong()))) {
                temporalDate = bookingDate
                temporalBooking = currBooking
            }
//            bureauNotificationManager.createNotifications(currBooking, context)
            createNotifications(context, currBooking)

        }
        updateBooking(temporalBooking)
    }

    private fun updateBooking(booking: BookingResponse.Booking) {
        bookingQR.postValue(booking)
    }

    fun getBuildingsAreas(buildings: List<Building.BuildingItem>) {
        viewModelScope.launch {
            try {
                buildings.forEach {
                    val service: Response<List<Area.AreaItem>>? =
                        it.id?.let { it1 -> repository.getAreaByBuildingIdProduction(it1) }
                    if (service != null) {
                        if (service.isSuccessful) {
                            val response = service.body()
                            it.totalAreas = response?.count().toString()
                        } else {
                            withContext(Dispatchers.Main) {
                                mainActivityListener.fetchBadRequest(ErrorMessage.REQUEST_FAILED)
                            }
                        }
                    }
                }
                buildingsAreas.postValue(buildings)
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    mainActivityListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getBuildingZoneId(): ZoneId {
        val timeZoneString =
            if (!buildingTimeZone.isNullOrBlank()) buildingTimeZone else myBuildings.value?.find { it.id == buildingId.value }?.timezone
        timeZoneString?.also {
            return ZoneId.of(it).normalized()
        }
        return ZoneId.systemDefault()
    }

    fun getBuildingZoneId(buildingId: String): ZoneId {
        val timeZoneString = myBuildings.value?.find { it.id == buildingId }?.timezone
        timeZoneString?.also {
            return ZoneId.of(it).normalized()
        }
        return ZoneId.systemDefault()
    }

    fun getCurrentUser() {
        viewModelScope.launch {
            try {
                val service = repository.getCurrentUser()
                if (service != null) {
                    if(service.isSuccessful) {
                        val response = service.body()
                        if (response != null) {
                            currentUser = service.body() as UserFav
                            getUserGroups(currentUser?.groups ?: emptyList())
                            val role = currentUser?.role.let { currentUser?.role } ?:""
                            prefs.saveData(role, SHARED_USER_ROLE)
                            getUserGroups(currentUser?.groups ?: emptyList())
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            mainActivityListener.fetchBadRequest(ErrorMessage.REQUEST_FAILED)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    mainActivityListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getBookingsConfiguration() {
        viewModelScope.launch {
            try {
                val service: Response<BookingConfiguration>? =
                    repository.getBookingsConfiguration()
                if (service != null) {
                    if (service.isSuccessful) {
                        val response = service.body()
                        response?.let { bookingConfiguration ->
                            prefs.saveData(bookingConfiguration.limitAfterDeadLine.toString(),SHARED_BOOKING_LIMIT_AFTER_DEADLINE)
                            prefs.saveData(bookingConfiguration.limitBeforeDeadLine.toString(), SHARED_BOOKING_LIMIT_BEFORE_DEADLINE)
                        }
                    } else {
                        if ( service.code() != 401) {
                            withContext(Dispatchers.Main) {
                                mainActivityListener.fetchBadRequest(ErrorMessage.REQUEST_FAILED)
                            }
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    mainActivityListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getUserGroups(userGroups: List<String?>) {
        var userGroupsIdList = ""
        userGroups.forEachIndexed { index, groupId ->
            if (index == 0) userGroupsIdList += "id(${groupId})"
            else userGroupsIdList += ",id(${groupId})"
        }
        viewModelScope.launch {
            try {
                val response = repository.getGroups(userGroupsIdList)
                if (response != null) {
                    groups = response.body() ?: listOf(Groups.Group())
                    setBookingPermissions(groups)
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    mainActivityListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    private fun setBookingPermissions(groups: List<Groups.Group>) {
        val recurrencePermission = groups.filter { it.recurrenceBookings == true }.count()
        val bookForOthersPermission = groups.filter { it.bookForOthers == true }.count()
        if (recurrencePermission > 0) {
            prefs.saveBooleanData(true, RECURRENCE_BOOKING_PERMISSION)
        } else {
            prefs.saveBooleanData(false, RECURRENCE_BOOKING_PERMISSION)
        }
        if (bookForOthersPermission > 0) {
            prefs.saveBooleanData(true, BOOK_FOR_OTHERS_PERMISSION)
        } else {
            prefs.saveBooleanData(false, BOOK_FOR_OTHERS_PERMISSION)
        }
    }

    fun fetchUsers(onSuccess: (List<User>?) -> Unit) {
        viewModelScope.launch {
            try {
                val response = repository.fetchUsers()
                if (response != null) {
                    if (response.isSuccessful) {
                        onSuccess(response.body())
                    } else {
                        if (response.code() != 401) {
                            mainActivityListener.fetchBadRequest(ErrorMessage.REQUEST_FAILED)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    mainActivityListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }
}
