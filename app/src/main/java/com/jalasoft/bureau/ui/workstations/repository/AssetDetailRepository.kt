package com.jalasoft.bureau.ui.workstations.repository

import com.jalasoft.bureau.data.interfaces.AssetService
import com.jalasoft.bureau.data.interfaces.BookingService
import com.jalasoft.bureau.data.interfaces.IBookingAvailability
import com.jalasoft.bureau.data.interfaces.MyBookingsService
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.booking.PostBooking
import com.jalasoft.bureau.data.model.favorites.UserFav
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

open class AssetDetailRepository
@Inject
constructor(
    private val retrofit: Retrofit,
    private val assetDetailProductionService: AssetDetailProductionService
): IAssetDetailRepository {

    open suspend fun getAssetByIdProduction(
        assetId: String
    ): Response<Asset.AssetItem>? {
        return retrofit.create(AssetService::class.java)
            ?.getAssetByIdProduction(assetId)
    }

    open suspend fun getBookingsByDate(
        startTime: String,
        endTime: String,
        assetId: String
    ): Response<ArrayList<BookingResponse.Booking>>? {
        return retrofit.create(BookingService::class.java)
            ?.getBookingsByDate(
                startTime,
                endTime,
                "assetId eq id(${assetId})",
                "status.state ne string(UserCancelled)",
                "status.state ne string(SystemCancelled)"
            )
    }

    open suspend fun postBooking(
        booking: PostBooking
    ): Response<BookingResponse.Booking>? {
        return retrofit.create(IBookingAvailability::class.java)
            ?.postNewBooking(booking)
    }

    open suspend fun cancelBooking(
        bookingId: String
    ): Response<BookingResponse.Booking>? {
        return retrofit.create((MyBookingsService::class.java))?.deleteBookingProduction(
                bookingId
            )
    }

    override suspend fun getUserInfo(
    ): Response<UserFav>? {
        return withContext(Dispatchers.IO) {
            assetDetailProductionService.getUserInfo()
        }
    }

    override suspend fun deleteFavoriteAsset(
        idAsset: String
    ): Response<UserFav>? {
        return withContext(Dispatchers.IO) {
            assetDetailProductionService.deleteFavoritesAsset(idAsset)
        }
    }

    override suspend fun postFavoriteAsset(
        idAsset: String
    ): Response<UserFav>? {
        return withContext(Dispatchers.IO) {
            assetDetailProductionService.postFavoritesAsset(idAsset)
        }
    }
}