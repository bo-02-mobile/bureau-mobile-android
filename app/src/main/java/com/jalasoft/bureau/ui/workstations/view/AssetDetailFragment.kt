package com.jalasoft.bureau.ui.workstations.view

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.activityViewModels
import com.alamkanak.weekview.MonthLoader
import com.alamkanak.weekview.WeekViewEvent
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.interfaces.*
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.booking.PostBooking
import com.jalasoft.bureau.data.model.booking.Status
import com.jalasoft.bureau.data.model.common.Owner
import com.jalasoft.bureau.data.model.common.RecurrenceRule
import com.jalasoft.bureau.databinding.BottomSheetBookingRejectedBinding
import com.jalasoft.bureau.databinding.BottomSheetBookingSuccessBinding
import com.jalasoft.bureau.databinding.BottomSheetDialogBinding
import com.jalasoft.bureau.databinding.FragmentAssetDetailBinding
import com.jalasoft.bureau.ui.bookings.viewmodel.MyBookingsViewModel
import com.jalasoft.bureau.ui.main.view.MainActivity
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.workstations.viewmodel.AssetDetailFragmentViewModel
import com.jalasoft.bureau.ui.workstations.viewmodel.BookingOptionsViewModel
import com.jalasoft.bureau.utils.*
import com.jalasoft.bureau.utils.calendar.BaseCalendar
import dagger.hilt.android.AndroidEntryPoint
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.time.*
import java.time.format.DateTimeFormatter
import java.util.*
import javax.inject.Inject
import kotlin.math.roundToInt

@AndroidEntryPoint
class AssetDetailFragment(
    private var assetId: String,
    private var buildingId: String,
    private var areaId: String
) : BottomSheetDialogFragment(), BadRequest, CalendarEventListener, BookingOptionsListener {

    // region Properties
    lateinit var assetDetailBinding: FragmentAssetDetailBinding
    private val assetDetailViewModel: AssetDetailFragmentViewModel by activityViewModels()
    private val myBookingsViewModel: MyBookingsViewModel by activityViewModels()
    var isBottomSheetExpanded: Boolean = false
    var isFromFavorites: Boolean = false
    var newBookingFromQR: Boolean = false
    private var isFixedResource = false
    private var notInBookingWindow = false
    private lateinit var buildingTimezone: TimeZone
    private lateinit var buildingZoneId: ZoneId
    private var booking = false

    //selectedEventId.- empty string denotes that no event has been selected
    private var selectedEventId: String = ""
    lateinit var bottomSheetDialogListener: BottomSheetDialogListener
    lateinit var bottomSheetFromQRDialogListener: BottomSheetFromQRDialogListener
    private val mainViewModel: MainActivityViewModel by activityViewModels()
    private var bookingIsOverlapped = false

    @Inject
    lateinit var optionsViewModelFactory: BookingOptionsViewModel.AssistedFactory
    private val optionsViewModel: BookingOptionsViewModel by activityViewModels {
        BookingOptionsViewModel.provideFactory(optionsViewModelFactory, mainViewModel.myDate.value)
    }
    var dialogAlert: AlertDialog? = null
    // endregion Properties

    private fun initBottomSheet() {
        // setting bottomSheet draggability to false
        (dialog as BottomSheetDialog).behavior.isDraggable = false
        val displayMetrics = requireContext().resources.displayMetrics
        // convert from dp to pixels
        (dialog as BottomSheetDialog).behavior.peekHeight =
            (185 * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
        dialog?.let {
            val bottomSheet = it.findViewById<View>(R.id.design_bottom_sheet)
            bottomSheet.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
        }
        // check if bottomSheet should be expanded from the start
        bottomSheetHeaderContainer()
    }

    private fun initNormalDialog() {
        (dialog as BottomSheetDialog).behavior.isDraggable = false
        (dialog as BottomSheetDialog).behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        bottomSheetDialogListener.didSelectBottomSheetDialog()
        optionsViewModel.resetValues()
        optionsViewModel.endDate.postValue(mainViewModel.myDate.value?.plusMonths(1))
        optionsViewModel.selection.postValue(RECURRENCE_OPTION_NEVER)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_asset_detail, container, false)
        assetDetailBinding = FragmentAssetDetailBinding.bind(view)
        return assetDetailBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        assetDetailViewModel.listener = this
        // First, we initialize the calendar with calendarEventListener which we
        // will use to set a onClick event
        context?.let { context ->
            BaseCalendar(context, assetDetailBinding, this).initCalendarSettingsFirst()
        }
        buildingZoneId = mainViewModel.getBuildingZoneId(buildingId)
        buildingTimezone = DateUtils.getTimeZoneFromZoneId(buildingZoneId)
        checkArguments()
        sendRequests()
        getListBookings()
        initSubscriptions()
        initSubscriptionOverlapped()
        initButtons()
        initButtonListener()
        checkOverlappingEvent()
        if (!resources.getBoolean(R.bool.isTablet)) initBottomSheet() else initNormalDialog()
    }

    override fun onDestroy() {
        super.onDestroy()
        assetDetailViewModel.myAsset.postValue(null)
        assetDetailViewModel.myBookings.postValue(null)
        assetDetailViewModel.updateFavoritesList.postValue(false)
        assetDetailViewModel.updateBookingsList.postValue(false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        if (!resources.getBoolean(R.bool.isTablet)) return super.onCreateDialog(savedInstanceState)
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener {
                val bottomSheet =
                    findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
                bottomSheet.setBackgroundResource(android.R.color.transparent)
            }
        }
    }

    // region get booking request and favorites
    private fun sendRequests() {
        assetDetailViewModel.getUserInfo(assetId)
    }

    private fun getListBookings() {
        context?.let {
            // let's retrieve the assets detail info
            assetDetailViewModel.getAssetById(assetId)
            // now, retrieve detail of the booking
            val date = mainViewModel.myDate.value
            val startTime = mainViewModel.userStartTime.value
            val endTime = mainViewModel.userEndTime.value
            val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
            val start = ZonedDateTime.of(date, startTime, buildingZoneId).plusDays(-1)
                .format(formatter)
            val end = ZonedDateTime.of(date, endTime, buildingZoneId).plusDays(2)
                .format(formatter)
            assetDetailViewModel.getBookingByDate(start, end, assetId)
        }
    }
    // endregion get booking request and favorites

    // region get arguments
    private fun checkArguments() {
        arguments?.let {
            assetId = it.getString(ASSET_ID).toString()
            buildingId = it.getString(BUILDING_ID).toString()
            areaId = it.getString(AREA_ID).toString()
        }
    }
    // endregion get arguments

    private fun changeInitialStartTime(startTime: LocalTime?) {
        if (startTime != null && startTime.hour > 6) {
            assetDetailBinding.weekView.goToHour(startTime.hour.toDouble() - 2)
        }
    }

    private fun bottomSheetHeaderContainer() {
        if (!isBottomSheetExpanded) {
            (dialog as BottomSheetDialog).behavior.state = BottomSheetBehavior.STATE_COLLAPSED
            assetDetailBinding.bottomSheetArrow.setImageResource(R.drawable.ic_arrow_up)
        } else {
            (dialog as BottomSheetDialog).behavior.state = BottomSheetBehavior.STATE_EXPANDED
            assetDetailBinding.bottomSheetArrow.setImageResource(R.drawable.ic_arrow_down)
        }
        isBottomSheetExpanded = !isBottomSheetExpanded
    }

    // region initButtons
    private fun initButtons() {
        val date = mainViewModel.myDate.value
        val startTime = mainViewModel.userStartTime.value
        val endTime = mainViewModel.userEndTime.value
        val formatter = DateTimeFormatter.ofPattern(LOCAL_TIME_FORMAT)
        val dateFormatter = DateTimeFormatter.ofPattern(DATETIME_FORMAT, Locale.ENGLISH)
        val start = startTime?.format(formatter)
        val end = endTime?.format(formatter)
        val calendarValue = ZonedDateTime.of(date, startTime, buildingZoneId).format(dateFormatter)
        changeInitialStartTime(startTime)
        assetDetailBinding.btnTimeStart.text = start
        assetDetailBinding.btnTimeEnd.text = end
        assetDetailBinding.assetCalendar.text = calendarValue
        updateDateTimeText()

        assetDetailBinding.bottomSheetHeaderContainer.setOnClickListener {
            if (isFromFavorites) this.dismiss() else bottomSheetHeaderContainer()
        }

        assetDetailBinding.assetCalendar.setOnClickListener {
            val millis =
                mainViewModel.myDate.value?.atStartOfDay(buildingZoneId)?.toInstant()
                    ?.toEpochMilli()
            val calendarPicker = MaterialDatePicker.Builder.datePicker()
                .setTitleText(getString(R.string.day_booking))
                .setSelection(millis)
                .build()
            calendarPicker.show(parentFragmentManager, DATE_PICKER)
            calendarPicker.addOnPositiveButtonClickListener { timeMillisecond ->
                val showMessage = mainViewModel.postDate(timeMillisecond)
                if (showMessage) {
                    val snack = dialog?.window?.decorView?.let { it1 ->
                        Snackbar.make(
                            it1,
                            getString(R.string.validateDate),
                            Snackbar.LENGTH_LONG
                        )
                    }
                    snack?.show()
                }
                getListBookings()
            }
        }

        assetDetailBinding.bookingOptionsButton.setOnClickListener {
            val optionsBottomSheet = BookingOptionsFragment()
            optionsBottomSheet.bookingOptionsListener = this@AssetDetailFragment
            if (!optionsBottomSheet.isAdded) {
                optionsBottomSheet.show(parentFragmentManager, BOOKING_OPTIONS_BOTTOM_SHEET)
            }
        }

        assetDetailBinding.cancelButtonIcon?.setOnClickListener {
            dialog?.dismiss()
        }

        assetDetailBinding.heartButton.setOnClickListener {
            assetDetailBinding.heartButton.isEnabled = false
            if (assetDetailViewModel.isFavorite.value == true) {
                assetDetailViewModel.deleteFavoriteAsset(assetId) {
                    assetDetailBinding.heartButton.isEnabled = true
                }
                return@setOnClickListener
            }
            assetDetailViewModel.postFavoriteAsset(assetId) {
                assetDetailBinding.heartButton.isEnabled = true
            }
        }
    }
    // endregion initButtons

    // region button click listeners
    private fun initButtonListener() {
        assetDetailBinding.btnBookNow.setOnClickListener {
            if (booking) {
                assetDetailBinding.btnBookNow.isEnabled = false
                val startTime = DateUtils.getFormattedDate(
                    mainViewModel.myDate.value,
                    mainViewModel.userStartTime.value,
                    buildingZoneId
                )
                val endTime = DateUtils.getFormattedDate(
                    mainViewModel.myDate.value,
                    mainViewModel.userEndTime.value,
                    buildingZoneId
                )
                context?.let { context ->
                    val owner = Owner(
                        mainViewModel.userBookFor.value?.email,
                        null,
                        mainViewModel.userBookFor.value?.name
                    )
                    var rule = RecurrenceRule()
                    val date = optionsViewModel.endDate.value ?: mainViewModel.myDate.value
                    val userEndTime = mainViewModel.userEndTime.value
                    val buildingZoneId = mainViewModel.getBuildingZoneId()
                    optionsViewModel.selection.value?.let {
                        rule = assetDetailViewModel.createRecurrenceRule(
                            it,
                            optionsViewModel.willPostRecurrentBooking,
                            date,
                            userEndTime,
                            buildingZoneId,
                            optionsViewModel.monthSelected,
                            optionsViewModel.onDaySelected,
                            optionsViewModel.weekNumberSelected,
                            optionsViewModel.everySelection,
                            optionsViewModel.daySelected,
                            optionsViewModel.weekdaysSelected,
                            optionsViewModel.weekDaySelected
                        )
                    }
                    val status = Status(PENDING)
                    val booking = PostBooking(
                        assetId,
                        endTime,
                        optionsViewModel.willPostRecurrentBooking,
                        owner,
                        rule,
                        startTime,
                        status,
                    )
                    val onSuccess = { booking: BookingResponse.Booking, context: Context ->
                        mainViewModel.highlightedBookId = booking.id
                        myBookingsViewModel.createNotifications(booking, context)
                    }
                    val myDate = mainViewModel.myDate.value
                    val tempStartTime = mainViewModel.userStartTime.value
                    val tempEndTime = mainViewModel.userEndTime.value
                    val showBookingConfirmedDialog = { it: BookingResponse.Booking ->
                        if (newBookingFromQR) {
                            bottomSheetFromQRDialogListener.didConfirmBooking(assetId)
                        } else showBookingConfirmedDialog(it)
                        // add notification creation
                    }
                    assetDetailViewModel.postBooking(
                        context,
                        assetId,
                        startTime,
                        endTime,
                        booking,
                        myDate,
                        tempStartTime,
                        tempEndTime,
                        ::addPostBookingFailed,
                        showBookingConfirmedDialog,
                        onSuccess,
                        mainViewModel::resetUserBookFor
                    )
                }
            } else {
                showCancelBookingDialog()
            }
        }

        assetDetailBinding.btnTimeStart.setOnClickListener {
            val localDate = mainViewModel.userStartTime.value
            val timePicker = localDate?.let { date ->
                MaterialTimePicker.Builder()
                    .setInputMode(MaterialTimePicker.INPUT_MODE_KEYBOARD)
                    .setTimeFormat(TimeFormat.CLOCK_12H)
                    .setHour(date.hour)
                    .setMinute(date.minute)
                    .setTitleText(getString(R.string.select_start_time))
                    .build()
            }
            timePicker?.show(parentFragmentManager, START_TIME_PICKER_TAG)
            timePicker?.addOnPositiveButtonClickListener {
                mainViewModel.postStartTime(timePicker.hour, timePicker.minute)
                getListBookings()
            }
        }

        assetDetailBinding.btnTimeEnd.setOnClickListener {
            val localDate = mainViewModel.userEndTime.value
            val timePicker = localDate?.let { date ->
                MaterialTimePicker.Builder()
                    .setInputMode(MaterialTimePicker.INPUT_MODE_KEYBOARD)
                    .setTimeFormat(TimeFormat.CLOCK_12H)
                    .setHour(date.hour)
                    .setMinute(date.minute)
                    .setTitleText(SELECT_END_TIME)
                    .build()
            }
            timePicker?.show(parentFragmentManager, END_TIME_PICKER_TAG)
            timePicker?.addOnPositiveButtonClickListener {
                val showMessage = mainViewModel.postEndTime(timePicker.hour, timePicker.minute)
                if (showMessage) {
                    val snack = dialog?.window?.decorView?.let { it1 ->
                        Snackbar.make(
                            it1,
                            VALIDATE_END_TIME, Snackbar.LENGTH_LONG
                        )
                    }
                    snack?.anchorView = (activity as MainActivity).binding.bottomNavView
                    snack?.show()
                }
                getListBookings()
            }
        }
    }
    // endregion button click listeners

    private fun checkOverlappingEvent() {
        selectedEventId = ""
        val startTime = DateUtils.getFormattedDate(
            mainViewModel.myDate.value,
            mainViewModel.userStartTime.value,
            buildingZoneId
        )
        val endTime = DateUtils.getFormattedDate(
            mainViewModel.myDate.value,
            mainViewModel.userEndTime.value,
            buildingZoneId
        )
        assetDetailViewModel.checkOverlappingEvent(startTime, endTime, assetId)
    }

    private fun updateDateTimeText() {
        assetDetailBinding.bookingOptionsButton.text = String.format(
            getString(R.string.date_format),
            assetDetailBinding.assetCalendar.text,
            assetDetailBinding.btnTimeStart.text,
            assetDetailBinding.btnTimeEnd.text
        )
    }

    // region init Subscriptions
    private fun initSubscriptions() {

        mainViewModel.userStartTime.observe(viewLifecycleOwner) {
            checkOverlappingEvent()
            val formatter = DateTimeFormatter.ofPattern(LOCAL_TIME_FORMAT, Locale.US)
            assetDetailBinding.btnTimeStart.text = it.format(formatter)
            assetDetailBinding.weekView.goToHour(it.hour.toDouble() - 2)
            updateDateTimeText()
        }

        mainViewModel.userEndTime.observe(viewLifecycleOwner) {
            checkOverlappingEvent()
            val formatter = DateTimeFormatter.ofPattern(LOCAL_TIME_FORMAT, Locale.US)
            assetDetailBinding.btnTimeEnd.text = it.format(formatter)
            updateDateTimeText()
        }

        mainViewModel.myDate.observe(viewLifecycleOwner) {
            checkOverlappingEvent()
            val formatter = DateTimeFormatter.ofPattern(DATETIME_FORMAT, Locale.US)
            assetDetailBinding.assetCalendar.text = it.format(formatter)
            updateDateTimeText()
        }

        assetDetailViewModel.myAsset.observe(viewLifecycleOwner) { assetItem ->
            if (assetItem == null) {
                return@observe
            }
            assetDetailBinding.assetCode.text = assetItem.code
            assetDetailBinding.assetCodeHeader.text = assetItem.code
            assetDetailBinding.assetType.text = assetItem.properties?.type?.let {
                StringUtils.capitalize(
                    it
                )
            }
            FirebaseCrashlyticsUtils.setCrashlyticsKey(KEY_ASSET_ID, assetItem.id)
            assetDetailBinding.assetWorkstation.text =
                assetItem.properties?.labels?.joinToString(separator = " • ") { "$it" }
            // If 'owner' is set, it is a fixed resource and it can not be booked
            assetItem.properties?.owner?.let {
                hideCalendar(true, hideButton = true)
                assetDetailBinding.noCalendarTextTitle.text =
                    resources.getString(R.string.fixed_resource_assigned_to)
                assetDetailBinding.noCalendarTextMessage.text =
                    if (it.name.isNullOrEmpty()) it.email else it.name
                assetDetailBinding.btnBookNow.isEnabled = false
                isFixedResource = true
            } ?: run {
                // If it does not have owner check booking window.
                isFixedResource = false
                val bookingWindow = assetItem.policies?.bookingWindow ?: 0
                val dayDiff = Period.between(LocalDate.now(), mainViewModel.myDate.value).days
                if (dayDiff >= bookingWindow && bookingWindow != 0) {
                    hideCalendar(true, hideButton = false)
                    assetDetailBinding.noCalendarTextTitle.text = getString(R.string.booking_policy)
                    assetDetailBinding.noCalendarTextMessage.text =
                        String.format(getString(R.string.booking_window_message), bookingWindow)
                    assetDetailBinding.btnBookNow.isEnabled = false
                    notInBookingWindow = true
                } else {
                    notInBookingWindow = false
                    hideCalendar(false, hideButton = false)
                }
            }
        }

        assetDetailViewModel.isOverlapped.observe(viewLifecycleOwner) { isOverlapped ->
            assetDetailBinding.btnBookNow.text = resources.getString(R.string.book_now)
            val userStartTime = mainViewModel.userStartTime.value
            val userEndTime = mainViewModel.userEndTime.value
            bookingIsOverlapped = isOverlapped
            if (isOverlapped || isFixedResource || notInBookingWindow || assetDetailViewModel.isOverlappedWithSchedule(
                    userStartTime,
                    userEndTime
                )
            ) {
                assetDetailBinding.btnBookNow.visibility = View.INVISIBLE
                assetDetailBinding.btnBookNow.isEnabled = false
                booking = false
                assetDetailBinding.btnBookNow.setBackgroundColor(requireContext().getColor(R.color.disabled_button))
            } else {
                assetDetailBinding.btnBookNow.visibility = View.VISIBLE
                assetDetailBinding.btnBookNow.isEnabled = true
                booking = true
                assetDetailBinding.btnBookNow.setBackgroundColor(requireContext().getColor(R.color.book_now_background))
            }
        }
    }
    // endregion init Subscriptions

    private fun initSubscriptionOverlapped() {
        assetDetailViewModel.isFavorite.observe(viewLifecycleOwner) { isFavorite ->
            if (isFavorite) {
                requireContext().getColor(R.color.main_color)
                    .let { assetDetailBinding.heartButton.setColorFilter(it) }
            } else {
                requireContext().getColor(R.color.medium_gray)
                    .let { assetDetailBinding.heartButton.setColorFilter(it) }
            }
        }

        assetDetailViewModel.overlappedBookingsStatus.observe(viewLifecycleOwner) { response ->
            assetDetailBinding.weekView.monthChangeListener =
                object : MonthLoader.MonthChangeListener {
                    override fun onMonthChange(
                        newYear: Int,
                        newMonth: Int
                    ): MutableList<out WeekViewEvent> {
                        val bookings =
                            ArrayList<BookingResponse.Booking>(response.first ?: ArrayList())
                        val isOverlapped = response.second
                        val events = ArrayList<WeekViewEvent>()
                        val myDate = mainViewModel.myDate.value
                        val userStartTime = mainViewModel.userStartTime.value
                        val userEndTime = mainViewModel.userEndTime.value
                        val userBookFor = mainViewModel.userBookFor.value
                        val tempEvent = assetDetailViewModel.getCurrentBooking(
                            myDate,
                            userStartTime,
                            userEndTime,
                            userBookFor
                        )
                        bookings.add(tempEvent)
                        val myBuildings = mainViewModel.myBuildings.value
                        bookings.addAll(
                            assetDetailViewModel.getScheduleBookings(
                                myDate,
                                myBuildings
                            )
                        )
                        for (bookingResponse: BookingResponse.Booking in bookings) {
                            val startTime = Calendar.getInstance(buildingTimezone)
                            startTime.time = DateTime(
                                bookingResponse.startTime,
                                DateTimeZone.forTimeZone(buildingTimezone)
                            ).toDate()
                            startTime.set(Calendar.MONTH, newMonth - 1)
                            var endTime = startTime.clone() as Calendar
                            endTime.time = DateTime(
                                bookingResponse.endTime,
                                DateTimeZone.forTimeZone(buildingTimezone)
                            ).toDate()
                            endTime.set(Calendar.MONTH, newMonth - 1)
                            val eventName =
                                if (bookingResponse.owner?.name.isNullOrBlank()) bookingResponse.owner?.email
                                else bookingResponse.owner?.name
                            var event =
                                WeekViewEvent(
                                    bookingResponse.id,
                                    eventName, startTime, endTime
                                )
                            if (isMinimumHeightOfTime(startTime, endTime)) {
                                val modifiedTime = startTime.clone() as Calendar
                                modifiedTime.add(Calendar.MINUTE, 30)
                                event =
                                    WeekViewEvent(
                                        bookingResponse.id,
                                        eventName, startTime, modifiedTime
                                    )
                            }
                            val colorForEvent =
                                getColorForEvent(
                                    bookingResponse.id,
                                    bookingResponse.owner?.email,
                                    isOverlapped ?: false
                                )
                            event.color =
                                ResourcesCompat.getColor(
                                    resources,
                                    colorForEvent,
                                    null
                                )
                            events.add(event)
                            // for new Booking from QR, change endTime filter if overlapped
                            val date = Calendar.getInstance()
                            date.time = DateTime(
                                bookingResponse.startTime,
                                DateTimeZone.forTimeZone(buildingTimezone)
                            ).toDate()
                            val bookingDate = LocalDateTime.ofInstant(
                                date.toInstant(),
                                buildingZoneId
                            )
                            val hour = startTime.get(Calendar.HOUR_OF_DAY)
                            val minutes = startTime.get(Calendar.MINUTE)
                            if (newBookingFromQR && bookingResponse.id != TEMPORAL_EVENT_ID && bookingResponse.assetId == assetId
                                && mainViewModel.userEndTime.value?.isAfter(
                                    LocalTime.of(
                                        hour,
                                        minutes
                                    )
                                ) == true
                                && bookingDate.isBefore(
                                    LocalDateTime.now().withHour(0).withMinute(0).plusDays(1)
                                )
                            ) {
                                mainViewModel.userEndTime.value = LocalTime.of(hour, minutes)
                            }
                        }
                        return events
                    }
                }
            // check for changes
            assetDetailBinding.weekView.notifyDataSetChanged()
            val dateTime = mainViewModel.myDate.value?.let { DateUtils.localDateToDate(it) }
            DateUtils.dateToCalendar(dateTime)?.let { assetDetailBinding.weekView.goToDate(it) }
        }
    }


    private fun hideCalendar(hideCalendar: Boolean, hideButton: Boolean) {
        assetDetailBinding.weekView.visibility = if (hideCalendar) View.GONE else View.VISIBLE
        assetDetailBinding.noCalendarGroup.visibility =
            if (hideCalendar) View.VISIBLE else View.GONE
        assetDetailBinding.timePickers.visibility = if (hideButton) View.GONE else View.VISIBLE
    }

    private fun getColorForEvent(
        bookingId: String,
        ownerEmail: String?,
        isOverlapped: Boolean
    ): Int {
        return if (bookingId == SCHEDULED_EVENT_ID) R.color.overlapped_event
        else if (bookingId == TEMPORAL_EVENT_ID) {
            if (isOverlapped) {
                R.color.overlapped_event
            } else {
                R.color.user_booking
            }
        } else if (bookingId == selectedEventId) {
            R.color.temporal_event
        } else {
            if (mainViewModel.userInfo.value?.email == ownerEmail) {
                R.color.temporal_user_booking
            } else {
                R.color.event_occupied
            }
        }
    }

    private fun addPostBookingFailed(errorMessage: String) {
        if (errorMessage != "") {
            showBookingRejectedDialog(errorMessage)
        } else {
            context?.let {
                MaterialAlertDialogBuilder(it)
                    .setTitle(getString(R.string.book_failed_title))
                    .setMessage(getString(R.string.book_failed_message))
                    .setPositiveButton(resources.getString(R.string.i_understand)) { dialog, _ ->
                        dialog.dismiss()
                    }
                    .show()
            }
        }
    }

    override fun fetchBadRequest(errorType: ErrorMessage) {
        if (dialogAlert?.isShowing == true) {
            return
        }
        context?.let {
            dialogAlert = MaterialAlertDialogBuilder(it)
                .setTitle(resources.getString(errorType.message().first))
                .setMessage(resources.getString(errorType.message().second))
                .setPositiveButton(resources.getString(R.string.i_understand)) { dialog, _ ->
                    dialog.dismiss()
                }
                .create()
                .apply {
                    show()
                }
        }
    }

    private fun showBookingConfirmedDialog(booking: BookingResponse.Booking) {
        context?.let { context ->
            var assetType = assetDetailViewModel.myAsset.value?.properties?.type?.let {
                StringUtils.capitalize(it)
            }
            val assetCode = assetDetailViewModel.myAsset.value?.code
            val dialog = BottomSheetDialog(context)
            val view = BottomSheetBookingSuccessBinding.inflate(layoutInflater)
            val btnCloseModal = view.actionModeCloseButton
            val tvTitle = view.tvBookingSuccessTitle
            val tvDescription = view.tvBookingSuccessBody
            if (assetType == null) assetType = "Workstation"
            tvTitle.text = String.format(getString(R.string.asset_title), assetType, assetCode)
            tvDescription.text =
                StringUtils.capitalize(booking.reservationMessage ?: datesToMessage())
            btnCloseModal.setOnClickListener {
                dialog.dismiss()
            }
            dialog.setContentView(view.root)
            dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            dialog.show()
            dialog.setOnDismissListener {
                navigateToMyBookings()
            }
        }
    }

    private fun showCancelBookingDialog() {
        context?.let { context ->
            val assetType = assetDetailViewModel.myAsset.value?.properties?.type?.let {
                StringUtils.capitalize(it)
            }
            val assetCode = assetDetailViewModel.myAsset.value?.code
            val dialog = BottomSheetDialog(context)
            val view = BottomSheetDialogBinding.inflate(layoutInflater)
            val btnCancelBooking = view.idBtnDismiss
            val btnCloseModal = view.actionModeCloseButton
            val assetTitle = view.workstationCode
            btnCancelBooking.setTextColor(ContextCompat.getColor(context, R.color.white))
            assetTitle.text = String.format(getString(R.string.asset_title), assetType, assetCode)
            btnCloseModal.setOnClickListener {
                dialog.dismiss()
            }
            btnCancelBooking.setOnClickListener {
                val myDate = mainViewModel.myDate.value
                val userStartTime = mainViewModel.userStartTime.value
                val userEndTime = mainViewModel.userEndTime.value
                val buildingZoneId = mainViewModel.getBuildingZoneId()
                assetDetailViewModel.cancelBooking(
                    selectedEventId,
                    assetId,
                    myDate,
                    userStartTime,
                    userEndTime,
                    buildingZoneId
                )
                // once the booking is canceled, button should be set as Book Now
                dialog.dismiss()
            }
            dialog.setContentView(view.root)
            dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            dialog.show()
            dialog.setOnDismissListener {
                navigateToMyBookings()
            }
        }
    }

    private fun showBookingRejectedDialog(errorMessage: String) {
        context?.let { context ->
            val dialog = BottomSheetDialog(context)
            val view = BottomSheetBookingRejectedBinding.inflate(layoutInflater)
            val btnCancel = view.idBtnDismiss
            view.descriptionModalB.text = errorMessage
            view.tvMessageBody.text = datesToMessage()
            btnCancel.setTextColor(ContextCompat.getColor(context, R.color.white))
            view.actionModeCloseButton.setOnClickListener {
                dialog.dismiss()
            }
            btnCancel.setOnClickListener {
                dialog.dismiss()
            }
            dialog.setContentView(view.root)
            dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            dialog.show()
        }
    }

    private fun datesToMessage(): String {
        val date = mainViewModel.myDate.value
        val startTime = mainViewModel.userStartTime.value
        val endTime = mainViewModel.userEndTime.value
        val formatterDate = DateTimeFormatter.ofPattern(LOCAL_DATE_FORMAT_WITHOUT_HOUR)
        val formatterTime = DateTimeFormatter.ofPattern(LOCAL_TIME_FORMAT)
        val tempDate =
            ZonedDateTime.of(date, startTime, buildingZoneId).format(formatterDate)
        val tempStartTime =
            ZonedDateTime.of(date, startTime, buildingZoneId).format(formatterTime)
        val tempEndTime =
            ZonedDateTime.of(date, endTime, buildingZoneId).format(formatterTime)
        return String.format(getString(R.string.date_format), tempDate, tempStartTime, tempEndTime)
    }

    private fun navigateToMyBookings() {
        this.dismiss()
        val bottomNav = (activity as MainActivity).binding.bottomNavView
        val item: MenuItem = bottomNav.menu.getItem(MY_BOOKING_TAB)
        bottomNav.selectedItemId = item.itemId
    }


    override fun didSelectEvent(event: WeekViewEvent) {
        val selectedBooking = assetDetailViewModel.myBookings.value?.find { it.id == event.id }
        assetDetailBinding.btnBookNow.visibility = View.INVISIBLE
        if (event.id != TEMPORAL_EVENT_ID) {
            assetDetailBinding.btnBookNow.isEnabled = true
            selectedEventId = event.id.toString()
            assetDetailBinding.btnBookNow.text = resources.getString(R.string.cancel_booking)
            booking = false
            assetDetailBinding.btnBookNow.visibility = View.VISIBLE
            assetDetailBinding.btnBookNow.setBackgroundColor(requireContext().getColor(R.color.cancel_booking_background))
            if (selectedBooking?.owner?.id != mainViewModel.currentUser?.id) {
                assetDetailBinding.btnBookNow.visibility = View.INVISIBLE
            }
        }

        val userStartTime = mainViewModel.userStartTime.value
        val userEndTime = mainViewModel.userEndTime.value
        if (event.name == mainViewModel.userInfo.value?.name && !isFixedResource && !notInBookingWindow && !bookingIsOverlapped
            && !assetDetailViewModel.isOverlappedWithSchedule(userStartTime, userEndTime)
        ) {
            assetDetailBinding.btnBookNow.visibility = View.VISIBLE
            selectedEventId = event.id.toString()
            assetDetailBinding.btnBookNow.isEnabled = true
            assetDetailBinding.btnBookNow.text = resources.getString(R.string.book_now)
            booking = true
            assetDetailBinding.btnBookNow.setBackgroundColor(requireContext().getColor(R.color.book_now_background))
        }

        assetDetailViewModel.myBookings.postValue(assetDetailViewModel.myBookings.value?.filter { item -> item.id != TEMPORAL_EVENT_ID && item.id != SCHEDULED_EVENT_ID }
            ?.toCollection(ArrayList()))
    }

    override fun didDoneButtonPressed() {
        getListBookings()
    }

    private fun isMinimumHeightOfTime(startLocalTime: Calendar, endLocalTime: Calendar): Boolean {
        val diffTime = getDiffTime(startLocalTime.toInstant(), endLocalTime.toInstant())
        if (diffTime <= 30) return true
        return false
    }

    private fun getDiffTime(startLocalTime: java.time.Instant, endLocalTime: java.time.Instant): Int {
        return Duration.between(startLocalTime, endLocalTime).toMinutes().toInt()
    }
}