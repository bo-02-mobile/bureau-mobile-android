package com.jalasoft.bureau.ui.main.repository

import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.booking.BookingAvailability
import com.jalasoft.bureau.data.model.booking.BookingConfiguration
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.data.model.favorites.UserFav
import com.jalasoft.bureau.data.model.token.User
import com.jalasoft.bureau.data.model.user.Groups
import com.jalasoft.bureau.ui.areas.repository.AreaProductionService
import com.jalasoft.bureau.ui.buildings.repository.BuildingProductionService
import com.jalasoft.bureau.ui.login.repository.UserService
import com.jalasoft.bureau.ui.workstations.repository.BookingAvailabilityService
import com.jalasoft.bureau.ui.workstations.repository.EventService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.time.ZonedDateTime
import javax.inject.Inject

open class MainActivityRepository
@Inject
constructor(
    private val buildingProductionService: BuildingProductionService,
    private val areaProductionService: AreaProductionService,
    private val eventService: EventService,
    private val bookingAvailability: BookingAvailabilityService,
    private val userService: UserService
) : MainRepository {

    override suspend fun getBuildingsProduction(): Response<List<Building.BuildingItem>>? {
        return buildingProductionService.getBuildingsProduction()
    }

    override suspend fun getAreaByBuildingIdProduction(
        buildingId: String
    ): Response<List<Area.AreaItem>>? {
        return areaProductionService.getAreaByBuildingIdProduction(buildingId)
    }

    override suspend fun getBookingAvailabilityProduction(
        workstationId: String,
        startTime: String,
        endTime: String
    ): Response<BookingAvailability>? {
        return bookingAvailability.getBookingAvailabilityProduction(
            workstationId,
            startTime,
            endTime
        )
    }

    override suspend fun postBooking(
        workstationId: String,
        startTime: String,
        endTime: String
    ): Response<BookingResponse.Booking>? {
        return bookingAvailability.postBooking(workstationId, startTime, endTime)
    }

    override suspend fun getEventsProduction(
        workstationId: String,
        date: String
    ): Response<ArrayList<BookingResponse.Booking>>? {
        return eventService.getEventsProduction(workstationId, ZonedDateTime.parse(date))
    }

    override suspend fun fetchUser(): Response<User>? {
        return withContext(Dispatchers.IO) {
            userService.getUserInfo()
        }
    }

    override suspend fun fetchUsers(): Response<List<User>>? {
        return withContext(Dispatchers.IO) {
            userService.getUsersInfo()
        }
    }

    override suspend fun getCurrentUser(): Response<UserFav>? {
        return withContext(Dispatchers.IO) {
            userService.getCurrentUser()
        }
    }

    override suspend fun getGroups(userGroups: String?): Response<List<Groups.Group>>? {
        return withContext(Dispatchers.IO) {
            userService.getGroups(userGroups)
        }
    }
    
    override suspend fun getBookingsConfiguration(): Response<BookingConfiguration>? {
        return withContext(Dispatchers.IO) {
            bookingAvailability.getBookingsConfiguration()
        }
    }
}
