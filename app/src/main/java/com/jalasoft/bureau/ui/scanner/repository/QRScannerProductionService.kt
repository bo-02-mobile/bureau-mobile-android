package com.jalasoft.bureau.ui.scanner.repository

import com.jalasoft.bureau.data.interfaces.BookingConfirmService
import com.jalasoft.bureau.data.model.scanner.BookingConfirmRequestBody
import com.jalasoft.bureau.data.model.scanner.BookingConfirmResponse
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class QRScannerProductionService
@Inject
constructor(private val retrofit: Retrofit) {

    suspend fun confirmBooking(
        bookingId: String,
        requestBody: BookingConfirmRequestBody
    ): Response<BookingConfirmResponse>? {
        return retrofit.create(BookingConfirmService::class.java)
            ?.putWorkstationConfirm(
                bookingId,
                listOf(requestBody)
            )
    }
}