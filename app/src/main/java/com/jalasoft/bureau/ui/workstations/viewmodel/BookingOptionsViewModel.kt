package com.jalasoft.bureau.ui.workstations.viewmodel

import androidx.lifecycle.*
import com.jalasoft.bureau.data.model.token.User
import com.jalasoft.bureau.ui.main.repository.MainActivityRepository
import com.jalasoft.bureau.utils.*
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.*
import javax.inject.Inject

class BookingOptionsViewModel
@AssistedInject
constructor(@Assisted myDate: LocalDate?) : ViewModel() {

    var willPostRecurrentBooking = false
    var tempDate: LocalDate? = LocalDate.now()
    var tempStartTime: LocalTime? = LocalTime.now()
    var tempEndTime: LocalTime? = LocalTime.now()
    var endDate = MutableLiveData<LocalDate>(myDate?.plusMonths(1))
    var selection = MutableLiveData(RECURRENCE_OPTION_NEVER)
    var everySelection = 1
    var weekdaysSelected = BooleanArray(7)
    var monthSelected = 1
    var daySelected = 1
    var weekNumberSelected = 1
    var weekDaySelected = 1
    var onDaySelected = true
    val usersList = MutableLiveData<List<User>>()

    init {
        endDate = MutableLiveData<LocalDate>(myDate?.plusMonths(1))
    }

    @dagger.assisted.AssistedFactory
    interface AssistedFactory {
        fun create(myDate: LocalDate?): BookingOptionsViewModel
    }

    companion object {
        fun provideFactory(
            assistedFactory: AssistedFactory,
            myDate: LocalDate?
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return assistedFactory.create(myDate) as T
            }
        }
    }

    fun resetValues() {
        everySelection = 1
        weekdaysSelected = BooleanArray(7)
        weekdaysSelected[0] = true
        monthSelected = 1
        daySelected = 1
        weekNumberSelected = 1
        weekDaySelected = 1
        onDaySelected = true
    }

    fun getRecurrenceOptionPos(): Int {
        return when (selection.value) {
            RECURRENCE_OPTION_NEVER -> 0
            RECURRENCE_OPTION_DAILY -> 1
            RECURRENCE_OPTION_WEEKLY -> 2
            RECURRENCE_OPTION_MONTHLY -> 3
            RECURRENCE_OPTION_YEARLY -> 4
            else -> -1
        }
    }

    fun getLimitForEveryOption(): Int {
        return when (selection.value) {
            RECURRENCE_OPTION_DAILY -> 99
            RECURRENCE_OPTION_WEEKLY -> 52
            RECURRENCE_OPTION_MONTHLY -> 11
            RECURRENCE_OPTION_YEARLY -> 1
            else -> 0
        }
    }

    fun checkWeekday(index: Int) {
        var selected = 0
        weekdaysSelected.forEach { if (it) selected++ }
        if (!(selected == 1 && weekdaysSelected[index])) weekdaysSelected[index] = !weekdaysSelected[index]
    }

    fun returnAlphaForWeekday(index: Int): Float {
        return if (weekdaysSelected[index]) 1F else 0F
    }

    fun initDateTimeValues(
        myDate: LocalDate?,
        userStartTime: LocalTime?,
        userEndTime: LocalTime?
    ) {
        tempDate = myDate
        tempStartTime = userStartTime
        tempEndTime = userEndTime
    }

    fun resetDateTimeFilters(
        resetDateTime: (myDate: LocalDate?, userStartTime: LocalTime?, userEndTime: LocalTime?) -> Unit
    ) {
        resetDateTime(tempDate, tempStartTime, tempEndTime)
    }

    fun updateEndDate(myDate: LocalDate) {
        if (myDate.atStartOfDay() >= endDate.value?.atStartOfDay()) {
            viewModelScope.launch(Dispatchers.Main) {
                endDate.postValue(myDate.plusMonths(1))
            }
        }
    }

    fun postEndDate(millis: Long, myDate: LocalDate): Boolean {
        val showMessage: Boolean
        val localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneId.of("UTC"))
        if (myDate.atStartOfDay() >= localDateTime.toLocalDate().atStartOfDay()) {
            endDate.postValue(myDate.plusMonths(1))
            showMessage = true
        } else {
            showMessage = false
            endDate.postValue(localDateTime.toLocalDate())
        }
        return showMessage
    }
}