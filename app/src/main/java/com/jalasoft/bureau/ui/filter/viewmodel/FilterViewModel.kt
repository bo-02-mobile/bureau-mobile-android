package com.jalasoft.bureau.ui.filter.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.data.model.asset.AssetLabel
import com.jalasoft.bureau.data.model.asset.AssetType
import com.jalasoft.bureau.ui.filter.repository.FilterRepository
import com.jalasoft.bureau.utils.ErrorMessage
import com.jalasoft.bureau.utils.extensions.getViewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class FilterViewModel
@Inject
constructor(
    private val filterRepository: FilterRepository
) : ViewModel() {

    lateinit var listener: BadRequest
    val filterTypes = MutableLiveData<List<AssetType.AssetTypeItem>>()
    val filterLabels = MutableLiveData<List<AssetLabel>>()
    val spaceTypeSelected = MutableLiveData<Int>(0)
    val spaceType = MutableLiveData<String>("all")
    val arrLabels = MutableLiveData<List<String>>(emptyList())
    val switchAvailableSpaces = MutableLiveData<Boolean>(false)
    val countFilterSelected = MutableLiveData<Int>(0)

    fun getFiltersTypes() {
        viewModelScope.launch {
            try {
                val response = filterRepository.getFiltersTypes()
                response?.let { response ->
                    if (response.isSuccessful) {
                        val body = response.body()
                        body?.let { assetsType ->
                            // Let's create a temporal Type with value "All"
                            val tempAssetsTypes = ArrayList<AssetType.AssetTypeItem>()
                            val temp = AssetType.AssetTypeItem("all", emptyList(),"All", "All", "All")
                            tempAssetsTypes.add(temp)
                            tempAssetsTypes.addAll(assetsType)
                            filterTypes.postValue(tempAssetsTypes)
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            listener.fetchBadRequest(ErrorMessage.REQUEST_FAILED)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    listener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getFiltersLabels() {
        viewModelScope.launch {
            try {
                val response = filterRepository.getFiltersLabels()
                response?.let { response ->
                    if (response.isSuccessful) {
                        val body = response.body()
                        body?.let { assetsType ->
                            filterLabels.postValue(assetsType)
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            listener.fetchBadRequest(ErrorMessage.REQUEST_FAILED)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    listener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun clearAll() {
        this.arrLabels.postValue(emptyList())
        this.spaceTypeSelected.postValue(0)
        this.spaceType.postValue("all")
        this.switchAvailableSpaces.postValue(false)
    }

    fun saveFilterState(
        spaceTypeSelected: Int,
        spaceType: String,
        tempArrLabels: List<String>,
        switchAvailableSpaces: Boolean
    ) {
        this.spaceType.postValue(spaceType)
        this.spaceTypeSelected.postValue(spaceTypeSelected)
        this.switchAvailableSpaces.postValue(switchAvailableSpaces)
        val copy = ArrayList(tempArrLabels)
        this.arrLabels.postValue(copy)
        countFilterSelected(spaceType,tempArrLabels,switchAvailableSpaces)
    }

    fun countFilterSelected(
        spaceType: String,
        tempArrLabels: List<String>,
        switchAvailableSpaces: Boolean
    ) {
        var count = 0
        if(spaceType != "all") count++
        if(switchAvailableSpaces) count++
        count += tempArrLabels.size
        countFilterSelected.postValue(count)
    }

    fun getFiltersByAssetType(typesList: List<AssetType.AssetTypeItem>, labelsList: List<AssetLabel>, typeSelected: Int): ArrayList<AssetLabel> {
        val filteredLabels = ArrayList<AssetLabel>()
        if (typeSelected == 0) {
            return ArrayList(labelsList)
        } else {
            for (label in labelsList) {
                if (label.id in typesList[typeSelected].labels) {
                    filteredLabels.add(label)
                }
            }
            return filteredLabels
        }
    }
}