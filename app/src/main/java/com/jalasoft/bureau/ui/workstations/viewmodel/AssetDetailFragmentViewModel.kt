package com.jalasoft.bureau.ui.workstations.viewmodel

import android.content.Context
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.booking.PostBooking
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.data.model.common.Owner
import com.jalasoft.bureau.data.model.common.RecurrenceRule
import com.jalasoft.bureau.data.model.favorites.UserFav
import com.jalasoft.bureau.data.model.token.User
import com.jalasoft.bureau.notifications.BNotificationManager
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.workstations.repository.AssetDetailRepository
import com.jalasoft.bureau.utils.*
import com.jalasoft.bureau.utils.extensions.getViewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONObject
import retrofit2.Response
import java.time.*
import java.time.format.DateTimeFormatter
import javax.inject.Inject

@HiltViewModel
class AssetDetailFragmentViewModel
@Inject
constructor(private val repository: AssetDetailRepository) : ViewModel() {

    val myAsset = MutableLiveData<Asset.AssetItem>()
    val myBookings = MutableLiveData<ArrayList<BookingResponse.Booking>>()
    lateinit var listener: BadRequest
    var isOverlapped = MutableLiveData<Boolean>()
    var scheduledBookings: List<BookingResponse.Booking> = listOf()
    private val bureauNotificationManager = BNotificationManager()
    val overlappedBookingsStatus : MediatorLiveData<Pair<ArrayList<BookingResponse.Booking>?, Boolean?>> = MediatorLiveData<Pair<ArrayList<BookingResponse.Booking>?, Boolean?>>().apply {
        addSource(myBookings) { value = Pair(it, isOverlapped.value) }
        addSource(isOverlapped) { value = Pair(myBookings.value, it) }
    }
    var isFavorite = MutableLiveData<Boolean>()
    var isEmptyList = MutableLiveData<Boolean>(false)
    var updateFavoritesList = MutableLiveData<Boolean>(false)
    var updateBookingsList = MutableLiveData<Boolean>(false)

    fun getAssetById(assetId: String) {
        viewModelScope.launch {
            try {
                val response = repository.getAssetByIdProduction(assetId)
                response?.let { response ->
                    if (response.isSuccessful) {
                        val body = response.body()
                        body?.let { assetItem ->
                            withContext(Dispatchers.Main) {
                                myAsset.postValue(assetItem)
                            }
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            listener.fetchBadRequest(ErrorMessage.REQUEST_FAILED_ASSET)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    listener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getBookingByDate(startTime: String, endTime: String, assetId: String) {
        viewModelScope.launch {
            try {
                val response = repository.getBookingsByDate(startTime, endTime, assetId)
                response?.let { response ->
                    if (response.isSuccessful) {
                        val body = response.body()
                        body?.let { bookings ->
                            withContext(Dispatchers.Main) {
                                myBookings.postValue(bookings)
                            }
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            listener.fetchBadRequest(ErrorMessage.REQUEST_FAILED_ASSET)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    listener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun checkOverlappingEvent(
        startTime: String,
        endTime: String,
        assetId: String
    ) {
        viewModelScope.launch {
            try {
                val response = repository.getBookingsByDate(startTime, endTime, assetId)
                response?.let { response ->
                    if (response.isSuccessful) {
                        val body = response.body()
                        body?.let { bookings ->
                            withContext(Dispatchers.Main) {
                                val len = bookings.size
                                if (len == 0) {
                                    isOverlapped.postValue(false)
                                } else if (len > 0) {
                                    isOverlapped.postValue(true)
                                }
                            }
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    listener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getCurrentBooking(
        myDate: LocalDate?,
        userStartTime: LocalTime?,
        userEndTime: LocalTime?,
        userBookFor: User?
    ): BookingResponse.Booking {
        val startTime = LocalDateTime.of(myDate, userStartTime).toString()
        val endTime = LocalDateTime.of(myDate, userEndTime).toString()

        return BookingResponse.Booking(
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            endTime,
            TEMPORAL_EVENT_ID,
            isPermanent = false,
            isRecurrent = false,
            Owner(
                userBookFor?.email,
                null,
                userBookFor?.name
            ),
            RecurrenceRule(),
            startTime,
            "",
            1,
            "",
            null,
            null
        )
    }

    fun getScheduleTimes(myDate: LocalDate?, myBuildings: List<Building.BuildingItem>?): MutableList<LocalDateTime> {
        val date = myDate ?: LocalDate.now().withYear(0)
        val list = mutableListOf(LocalDateTime.of(date.year, date.month, date.dayOfMonth, 0, 0))
        if (myAsset.value?.buildingId.isNullOrEmpty()) {
            return mutableListOf()
        }
        val buildingTimezone =
            myBuildings?.find { it.id == myAsset.value?.buildingId }?.timezone ?: TIMEZONE_UTC
        val timezone = ZoneId.of(buildingTimezone).normalized()
        myAsset.value?.policies?.schedule?.forEach { schedule ->
            val start = DateUtils.stringToLocalDateTime(date, schedule.startTime, timezone)
            val end = DateUtils.stringToLocalDateTime(date, schedule.endTime, timezone)
            list.add(start)
            list.add(end)
        }
        list.add(LocalDateTime.of(date.year, date.month, date.dayOfMonth, 23, 59))
        if(list.size<=2) return mutableListOf()
        if(list[1].isEqual(list[2].plusMinutes(1))) return mutableListOf()
        return list
    }

    fun getScheduleBookings(
        myDate: LocalDate?,
        myBuildings: List<Building.BuildingItem>?
    ): List<BookingResponse.Booking> {
        val list: MutableList<BookingResponse.Booking> = mutableListOf()
        val schedules = getScheduleTimes(myDate, myBuildings)
        for (i in 0 until schedules.size step 2) {
            list.add(BookingResponse.Booking(
                "", "", "", "", "", "", "", "",
                schedules[i+1].toString(),
                SCHEDULED_EVENT_ID,
                false, false, null, null,
                schedules[i].toString(),
                "",
                1,
                "",
                null, null)
            )
        }
        scheduledBookings = list
        return list
    }

    fun isOverlappedWithSchedule(
        userStartTime: LocalTime?,
        userEndTime: LocalTime?
    ): Boolean {
        scheduledBookings.forEach { schedule ->
            val start = DateUtils.stringToLocalTime(schedule.startTime)
            val end = DateUtils.stringToLocalTime(schedule.endTime)
            val userStart = userStartTime ?: LocalTime.of(0, 0)
            val userEnd = userEndTime ?: LocalTime.of(0, 0)
            if((userStart.isAfter(start) && userStart.isBefore(end))
                || (userEnd.isAfter(start) && userStart.isBefore(end))
            ) return true
        }
        return false
    }

    fun postBooking(
        context: Context,
        assetId: String,
        startTime: String,
        endTime: String,
        booking: PostBooking,
        date: LocalDate?,
        tempStartTime: LocalTime?,
        tempEndTime: LocalTime?,
        afterPostBookingFailed: (String) -> Unit,
        showBookingConfirmedDialog: (BookingResponse.Booking) -> Unit,
        onSuccess: (booking: BookingResponse.Booking, context: Context) -> Unit,
        resetUserBookFor: () -> Unit
    ) {
        viewModelScope.launch {
            try {
                val response = repository.postBooking(booking)
                response?.let { response ->
                    if (response.isSuccessful) {
                        val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
                        val start =
                            ZonedDateTime.of(date, tempStartTime, ZoneId.systemDefault()).plusDays(-1)
                                .format(formatter)
                        val end =
                            ZonedDateTime.of(date, tempEndTime, ZoneId.systemDefault()).plusDays(2)
                                .format(formatter)
                        // check overlapped events
                        checkOverlappingEvent(startTime, endTime, assetId)
                        getBookingByDate(start, end, assetId)
                        // create notification
                        response.body()?.let { booking ->
                            onSuccess(booking, context)
                            val dateBookingStart = DateUtils.dateFormatUTC(booking.startTime, API_DATE_FORMAT_2, API_DATE_FORMAT)
                            val dateBookingEndTime = DateUtils.dateFormatUTC(booking.endTime, API_DATE_FORMAT_2, API_DATE_FORMAT)
                            booking.startTime = dateBookingStart ?: ""
                            booking.endTime = dateBookingEndTime ?: ""
                            showBookingConfirmedDialog(booking)
                            updateBookingsList.postValue(true)
                        }
                        resetUserBookFor()
                    } else {
                        try {
                            val errorMessage =
                                JSONObject(response.errorBody()!!.string()).getString("Error")
                            withContext(Dispatchers.Main) {
                                afterPostBookingFailed(errorMessage)
                            }
                        } catch (e: Exception) {
                            withContext(Dispatchers.Main) {
                                afterPostBookingFailed("")
                            }
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    listener.fetchBadRequest(ErrorMessage.REQUEST_FAILED_ASSET)
                }
            }
        }
    }

    fun createRecurrenceRule(
        option: String,
        isRecurrent: Boolean,
        date: LocalDate?,
        userEndTime: LocalTime?,
        buildingZoneId: ZoneId,
        monthSelected: Int,
        onDaySelected: Boolean,
        weekNumberSelected: Int,
        everySelection: Int,
        daySelected: Int,
        weekdaysSelected: BooleanArray,
        weekDaySelected: Int
    ): RecurrenceRule {
        val rule = RecurrenceRule()
        rule.isCustom = option != RECURRENCE_OPTION_NEVER && isRecurrent
        val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
        rule.endDate = ZonedDateTime.of(date, userEndTime ?: LocalTime.of(0,0), buildingZoneId).format(formatter)
        if (!isRecurrent) return rule
        rule.type =
            if (option != RECURRENCE_OPTION_NEVER) option.lowercase()
            else ""
        rule.onMonth =
            if (option == RECURRENCE_OPTION_YEARLY) monthSelected
            else null
        rule.onWeek =
            if ((option == RECURRENCE_OPTION_YEARLY || option == RECURRENCE_OPTION_MONTHLY) && !onDaySelected)
                weekNumberSelected
            else null
        rule.onDays = getArrayDays(option, weekdaysSelected, onDaySelected, weekDaySelected)
        rule.every =
            if (option == RECURRENCE_OPTION_YEARLY || option == RECURRENCE_OPTION_NEVER) 0
            else everySelection
        rule.onDate =
            if ((option == RECURRENCE_OPTION_YEARLY || option == RECURRENCE_OPTION_MONTHLY) && onDaySelected)
                daySelected
            else null
        return rule
    }

    fun getArrayDays(
        option: String,
        weekdaysSelected: BooleanArray,
        onDaySelected: Boolean,
        weekDaySelected: Int
    ): Array<String> {
        if (option == RECURRENCE_OPTION_NEVER || option == RECURRENCE_OPTION_DAILY) return arrayOf()
        if (option == RECURRENCE_OPTION_WEEKLY) {
            var array = arrayOf<String>()
            weekdaysSelected.forEachIndexed { index, day ->
                if (day) array = array.plusElement(getWeekDayName(index))
            }
            return array
        }
        else if (onDaySelected) {
            return arrayOf()
        }
        return arrayOf(getWeekDayName(weekDaySelected))
    }

    fun getWeekDayName(day: Int): String {
        return when (day) {
            1 -> MONDAY
            2 -> TUESDAY
            3 -> WEDNESDAY
            4 -> THURSDAY
            5 -> FRIDAY
            6 -> SATURDAY
            else -> SUNDAY
        }
    }

    fun cancelBooking(
        bookingId: String,
        assetId: String,
        myDate: LocalDate?,
        userStartTime: LocalTime?,
        userEndTime: LocalTime?,
        buildingZoneId: ZoneId
    ) {
        viewModelScope.launch {
            try {
                val startTime = DateUtils.getFormattedDate(myDate, userStartTime, buildingZoneId)
                val endTime = DateUtils.getFormattedDate(myDate, userEndTime, buildingZoneId)
                val response = repository.cancelBooking(bookingId)
                response?.let { response ->
                    if (response.isSuccessful) {
                        val body = response.body()
                        body?.let {
                            withContext(Dispatchers.Main) {
                                val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
                                val start =
                                    ZonedDateTime.of(myDate, userStartTime, buildingZoneId)
                                        .plusDays(-1)
                                        .format(formatter)
                                val end =
                                    ZonedDateTime.of(myDate, userEndTime, buildingZoneId)
                                        .plusDays(2)
                                        .format(formatter)
                                // get all bookings
                                checkOverlappingEvent(startTime, endTime, assetId)
                                getBookingByDate(start, end, assetId)
                                updateBookingsList.postValue(true)
                            }
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            listener.fetchBadRequest(ErrorMessage.REQUEST_FAILED_ASSET)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    listener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getUserInfo(
        assetId: String
    ) {
        viewModelScope.launch {
            try {
                val response = repository.getUserInfo()
                response?.let { response ->
                    if (response.isSuccessful) {
                        val body = response.body()
                        body?.let { user ->
                            withContext(Dispatchers.Main) {
                                val favList = user.favoriteAssets
                                if (favList.isNotEmpty()) {
                                    val foundValue = favList.find { item -> item == assetId }
                                    if (foundValue != null) {
                                        if (foundValue.isNotEmpty()) isFavorite.postValue(true) else isFavorite.postValue(false)
                                    } else {
                                        isFavorite.postValue(false)
                                    }
                                } else {
                                    isFavorite.postValue(false)
                                }
                            }
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            listener.fetchBadRequest(ErrorMessage.REQUEST_FAILED_ASSET)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    listener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun postFavoriteAsset(idAsset: String, onPost: () -> Unit = {}) {
        viewModelScope.launch {
            try {
                val service: Response<UserFav>? =
                    repository.postFavoriteAsset(idAsset)
                if (service == null || !service.isSuccessful) {
                    withContext(Dispatchers.Main) {
                        listener.fetchBadRequest(ErrorMessage.REQUEST_FAILED_ASSET)
                    }
                    return@launch
                }
                val response = service.body()
                response?.let { user ->
                    withContext(Dispatchers.Main) {
                        val favList = user.favoriteAssets
                        if (favList.isNotEmpty()) {
                            val foundValue = favList.find { item -> item == idAsset }
                            if (foundValue.isNullOrEmpty()) isFavorite.postValue(false) else isFavorite.postValue(true)
                        } else {
                            isFavorite.postValue(false)
                        }
                        updateFavoritesList.postValue(true)
                        onPost()
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    listener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun deleteFavoriteAsset(idAsset: String, onDelete: () -> (Unit) = {}) {
        viewModelScope.launch {
            try {
                val service: Response<UserFav>? =
                    repository.deleteFavoriteAsset(idAsset)
                if (service == null || !service.isSuccessful) {
                    withContext(Dispatchers.Main) {
                        listener.fetchBadRequest(ErrorMessage.REQUEST_FAILED_ASSET)
                    }
                    return@launch
                }
                val response = service.body()
                response?.let { user ->
                    withContext(Dispatchers.Main) {
                        val favList = user.favoriteAssets
                        if (favList.isNotEmpty()) {
                            val foundValue = favList.find { item -> item == idAsset }
                            if (foundValue.isNullOrEmpty()) isFavorite.postValue(false) else isFavorite.postValue(true)
                            isEmptyList.postValue(false)
                        } else {
                            isEmptyList.postValue(true)
                            isFavorite.postValue(false)
                        }
                        updateFavoritesList.postValue(true)
                        onDelete()
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    listener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }
}