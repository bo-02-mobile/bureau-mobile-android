package com.jalasoft.bureau.ui.main.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.provider.Settings
import android.view.*
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.data.interfaces.BookingAvailabilityListener
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.databinding.ActivityMainBinding
import com.jalasoft.bureau.databinding.BottomSheetBookingSuccessBinding
import com.jalasoft.bureau.databinding.FilterComponentBinding
import com.jalasoft.bureau.ui.bookings.viewmodel.MyBookingsViewModel
import com.jalasoft.bureau.ui.buildings.view.BuildingFragmentDirections
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.utils.*
import com.jalasoft.bureau.utils.DarkModeUtils.Companion.darkMode
import com.jalasoft.bureau.utils.login.AuthStateManager

import dagger.hilt.android.AndroidEntryPoint
import com.jalasoft.bureau.widgets.BookingWidget
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.openid.appauth.AuthorizationException
import net.openid.appauth.AuthorizationResponse
import org.joda.time.DateTime
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), BadRequest, BookingAvailabilityListener {

    lateinit var binding: ActivityMainBinding
    private val viewModel: MainActivityViewModel by viewModels()
    private val myBookingsViewModel: MyBookingsViewModel by viewModels()
    private lateinit var filterBinding: FilterComponentBinding
    private lateinit var navController: NavController
    private var toggle : ActionBarDrawerToggle? = null
    var dialogAlert: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        filterBinding = FilterComponentBinding.inflate(layoutInflater)
        viewModel.mainActivityListener = this
        viewModel.bookingAvailabilityListener = this
        viewModel.prefs = Prefs(this)
        darkMode(viewModel.prefs.getData(SHARED_USER_APPEARANCE))
        setContentView(binding.root)
        initSubscription()
        checkAuthorization()
        checkForInternetConnection()
        initFilterValues()
        verifyNotification()
        val bottomNavView = binding.bottomNavView
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fragment_container) as NavHostFragment
        navController = navHostFragment.navController
        bottomNavView.setupWithNavController(navController)
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.buildingFragment, R.id.myBookingListFragment,
                R.id.qrScannerFragment, R.id.profileFragment
            )
        )
        if(!resources.getBoolean(R.bool.isTablet)){
            setUpToolbar()
            setupActionBarWithNavController(navController, appBarConfiguration)
            if(navController.currentDestination?.id == R.id.buildingFragment) {
                val buildingId: String? = viewModel.prefs.getData(SHARED_BUILDING_ID)
                val buildingName: String? = viewModel.prefs.getData(SHARED_BUILDING_NAME)
                val buildingTimeZone: String? = viewModel.prefs.getData(SHARED_BUILDING_ZONE_ID)
                val switchStatus = viewModel.prefs.getData(
                    SHARED_USER_SAVE_BUILDING)
                if (!buildingId.isNullOrEmpty()
                    && !buildingName.isNullOrEmpty()
                    && (switchStatus.isNullOrEmpty() || switchStatus == "ON" )) {
                    viewModel.buildingId.value = buildingId
                    viewModel.buildingTimeZone = buildingTimeZone
                    val action: NavDirections =
                        BuildingFragmentDirections.actionBuildingFragmentToNewAreaFragment(
                            buildingId,
                            buildingName
                        )
                    navController.navigate(action)
                }
            }
        } else {
            val window: Window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.WHITE
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            binding.drawerToolbar?.setBackgroundColor(resources.getColor(R.color.background))
            setSupportActionBar(binding.drawerToolbar)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            val drawerLayout: DrawerLayout = binding.drawerLayout!!

            toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
            drawerLayout.addDrawerListener(toggle!!)
            drawerLayout.openDrawer(Gravity.LEFT)
            toggle!!.syncState()

            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            expandShrinkAreaMap()
        }
        // Check if app was opened from widget
        verifyWidgetIntent()
        // Check if app was opened from weblink
        WebLink(binding, intent.data.toString(),viewModel).openWebLink()
        // Get shortcuts extras and redirect to specific tab
        when (intent.getIntExtra(SHORTCUT_EXTRA, NO_SELECTED_TAB)) {
            MY_BOOKING_TAB -> {
                val item: MenuItem = bottomNavView.menu.getItem(MY_BOOKING_TAB)
                bottomNavView.selectedItemId = item.itemId
            }
            QR_TAB -> {
                val item: MenuItem = bottomNavView.menu.getItem(QR_TAB)
                bottomNavView.selectedItemId = item.itemId
            }
            BOOKING_TAB -> {
                val item: MenuItem = bottomNavView.menu.getItem(BOOKING_TAB)
                bottomNavView.selectedItemId = item.itemId
            }
            NO_SELECTED_TAB -> {
                return
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(toggle?.onOptionsItemSelected(item) == true){true}
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    fun moveToQrScreen() {
        val item: MenuItem = binding.bottomNavView.menu.getItem(QR_TAB)
        binding.bottomNavView.selectedItemId = item.itemId
    }

    private fun checkAuthorization() {
        if (AuthStateManager.getInstance(this).current.isAuthorized) {
            setUserInfo()
        } else {
            setUpTokenRequest()
        }
    }

    private fun initFilterValues() {
        viewModel.initDateTimeFiltersDefault(true)
    }

    private fun setUpTokenRequest() {
        val resp = AuthorizationResponse.fromIntent(intent)
        val ex = AuthorizationException.fromIntent(intent)

        if (resp != null) {
            AuthStateManager.getInstance(this).updateAfterAuthorization(resp, ex)
            viewModel.performTokenRequest(this, resp)
        }
    }

    private fun setUpToolbar() {
        setSupportActionBar(binding.appBar.toolbar)
        supportActionBar?.title = EMPTY_TITLE
        supportActionBar?.let { actionBar ->
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }
    }

    private fun setUserInfo() {
        viewModel.fetchUser()
        viewModel.getCurrentUser()
    }


    private fun initSubscription() {
        viewModel.tokenRequestResponse.observe(this) {
            AuthStateManager.getInstance(this).updateAfterTokenResponse(it.resp, it.ex)
            viewModel.fetchUser()
            getBookingsConfiguration()
            verifyNotification()
            viewModel.getCurrentUser()
        }
    }

    private fun checkForInternetConnection() {
        if (!GlobalConstants.hasNetwork(this)) {
            MaterialAlertDialogBuilder(this)
                .setTitle(resources.getString(R.string.no_internet_connection_title))
                .setMessage(resources.getString(R.string.no_internet_connection_message))
                .setPositiveButton(resources.getString(R.string.go_to_settings)) { _, _ ->
                    startActivity(Intent(Settings.ACTION_SETTINGS))
                }
                .show()
        }
    }

    override fun fetchBadRequest(errorType: ErrorMessage) {
        viewModel.workstationIsLoading.postValue(false)
        if (dialogAlert?.isShowing == true) {return}
        if(this.isFinishing) return
        dialogAlert = MaterialAlertDialogBuilder(this)
            .setTitle(resources.getString(errorType.message().first))
            .setMessage(resources.getString(errorType.message().second))
            .setPositiveButton(resources.getString(R.string.i_understand)) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
            .apply {
                show()
            }
    }

    override fun didBookingSuccessful(workstationCode: String) {
        val dialog = BottomSheetDialog(this)
        val view = BottomSheetBookingSuccessBinding.inflate(layoutInflater)
        val btnCloseModal = view.actionModeCloseButton
        val tvDescription = view.tvBookingSuccessBody
        val title = view.tvBookingSuccessTitle
        val formatter = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
        val date = formatter.format(DateTime(viewModel.myDate.value).toDate())
        val startTime = DateTime(viewModel.userStartTime.value)
        val endTime = DateTime(viewModel.userEndTime.value)
        val workstationPlaceholder = resources.getString(R.string.qr_bottom_sheet_workstation)
        title.text = String.format(workstationPlaceholder, workstationCode)
        val time = String.format(
            "from %02d:%02d to %02d:%02d",
            startTime.hourOfDay,
            startTime.minuteOfHour,
            endTime.hourOfDay,
            endTime.minuteOfHour
        )
        tvDescription.text = String.format(DATE_TIME_PATTERN, date, time)
        btnCloseModal.setOnClickListener {
            dialog.dismiss()
        }
        dialog.setContentView(view.root)
        dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        dialog.show()
    }

    override fun onBackPressed() {
        val canNavigateUp: Boolean = findNavController(R.id.fragment_container).popBackStack()
        if (!canNavigateUp) finish()
    }

    override fun didPostBookingFailed() {
        createDialog(
            resources.getString(R.string.booking_conflict_title),
            resources.getString(R.string.booking_conflict_body)
        )
    }

    private fun createDialog(title: String, body: String) {
        MaterialAlertDialogBuilder(this)
            .setTitle(title)
            .setMessage(body)
            .setPositiveButton(resources.getString(R.string.i_understand)) { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }

    private fun verifyNotification() {
        viewModel.userInfo.observe(this) {
            val isNotification = intent.getBooleanExtra(IS_NOTIFICATION, false)
            if (isNotification) {
                CoroutineScope(Dispatchers.Main).launch {
                    val bookingId = intent.getStringExtra(BOOKING_ID)
                    viewModel.userBookingId.postValue(bookingId)
                    binding.bottomNavView.selectedItemId = R.id.myBookingListFragment
                }
            } else {
                viewModel.getBookings(this, myBookingsViewModel::createNotifications)
                updateWidget()
            }
        }
    }
    private fun updateWidget() {
        val ownerEmail = viewModel.userInfo.value?.email.toString()
        val userBookingId = viewModel.userBookingId.value
        val buildings = viewModel.myBuildings.value
        myBookingsViewModel.getAllBookings(ownerEmail, userBookingId, false, ::setBookingId, buildings)
        BookingWidget.refreshWidget(this)
    }

    private fun setBookingId(booking: BookingResponse.Booking) {
       viewModel.userBookingId.value= booking.id
    }

    private fun verifyWidgetIntent() {
        val isWidgetAction = intent.extras?.get(WIDGET_ACTION)
        if (isWidgetAction == true) {
            val prefs = Prefs(applicationContext)
            val ownerEmail = prefs.getData(SHARED_USER_EMAIL) ?: ""
            if (ownerEmail.isNotBlank()) {
                navController.popBackStack(R.id.buildingFragment, false)
            }
        }
    }

    private fun getBookingsConfiguration() {
        viewModel.getBookingsConfiguration()
    }

    private fun expandShrinkAreaMap() {
        binding.buttonExpandShrink?.setOnClickListener {
            if (binding.leftNavContainer?.visibility == View.GONE) {
                binding.leftNavContainer?.visibility = View.VISIBLE
                binding.buttonExpandShrink?.setImageResource(R.drawable.ic_expand)
                binding.leftNavContainer?.animate()?.alpha(1.0f)?.duration = 500
            } else {
                binding.leftNavContainer?.visibility = View.GONE
                binding.buttonExpandShrink?.setImageResource(R.drawable.ic_shrink)
                binding.leftNavContainer?.animate()?.alpha(0.0f)?.duration = 500
            }
        }
    }

    override fun onResume() {
        getBookingsConfiguration()
        super.onResume()
    }
}
