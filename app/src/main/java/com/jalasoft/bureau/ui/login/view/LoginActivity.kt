package com.jalasoft.bureau.ui.login.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.databinding.ActivityLoginBinding
import com.jalasoft.bureau.ui.login.viewmodel.LoginViewModel
import com.jalasoft.bureau.ui.main.view.MainActivity
import com.jalasoft.bureau.utils.DarkModeUtils
import com.jalasoft.bureau.utils.ErrorMessage
import com.jalasoft.bureau.utils.SHARED_USER_APPEARANCE
import com.jalasoft.bureau.utils.login.AuthStateManager
import com.jalasoft.bureau.widgets.BookingWidget
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : AppCompatActivity(), BadRequest {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: LoginViewModel
    var dialogAlert: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val prefs = Prefs(this)
        DarkModeUtils.darkMode(prefs.getData(SHARED_USER_APPEARANCE))
        DarkModeUtils.setup(this)
        supportActionBar?.hide()
        binding = ActivityLoginBinding.inflate(layoutInflater)
        viewModel = LoginViewModel()
        setContentView(binding.root)
        viewModel.listener = this
        val isAuthenticated = AuthStateManager.getInstance(this).current.isAuthorized
        if (isAuthenticated) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
            return
        }

        binding.loginButton.setOnClickListener {
            viewModel.login(LoginActivity@this){
                finish()
            }
        }
    }

    override fun fetchBadRequest(errorType: ErrorMessage) {
        if (dialogAlert?.isShowing == true) {
            return
        }
        dialogAlert = MaterialAlertDialogBuilder(this)
            .setTitle(resources.getString(errorType.message().first))
            .setMessage(resources.getString(errorType.message().second))
            .setPositiveButton(resources.getString(R.string.i_understand)) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
            .apply {
                show()
            }
    }

}

