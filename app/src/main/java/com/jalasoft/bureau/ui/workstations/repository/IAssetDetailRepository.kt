package com.jalasoft.bureau.ui.workstations.repository

import com.jalasoft.bureau.data.model.favorites.UserFav
import retrofit2.Response

interface IAssetDetailRepository {
    suspend fun getUserInfo(
    ): Response<UserFav>?

    suspend fun deleteFavoriteAsset(
        idAsset: String
    ): Response<UserFav>?

    suspend fun postFavoriteAsset(
        idAsset: String
    ): Response<UserFav>?
}