package com.jalasoft.bureau.ui.favorites.repository

import com.jalasoft.bureau.data.interfaces.BookingService
import com.jalasoft.bureau.data.interfaces.FavoriteService
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.favorites.UserFav
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class FavoritesProductionService
@Inject
constructor(private val retrofit: Retrofit) {

    suspend fun getUserInfo(
    ): Response<UserFav>? {
        return retrofit.create(FavoriteService::class.java)?.getUserInfo()
    }

    suspend fun getFavoritesAssets(
        listFav: String?
    ): Response<List<Asset.AssetItem>>? {
        return retrofit.create(FavoriteService::class.java)?.getFavoritesAssets(
                "_id in array(${listFav})",
                "public eq bool(${true})",
                "asc(name)"
            )
    }

    suspend fun deleteFavoritesAsset(
        idAsset: String
    ): Response<UserFav>? {
        return retrofit.create((FavoriteService::class.java))?.deleteFavoriteAsset(
                idAsset
            )
    }

    suspend fun getBookingsByDate(
        startTime: String,
        endTime: String,
        assetId: String
    ): Response<ArrayList<BookingResponse.Booking>>? {
        return retrofit.create(BookingService::class.java)
            ?.getBookingsByDate(
                startTime,
                endTime,
                "assetId eq id(${assetId})",
                "status.state ne string(UserCancelled)",
                "status.state ne string(SystemCancelled)"
            )
    }
}