package com.jalasoft.bureau.ui.workstations.repository

import com.jalasoft.bureau.data.interfaces.BookingService
import com.jalasoft.bureau.data.model.booking.BookingResponse
import retrofit2.Response
import retrofit2.Retrofit
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import javax.inject.Inject

class EventService
@Inject
constructor(private val retrofit: Retrofit) {

    suspend fun getEventsProduction(
        workstationId: String,
        date: ZonedDateTime
    ): Response<ArrayList<BookingResponse.Booking>>? {
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ")
        return retrofit.create(BookingService::class.java)
            ?.getBookingsByDate(
                date.format(formatter),
                date.plusDays(1).format(formatter),
                "assetId eq id($workstationId)",
                "status.state ne string(UserCancelled)",
                "status.state ne string(SystemCancelled)"
            )
    }

}