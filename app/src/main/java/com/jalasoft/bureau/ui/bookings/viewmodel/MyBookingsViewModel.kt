package com.jalasoft.bureau.ui.bookings.viewmodel

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.notifications.BNotificationManager
import com.jalasoft.bureau.notifications.NotificationReceiver
import com.jalasoft.bureau.ui.bookings.repository.MainMyBookingsRepository
import com.jalasoft.bureau.utils.*
import com.jalasoft.bureau.utils.extensions.formatNotificationField
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import retrofit2.Response
import java.time.*
import java.time.format.DateTimeFormatter
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@HiltViewModel
class MyBookingsViewModel
@Inject
constructor(
    private val repository: MainMyBookingsRepository
) : ViewModel() {

    val upcomingBookings = MutableLiveData<List<BookingResponse.Booking>>()
    val todayBookings = MutableLiveData<List<BookingResponse.Booking>>()
    val listCount = MutableLiveData<Int>()
    val assetDetails = MutableLiveData<Asset.AssetItem>()
    val buildingDetails = MutableLiveData<Building.BuildingItem>()
    var bookingActivityListener: BadRequest? = null
    val myEvents = MutableLiveData<ArrayList<BookingResponse.Booking>>()
    val isLoading = MutableLiveData<Boolean>()
    var nonPermanentBookings: List<BookingResponse.Booking> = emptyList()
    var permanentBookings: List<BookingResponse.Booking> = emptyList()
    var nonPermanentBookingsWidget: List<BookingResponse.Booking>? = emptyList()
    var permanentBookingsWidget: List<BookingResponse.Booking>? = emptyList()
    var updateBookingsList = MutableLiveData<Boolean>(false)

    fun getAllBookings(
        ownerEmail: String,
        userBookingId: String?,
        fromWidget: Boolean,
        goToBooking: (booking: BookingResponse.Booking) -> Unit,
        buildings: List<Building.BuildingItem>?
    ) {
        nonPermanentBookings = emptyList()
        permanentBookings = emptyList()
        isLoading.postValue(true)
        viewModelScope.launch {
            try {
                val date = LocalDate.now()
                val time = LocalTime.of(0, 0, 0)
                val formatter = DateTimeFormatter.ofPattern(API_DATE_FORMAT)
                val dateTime = ZonedDateTime.of(date, time, ZoneId.systemDefault()).format(formatter)
                val endTime = ZonedDateTime.of(date.plusDays(1), time, ZoneId.systemDefault()).format(formatter)
                val apiResponse1 = async { repository.getBookingsProduction(dateTime, endTime, ownerEmail) }
                val apiResponse2 = async { repository.getUpcommingBookings(endTime, ownerEmail) }
                val apiResponse3 = async { repository.getPermanentBookings(ownerEmail) }
                val firstResponse = apiResponse1.await()
                val  secondResponse = apiResponse2.await()
                val thirdResponse = apiResponse3.await()
                nonPermanentBookings = (firstResponse?.body()?.sortedBy { it.startTime } ?: emptyList()).distinctBy { it.recurrenceId }
                val upcommingBookings =  secondResponse?.body()?.sortedBy { it.startTime } ?: emptyList()
                val permanentAssets = thirdResponse?.body()?.sortedBy { it.policies?.schedule?.get(0)?.startTime } ?: emptyList()
                async { convertAssetPermanentToBookings(permanentAssets) }.await()
                val bookingsList = nonPermanentBookings + permanentBookings + upcommingBookings
                val filteredBookings = mutableListOf<BookingResponse.Booking>()
                buildings?.forEach { buildingItem ->
                    filteredBookings.addAll(bookingsList.filter { it.buildingId == buildingItem.id })
                }
                manageBookingData(filteredBookings, userBookingId, fromWidget, goToBooking)
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    bookingActivityListener?.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    private fun manageBookingData(
        bookings: List<BookingResponse.Booking>?, userBookingId: String?, fromWidget: Boolean,
        goToBooking: (booking: BookingResponse.Booking) -> Unit
    ) {
        val upcomingList = mutableListOf<BookingResponse.Booking>()
        val todayList = mutableListOf<BookingResponse.Booking>()
        val nowDate = LocalDateTime.now().toLocalDate()
        bookings?.sortedBy { it.startTime }?.forEach {
            val responseDate = LocalDateTime.ofInstant(
                Instant.parse(it.startTime),
                ZoneId.systemDefault()
            ).toLocalDate()
            if (it.id == userBookingId) {
                goToBooking(it)
            }
            if (nowDate == responseDate || it.isPermanent == true) {
                todayList.add(it)
            } else {
                upcomingList.add(it)
            }
        }
        if (fromWidget) {
            nonPermanentBookingsWidget = nonPermanentBookings
            permanentBookingsWidget = permanentBookings
        } else {
            todayBookings.postValue(todayList)
            upcomingBookings.postValue(upcomingList)
            listCount.postValue(todayList.size + upcomingList.size)
            isLoading.postValue(false)
        }
    }

    fun getAssetDetails(assetId: String) {
        viewModelScope.launch {
            try {
                val service: Response<Asset.AssetItem>? =
                    repository.getAssetDetailsProduction(assetId)
                if (service == null || !service.isSuccessful) {
                    withContext(Dispatchers.Main) {
                        bookingActivityListener?.fetchBadRequest(ErrorMessage.REQUEST_FAILED)
                    }
                    return@launch
                } else {
                    val response = service.body()
                    response?.let {
                        withContext(Dispatchers.Main) {
                            assetDetails.postValue(it)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    bookingActivityListener?.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getBuildingDetails(buildingId: String) {
        viewModelScope.launch {
            try {
                val response = repository.getBuildingDetails(buildingId)
                if (response == null || !response.isSuccessful) {
                    bookingActivityListener?.fetchBadRequest(ErrorMessage.REQUEST_FAILED)
                } else {
                    buildingDetails.postValue(response.body())
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    bookingActivityListener?.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getBuildingByBuildingId(
        buildingId: String,
        completionHandler: (buildingTimezone: String) -> Unit
    ) {
        viewModelScope.launch {
            try {
                val response = repository.getBuildingDetails(buildingId)
                if (response == null || !response.isSuccessful) {
                    bookingActivityListener?.fetchBadRequest(ErrorMessage.REQUEST_FAILED)
                } else {
                    withContext(Dispatchers.Main) {
                        completionHandler(response.body()?.timezone ?: "")
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    bookingActivityListener?.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun deleteBooking(context: Context, idBooking: String, onDelete: () -> (Unit), ) {
        viewModelScope.launch {
            try {
                val service: Response<BookingResponse.Booking>? =
                    repository.deleteBookingProduction(idBooking)
                if (service == null || !service.isSuccessful) {
                    withContext(Dispatchers.Main) {
                        bookingActivityListener?.fetchBadRequest(ErrorMessage.REQUEST_FAILED)
                    }
                    return@launch
                }
                val response = service.body()
                response?.let { booking ->
                    withContext(Dispatchers.Main) {
                        updateBookingsList.postValue(true)
                        onDelete()
                        deleteNotificationByBookingId(booking.id, context)
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    bookingActivityListener?.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getAllEvents(workstationId: String, dateSelected: ZonedDateTime) {
        viewModelScope.launch {
            try {
                val service: Response<ArrayList<BookingResponse.Booking>>? =
                    repository.getEventsProduction(workstationId, dateSelected)
                service?.let { safeService ->
                    if (safeService.isSuccessful) {
                        val response = safeService.body()
                        response?.let {
                            withContext(Dispatchers.Main) {
                                myEvents.postValue(it)
                            }
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            bookingActivityListener?.fetchBadRequest(ErrorMessage.REQUEST_FAILED)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    bookingActivityListener?.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getBookings(
        ownerEmail: String,
        onCompletion: (booking: List<BookingResponse.Booking>) -> Unit,

    ) {
        viewModelScope.launch {
            try {
                val date = LocalDate.now()
                val time = LocalTime.of(0, 0, 0)
                val formatter = DateTimeFormatter.ofPattern(API_DATE_FORMAT)
                val dateTime = ZonedDateTime.of(date, time, ZoneId.systemDefault()).format(formatter)
                val service: Response<List<BookingResponse.Booking>>? =
                    repository.getBookingsProduction(dateTime, dateTime, ownerEmail)
                service?.let { safeService ->
                    if (safeService.isSuccessful) {
                        val response = safeService.body()
                        response?.let { bookingList ->
                            withContext(Dispatchers.Main) {
                                if (bookingList.isEmpty()) onCompletion(emptyList())
                                else onCompletion(bookingList)
                            }
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    bookingActivityListener?.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun bookingListIsEmpty(): Boolean {
        return upcomingBookings.value.isNullOrEmpty() && todayBookings.value.isNullOrEmpty()
    }

    fun getBuildingZoneId(): ZoneId {
        buildingDetails.value?.timezone?.also {
            return ZoneId.of(it).normalized()
        }
        return ZoneId.systemDefault()
    }

    fun formatRangeHour(
        responseDate: LocalDateTime,
        limitBeforeDeadLine: String,
        limitAfterDeadLine: String,
        context: Context
    ): String {
        val formatter = DateTimeFormatter.ofPattern(LOCAL_TIME_FORMAT)
        val start = responseDate.minusMinutes(limitBeforeDeadLine.toLong()).format(formatter)
        val end = responseDate.plusMinutes(limitAfterDeadLine.toLong()).format(formatter)
        return context.getString(R.string.booking_validation_range, start, end)
    }

    private suspend fun convertAssetPermanentToBookings(permanentAssets: List<Asset.AssetItem>) {
        val tempPermanentBookings = ArrayList<BookingResponse.Booking>()
        permanentAssets.forEach { asset ->
            val areaRequest = repository.getAreaDetailByAreaId(asset.areaId!!)
            val areaName = areaRequest?.body()?.name ?: ""
            tempPermanentBookings.add(
                BookingResponse.Booking(
                    asset.areaId,
                    areaName, asset.code!!, asset.id!!, asset.properties?.type,
                    asset.buildingId!!, null, asset.createdAt!!, asset.createdAt,
                    asset.id, true, false, asset.properties?.owner,
                    null, asset.createdAt, asset.updatedAt!!, 1, null, null, null
                )
            )
        }
        permanentBookings = tempPermanentBookings
    }

    // region Init Notification
    fun createNotifications(context: Context, booking: BookingResponse.Booking) {
        val prefs = Prefs(context)
        val limitBeforeDeadLine = prefs.getData(SHARED_BOOKING_LIMIT_BEFORE_DEADLINE) ?: NOTIFICATION_DEFAULT_RANGE
        val limitAfterDeadLine = prefs.getData(SHARED_BOOKING_LIMIT_AFTER_DEADLINE) ?: NOTIFICATION_DEFAULT_RANGE
        getBuildingByBuildingId(booking.buildingId) { buildingTimezone ->
            val bookingDate = LocalDateTime.ofInstant(
                Instant.parse(booking.startTime),
                ZoneId.systemDefault()
            )
            val nowDate = LocalDateTime.now()
            val beforeDeadLine = bookingDate.minusMinutes(limitBeforeDeadLine.toLong())
            val afterDeadLine = bookingDate.plusMinutes(limitAfterDeadLine.toLong() - NOTIFICATION_RANGE)
            if (nowDate.isBefore(beforeDeadLine)) {
                createLocalNotification(context, beforeDeadLine, booking, limitBeforeDeadLine, buildingTimezone)
            }
            if (booking.confirmedAt == null && nowDate.isBefore(afterDeadLine)) { // this means that the booking is not confirmed yet
                createCancellationWarningNotification(context, afterDeadLine, booking)
            }
        }
    }


    // region Clear All Notifications
    fun clearAllNotifications(context: Context, onCompleteClearNotifications: () -> Unit) {
        val prefs = Prefs(context)
        val userEmail = prefs.getData(SHARED_USER_EMAIL) ?: ""
        getBookings(userEmail) { bookings ->
            bookings.forEach { booking ->
                deleteNotificationByBookingId(booking.id, context)
            }
            onCompleteClearNotifications()
        }
    }
    // endregion Clear All Notifications

    // region Delete Notification By Booking Id
    fun deleteNotificationByBookingId(bookingId: String, context: Context) {
        val alarmManager = context.getSystemService(AppCompatActivity.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, NotificationReceiver::class.java)
        intent.action = bookingId
        val pendingIntent = PendingIntent.getBroadcast(context,
            BNotificationManager.YOUR_BOOKING_IS_COMING, intent, PendingIntent.FLAG_IMMUTABLE)
        alarmManager.cancel(pendingIntent)
        intent.action = "${bookingId}cancel"
        val pendingIntentCancel = PendingIntent.getBroadcast(context,
            BNotificationManager.BOOKING_CANCELLATION_WARNING, intent, PendingIntent.FLAG_IMMUTABLE)
        alarmManager.cancel(pendingIntentCancel)
    }
    // endregion Delete Notification By Booking Id


    // region Init Notification
    fun createNotifications(booking: BookingResponse.Booking, context: Context) {
        val prefs = Prefs(context)
        val limitBeforeDeadLine = prefs.getData(SHARED_BOOKING_LIMIT_BEFORE_DEADLINE) ?: NOTIFICATION_DEFAULT_RANGE
        val limitAfterDeadLine = prefs.getData(SHARED_BOOKING_LIMIT_AFTER_DEADLINE) ?: NOTIFICATION_DEFAULT_RANGE
        getBuildingByBuildingId(booking.buildingId) { buildingTimezone ->
            val bookingDate = LocalDateTime.ofInstant(
                Instant.parse(booking.startTime),
                ZoneId.systemDefault()
            )
            val nowDate = LocalDateTime.now()
            val beforeDeadLine = bookingDate.minusMinutes(limitBeforeDeadLine.toLong())
            val afterDeadLine = bookingDate.plusMinutes(limitAfterDeadLine.toLong() - NOTIFICATION_RANGE)
            if (nowDate.isBefore(beforeDeadLine)) {
                createLocalNotification(context, beforeDeadLine, booking, limitBeforeDeadLine, buildingTimezone)
            }
            if (booking.confirmedAt == null && nowDate.isBefore(afterDeadLine)) { // this means that the booking is not confirmed yet
                createCancellationWarningNotification(context, afterDeadLine, booking)
            }
        }
    }
    // endregion Init Notification

    // region Create Local Notification For Validation
    private fun createLocalNotification(
        context: Context,
        bookingStartTime: LocalDateTime,
        booking: BookingResponse.Booking,
        limitBeforeDeadLine: String,
        buildingTimezone: String
    ) {
        val dateBooking = booking.startTime.formatNotificationField(LOCAL_DATE_FORMAT, buildingTimezone)
        val startHour = booking.startTime.formatNotificationField(LOCAL_TIME_FORMAT, buildingTimezone)
        val finishHour = booking.endTime.formatNotificationField(LOCAL_TIME_FORMAT, buildingTimezone)
        val title = context.getString(R.string.your_booking_is_coming)
        val dateTimeStr = "$dateBooking $startHour to $finishHour"
        val dateStr = context.getString(R.string.booking_notification_reservation, booking.assetCode, limitBeforeDeadLine, dateTimeStr, buildingTimezone)
        val alarmManager = context.getSystemService(AppCompatActivity.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, NotificationReceiver::class.java)
        intent.action = booking.id
        intent.putExtra(BOOKING_TITLE,title)
        intent.putExtra(BOOKING_DATE, dateStr )
        intent.putExtra(BOOKING_ID, booking.id)
        val pendingIntent = PendingIntent.getBroadcast(context,
            BNotificationManager.YOUR_BOOKING_IS_COMING, intent, PendingIntent.FLAG_IMMUTABLE)
        val calendar = Calendar.getInstance()
        calendar.set(bookingStartTime.year, bookingStartTime.monthValue - 1, bookingStartTime.dayOfMonth, bookingStartTime.hour, bookingStartTime.minute , 0)
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)
    }
    // endregion Create Local Notification For Validation

    // region Create Cancellation Warning Notification
    fun createCancellationWarningNotification(
        context: Context,
        bookingDateAfter: LocalDateTime,
        booking: BookingResponse.Booking,
    ) {
        val title = context.getString(R.string.booking_cancellation_warning)
        val dateStr = context.getString(R.string.booking_notification_cancellation_warning, booking.assetCode)
        val alarmManager = context.getSystemService(AppCompatActivity.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, NotificationReceiver::class.java)
        intent.action = "${booking.id}cancel"
        intent.putExtra(BOOKING_TITLE,title)
        intent.putExtra(BOOKING_DATE, dateStr )
        intent.putExtra(BOOKING_ID, booking.id)
        val pendingIntent = PendingIntent.getBroadcast(context,
            BNotificationManager.BOOKING_CANCELLATION_WARNING, intent, PendingIntent.FLAG_IMMUTABLE)
        val calendar = Calendar.getInstance()
        calendar.set(bookingDateAfter.year, bookingDateAfter.monthValue - 1, bookingDateAfter.dayOfMonth, bookingDateAfter.hour, bookingDateAfter.minute, 0)
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)
    }
    // endregion Create Cancellation Warning Notification
}