package com.jalasoft.bureau.ui.favorites.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.adapter.FavoritesAdapter
import com.jalasoft.bureau.data.interfaces.BottomSheetDialogListener
import com.jalasoft.bureau.data.interfaces.FavoritesListener
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.databinding.FragmentFavoritesBinding
import com.jalasoft.bureau.ui.favorites.viewmodel.FavoritesViewModel
import com.jalasoft.bureau.ui.main.view.MainActivity
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.workstations.view.AssetDetailFragment
import com.jalasoft.bureau.ui.workstations.viewmodel.AssetDetailFragmentViewModel
import com.jalasoft.bureau.utils.BOTTOM_SHEET_FAVORITE_ASSET_DETAIL
import com.jalasoft.bureau.utils.DateUtils
import dagger.hilt.android.AndroidEntryPoint
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId

@AndroidEntryPoint
class FavoritesFragment : Fragment(), FavoritesListener, BottomSheetDialogListener {
    private val favoritesViewModel: FavoritesViewModel by activityViewModels()
    private val assetDetailFragmentViewModel: AssetDetailFragmentViewModel by activityViewModels()
    private val mainViewModel: MainActivityViewModel by activityViewModels()
    private lateinit var recycler: RecyclerView
    private lateinit var favoritesListBinding: FragmentFavoritesBinding
    lateinit var adapter: FavoritesAdapter
    private val assetDetailViewModel: AssetDetailFragmentViewModel by activityViewModels()
    lateinit var bottomSheetAssetDetail: AssetDetailFragment

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if (!resources.getBoolean(R.bool.isTablet)) {
            setupToolbar()
        } else {
            observeAssetDetailsFavoriteStatus()
        }
        val view = inflater.inflate(R.layout.fragment_favorites, container, false)
        favoritesListBinding = FragmentFavoritesBinding.bind(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Firstly we call all the building from the api
        favoritesViewModel.favoritesActivityListener = (activity as MainActivity)
        sendRequests()
        initRecycler()
        initSubscriptions()
        initFavoriteSubscriptions()
        refreshLayoutFavorites()
    }

    override fun onPause() {
        super.onPause()
        // isInitialized is a property of the lateinit keyword
        if(this::bottomSheetAssetDetail.isInitialized) {
            bottomSheetAssetDetail.dismiss()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        favoritesViewModel.assetList.postValue(null)
    }

    private fun refreshLayoutFavorites() {
        val refreshLayout = favoritesListBinding.swipeRefreshLayout
        refreshLayout.setOnRefreshListener {
            sendRequests()
            refreshLayout.isRefreshing = false
        }
    }

    private fun sendRequests() {
        context?.let {favoritesViewModel.getUserInfo()}
    }

    private fun setupToolbar() {
        (activity as MainActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
        (activity as MainActivity).binding.appBar.toolbar.visibility = View.VISIBLE
        (activity as MainActivity).binding.appBar.title.visibility = View.GONE
        (activity as MainActivity).binding.appBar.logo.visibility = View.VISIBLE
    }

    private fun assetAvailability(assetList: List<Asset.AssetItem>) {
        val startTime = DateUtils.getFormattedDate(mainViewModel.myDate.value, mainViewModel.userStartTime.value, ZoneId.systemDefault())
        val endTime = DateUtils.getFormattedDate(mainViewModel.myDate.value, mainViewModel.userEndTime.value, ZoneId.systemDefault())
        if (!assetList.isNullOrEmpty()) {
            context?.let {
                val mainStartTime = mainViewModel.userStartTime.value ?: LocalTime.now()
                val mainEndTime = mainViewModel.userEndTime.value ?: LocalTime.now()
                val dateSelected = mainViewModel.myDate.value ?: LocalDate.now()
                val buildings = mainViewModel.myBuildings.value
                favoritesViewModel.checkOverlappingEvent(startTime, endTime, assetList, mainStartTime, mainEndTime, dateSelected, buildings)
            }
        }
    }

    private fun initSubscriptions() {
        favoritesViewModel.listCount.observe(viewLifecycleOwner) { listCount ->
            favoritesListBinding.tvCount.text = String.format("(%s)", listCount.toString())
            if (listCount == 0) {
                favoritesListBinding.emptyMsg.visibility = View.VISIBLE
                favoritesListBinding.favoritesRV.visibility = View.GONE
            }
        }

        favoritesViewModel.isLoading.observe(viewLifecycleOwner) { isLoading ->
            if (isLoading) {
                favoritesListBinding.progressCircularContainer.visibility = View.VISIBLE
                return@observe
            }
            if (favoritesViewModel.assetList.value?.isEmpty() == true) {
                favoritesListBinding.emptyMsg.visibility = View.VISIBLE
            }
            favoritesListBinding.progressCircularContainer.visibility = View.GONE
        }
        assetDetailFragmentViewModel.isEmptyList.observe(viewLifecycleOwner) { isEmpty ->
            if (isEmpty == true) {
                favoritesListBinding.favoritesRV.visibility = View.GONE
                favoritesViewModel.listCount.postValue(0)
            }
        }
    }

    private fun initFavoriteSubscriptions() {
        favoritesViewModel.assetListAvailable.observe(viewLifecycleOwner) {
            adapter = FavoritesAdapter(it, this, resources)
            recycler.adapter = adapter
        }

        favoritesViewModel.liveListFav.observe(viewLifecycleOwner) { assetsFav ->
            context?.let {
                if (assetsFav.isNotEmpty()) {
                    val buildings = mainViewModel.myBuildings.value
                    favoritesViewModel.getFavoritesAssets(assetsFav, buildings)
                }
            }
        }

        favoritesViewModel.assetList.observe(viewLifecycleOwner) { assetList ->
            if (assetList == favoritesViewModel.assetListAvailable.value || assetList.isNullOrEmpty()) {
                return@observe
            }
            assetAvailability(assetList)
            if (assetList.isEmpty() && assetDetailFragmentViewModel.isEmptyList.value == true) {
                favoritesListBinding.favoritesRV.visibility = View.GONE
            } else {
                favoritesListBinding.favoritesRV.visibility = View.VISIBLE
                favoritesListBinding.emptyMsg.visibility = View.GONE
            }
        }
    }

    private fun initRecycler() {
        recycler = favoritesListBinding.favoritesRV
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.setHasFixedSize(true)
        recycler.isNestedScrollingEnabled = false
    }

    override fun didFavoriteSelected(asset: Asset.AssetItem) {
        val assetId = asset.id ?: ""
        val buildingId = asset.buildingId ?: ""
        val areaId = asset.areaId ?: ""

        bottomSheetAssetDetail = AssetDetailFragment(
            assetId,
            buildingId,
            areaId
        )
        bottomSheetAssetDetail.bottomSheetDialogListener =
            this@FavoritesFragment
        if (!bottomSheetAssetDetail.isAdded) {
            bottomSheetAssetDetail.isBottomSheetExpanded = true
            bottomSheetAssetDetail.isFromFavorites = true
            bottomSheetAssetDetail.show(
                parentFragmentManager,
                BOTTOM_SHEET_FAVORITE_ASSET_DETAIL
            )
        }
    }

    override fun didHeartPressed(assetId: String, onDelete: () -> Unit) {
        favoritesViewModel.deleteFavoriteAsset(assetId) {
            onDelete()
        }
    }

    override fun didSelectBottomSheetDialog() {
        // Nothing to do after asset details dialog is closed
    }

    private fun observeAssetDetailsFavoriteStatus() {
        assetDetailViewModel.updateFavoritesList.observe(viewLifecycleOwner) { updateList ->
            if (updateList) {
                sendRequests()
            }
        }
    }
}