package com.jalasoft.bureau.ui.filter.repository

import com.jalasoft.bureau.data.interfaces.AssetService
import com.jalasoft.bureau.data.model.asset.AssetLabel
import com.jalasoft.bureau.data.model.asset.AssetType
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

open class FilterRepository
@Inject
constructor(private val retrofit: Retrofit) {

    open suspend fun getFiltersTypes(): Response<List<AssetType.AssetTypeItem>>? {
        return retrofit.create(AssetService::class.java)
            ?.getAssetsType("asc(name)")
    }

    open suspend fun getFiltersLabels(): Response<List<AssetLabel>>? {
        return retrofit.create(AssetService::class.java)
            ?.getAssetsLabels()
    }
}