package com.jalasoft.bureau.ui.filter.view

import android.app.Dialog
import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.adapter.FeatureTypeAdapter
import com.jalasoft.bureau.data.adapter.SpaceTypeAdapter
import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.data.interfaces.FilterSpaceListener
import com.jalasoft.bureau.data.model.asset.AssetLabel
import com.jalasoft.bureau.databinding.FragmentFilterBinding
import com.jalasoft.bureau.ui.filter.viewmodel.FilterViewModel
import com.jalasoft.bureau.utils.BOTTOM_SHEET_FILTER_PADDING
import com.jalasoft.bureau.utils.ErrorMessage
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FilterFragment : BottomSheetDialogFragment(), FilterSpaceListener, BadRequest {

    lateinit var filterBinding: FragmentFilterBinding
    lateinit var spaceRecycler: RecyclerView
    lateinit var featureRecycler: RecyclerView
    lateinit var adapterSpaceType: SpaceTypeAdapter
    lateinit var adapterFeatureType: FeatureTypeAdapter
    val filterViewModel: FilterViewModel by activityViewModels()
    var spaceTypeSelected: Int = 0
    var spaceType: String = "all"
    val tempArrLabels = ArrayList<String>()
    var switchAvailableSpaces: Boolean = false
    var filteredLabels: ArrayList<AssetLabel> = ArrayList<AssetLabel>()
    var dialogAlert: AlertDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_filter, container, false)
        filterBinding = FragmentFilterBinding.bind(view)
        return filterBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // First, let's call all the filters types
        filterViewModel.listener = this
        context?.let { context ->
            filterViewModel.getFiltersTypes()
            filterViewModel.getFiltersLabels()
        }
        initValues()
        initButtons()
        initSpaceTypeRecyclerView()
        initFeatureTypeRecyclerView()
        initObservableObjects()
        if(!resources.getBoolean(R.bool.isTablet)) initBottomSheet() else initNormalDialog()
    }

    override fun getTheme(): Int {
        if (!resources.getBoolean(R.bool.isTablet)) {
            return R.style.AppBottomSheetDialogTheme
        }
        return super.getTheme()
    }

    private fun initNormalDialog() {
        (dialog as BottomSheetDialog).behavior.isDraggable = false
        (dialog as BottomSheetDialog).behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        if(!resources.getBoolean(R.bool.isTablet)) return super.onCreateDialog(savedInstanceState)
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener {
                val bottomSheet = findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
                bottomSheet.setBackgroundResource(android.R.color.transparent)
            }
        }
    }

    fun initValues() {
        spaceTypeSelected = filterViewModel.spaceTypeSelected.value!!
        spaceType = filterViewModel.spaceType.value.toString()
        tempArrLabels.clear()
        filterViewModel.arrLabels.value?.let { arrLabels ->
            tempArrLabels.addAll(arrLabels)
        }
        switchAvailableSpaces = filterViewModel.switchAvailableSpaces.value == true
    }

    fun initBottomSheet() {
        (dialog as BottomSheetDialog).behavior.isDraggable = false
        (dialog as BottomSheetDialog).behavior.state = BottomSheetBehavior.STATE_EXPANDED
        dialog?.let {
            val bottomSheet = it.findViewById<View>(R.id.design_bottom_sheet)
            bottomSheet.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
        }
        (dialog as BottomSheetDialog).behavior.maxHeight = Resources.getSystem().getDisplayMetrics().heightPixels - BOTTOM_SHEET_FILTER_PADDING
    }

    fun initButtons() {
        filterBinding.btnCancel.setOnClickListener {
            initValues()
            this.dismiss()
        }

        filterBinding.btnDone.setOnClickListener {
            filterViewModel.saveFilterState(
                spaceTypeSelected,
                spaceType,
                tempArrLabels,
                switchAvailableSpaces
            )
            this.dismiss()
        }

        filterBinding.btnClearAll.setOnClickListener {
            filterViewModel.clearAll()
            this.spaceTypeSelected = 0
            this.spaceType = "all"
            this.tempArrLabels.clear()
            this.switchAvailableSpaces = false
        }

        filterBinding.switchAvailableSpaces.setOnClickListener {
            switchAvailableSpaces = !switchAvailableSpaces
        }

    }

    fun initSpaceTypeRecyclerView() {
        spaceRecycler = filterBinding.rvSpaceType
        spaceRecycler.setHasFixedSize(true)
        spaceRecycler.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    fun initFeatureTypeRecyclerView() {
        featureRecycler = filterBinding.rvFeaturesType
        featureRecycler.setHasFixedSize(true)
        featureRecycler.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    override fun didSelectSpaceType(selected: Int) {
        val lastSelected = adapterSpaceType.selectedIndex
        spaceTypeSelected = selected
        spaceType = filterViewModel.filterTypes.value?.get(selected)?.id.toString()
        adapterSpaceType.selectedIndex = selected
        adapterSpaceType.notifyItemChanged(lastSelected)

        featureRecycler.adapter =
            filterViewModel.filterTypes.value?.let { assetsTypes ->
                filterViewModel.filterLabels.value?.let { arrLabels ->
                    filteredLabels =
                        filterViewModel.getFiltersByAssetType(assetsTypes, arrLabels, selected)
                    if (selected != 0) {
                        val assetTempArrLabels =
                            assetsTypes[selected].labels intersect tempArrLabels.toSet()
                        tempArrLabels.clear()
                        tempArrLabels.addAll(assetTempArrLabels)
                    }
                    FeatureTypeAdapter(filteredLabels, this, tempArrLabels)
                }
            }
    }

    override fun didSelectLabels(selected: Int) {
        filterViewModel.filterTypes.value?.let { assetsTypes ->
            filterViewModel.filterLabels.value?.let { arrLabels ->
                filteredLabels =
                    filterViewModel.getFiltersByAssetType(assetsTypes, arrLabels, spaceTypeSelected)
                val selectedLabelId = filteredLabels[selected].id.toString()
                if (tempArrLabels.contains(selectedLabelId)) {
                    tempArrLabels.remove(selectedLabelId)
                } else {
                    tempArrLabels.add(selectedLabelId)
                }
                adapterFeatureType.arrLabels = tempArrLabels
            }
        }
    }

    fun initObservableObjects() {
        filterViewModel.filterTypes.observe(viewLifecycleOwner) { assetsTypes ->
            adapterSpaceType =
                filterViewModel.spaceTypeSelected.value.let { spaceTypeSelected ->
                    SpaceTypeAdapter(
                        assetsTypes, this,
                        spaceTypeSelected ?: 0
                    )
                }
            spaceRecycler.adapter = adapterSpaceType
        }

        filterViewModel.filterLabels.observe(viewLifecycleOwner) { assetsLabels ->
            adapterFeatureType =
                filterViewModel.arrLabels.value.let { arrLabels ->
                    if (filterViewModel.filterTypes.value.isNullOrEmpty()) {
                        FeatureTypeAdapter(assetsLabels, this, arrLabels ?: emptyList())
                    } else {
                        filterViewModel.filterTypes.value.let { assetsTypes ->
                            filteredLabels = filterViewModel.getFiltersByAssetType(
                                assetsTypes ?: emptyList(),
                                assetsLabels,
                                spaceTypeSelected
                            )
                            FeatureTypeAdapter(filteredLabels, this, tempArrLabels)
                        }
                    }
                }
            featureRecycler.adapter = adapterFeatureType
        }

        filterViewModel.switchAvailableSpaces.observe(viewLifecycleOwner) {
            filterBinding.switchAvailableSpaces.isChecked = it
            spaceRecycler.adapter =
                filterViewModel.spaceTypeSelected.value?.let { spaceTypeSelected ->
                    filterViewModel.filterTypes.value?.let { filterType ->
                        SpaceTypeAdapter(
                            filterType, this,
                            spaceTypeSelected
                        )
                    }
                }
            featureRecycler.adapter =
                filterViewModel.arrLabels.value?.let { arrLabels ->
                    filterViewModel.filterLabels.value?.let { assetLabels ->
                        FeatureTypeAdapter(assetLabels, this, arrLabels)
                    }
                }
        }
    }

    // Show a dialog when a bad request is retreave from the server
    // error when status code is >= 300
    override fun fetchBadRequest(errorType: ErrorMessage) {
        if (dialogAlert?.isShowing == true) {return}
        context?.let {
            dialogAlert = MaterialAlertDialogBuilder(it)
                .setTitle(resources.getString(errorType.message().first))
                .setMessage(resources.getString(errorType.message().second))
                .setPositiveButton(resources.getString(R.string.i_understand)) { dialog, _ ->
                    dialog.dismiss()
                }
                .create()
                .apply {
                    show()
                }
        }
    }

}