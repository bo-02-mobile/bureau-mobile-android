package com.jalasoft.bureau.ui.profile.viewmodel

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jalasoft.bureau.BuildConfig
import com.jalasoft.bureau.data.model.scanner.BookingConfirmResponse
import com.jalasoft.bureau.ui.login.view.LoginActivity
import com.jalasoft.bureau.ui.main.view.MainActivity
import com.jalasoft.bureau.utils.login.AuthStateManager
import dagger.hilt.android.lifecycle.HiltViewModel
import net.openid.appauth.AuthState
import net.openid.appauth.AuthorizationService
import net.openid.appauth.AuthorizationServiceConfiguration
import net.openid.appauth.EndSessionRequest
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel
@Inject
constructor() : ViewModel() {

    val bookingConfirmResponse = MutableLiveData<BookingConfirmResponse>()

    fun logOut(context: Context) {
        AuthStateManager.getInstance(context).replace(AuthState())
    }
     fun endSession(context: Context, onLogoutCompletion: () -> Unit) {
        val tokenId = AuthStateManager.getInstance(context).current.idToken ?: ""
        AuthorizationServiceConfiguration.fetchFromUrl(Uri.parse(BuildConfig.LOGIN_CONFIG_URI)) { config, _ ->
            config?.let {
                val endSessionRequest = EndSessionRequest.Builder(config,tokenId, Uri.parse(
                    BuildConfig.LOGIN_REDIRECT_URI)).build()
                val authService = AuthorizationService(context)
                authService.performEndSessionRequest(
                    endSessionRequest,
                    PendingIntent.getActivity(
                        context,
                        0,
                        Intent(context, LoginActivity::class.java),
                        0 or PendingIntent.FLAG_IMMUTABLE
                    ),
                    PendingIntent.getActivity(
                        context,
                        0,
                        Intent(context, MainActivity::class.java),
                        0 or PendingIntent.FLAG_IMMUTABLE
                    )
                )
                onLogoutCompletion()
            }
        }
    }

}