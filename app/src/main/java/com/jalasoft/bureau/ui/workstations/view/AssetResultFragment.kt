package com.jalasoft.bureau.ui.workstations.view

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.BaseRequestOptions
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.data.interfaces.BottomSheetDialogListener
import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.workstation.Marker
import com.jalasoft.bureau.databinding.FragmentAssetResultBinding
import com.jalasoft.bureau.ui.filter.view.FilterFragment
import com.jalasoft.bureau.ui.filter.viewmodel.FilterViewModel
import com.jalasoft.bureau.ui.main.view.MainActivity
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.workstations.viewmodel.AssetResultFragmentViewModel
import com.jalasoft.bureau.utils.*
import com.jalasoft.bureau.utils.extensions.formatCalendar
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.math.abs


class AssetResultFragment : Fragment(), BadRequest, BottomSheetDialogListener {

    private lateinit var binding: FragmentAssetResultBinding
    private lateinit var areaId: String
    private lateinit var areaCode: String
    private lateinit var buildingId: String

    // image width and height references the dimensions of the image loaded
    private var imageWidth: Int = 0
    private var imageHeight: Int = 0

    private var canLoadOverlappedBookings = false
    private val assetViewModel: AssetResultFragmentViewModel by activityViewModels()
    private val viewModel: MainActivityViewModel by activityViewModels()
    private val filterViewModel: FilterViewModel by activityViewModels()
    var isBottomSheetOpen: Boolean = false
    lateinit var btnFilter: AppCompatTextView
    var dialogAlert: AlertDialog? = null
    lateinit var bottomSheetAssetDetail: AssetDetailFragment

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_asset_result, container, false)
        binding = FragmentAssetResultBinding.bind(view)
        btnFilter = view.findViewById(R.id.btn_filter)
        btnFilter.text = getString(R.string.filter)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        assetViewModel.assetListener = this
        observeLoading()
        if (!resources.getBoolean(R.bool.isTablet)) {
            checkArguments()
            setupToolbar()
            assetViewModel.getAreaDetail(areaId)
        } else {
            areaCode = ""
            areaId = ""
            buildingId = ""
            checkAssetParameters()
            viewModel.areaCode.observe(viewLifecycleOwner) {
                if (it == areaCode) {
                    return@observe
                }
                buildingId = viewModel.prefs.getData(SHARED_BUILDING_ID).toString()
                areaCode = viewModel.areaCode.value.toString()
                binding.areaCode?.text = areaCode
                areaId = viewModel.areaId.value.toString()
                checkAssetParameters()
                if (areaId.isNotEmpty()) {
                    context?.let {assetViewModel.getAreaDetail(areaId) }
                }
            }
        }
        initSubscriptions()
        initButtonListeners()
        binding.filterComponent.btnFilter.setOnClickListener {
            val filterBottomSheet = FilterFragment()
            if (!filterBottomSheet.isAdded) {
                filterBottomSheet.show(parentFragmentManager, FILTER_BOTTOM_SHEET)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if(isBottomSheetOpen){
            bottomSheetAssetDetail.dismiss()
        }
    }

    private fun checkArguments() {
        arguments?.let {
            areaId = it.getString(AREA_ID).toString()
            areaCode = it.getString(AREA_CODE).toString()
            buildingId = it.getString(BUILDING_ID).toString()
        }
    }

    private fun setupToolbar() {
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainActivity).binding.appBar.title.text = areaCode
        (activity as MainActivity).binding.appBar.title.visibility = View.VISIBLE
        (activity as MainActivity).binding.appBar.logo.visibility = View.GONE
        (activity as MainActivity).binding.bottomNavView.menu.getItem(BOOKING_TAB).isChecked = true
    }

    private fun getOverlappedBooking() {
        val startTime = DateUtils.getFormattedDate(
            viewModel.myDate.value,
            viewModel.userStartTime.value,
            viewModel.getBuildingZoneId()
        )
        val endTime = DateUtils.getFormattedDate(
            viewModel.myDate.value,
            viewModel.userEndTime.value,
            viewModel.getBuildingZoneId()
        )
        assetViewModel.getOverlappedBookings(startTime, endTime)
    }

    private fun initSubscriptions() {
        assetViewModel.area.observe(viewLifecycleOwner) {
            if (it == null) {
                return@observe
            }
            getOverlappedBooking()
        }
        assetViewModel.myAssets.observe(viewLifecycleOwner) { myAssets ->
            if (myAssets == null) {
                return@observe
            }
            val startTime = viewModel.userStartTime.value ?: LocalTime.now()
            val endTime = viewModel.userEndTime.value ?: LocalTime.now()
            val dateSelected = viewModel.myDate.value ?: LocalDate.now()
            val buildingZoneId = viewModel.getBuildingZoneId()
            val availableAssets = assetViewModel.checkWorkstationSchedules(
                myAssets,
                startTime,
                endTime,
                dateSelected,
                buildingZoneId
            )
            val area = assetViewModel.area.value ?: return@observe
            setLayout(area, availableAssets)
        }

        viewModel.myDate.observe(viewLifecycleOwner) { currentTime ->
            binding.filterComponent.areaCalendar.text = currentTime.formatCalendar(requireContext())
            reloadLayoutMap()
        }

        viewModel.userStartTime.observe(viewLifecycleOwner) {
            val formatter = DateTimeFormatter.ofPattern(LOCAL_TIME_FORMAT, Locale.ENGLISH)
            binding.filterComponent.btnTimeStart.text = it.format(formatter)
            reloadLayoutMap()
        }

        viewModel.userEndTime.observe(viewLifecycleOwner) {
            val formatter = DateTimeFormatter.ofPattern(LOCAL_TIME_FORMAT, Locale.ENGLISH)
            binding.filterComponent.btnTimeEnd.text = it.format(formatter)
            reloadLayoutMap()
        }
        assetViewModel.overLappedBookings.observe(viewLifecycleOwner) { overlappedBookings ->
            val tempOverLappedBooking = arrayListOf<String>()
            overlappedBookings.forEach { item -> tempOverLappedBooking.add(item.assetId) }
            assetViewModel.getAssetOverlapped(
                buildingId,
                tempOverLappedBooking
            )
            canLoadOverlappedBookings = true
        }

        assetViewModel.loadAsset.observe(viewLifecycleOwner) { loadAsset ->
            if (loadAsset) {
                callAssetsByBuildingId()
            }
        }

        filterViewModel.countFilterSelected.observe(viewLifecycleOwner) {
            if (assetViewModel.loadAsset.value == true) {
                callAssetsByBuildingId()
            }
            if (it == 0) btnFilter.text = getString(R.string.filter).uppercase(Locale.getDefault())
            else btnFilter.text = String.format(getString(R.string.filterS), it)
        }
    }

    private fun reloadLayoutMap() {
        if (canLoadOverlappedBookings) {
            getOverlappedBooking()
            canLoadOverlappedBookings = false
        }
    }

    private fun initButtonListeners() {
        binding.filterComponent.areaCalendar.setOnClickListener {
            val parentView = it
            val millis =
                viewModel.myDate.value?.atStartOfDay(viewModel.getBuildingZoneId())?.toInstant()
                    ?.toEpochMilli()
            val calendarPicker = MaterialDatePicker.Builder.datePicker()
                .setTitleText(getString(R.string.day_booking))
                .setSelection(millis)
                .build()
            calendarPicker.show(parentFragmentManager, DATE_PICKER)
            calendarPicker.addOnPositiveButtonClickListener { timeMillisecond ->
                val showMessage = viewModel.postDate(timeMillisecond)
                if (showMessage) {
                    val snack = Snackbar.make(parentView, getString(R.string.validateDate), Snackbar.LENGTH_LONG)
                    snack.show()
                }
                binding.filterComponent.areaCalendar.text =
                    calendarPicker.headerText.substringBefore(',')
            }
        }

        binding.filterComponent.btnTimeStart.setOnClickListener {
            val localDate = viewModel.userStartTime.value
            val timePicker = localDate?.let { date ->
                MaterialTimePicker.Builder()
                    .setInputMode(MaterialTimePicker.INPUT_MODE_KEYBOARD)
                    .setTimeFormat(TimeFormat.CLOCK_12H)
                    .setHour(date.hour)
                    .setMinute(date.minute)
                    .setTitleText(getString(R.string.select_start_time))
                    .build()
            }
            timePicker?.show(parentFragmentManager, START_TIME_PICKER_TAG)
            timePicker?.addOnPositiveButtonClickListener {
                viewModel.postStartTime(timePicker.hour, timePicker.minute)
            }
        }

        binding.filterComponent.btnTimeEnd.setOnClickListener {
            val parentView = it
            val localDate = viewModel.userEndTime.value
            val timePicker = localDate?.let { date ->
                MaterialTimePicker.Builder()
                    .setInputMode(MaterialTimePicker.INPUT_MODE_KEYBOARD)
                    .setTimeFormat(TimeFormat.CLOCK_12H)
                    .setHour(date.hour)
                    .setMinute(date.minute)
                    .setTitleText(SELECT_END_TIME)
                    .build()
            }
            timePicker?.show(parentFragmentManager, END_TIME_PICKER_TAG)
            timePicker?.addOnPositiveButtonClickListener {
                val showMessage = viewModel.postEndTime(timePicker.hour, timePicker.minute)
                if (showMessage) {
                    val snack = Snackbar.make(parentView, VALIDATE_END_TIME, Snackbar.LENGTH_LONG)
                    snack.anchorView = (activity as MainActivity).binding.bottomNavView
                    snack.show()
                }
            }
        }
    }

    private fun setLayout(area: Area.AreaItem, availableAssets: List<String>) {
        val parent = binding.mapViewContainer.parent as? ViewGroup
        binding.mapViewContainer.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Glide.with(this)
            .asBitmap()
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .load(
                if (!area.background?.url.isNullOrEmpty()) area.background?.url
                else "empty url"
            )
            .apply(RequestOptions().override(parent?.width ?: 0, parent?.height ?: 0))
            .into(object : CustomTarget<Bitmap?>() {
                override fun onResourceReady(
                    resource: Bitmap,
                    transition: Transition<in Bitmap?>?
                ) {
                    binding.noLayoutMessageContainer.visibility = View.GONE
                    val mapViewImage = resource.copy(resource.config, false)
                    binding.mapView.setImage(ImageSource.bitmap(mapViewImage))
                    imageWidth = binding.mapViewContainer.measuredWidth
                    imageHeight = binding.mapViewContainer.measuredHeight
                    setMarkersOnLayout(
                        area.markers as List<Area.AreaItem.Marker>,
                        mapViewImage.width,
                        mapViewImage.height,
                        availableAssets
                    )
                    assetViewModel.isAreaMapLoading.postValue(false)
                }

                override fun onLoadCleared(placeholder: Drawable?) {}

                override fun onLoadFailed(errorDrawable: Drawable?) {
                    binding.noLayoutMessageContainer.visibility = View.VISIBLE
                    binding.mapView.visibility = View.GONE
                    assetViewModel.isAreaMapLoading.postValue(false)
                    super.onLoadFailed(errorDrawable)
                }
            })
    }

    private fun setMarkersOnLayout(
        markersResponse: List<Area.AreaItem.Marker>,
        width: Int,
        height: Int,
        availableAssets: List<String>
    ) {
        binding.progressBar?.visibility = View.GONE
        val markers = mutableListOf<Marker>()
        val unavailableMarkers = mutableListOf<Marker>()
        val allMarkers = mutableListOf<Marker>()
        markersResponse.forEach { marker ->
            if (marker.public == true) {
                val x = marker.x.toFloat() * width
                val y = marker.y.toFloat() * height
                if (availableAssets.contains(marker.assetId)) {
                    val newMarker = context?.let { Marker(it, x, y, true) } ?: return
                    markers.add(newMarker)
                } else {
                    val newMarker = context?.let { Marker(it, x, y, false) } ?: return
                    unavailableMarkers.add(newMarker)
                }
            }
        }
        allMarkers.addAll(markers + unavailableMarkers)
        if (markers.isEmpty() && (filterViewModel.countFilterSelected.value ?: 0) > 0) {
            Snackbar.make(binding.mapView, getString(R.string.mismatchedFilters), Snackbar.LENGTH_LONG).show()
        } else if (allMarkers.isNullOrEmpty() || markers.isNullOrEmpty()) {
            val snack = Snackbar.make(binding.mapView, getString(R.string.emptyMap), Snackbar.LENGTH_LONG)
            snack.anchorView = (activity as MainActivity).binding.bottomNavView
            snack.show()
        }
        if (allMarkers.isEmpty()) {
            if (binding.mapView.isReady) {
                binding.mapView.setPin(emptyList())
            }
            return
        }
        setMarkersRecognizer(allMarkers, markers, unavailableMarkers, width)
    }

    private fun setMarkersRecognizer(allMarkers: List<Marker>, markers: List<Marker>, unavailableMarkers: List<Marker>, width: Int) {
        // set setMarkerZoomAndCenterAnimation and pass one parameter to animate the map
        if (filterViewModel.switchAvailableSpaces.value == true) {
            binding.mapView.setPin(markers)
            setOnGestureRecognizer(markers, emptyList(), width)
        } else {
            binding.mapView.setPin(allMarkers)
            setOnGestureRecognizer(markers, unavailableMarkers, width)
        }
    }

    private fun setMarkerZoomAndCenterAnimation(marker: Marker) {
        val maxScale = binding.mapView.maxScale
        val minScale = binding.mapView.minScale
        val scale = SCALE_MULTIPLIER * (maxScale - minScale) + minScale
        val center = marker.getPoint()
        binding.mapView.animateScaleAndCenter(scale, center)?.apply {
            withDuration(ANIMATION_DURATION.toLong())
            withEasing(SubsamplingScaleImageView.EASE_OUT_QUAD)
            withInterruptible(false)
            start()
        }
    }

    private fun setOnGestureRecognizer(markers: List<Marker>, uMarkers: List<Marker>, width: Int) {
        val gestureDetector =
            GestureDetector(activity, object : GestureDetector.SimpleOnGestureListener() {
                override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
                    binding.mapView.viewToSourceCoord(e.x, e.y)?.let {
                        val tempMarkers = markers + uMarkers
                        tempMarkers.forEachIndexed { _, marker ->
                            val zoomFactor = binding.mapView.maxScale / binding.mapView.scale
                            val isNearbyX = abs(marker.getPoint().x - it.x) <
                                    MAX_AREA_SELECTION * (zoomFactor / 2)
                            val isNearbyY = if (it.y > marker.getPoint().y) false else marker.getPoint().y - it.y < MAX_AREA_SELECTION * zoomFactor
                            if (isNearbyX && isNearbyY) {
                                tempMarkers.forEach { it.setDefault() }
                                val tempArea = assetViewModel.area.value?.markers?.filter { item ->
                                    item?.x?.toFloat()?.times(width) == marker.getPoint().x
                                }?.get(0)
                                openBottomSheet(tempArea)
                                marker.select()
                                setMarkerZoomAndCenterAnimation(marker)
                            }
                        }
                        binding.mapView.setPin(markers + uMarkers)
                    }
                    return true
                }
            })
        binding.mapView.setOnTouchListener { view, motionEvent ->
            gestureDetector.onTouchEvent(motionEvent)
            view.performClick()
        }
        binding.mapView.visibility = View.VISIBLE
    }

    private fun openBottomSheet(selectedArea: Area.AreaItem.Marker?) {
        if (selectedArea != null) {
            bottomSheetAssetDetail = AssetDetailFragment(
                selectedArea.assetId,
                buildingId,
                areaId
            )
            bottomSheetAssetDetail.bottomSheetDialogListener =
                this@AssetResultFragment
            if (!bottomSheetAssetDetail.isAdded && !bottomSheetAssetDetail.isVisible) {
                bottomSheetAssetDetail.show(
                    parentFragmentManager,
                    BOTTOM_SHEET_ASSET_DETAIL
                )
                isBottomSheetOpen = !isBottomSheetOpen
            }
        }
    }

    override fun didSelectBottomSheetDialog() {
        isBottomSheetOpen = !isBottomSheetOpen
    }

    private fun callAssetsByBuildingId() {
        val filterSpaceType = filterViewModel.spaceType.value?.let { it } ?: return
        val filterArrLabels = filterViewModel.arrLabels.value?.let { it } ?: return
        context?.let {
            assetViewModel.getAssetByBuildingId(
                buildingId, filterSpaceType, filterArrLabels
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        assetViewModel.clearData()
    }

    override fun fetchBadRequest(errorType: ErrorMessage) {
        if (dialogAlert?.isShowing == true) {
            return
        }
        context?.let {
            dialogAlert = MaterialAlertDialogBuilder(it)
                .setTitle(resources.getString(errorType.message().first))
                .setMessage(resources.getString(errorType.message().second))
                .setPositiveButton(resources.getString(R.string.i_understand)) { dialog, _ ->
                    dialog.dismiss()
                }
                .create()
                .apply {
                    show()
                }
        }
    }

    private fun checkAssetParameters() {
        if (areaCode.isNullOrEmpty() || areaId.isNullOrEmpty() || buildingId.isNullOrEmpty()) {
            binding.noAreaSelectedContainer?.visibility = View.VISIBLE
        } else {
            binding.noAreaSelectedContainer?.visibility = View.GONE
        }
    }

    private fun observeLoading() {
        assetViewModel.isAreaMapLoading.observe(viewLifecycleOwner) { isAreaMapLoading ->
            if (isAreaMapLoading) {
                binding.progressCircularContainer?.visibility = View.VISIBLE
                binding.mapView.visibility = View.GONE
                return@observe
            }
            binding.progressCircularContainer?.visibility = View.GONE
            binding.progressBar?.visibility = View.GONE
            binding.mapView.visibility = View.VISIBLE
        }
    }
}