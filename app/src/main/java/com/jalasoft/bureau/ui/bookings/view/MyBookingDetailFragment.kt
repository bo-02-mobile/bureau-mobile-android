package com.jalasoft.bureau.ui.bookings.view

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.alamkanak.weekview.MonthLoader
import com.alamkanak.weekview.WeekViewEvent
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.common.RecurrenceRule
import com.jalasoft.bureau.databinding.BottomSheetDialogBinding
import com.jalasoft.bureau.databinding.FragmentMyBookingDetailBinding
import com.jalasoft.bureau.ui.bookings.viewmodel.MyBookingsViewModel
import com.jalasoft.bureau.ui.main.view.MainActivity
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.utils.*
import dagger.hilt.android.AndroidEntryPoint
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.text.SimpleDateFormat
import java.time.LocalTime
import java.time.ZonedDateTime
import java.util.*

@AndroidEntryPoint
class MyBookingDetailFragment : DialogFragment() {
    private val myBookingsViewModel: MyBookingsViewModel by activityViewModels()
    private lateinit var binding: FragmentMyBookingDetailBinding
    lateinit var workstationID: String
    private lateinit var bookingID: String
    private lateinit var buildingID: String
    private lateinit var workstationCode: String
    private var bookingStartTime: String? = ""
    private var bookingEndTime: String? = ""
    private lateinit var assetType: String
    private lateinit var userBooking: BookingResponse.Booking
    private lateinit var startTime: String
    private val viewModel: MainActivityViewModel by activityViewModels()
    private var prefs: Prefs? = null
    private lateinit var limitBeforeDeadLine: String
    private lateinit var limitAfterDeadLine: String
    var isPermanent: Boolean? = null
    private lateinit var areaName: String
    private lateinit var bookingRangeHourText: String
    private var isBookingValidationInRange = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentMyBookingDetailBinding.inflate(inflater, container, false)
        arguments?.let {
            val isValidated = it.getString(VALIDATED_AT)
            val isToday = it.getBoolean(IS_TODAY)
            val endTime = it.getString(END_TIME).toString()
            val reservationMessage = it.getString(RES_MESSAGE).toString()
            startTime = it.getString(START_TIME).toString()
            assetType = it.getString(ASSET_TYPE).toString()
            workstationCode = it.getString(PLACE_CODE).toString()
            workstationID = it.getString(WORKSTATION_ID).toString()
            bookingID = it.getString(BOOKING_ID).toString()
            buildingID = it.getString(BUILDING_ID).toString()
            areaName = it.getString(AREA_NAME).toString()
            isPermanent = it.getBoolean(IS_PERMANENT)
            bookingRangeHourText = it.getString(BOOKING_RANGE_HOUR_TEXT).toString()
            isBookingValidationInRange = it.getBoolean(IS_BOOKING_VALIDATION_IN_RANGE)
            prefs = Prefs(requireContext())
            limitBeforeDeadLine = prefs?.getData(SHARED_BOOKING_LIMIT_BEFORE_DEADLINE) ?: NOTIFICATION_DEFAULT_RANGE
            limitAfterDeadLine = prefs?.getData(SHARED_BOOKING_LIMIT_AFTER_DEADLINE) ?: NOTIFICATION_DEFAULT_RANGE
            if (!resources.getBoolean(R.bool.isTablet)) {
                setupToolbar(workstationCode)
            }
            binding.codeWorkstation.text = workstationCode
            userBooking = BookingResponse.Booking(
                "", "", "", workstationID, "", buildingID, null, "", "", bookingID, false,
                false, null, RecurrenceRule(), "", "", 0, "", null, null
            )
            // Initializing week view with booking date
            initializingWeekViewCalendarDate(startTime)
            // Checking if the booking has already been validated
            if (isValidated != null || isPermanent == true) {
                setIsValidatedText(true, isToday)
            } else {
                setIsValidatedText(false, isToday)
            }
            // Initializing week view with booking hour
            if (isPermanent != true) {
                binding.dateTV.text = StringUtils.capitalize(reservationMessage)
                bookingStartTime = convertGMTtoLocalDate(startTime)
                bookingEndTime = convertGMTtoLocalDate(endTime)
                setWeekViewHour()
            } else {
                binding.dateTV.text = IS_PERMANENT_MESSAGE
                binding.weekView.visibility = View.INVISIBLE
                binding.cancelButton.visibility = View.GONE
            }
            myBookingsViewModel.todayBookings.postValue(null)
            myBookingsViewModel.upcomingBookings.postValue(null)
        }
        return binding.root
    }

    private fun initializingWeekViewCalendarDate(dateWeek: String) {
        val timeWeekCalendar = Calendar.getInstance()
        timeWeekCalendar.time = DateTime(
            dateWeek, DateTimeZone.forTimeZone(TimeZone.getDefault())
        ).toDate()
        binding.weekView.goToDate(timeWeekCalendar)
    }

    private fun setIsValidatedText(isValidated: Boolean, isToday: Boolean) {
        if (!isToday) {
            binding.qrArea.visibility = View.GONE
            return
        }
        if (isValidated) {
            context?.let {
                binding.validatedTV.text = it.getString(R.string.validated)
                binding.validatedTV.setTextColor(
                    ContextCompat.getColor(
                        it,
                        R.color.bureau_secondary_color
                    )
                )
                binding.qrButton.setImageResource(R.drawable.ic_nc_vqrcode)
                binding.qrButton.isEnabled = false
                binding.validatedTV.isEnabled = false
            }
        } else {
                if (isBookingValidationInRange) {
                    binding.qrButton.setImageResource(R.drawable.ic_nc_qrcode)
                    binding.qrButton.isEnabled = true
                    binding.validatedTV.isEnabled = true
                } else {
                    binding.validatedTV.text = bookingRangeHourText
                    binding.validatedTV.gravity = Gravity.CENTER
                    binding.validatedTV.textSize = 12.0F
                    binding.qrButton.setImageResource(0)
                    binding.qrButton.isEnabled = false
                    binding.validatedTV.isEnabled = false
                }
        }

        binding.validatedTV.setOnClickListener {
            didQrButtonPressed()
        }

        binding.qrButton.setOnClickListener {
            didQrButtonPressed()
        }
    }

    private fun didQrButtonPressed() {
        viewModel.bookingQR.postValue(userBooking)
        (activity as MainActivity).moveToQrScreen()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let { context ->
            if (isPermanent != true && !bookingStartTime.isNullOrBlank()) {
                val date = ZonedDateTime.parse(bookingStartTime).toLocalDate()
                val time = LocalTime.of(0, 0, 0, 0)
                val dateTime = ZonedDateTime.of(date, time, myBookingsViewModel.getBuildingZoneId())
                myBookingsViewModel.getBuildingDetails(buildingID)
                myBookingsViewModel.getAllEvents(workstationID, dateTime)
            }
            myBookingsViewModel.getAssetDetails(workstationID)
        }

        initRecycler()
        cancelButtonListener()
        subscribeCalendar()
    }

    override fun onDestroy() {
        super.onDestroy()
        myBookingsViewModel.updateBookingsList.postValue(false)
    }

    private fun convertGMTtoLocalDate(dateStr: String): String {
        val df = SimpleDateFormat(API_DATE_FORMAT, Locale.ENGLISH)
        df.timeZone = TimeZone.getTimeZone("UTC")
        val date = df.parse(dateStr)
        df.timeZone = TimeZone.getDefault()
        var converted = dateStr
        date?.let {
            converted = df.format(date)
        }
        return converted
    }

    private fun setupToolbar(code: String) {
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainActivity).binding.appBar.toolbar.visibility = View.VISIBLE
        (activity as MainActivity).binding.appBar.title.text = code
        (activity as MainActivity).binding.appBar.title.visibility = View.VISIBLE
        (activity as MainActivity).binding.bottomNavView.menu.getItem(MY_BOOKING_TAB).isChecked =
            true
    }

    private fun cancelButtonListener() {
        binding.cancelButton.setOnClickListener {
                val dialog = BottomSheetDialog(requireContext())
                val view = BottomSheetDialogBinding.inflate(layoutInflater)
                val btnCancelBooking = view.idBtnDismiss
                val btnCloseModal = view.actionModeCloseButton
                val tvTitle = view.titleModalB
                val tvDescription = view.descriptionModalB
                val workstationCodeTV = view.workstationCode
                workstationCodeTV.text =
                    String.format(getString(R.string.asset_title), assetType, workstationCode)
                tvTitle.text = getString(R.string.cancel_title)
                tvDescription.text = getString(R.string.cancel_description)
                btnCloseModal.setOnClickListener {
                    dialog.dismiss()
                }
                btnCancelBooking.setOnClickListener {
                    myBookingsViewModel.deleteBooking(
                        requireContext(), bookingID
                    ) {
                        dialog.dismiss()
                        navigateToPreviousScreen()
                        myBookingsViewModel.deleteNotificationByBookingId(bookingID, requireContext())
                    }
                }
                dialog.setContentView(view.root)
                dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
                dialog.show()
        }

        binding.cancelButtonIcon?.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun navigateToPreviousScreen() {
        findNavController().navigateUp()
    }

    private fun initRecycler() {
        myBookingsViewModel.assetDetails.observe(viewLifecycleOwner) { workstationDetails ->
            binding.featuresWorkstation.text = (workstationDetails.properties?.labels as ArrayList<String>).joinToString(separator = " • ") { "$it" }
        }
        binding.tvAreaName.text = areaName
        binding.tvAssetType.text = assetType
    }

    private fun setWeekViewHour() {
        val parser = SimpleDateFormat(API_DATE_FORMAT, Locale.ENGLISH)
        val formatter = SimpleDateFormat(ONLY_HOUR, Locale.ENGLISH)
        val parsedDate = parser.parse(bookingStartTime)
        parsedDate?.let {
            val hourBooking = formatter.format(parsedDate).toString().toDouble() - 2.0
            binding.weekView.goToHour(hourBooking)
        }
    }

    private fun changeEventBackground(bookingOwner: String?, bookingEventID: String?): Int {

        if (bookingID == bookingEventID) {
            return ResourcesCompat.getColor(
                resources,
                R.color.user_booking,
                null
            )
        }
        if (bookingOwner == viewModel.userInfo.value?.name) {
            return ResourcesCompat.getColor(
                resources,
                R.color.temporal_user_booking,
                null
            )
        }
        return ResourcesCompat.getColor(
            resources,
            R.color.event_occupied,
            null
        )

    }


    private fun subscribeCalendar() {
        myBookingsViewModel.myEvents.observe(viewLifecycleOwner) { bookings ->
            binding.weekView.monthChangeListener = object : MonthLoader.MonthChangeListener {
                override fun onMonthChange(
                    newYear: Int,
                    newMonth: Int,
                ): MutableList<out WeekViewEvent> {
                    val timeZone =
                        DateUtils.getTimeZoneFromZoneId(myBookingsViewModel.getBuildingZoneId())
                    val events = ArrayList<WeekViewEvent>()
                    for (bookingResponse: BookingResponse.Booking in bookings) {
                        val startTime = Calendar.getInstance(timeZone)
                        startTime.time = DateTime(
                            bookingResponse.startTime,
                            DateTimeZone.forTimeZone(timeZone)
                        ).toDate()
                        startTime.set(Calendar.MONTH, newMonth - 1)
                        val endTime = startTime.clone() as Calendar
                        endTime.time = DateTime(
                            bookingResponse.endTime,
                            DateTimeZone.forTimeZone(timeZone)
                        ).toDate()
                        endTime.set(Calendar.MONTH, newMonth - 1)
                        val event =
                            WeekViewEvent(
                                bookingResponse.id,
                                bookingResponse.owner?.name,
                                startTime,
                                endTime
                            )
                        event.color =
                            changeEventBackground(bookingResponse.owner?.name, bookingResponse.id)
                        events.add(event)
                    }
                    return events
                }
            }
            binding.weekView.notifyDataSetChanged()
        }
    }

}