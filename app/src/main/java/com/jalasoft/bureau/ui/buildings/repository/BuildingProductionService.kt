package com.jalasoft.bureau.ui.buildings.repository

import com.jalasoft.bureau.data.interfaces.BuildingService
import com.jalasoft.bureau.data.model.building.Building
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class BuildingProductionService
@Inject
constructor(private val retrofit: Retrofit) {

    suspend fun getBuildingsProduction(): Response<List<Building.BuildingItem>>? {
        return retrofit.create(BuildingService::class.java)
            ?.getBuildingsAuthorization()
    }
}