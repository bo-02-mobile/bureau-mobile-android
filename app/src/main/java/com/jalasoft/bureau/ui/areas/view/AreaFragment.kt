package com.jalasoft.bureau.ui.areas.view

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.adapter.AreaAdapter
import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.data.interfaces.BookingOptionsListener
import com.jalasoft.bureau.data.interfaces.NavigationListener
import com.jalasoft.bureau.databinding.FragmentAreaBinding
import com.jalasoft.bureau.ui.areas.viewmodel.AreaFragmentViewModel
import com.jalasoft.bureau.ui.filter.view.FilterFragment
import com.jalasoft.bureau.ui.filter.viewmodel.FilterViewModel
import com.jalasoft.bureau.ui.main.view.MainActivity
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.workstations.view.BookingOptionsFragment
import com.jalasoft.bureau.ui.workstations.viewmodel.AssetResultFragmentViewModel
import com.jalasoft.bureau.utils.*
import com.jalasoft.bureau.utils.extensions.formatCalendar
import dagger.hilt.android.AndroidEntryPoint
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*

@AndroidEntryPoint
class AreaFragment : Fragment(), NavigationListener, BadRequest, BookingOptionsListener {

    // region Properties
    private lateinit var areaBinding: FragmentAreaBinding
    private lateinit var areaRecycler: RecyclerView
    private val areaFragmentViewModel: AreaFragmentViewModel by activityViewModels()
    private lateinit var buildingName: String
    lateinit var buildingId: String
    lateinit var adapter: AreaAdapter
    private val viewModel: MainActivityViewModel by activityViewModels()
    private val filterViewModel: FilterViewModel by activityViewModels()
    private val assetViewModel: AssetResultFragmentViewModel by activityViewModels()
    lateinit var btnFilter: AppCompatTextView
    var dialogAlert: AlertDialog? = null
    var selectedAreaId: String? = null
    var selectedAreaCode: String? = null
    // endregion Properties

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_area, container, false)
        areaBinding = FragmentAreaBinding.bind(view)
        btnFilter = view.findViewById(R.id.btn_filter)
        return areaBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // SO IMPORTANT !!! setting up the listener between the activity and the fragment
        areaFragmentViewModel.mainActivityListener = this
        if (!resources.getBoolean(R.bool.isTablet)) {
            //Firstly we call all the building from the api
            viewModel.getBuildings()
            checkArgumentsReceived()
            setupToolbar(buildingName)
            areaFragmentViewModel.getAreaByBuildingId(buildingId)
            initRecycler()
            initSubscriptions()
        } else {
            buildingName = ""
            buildingId = ""
            checkAreaParameters()
            viewModel.buildingId.observe(viewLifecycleOwner) {
                buildingName = viewModel.buildingName.value.toString()
                buildingId = viewModel.buildingId.value.toString()
                areaFragmentViewModel.getAreaByBuildingId(buildingId)
                checkAreaParameters()
                initRecycler()
                initSubscriptions()
                selectedAreaId = null
                selectedAreaCode = null
            }
            updateDateTimeText()
            areaBinding.filterComponent.btnBookingOptions?.setOnClickListener {
                val optionsBottomSheet = BookingOptionsFragment()
                optionsBottomSheet.bookingOptionsListener = this
                if (!optionsBottomSheet.isAdded) {
                    optionsBottomSheet.show(
                        parentFragmentManager,
                        BOOKING_OPTIONS_BOTTOM_SHEET_TABLET
                    )
                }
            }
            checkDateOrTimeFromFilterChanges()
        }
        initMainActivitySubscriptions()
        initButtonListeners()
        refreshLayoutBuilding()
        areaBinding.filterComponent.btnFilter.setOnClickListener {
            val filterBottomSheet = FilterFragment()
            if (!filterBottomSheet.isAdded) {
                filterBottomSheet.show(parentFragmentManager, FILTER_BOTTOM_SHEET)
            }
        }
    }

    private fun checkDateOrTimeFromFilterChanges() {
        viewModel.myDate.observe(viewLifecycleOwner) { _ ->
            getOverlappedBooking()
        }

        viewModel.userStartTime.observe(viewLifecycleOwner) { _ ->
            getOverlappedBooking()
        }

        viewModel.userEndTime.observe(viewLifecycleOwner) { _ ->
            getOverlappedBooking()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        areaFragmentViewModel.overLappedBookings.postValue(null)
    }

    // region get Arguments
    fun checkArgumentsReceived() {
        arguments?.let { bundle ->
            buildingId = bundle.getString(BUILDING_ID).toString()
            buildingName = bundle.get(BUILDING_NAME).toString()
        }
    }
    // endregion get Arguments

    // region Setup Toolbar
    private fun setupToolbar(title: String) {
        (activity as MainActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainActivity).binding.appBar.toolbar.visibility = View.VISIBLE
        (activity as MainActivity).binding.appBar.title.visibility = View.VISIBLE
        (activity as MainActivity).binding.appBar.title.text = title
        (activity as MainActivity).binding.appBar.logo.visibility = View.GONE
        (activity as MainActivity).binding.bottomNavView.menu.getItem(BOOKING_TAB).isChecked = true
    }
    // endregion Setup Toolbar

    // region Init Recycler
    private fun initRecycler() {
        areaRecycler = areaBinding.rvAreas
        areaRecycler.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        adapter = AreaAdapter(emptyList(), this, buildingName, emptyList(), "")
        areaRecycler.setHasFixedSize(true)
        areaRecycler.adapter = adapter
    }
    // endregion Init Recycler

    private fun checkForFilters(): Boolean {
        return !(filterViewModel.countFilterSelected.value == null || filterViewModel.countFilterSelected.value == 0)
    }

    // We will subscribe with the view model livedata variables
    // we share data with the parent activity and the fragment and vice versa
    private fun initSubscriptions() {
        // observing change of the area
        observeMyAssets()
        observeLoading()
        observeCounterFilterSelected()
        observeOverLappedBookings()
    }

    override fun onDestroy() {
        super.onDestroy()
        areaFragmentViewModel.clearAreas()
    }

    // region Observers
    private fun observeMyAssets() {
        areaFragmentViewModel.myAssets.observe(viewLifecycleOwner) { assets ->
            val areas = areaFragmentViewModel.myAreas.value
            areaBinding.tvCount.text = String.format("(%s)", areas?.size.toString())
            val startTime = viewModel.userStartTime.value ?: LocalTime.now()
            val endTime = viewModel.userEndTime.value ?: LocalTime.now()
            val dateSelected = viewModel.myDate.value ?: LocalDate.now()
            val zoneId = viewModel.getBuildingZoneId()
            val availableAssets = areaFragmentViewModel.checkWorkstationSchedules(
                assets,
                startTime,
                endTime,
                dateSelected,
                zoneId
            )
            if (areas != null) {
                adapter.setAreasOfABuilding(areas, availableAssets, checkForFilters())
            }
            if (!areas.isNullOrEmpty()) {
                adapter.selected = selectedAreaId ?: areas[0].id ?: ""
                viewModel.areaId.value = selectedAreaId ?: areas[0].id
                viewModel.areaCode.value = selectedAreaCode ?: areas[0].name
            } else {
                viewModel.areaId.value = ""
                viewModel.areaCode.value = ""
            }
        }
    }

    private fun observeLoading() {
        areaFragmentViewModel.isAreasLoading.observe(viewLifecycleOwner) { isLoading ->
            if (isLoading) {
                areaBinding.progressCircular.visibility = View.VISIBLE
                areaBinding.swipeRefreshLayout.visibility = View.INVISIBLE
            } else {
                areaBinding.progressCircular.visibility = View.INVISIBLE
                areaBinding.swipeRefreshLayout.visibility = View.VISIBLE
            }
        }
    }

    private fun observeCounterFilterSelected() {
        filterViewModel.countFilterSelected.observe(viewLifecycleOwner) {
            getOverlappedBooking()
            if (it == 0) btnFilter.text = getString(R.string.filter).uppercase(Locale.getDefault())
            else btnFilter.text = String.format(getString(R.string.filterS), it)
        }
    }

    private fun observeOverLappedBookings() {
        areaFragmentViewModel.overLappedBookings.observe(viewLifecycleOwner) { overlappedBookings ->
            if (overlappedBookings == null) {
                return@observe
            }
            val tempOverlappedBookings = arrayListOf<String>()
            overlappedBookings.forEach { item -> tempOverlappedBookings.add(item.assetId) }
            filterViewModel.spaceType.value?.let { spaceType ->
                filterViewModel.arrLabels.value?.let { arrLabels ->
                    areaFragmentViewModel.getAssetOverlapped(
                        buildingId,
                        tempOverlappedBookings,
                        spaceType,
                        arrLabels
                    )
                }
            }
        }
    }
    // endregion Observers

    private fun initMainActivitySubscriptions() {
        // observing when the date changes
        viewModel.myDate.observe(viewLifecycleOwner) { currentTime ->
            areaBinding.filterComponent.areaCalendar.text = currentTime.formatCalendar(requireContext())
            FirebaseCrashlyticsUtils.setCrashlyticsKey(KEY_DATE_VALUE, currentTime.toString())
            updateDateTimeText()
        }

        viewModel.userStartTime.observe(viewLifecycleOwner) {
            val formatter = DateTimeFormatter.ofPattern(LOCAL_TIME_FORMAT, Locale.ENGLISH)
            areaBinding.filterComponent.btnTimeStart.text = it.format(formatter)
            FirebaseCrashlyticsUtils.setCrashlyticsKey(KEY_START_TIME_VALUE, it.toString())
            updateDateTimeText()
        }

        viewModel.userEndTime.observe(viewLifecycleOwner) {
            val formatter = DateTimeFormatter.ofPattern(LOCAL_TIME_FORMAT, Locale.ENGLISH)
            areaBinding.filterComponent.btnTimeEnd.text = it.format(formatter)
            FirebaseCrashlyticsUtils.setCrashlyticsKey(KEY_END_TIME_VALUE, it.toString())
            updateDateTimeText()
        }
    }

    private fun updateDateTimeText() {
        areaBinding.filterComponent.btnBookingOptions?.text = String.format(
            FULL_DATETIME_FORMAT,
            areaBinding.filterComponent.areaCalendar.text,
            areaBinding.filterComponent.btnTimeStart.text,
            areaBinding.filterComponent.btnTimeEnd.text
        )
    }

    // region Init Button and Spinner
    private fun initButtonListeners() {
        areaBinding.filterComponent.areaCalendar.setOnClickListener {
            val parentView = it
            val millis =
                viewModel.myDate.value?.atStartOfDay(viewModel.getBuildingZoneId())?.toInstant()
                    ?.toEpochMilli()
            val calendarPicker = MaterialDatePicker.Builder.datePicker()
                .setTitleText(getString(R.string.day_booking))
                .setSelection(millis)
                .build()
            calendarPicker.show(parentFragmentManager, DATE_PICKER)
            calendarPicker.addOnPositiveButtonClickListener { timeMillisecond ->
                val showMessage = viewModel.postDate(timeMillisecond)
                if (showMessage) {
                    val snack = Snackbar.make(parentView, getString(R.string.validateDate), Snackbar.LENGTH_LONG)
                    snack.show()
                } else {
                    getOverlappedBooking()
                }
                areaBinding.filterComponent.areaCalendar.text =
                    calendarPicker.headerText.substringBefore(',')
            }
        }

        areaBinding.filterComponent.btnTimeStart.setOnClickListener {
            val localDate = viewModel.userStartTime.value
            val timePicker = localDate?.let { date ->
                MaterialTimePicker.Builder()
                    .setInputMode(MaterialTimePicker.INPUT_MODE_KEYBOARD)
                    .setTimeFormat(TimeFormat.CLOCK_12H)
                    .setHour(date.hour)
                    .setMinute(date.minute)
                    .setTitleText(getString(R.string.select_start_time))
                    .build()
            }
            timePicker?.show(parentFragmentManager, START_TIME_PICKER_TAG)
            timePicker?.addOnPositiveButtonClickListener {
                viewModel.postStartTime(timePicker.hour, timePicker.minute)
                getOverlappedBooking()
            }
        }

        areaBinding.filterComponent.btnTimeEnd.setOnClickListener {
            val parentView = it
            val localDate = viewModel.userEndTime.value
            val timePicker = localDate?.let { date ->
                MaterialTimePicker.Builder()
                    .setInputMode(MaterialTimePicker.INPUT_MODE_KEYBOARD)
                    .setTimeFormat(TimeFormat.CLOCK_12H)
                    .setHour(date.hour)
                    .setMinute(date.minute)
                    .setTitleText(SELECT_END_TIME)
                    .build()
            }
            timePicker?.show(parentFragmentManager, END_TIME_PICKER_TAG)
            timePicker?.addOnPositiveButtonClickListener {
                val showMessage = viewModel.postEndTime(timePicker.hour, timePicker.minute)
                if (showMessage) {
                    val snack = Snackbar.make(parentView, VALIDATE_END_TIME, Snackbar.LENGTH_LONG)
                    snack.anchorView = (activity as MainActivity).binding.bottomNavView
                    snack.show()
                } else {
                    getOverlappedBooking()
                }
            }
        }
    }
    // endregion Init Button and Spinner

    private fun getOverlappedBooking() {
        val startTime = DateUtils.getFormattedDate(
            viewModel.myDate.value,
            viewModel.userStartTime.value,
            viewModel.getBuildingZoneId()
        )
        val endTime = DateUtils.getFormattedDate(
            viewModel.myDate.value,
            viewModel.userEndTime.value,
            viewModel.getBuildingZoneId()
        )
        areaFragmentViewModel.getOverlappedBookings(startTime, endTime)
    }

    // region Area Item Selected
    override fun didAreaSelected(areaId: String, areaCode: String) {
        FirebaseCrashlyticsUtils.setCrashlyticsKey(KEY_AREA_ID, areaId)
        viewModel.areaId.value = areaId
        viewModel.areaCode.value = areaCode
        if (!resources.getBoolean(R.bool.isTablet)) {
            assetViewModel.isAreaMapLoading.postValue(true)
            val action: NavDirections =
                AreaFragmentDirections.actionAreaFragmentToAssetResultFragment(
                    areaId,
                    areaCode,
                    buildingId
                )
            findNavController().navigate(action)
        } else {
            if (adapter.selected == areaId) return
            assetViewModel.isAreaMapLoading.postValue(true)
            adapter.selected = areaId
            selectedAreaId = areaId
            selectedAreaCode = areaCode
            adapter.notifyItemRangeChanged(0, adapter.myAreas.count())
        }
    }
    // endregion Area Item Selected

    override fun fetchBadRequest(errorType: ErrorMessage) {
        areaFragmentViewModel.isAreasLoading.postValue(false)
        if (dialogAlert?.isShowing == true) {
            return
        }
        context?.let {
        dialogAlert = MaterialAlertDialogBuilder(it)
            .setTitle(resources.getString(errorType.message().first))
            .setMessage(resources.getString(errorType.message().second))
            .setPositiveButton(resources.getString(R.string.i_understand)) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
            .apply {
                show()
            }
        }
    }

    private fun refreshLayoutBuilding() {
        val refreshLayout = areaBinding.swipeRefreshLayout
        refreshLayout.setOnRefreshListener {
            checkArgumentsReceived()
            initSubscriptions()
            refreshLayout.isRefreshing = false
        }
    }

    /**
     * Method to dismiss dialogs created from this fragment,
     * or subsequent fragments, when app is suspended.
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        var fragment: DialogFragment? = parentFragmentManager.findFragmentByTag(
            BOTTOM_SHEET_ASSET_DETAIL
        ) as DialogFragment?
        if (fragment != null) {
            fragment.dialog?.dismiss()
        }
        fragment = parentFragmentManager.findFragmentByTag(
            BOOKING_OPTIONS_BOTTOM_SHEET
        ) as DialogFragment?
        if (fragment != null) {
            fragment.dialog?.dismiss()
        }
        fragment = parentFragmentManager.findFragmentByTag(
            RECURRENCE_OPTIONS_BOTTOM_SHEET
        ) as DialogFragment?
        if (fragment != null) {
            fragment.dialog?.dismiss()
        }
        fragment = parentFragmentManager.findFragmentByTag(
            FILTER_BOTTOM_SHEET
        ) as DialogFragment?
        if (fragment != null) {
            fragment.dialog?.dismiss()
        }
        fragment = parentFragmentManager.findFragmentByTag(
            DATE_PICKER
        ) as DialogFragment?
        if (fragment != null) {
            fragment.dialog?.dismiss()
        }
        fragment = parentFragmentManager.findFragmentByTag(
            START_TIME_PICKER_TAG
        ) as DialogFragment?
        if (fragment != null) {
            fragment.dialog?.dismiss()
        }
        fragment = parentFragmentManager.findFragmentByTag(
            END_TIME_PICKER_TAG
        ) as DialogFragment?
        if (fragment != null) {
            fragment.dialog?.dismiss()
        }
    }

    private fun checkAreaParameters() {
        if (buildingId.isNullOrEmpty() || buildingName.isNullOrEmpty()) {
            areaBinding.noBuildingSelectedContainer?.visibility = View.VISIBLE
        } else {
            areaBinding.noBuildingSelectedContainer?.visibility = View.GONE
        }
    }

    override fun didDoneButtonPressed() {
        updateDateTimeText()
    }

}