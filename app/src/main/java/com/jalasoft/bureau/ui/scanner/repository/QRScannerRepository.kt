package com.jalasoft.bureau.ui.scanner.repository

import com.jalasoft.bureau.data.model.scanner.BookingConfirmRequestBody
import com.jalasoft.bureau.data.model.scanner.BookingConfirmResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

open class QRScannerRepository
@Inject
constructor(private val productionService: QRScannerProductionService) : IQRScannerRepository {

    open override suspend fun confirmBookingByCode(
        bookingId: String,
        requestBody: BookingConfirmRequestBody
    ): Response<BookingConfirmResponse>? {
        return withContext(Dispatchers.IO) {
            productionService.confirmBooking(bookingId, requestBody)
        }
    }
}

interface IQRScannerRepository {
    open suspend fun confirmBookingByCode(
        bookingId: String,
        requestBody: BookingConfirmRequestBody
    ): Response<BookingConfirmResponse>?
}