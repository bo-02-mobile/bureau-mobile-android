package com.jalasoft.bureau.ui.workstations.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.ui.areas.repository.AreaProductionService
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.workstations.repository.AssetDetailRepository
import com.jalasoft.bureau.ui.workstations.repository.AssetResultRepository
import com.jalasoft.bureau.utils.DateUtils
import com.jalasoft.bureau.utils.ErrorMessage
import com.jalasoft.bureau.utils.extensions.getViewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.time.LocalDate
import java.time.LocalTime
import java.time.Period
import java.time.ZoneId
import javax.inject.Inject

@HiltViewModel
class AssetResultFragmentViewModel
@Inject
constructor(var repository: AssetResultRepository, var areaRepository: AreaProductionService) : ViewModel() {

    var area = MutableLiveData<Area.AreaItem?>()
    lateinit var assetListener: BadRequest
    val myAssets = MutableLiveData<List<Asset.AssetItem>?>()
    var myOverlappedAssets: List<Asset.AssetItem> = emptyList()
    val overLappedBookings = MutableLiveData<List<BookingResponse.Booking>>()
    val loadAsset = MutableLiveData(false)
    val isLoading = MutableLiveData<Boolean>()
    val isAreaMapLoading = MutableLiveData(false)

    fun getAreaDetail(areaId: String) {
        isLoading.postValue(true)
        viewModelScope.launch {
            try {
                val response: Response<Area.AreaItem>? =
                    repository.getAreaDetailProduction(areaId)
                response?.let { response ->
                    if (response.isSuccessful) {
                        val body = response.body()
                        body?.let { body ->
                            withContext(Dispatchers.Main) {
                                area.postValue(body)
                            }
                        }
                        isLoading.postValue(false)
                    } else {
                        withContext(Dispatchers.Main) {
                            assetListener.fetchBadRequest(ErrorMessage.REQUEST_FAILED_ASSET_RESULT)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    assetListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getAssetOverlapped(
        buildingId: String,
        overlappedBookings: List<String>
    ) {
        var tempOverlappedBookings = overlappedBookings.joinToString(",") { "id($it)" }
        viewModelScope.launch {
            try {
                val response =
                    repository.getAssetsFromArea(buildingId, tempOverlappedBookings)
                response?.let { response ->
                    if (response.isSuccessful) {
                        val body = response.body()
                        body?.let { assets ->
                            withContext(Dispatchers.Main) {
                                myOverlappedAssets =
                                    (assets.filter { a -> a.properties?.owner == null })
                                loadAsset.postValue(true)
                            }
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            assetListener.fetchBadRequest(ErrorMessage.REQUEST_FAILED_ASSET_RESULT)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    assetListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getAssetByBuildingId(
        buildingId: String,
        spaceType: String,
        arrLabels: List<String>
    ) {
        viewModelScope.launch {
            try {
                val service: Response<List<Asset.AssetItem>>? = areaRepository.getAssetsProduction(
                    buildingId,
                    spaceType,
                    arrLabels
                )
                if (service != null) {
                    if (service.isSuccessful) {
                        val response = service.body()
                        response?.let { assets ->
                            withContext(Dispatchers.Main) {
                                myAssets.postValue(assets.filter { a ->
                                    a.properties?.owner == null && (a in myOverlappedAssets)
                                })
                            }
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            assetListener.fetchBadRequest(ErrorMessage.REQUEST_FAILED_ASSET_RESULT)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    assetListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun getOverlappedBookings(
        startTime: String,
        endTime: String,
    ) {
        viewModelScope.launch {
            try {
                val response = repository.getBookingsByDate(startTime, endTime)
                response?.let { response ->
                    if (response.isSuccessful) {
                        val body = response.body()
                        body?.let { bookings ->
                            withContext(Dispatchers.Main) {
                                overLappedBookings.postValue(bookings)
                            }
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    assetListener.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    fun checkWorkstationSchedules(
        assets: List<Asset.AssetItem>,
        startTime: LocalTime,
        endTime: LocalTime,
        dateSelected: LocalDate,
        buildingZoneId: ZoneId
    ): ArrayList<String> {
        val filteredAssets = assets.filter { assetItem ->
            var isInValidRange = true
            run breaking@{
                assetItem.policies?.schedule?.forEach { schedule ->
                    val scheduleStartTime = DateUtils.stringToLocalDateTime(
                        LocalDate.now(),
                        schedule.startTime,
                        buildingZoneId
                    ).toLocalTime()
                    val scheduleEndTime = DateUtils.stringToLocalDateTime(
                        LocalDate.now(),
                        schedule.endTime,
                        buildingZoneId
                    ).toLocalTime()
                    isInValidRange = startTime >= scheduleStartTime && endTime <= scheduleEndTime
                    if (isInValidRange) {
                        return@breaking
                    }
                }
            }
            val bookingWindow = assetItem.policies?.bookingWindow ?: 0
            val dayDiff = Period.between(LocalDate.now(), dateSelected).days
            if (dayDiff >= bookingWindow && bookingWindow != 0) {
                isInValidRange = false
            }
            return@filter isInValidRange
        }
        return ArrayList(filteredAssets.map { it.id ?: "" })
    }

    fun clearData() {
        overLappedBookings.value = emptyList()
        myAssets.postValue(null)
        loadAsset.value = false
        area.postValue(null)
    }
}