package com.jalasoft.bureau.notifications

import android.app.Notification
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.jalasoft.bureau.R
import com.jalasoft.bureau.ui.main.view.MainActivity
import com.jalasoft.bureau.utils.*
import kotlin.random.Random

class NotificationReceiver: BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val alarmSound: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val intentMain = Intent(context, MainActivity::class.java)
        val bookingId = intent.getStringExtra(BOOKING_ID)
        intentMain.putExtra(IS_NOTIFICATION, true)
        intentMain.putExtra(BOOKING_ID, bookingId)
        val title = intent.getStringExtra(BOOKING_TITLE)
        val content = intent.getStringExtra(BOOKING_DATE)
        val resultPendingIntent = PendingIntent.getActivity(
            context, NOTIFICATION_REQUEST_CODE, intentMain,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )

        val builder = NotificationCompat.Builder(context, NOTIFY_BUREAU)
            .setSmallIcon(R.drawable.ic_logo_image_only)
            .setColor(ContextCompat.getColor(context, R.color.bureau_main_color))
            .setContentTitle(title)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(content)
            )
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setAutoCancel(true)
            .setContentIntent(resultPendingIntent)
            .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)

        val notificationManager = NotificationManagerCompat.from(context)

        if (builder != null) {
            builder.setSound(alarmSound)
            notificationManager.notify(Random.nextInt(), builder.build())
        }
    }

}