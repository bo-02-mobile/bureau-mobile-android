package com.jalasoft.bureau.notifications

class BNotificationManager() {

    companion object {
        const val YOUR_BOOKING_IS_COMING = 0
        const val BOOKING_CANCELLATION_WARNING = 1
    }
    // endregion Properties
}