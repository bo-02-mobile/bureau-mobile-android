package com.jalasoft.bureau.widgets

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.SizeF
import android.widget.RemoteViews
import androidx.lifecycle.MutableLiveData
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.di.SharedPreferencesModule
import com.jalasoft.bureau.ui.bookings.repository.MainMyBookingsRepository
import com.jalasoft.bureau.ui.buildings.repository.BuildingProductionService
import com.jalasoft.bureau.ui.splashscreen.SplashActivity
import com.jalasoft.bureau.utils.*
import com.jalasoft.bureau.utils.login.AuthStateManager
import kotlinx.coroutines.*
import retrofit2.Response
import java.time.*
import java.time.format.DateTimeFormatter
import java.util.*
import javax.inject.Inject

open class BookingWidget: AppWidgetProvider() {

    @Inject @SharedPreferencesModule.ApplicationScope lateinit var applicationScope: CoroutineScope
    @Inject lateinit var repository: MainMyBookingsRepository
    private val job = SupervisorJob()
    private val coroutineScope = CoroutineScope(Dispatchers.IO + job)
    @Inject lateinit var mainMyBookingsRepository: MainMyBookingsRepository
    @Inject lateinit var buildingProductionService: BuildingProductionService

    var nonPermanentBookings: List<BookingResponse.Booking> = emptyList()
    var permanentBookings: List<BookingResponse.Booking> = emptyList()
    var bookingActivityListener: BadRequest? = null
    var buildingTimeZone: String? = null
    val myBuildings = MutableLiveData<List<Building.BuildingItem>>()
    val buildingId = MutableLiveData<String>()

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            getUpcomingBookings(context) {
                updateAppWidget(context, appWidgetManager, appWidgetId)
            }
        }
    }

    companion object {
        fun refreshWidget(context: Context) {
            val intentShortWidget = Intent(context, BookingWidget::class.java)
            val intentLargeWidget = Intent(context, LargeBookingWidget::class.java)
            intentShortWidget.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            intentLargeWidget.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            val ids: IntArray = AppWidgetManager.getInstance(context)
                .getAppWidgetIds(ComponentName(context, BookingWidget::class.java))
            val id2: IntArray = AppWidgetManager.getInstance(context)
                .getAppWidgetIds(ComponentName(context, LargeBookingWidget::class.java))
            intentLargeWidget.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, id2)
            intentShortWidget.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
            context.applicationContext.sendBroadcast(intentShortWidget)
            context.applicationContext.sendBroadcast(intentLargeWidget)
        }
    }

    override fun onAppWidgetOptionsChanged(
        context: Context?,
        appWidgetManager: AppWidgetManager?,
        appWidgetId: Int,
        newOptions: Bundle?
    ) {
        // Called when Widget is resized
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions)
        appWidgetManager?.getAppWidgetOptions(appWidgetId)?.let { bundle ->
            context?.let { context ->
                val width = bundle.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH)
                getUpcomingBookings(context) {
                    updateAppWidget(context, appWidgetManager, appWidgetId, width)
                }
            }
        }
    }

    private fun updateAppWidget(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetId: Int,
        width: Int = 200
    ) {
        var today = nonPermanentBookings
        var upcoming = permanentBookings
        if (AuthStateManager.getInstance(context).current.accessToken == null) {
            today = emptyList()
            upcoming = emptyList()
        }

        var booking: BookingResponse.Booking? = null
        var booking2: BookingResponse.Booking? = null
        if (today.isEmpty()) {
            if (upcoming.isNotEmpty()) {
                booking = upcoming[0]
                if (upcoming.size > 1) booking2 = upcoming[1]
            }
        } else {
            booking = today[0]
            if (today.size > 1) booking2 = today[1]
            else if (upcoming.isNotEmpty()) booking2 = upcoming[0]
        }

        val formatter = DateTimeFormatter.ofPattern(DATETIME_FORMAT, Locale.ENGLISH)
        val assetCode1 = booking?.assetCode ?: ""
        val assetCode2 = booking2?.assetCode ?: ""
        val assetName1 = booking?.areaName ?: ""
        val assetName2 = booking2?.areaName ?: ""
        val startTime = booking?.startTime?.let { dateTime ->
            LocalDateTime.ofInstant(
                Instant.parse(dateTime),
                getBuildingZoneId()
            )
        }
        val endTime = booking?.endTime?.let { dateTime ->
            LocalDateTime.ofInstant(
                Instant.parse(dateTime),
                getBuildingZoneId()
            )
        }
        val hourRange = startTime?.let { startTime ->
            endTime?.let { endTime ->
                DateUtils.hourRangeFormat(startTime, endTime)
            }
        }
        val startTime2 = booking2?.startTime?.let { dateTime ->
            LocalDateTime.ofInstant(
                Instant.parse(dateTime),
                getBuildingZoneId()
            )
        }
        val endTime2 = booking2?.endTime?.let { dateTime ->
            LocalDateTime.ofInstant(
                Instant.parse(dateTime),
                getBuildingZoneId()
            )
        }
        val hourRange2 = startTime2?.let { startTime ->
            endTime2?.let { endTime ->
                DateUtils.hourRangeFormat(startTime, endTime)
            }
        }
        val todayText = if (DateUtils.isToday(startTime)) TODAY_TEXT else null
        val today2Text = if (DateUtils.isToday(startTime2)) TODAY_TEXT else null

        // Construct the RemoteViews object
        val views = RemoteViews(context.packageName, R.layout.booking_widget)

        views.setTextViewText(R.id.assetText, assetCode1)
        if (booking?.isPermanent == true) {
            views.setTextViewText(R.id.titleText, TODAY_TEXT)
            views.setTextViewText(R.id.timeText, PERMANENT_BOOKING)
        } else {
            views.setTextViewText(
                R.id.titleText,
                todayText ?: startTime?.format(formatter) ?: NO_BOOKINGS_TITLE
            )
            views.setTextViewText(R.id.timeText, hourRange ?: "")
        }

        val largeViews = RemoteViews(context.packageName, R.layout.booking_widget_large)
        val title = if (startTime != null) R.string.my_bookings else R.string.no_bookings
        val time1 =
            if (startTime != null) (todayText ?: startTime.format(formatter)) + " from " + hourRange
            else ""
        val time2 =
            if (startTime2 != null) (today2Text
                ?: startTime2.format(formatter)) + " from " + hourRange2
            else ""
        val hyphen1 = if (assetName1 != "") " - " else ""
        val hyphen2 = if (assetName2 != "") " - " else ""
        largeViews.setTextViewText(R.id.titleText, title.toString())
        largeViews.setTextViewText(R.id.assetText, assetCode1 + hyphen1 + assetName1)
        largeViews.setTextViewText(R.id.asset2Text, assetCode2 + hyphen2 + assetName2)
        if (booking?.isPermanent == true) {
            largeViews.setTextViewText(R.id.timeText, PERMANENT_BOOKING)
        } else {
            largeViews.setTextViewText(R.id.timeText, time1)
        }
        if (booking2?.isPermanent == true) {
            largeViews.setTextViewText(R.id.time2Text, PERMANENT_BOOKING)
        } else {
            largeViews.setTextViewText(R.id.time2Text, time2)
        }
        // Set PendingIntent on buttons
        val prefs = Prefs(context)
        val ownerEmail = prefs.getData(SHARED_USER_EMAIL) ?: ""
        if (ownerEmail.isNotBlank()) {
            val intent = Intent(context, SplashActivity::class.java)
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
            intent.putExtra(WIDGET_ACTION, true)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.data = Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME))
            val pendingIntent = PendingIntent.getActivity(
                context,
                (0..appWidgetId).random(),
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT or PendingIntent.FLAG_MUTABLE
            )
            views.setOnClickPendingIntent(R.id.buttonBooking, pendingIntent)
            largeViews.setOnClickPendingIntent(R.id.buttonBooking, pendingIntent)
        }

        val viewMapping: Map<SizeF, RemoteViews> = mapOf(
            SizeF(WIDGET_WIDTH_SMALL, WIDGET_HEIGHT) to views,
            SizeF(WIDGET_WIDTH_LARGE, WIDGET_HEIGHT) to largeViews
        )

        // Instruct the widget manager to update the widget
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            appWidgetManager.updateAppWidget(appWidgetId, RemoteViews(viewMapping))
        } else {
            if (width < WIDGET_WIDTH_LARGE) appWidgetManager.updateAppWidget(appWidgetId, views)
            else appWidgetManager.updateAppWidget(appWidgetId, largeViews)
        }
    }

    private fun getUpcomingBookings(context: Context?, onSuccess: (bookings: List<BookingResponse.Booking>) -> Unit) {
        context?.let { safeContext ->
            val prefs = Prefs(safeContext)
            val ownerEmail = prefs.getData(SHARED_USER_EMAIL) ?: ""
            if (ownerEmail.isEmpty()) return@let
            getBuildings {
                myBuildings.postValue(it)
                getAllBookings(
                    ownerEmail,
                    myBuildings.value,
                    onSuccess
                )
            }
        }
    }

    private fun goToBooking(booking: BookingResponse.Booking) {}

    private fun getAllBookings(
        ownerEmail: String,
        buildings: List<Building.BuildingItem>?,
        onSuccess: (bookings: List<BookingResponse.Booking>) -> Unit
    ) {
        nonPermanentBookings = emptyList()
        permanentBookings = emptyList()
        applicationScope.launch(Dispatchers.IO) {
            try {
                val date = LocalDate.now()
                val time = LocalTime.now()
                val formatter = DateTimeFormatter.ofPattern(API_DATE_FORMAT)
                val dateTime = ZonedDateTime.of(date, time, ZoneId.systemDefault()).format(formatter)
                val endTime = ZonedDateTime.of(date.plusDays(1), time, ZoneId.systemDefault()).format(formatter)
                val apiResponse1 = async { repository.getBookingsProduction(dateTime, endTime, ownerEmail) }
                val apiResponse2 = async { repository.getUpcommingBookings(endTime, ownerEmail) }
                val apiResponse3 = async { repository.getPermanentBookings(ownerEmail) }
                val firstResponse = apiResponse1.await()
                val  secondResponse = apiResponse2.await()
                val thirdResponse = apiResponse3.await()
                nonPermanentBookings = (firstResponse?.body()?.sortedBy { it.startTime } ?: emptyList()).distinctBy { it.recurrenceId }
                val upcommingBookings =  secondResponse?.body()?.sortedBy { it.startTime } ?: emptyList()
                val permanentAssets = thirdResponse?.body()?.sortedBy { it.policies?.schedule?.get(0)?.startTime } ?: emptyList()
                async { convertAssetPermanentToBookings(permanentAssets) }.await()
                val bookingsList = nonPermanentBookings + permanentBookings + upcommingBookings

                val filteredBookings = mutableListOf<BookingResponse.Booking>()
                buildings?.forEach { buildingItem ->
                    filteredBookings.addAll(bookingsList.filter { it.buildingId == buildingItem.id })
                }
                onSuccess(bookingsList)
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    bookingActivityListener?.fetchBadRequest(ErrorMessage.CONNECTION_FAILED)
                }
            }
        }
    }

    private fun getBuildingZoneId(): ZoneId {
        val timeZoneString =
            if (!buildingTimeZone.isNullOrBlank()) buildingTimeZone else myBuildings.value?.find { it.id == buildingId.value }?.timezone
        timeZoneString?.also {
            return ZoneId.of(it).normalized()
        }
        return ZoneId.systemDefault()
    }

    private suspend fun convertAssetPermanentToBookings(permanentAssets: List<Asset.AssetItem>) {
        val tempPermanentBookings = ArrayList<BookingResponse.Booking>()
        permanentAssets.forEach { asset ->
            val areaRequest = repository.getAreaDetailByAreaId(asset.areaId!!)
            val areaName = areaRequest?.body()?.name ?: ""
            tempPermanentBookings.add(
                BookingResponse.Booking(
                    asset.areaId,
                    areaName, asset.code!!, asset.id!!, asset.properties?.type,
                    asset.buildingId!!, null, asset.createdAt!!, asset.createdAt,
                    asset.id, true, false, asset.properties?.owner,
                    null, asset.createdAt, asset.updatedAt!!, 1, null, null, null
                )
            )
        }
        permanentBookings = tempPermanentBookings
    }

    private fun getBuildings(onSuccess: (buildings: List<Building.BuildingItem>) -> Unit) {
        coroutineScope.launch {
            try {
                val service: Response<List<Building.BuildingItem>> =
                    buildingProductionService.getBuildingsProduction() ?: return@launch
                if (service.isSuccessful) {
                    val response = service.body()
                    response?.let {
                        withContext(Dispatchers.Main) {
                            val publicBuildings = it.filter { it.public == true }
                            onSuccess(publicBuildings)
                        }
                    }
                }
            } catch (ex: Exception) {
                return@launch
            }
        }
    }
}
