package com.jalasoft.bureau.widgets

import dagger.hilt.android.AndroidEntryPoint

// Class necessary to allow both Bookings Widgets to appear from selection separately

@AndroidEntryPoint
class LargeBookingWidget: BookingWidget()