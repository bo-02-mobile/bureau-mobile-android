package com.jalasoft.bureau.di

import com.jalasoft.bureau.notifications.BNotificationManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ManagerModule {

    @Singleton
    @Provides
    fun provideBNotificationManager(): BNotificationManager {
        return BNotificationManager()
    }

}