package com.jalasoft.bureau.di

import com.jalasoft.bureau.ui.bookings.repository.MainMyBookingsRepository
import com.jalasoft.bureau.ui.bookings.repository.MyBookingsProductionService
import com.jalasoft.bureau.ui.filter.repository.FilterRepository
import com.jalasoft.bureau.ui.scanner.repository.IQRScannerRepository
import com.jalasoft.bureau.ui.scanner.repository.QRScannerProductionService
import com.jalasoft.bureau.ui.scanner.repository.QRScannerRepository
import com.jalasoft.bureau.ui.workstations.repository.AssetDetailProductionService
import com.jalasoft.bureau.ui.workstations.repository.AssetDetailRepository
import com.jalasoft.bureau.ui.workstations.repository.AssetResultRepository
import com.jalasoft.bureau.ui.workstations.repository.EventService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {
    @Singleton
    @Provides
    fun provideFilterRepository(@RetrofitModule.OtherInterceptorOkHttpClient retrofit: Retrofit): FilterRepository {
        return FilterRepository(retrofit)
    }

    @Singleton
    @Provides
    fun provideAssetDetailRepository(@RetrofitModule.OtherInterceptorOkHttpClient retrofit: Retrofit, assetDetailProductionService: AssetDetailProductionService ): AssetDetailRepository {
        return AssetDetailRepository(retrofit, assetDetailProductionService)
    }

    @Singleton
    @Provides
    fun provideAssetResultRepository(@RetrofitModule.OtherInterceptorOkHttpClient retrofit: Retrofit): AssetResultRepository {
        return AssetResultRepository(retrofit)
    }

    @Singleton
    @Provides
    fun provideMainMyBookingsRepository(myBookingsProductionService: MyBookingsProductionService, eventService: EventService): MainMyBookingsRepository {
        return MainMyBookingsRepository(myBookingsProductionService, eventService)
    }

    @Singleton
    @Provides
    fun provideQRScannerRepository(qrScannerProductionService: QRScannerProductionService): IQRScannerRepository {
        return QRScannerRepository(qrScannerProductionService)
    }
}