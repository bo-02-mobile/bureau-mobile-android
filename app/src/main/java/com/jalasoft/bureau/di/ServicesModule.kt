package com.jalasoft.bureau.di

import com.jalasoft.bureau.ui.areas.repository.AreaProductionService
import com.jalasoft.bureau.ui.bookings.repository.MyBookingsProductionService
import com.jalasoft.bureau.ui.buildings.repository.BuildingProductionService
import com.jalasoft.bureau.ui.favorites.repository.FavoritesProductionService
import com.jalasoft.bureau.ui.login.repository.UserService
import com.jalasoft.bureau.ui.scanner.repository.QRScannerProductionService
import com.jalasoft.bureau.ui.workstations.repository.AssetDetailProductionService
import com.jalasoft.bureau.ui.workstations.repository.BookingAvailabilityService
import com.jalasoft.bureau.ui.workstations.repository.EventService
import com.jalasoft.bureau.ui.workstations.repository.WorkstationResultsProductionService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ServicesModule {

    @Singleton
    @Provides
    fun provideAreaProductionService(@RetrofitModule.OtherInterceptorOkHttpClient retrofit: Retrofit): AreaProductionService {
        return AreaProductionService(retrofit)
    }

    @Singleton
    @Provides
    fun provideMyBookingsProductionService(@RetrofitModule.OtherInterceptorOkHttpClient retrofit: Retrofit): MyBookingsProductionService {
        return MyBookingsProductionService(retrofit)
    }

    @Singleton
    @Provides
    fun provideBuildingProductionService(@RetrofitModule.OtherInterceptorOkHttpClient retrofit: Retrofit): BuildingProductionService {
        return BuildingProductionService(retrofit)
    }

    @Singleton
    @Provides
    fun provideFavoritesProductionService(@RetrofitModule.OtherInterceptorOkHttpClient retrofit: Retrofit): FavoritesProductionService {
        return FavoritesProductionService(retrofit)
    }

    @Singleton
    @Provides
    fun provideUserService(
        @RetrofitModule.AuthInterceptorOkHttpClient retrofitAuth: Retrofit,
        @RetrofitModule.OtherInterceptorOkHttpClient retrofit: Retrofit
    ): UserService {
        return UserService(retrofitAuth, retrofit)
    }

    @Singleton
    @Provides
    fun provideQRScannerProductionService(@RetrofitModule.OtherInterceptorOkHttpClient retrofit: Retrofit): QRScannerProductionService {
        return QRScannerProductionService(retrofit)
    }

    @Singleton
    @Provides
    fun provideAssetDetailProductionService(@RetrofitModule.OtherInterceptorOkHttpClient retrofit: Retrofit): AssetDetailProductionService {
        return AssetDetailProductionService(retrofit)
    }

    @Singleton
    @Provides
    fun provideBookingAvailabilityService(@RetrofitModule.OtherInterceptorOkHttpClient retrofit: Retrofit): BookingAvailabilityService {
        return BookingAvailabilityService(retrofit)
    }

    @Singleton
    @Provides
    fun provideEventService(@RetrofitModule.OtherInterceptorOkHttpClient retrofit: Retrofit): EventService {
        return EventService(retrofit)
    }

    @Singleton
    @Provides
    fun provideWorkstationResultsProductionService(@RetrofitModule.OtherInterceptorOkHttpClient retrofit: Retrofit): WorkstationResultsProductionService {
        return WorkstationResultsProductionService(retrofit)
    }
}