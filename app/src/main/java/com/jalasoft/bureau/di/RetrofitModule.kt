package com.jalasoft.bureau.di

import android.content.Context
import android.content.Intent
import com.jalasoft.bureau.BuildConfig
import com.jalasoft.bureau.ui.login.view.LoginActivity
import com.jalasoft.bureau.ui.main.view.MainActivity
import com.jalasoft.bureau.utils.GlobalConstants
import com.jalasoft.bureau.utils.login.AuthStateManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import net.openid.appauth.AuthorizationService
import net.openid.appauth.ClientSecretBasic
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RetrofitModule  {

    @Provides
    fun provideOkHttpClient(@ApplicationContext context: Context): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor { chain ->
                var request = chain.request()
                request = request.newBuilder().header("Authorization", GlobalConstants.getUserToken(context)).build()
                chain.proceed(request)
            }
            .addInterceptor { chain ->
                var request = chain.request()
                request = if (GlobalConstants.hasNetwork(context))
                    request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build()
                else
                    request.newBuilder().header(
                        "Cache-Control",
                        "public, only-if-cached, max-stale=" + 604800
                    ).build()
                chain.proceed(request)
            }
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .addInterceptor { chain ->
                val request = chain.request()
                val response = chain.proceed(request)
                if ( (response.code == 401 || response.code == 403) && AuthStateManager.getInstance(context).current.needsTokenRefresh) {
                    val authService = AuthorizationService(context)
                    val clientAuth = ClientSecretBasic(BuildConfig.LOGIN_CLIENT_SECRET)
                    authService.performTokenRequest(
                        AuthStateManager.getInstance(context).current.createTokenRefreshRequest(),
                        clientAuth
                    ) { resp, ex ->
                        AuthStateManager.getInstance(context).updateAfterTokenResponse(resp, ex)
                        if (resp != null) {
                            response.close()
                        } else {
                            (context as MainActivity).finish()
                            context.startActivity(Intent(context, LoginActivity::class.java))
                        }
                    }
                }
                return@addInterceptor response
            }
            .build()
    }

    @OtherInterceptorOkHttpClient
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
            return Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    @AuthInterceptorOkHttpClient
    @Provides
    fun getAuthRetrofitInstance(okHttpClient: OkHttpClient, @ApplicationContext context: Context): Retrofit {
        val userInfoEndpoint =
            AuthStateManager.getInstance(context).current.authorizationServiceConfiguration?.discoveryDoc?.userinfoEndpoint
        val url = String.format("%s%s", userInfoEndpoint, "/")
        return Retrofit.Builder()
            .baseUrl(url)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class AuthInterceptorOkHttpClient

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class OtherInterceptorOkHttpClient
}