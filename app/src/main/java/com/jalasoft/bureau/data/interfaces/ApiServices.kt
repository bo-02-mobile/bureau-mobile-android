package com.jalasoft.bureau.data.interfaces

import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.asset.AssetLabel
import com.jalasoft.bureau.data.model.asset.AssetType
import com.jalasoft.bureau.data.model.booking.*
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.data.model.favorites.UserFav
import com.jalasoft.bureau.data.model.scanner.BookingConfirmRequestBody
import com.jalasoft.bureau.data.model.scanner.BookingConfirmResponse
import com.jalasoft.bureau.data.model.token.User
import com.jalasoft.bureau.data.model.user.Groups
import com.jalasoft.bureau.data.model.workstation.Workstation
import com.jalasoft.bureau.utils.ApiUrl
import retrofit2.Response
import retrofit2.http.*

interface BuildingService {

    @GET("areas")
    suspend fun getAreasFromABuildingAuthorization(
        @Query("filters", encoded = true) buildingId: String,
        @Query("filters", encoded = true) public: String,
    ): Response<List<Area.AreaItem>>

    @GET("buildings")
    suspend fun getBuildingsAuthorization(
    ): Response<List<Building.BuildingItem>>

    @GET("buildings/{buildingId}")
    suspend fun getBuildingById(
        @Path("buildingId") buildingId: String,
    ): Response<Building.BuildingItem>
}

interface AssetService {

    @GET("assets/types")
    suspend fun getAssetsType(
        @Query("sorts", encoded = true) sort: String,
    ): Response<List<AssetType.AssetTypeItem>>

    @GET("assets/labels")
    suspend fun getAssetsLabels(
    ): Response<List<AssetLabel>>

    @GET("assets")
    suspend fun getAssetsProduction(
        @Query("filters", encoded = true) buildingId: String,
        @Query("filters", encoded = true) types: String?,
        @Query("filters", encoded = true) labels: List<String>,
        @Query("filters", encoded = true) public: String?,
    ): Response<List<Asset.AssetItem>>

    @GET("assets/{assetId}")
    suspend fun getAssetByIdProduction(
        @Path("assetId") assetId: String,
    ): Response<Asset.AssetItem>

    @GET("assets")
    suspend fun getAssetsByBuildingId(
        @Query("filters", encoded = true) buildingId: String,
        @Query("filters", encoded = true) overLappedBookings: String?,
        @Query("filters", encoded = true) types: String?,
        @Query("filters", encoded = true) labels: List<String>,
        @Query("filters", encoded = true) public: String?,
        @Query("sorts", encoded = true) sortName: String?,
    ): Response<List<Asset.AssetItem>>

    @GET("assets")
    suspend fun getAssetsFromArea(
        @Query("filters", encoded = true) buildingId: String,
        @Query("filters", encoded = true) overLappedBookings: String?,
        @Query("filters", encoded = true) public: String?,
    ): Response<List<Asset.AssetItem>>
}

interface AreaService {
    @GET("areas/{areaId}")
    suspend fun getAreaDetail(
        @Path("areaId") areaId: String,
        @Query("filters", encoded = true) public: String?,
    ): Response<Area.AreaItem>
}

interface WorkstationService {

    @GET("areas/{areaId}/workstations")
    suspend fun getWorkstations(
        @Path("areaId") areaId: String,
        @Query("StartTime") startTime: String,
        @Query("EndTime") endTime: String
    ): Response<List<Workstation.WorkstationItem>>

    @GET("areas/{areaId}/layout")
    suspend fun getWorkstationsLayout(
        @Path("areaId") areaId: String,
    ): Response<Workstation.WorkstationLayout>
}

interface BookingService {

    @GET("bookings/overlapped")
    suspend fun getBookingsByDate(
        @Query("StartTime") startTime: String,
        @Query("EndTime") endTime: String,
        @Query("filters", encoded = true) assetId: String,
        @Query("filters", encoded = true) userCancelled: String,
        @Query("filters", encoded = true) systemCancelled: String,
    ): Response<ArrayList<BookingResponse.Booking>>

    @GET("bookings/overlapped")
    suspend fun getOverlappedBookings(
        @Query("StartTime") startTime: String,
        @Query("EndTime") endTime: String,
        @Query("filters", encoded = true) userCancelled: String,
        @Query("filters", encoded = true) systemCancelled: String,
    ): Response<ArrayList<BookingResponse.Booking>>
}

interface MyBookingsService {
    @GET("bookings/groupByRecurrence")
    suspend fun getMyBookings(
        @Query("filters", encoded = true) startTime: String,
        @Query("filters", encoded = true) endTime: String,
        @Query("filters", encoded = true) ownerEmail: String,
        @Query("filters", encoded = true) userCancelled: String,
        @Query("filters", encoded = true) systemCancelled: String,
    ): Response<List<BookingResponse.Booking>>

    @GET("bookings/groupByRecurrence")
    suspend fun getUpcomingBookings(
        @Query("filters", encoded = true) startTime: String,
        @Query("filters", encoded = true) ownerEmail: String,
        @Query("filters", encoded = true) userCancelled: String,
        @Query("filters", encoded = true) systemCancelled: String,
    ): Response<List<BookingResponse.Booking>>

    @GET("Assets")
    suspend fun getMyPermanentBookings(
        @Query("filters", encoded = true) ownerEmail: String,
    ): Response<List<Asset.AssetItem>>

    @DELETE("bookings/{bookingId}")
    suspend fun deleteBookingProduction(
        @Path("bookingId") bookingId: String,
    ): Response<BookingResponse.Booking>
}

interface IBookingAvailability {
    @GET("workstations/{workstationId}/availability")
    suspend fun getBookingAvailability(
        @Path("workstationId") workstationId: String,
        @Query("StartTime") startTime: String,
        @Query("EndTime") endTime: String
    ): Response<BookingAvailability>

    @POST("workstations/{workstationId}/bookings")
    suspend fun postBooking(
        @Path("workstationId") workstationId: String,
        @Body body: BookingBody
    ): Response<BookingResponse.Booking>

    @POST("bookings")
    suspend fun postNewBooking(
        @Body body: PostBooking
    ): Response<BookingResponse.Booking>

    @GET("bureausettings/initialconfig")
    suspend fun getBookingConfiguration(
    ): Response<BookingConfiguration>
}

interface BookingConfirmService {
    @PATCH(ApiUrl.BOOKING_BY_ID)
    suspend fun putWorkstationConfirm(
        @Path("bookingId") bookingId: String,
        @Body requestBody: List<BookingConfirmRequestBody>
    ): Response<BookingConfirmResponse>
}

interface LoginService {
    @GET(".")
    suspend fun getUserInfo(
    ): Response<User>

    @GET("users")
    suspend fun getUsersInfo(
    ): Response<List<User>>

    @GET("groups")
    suspend fun getGroups(
        @Query("filters", encoded = true) userGroups: String?
    ): Response<List<Groups.Group>>
}

interface FavoriteService {
    @GET("users/me")
    suspend fun getUserInfo(
    ): Response<UserFav>

    @GET("assets")
    suspend fun getFavoritesAssets(
        @Query("filters", encoded = true) assets: String?,
        @Query("filters", encoded = true) public: String?,
        @Query("sorts", encoded = true) sort: String,
    ): Response<List<Asset.AssetItem>>

    @POST("users/favorites/{idAsset}")
    suspend fun postFavoriteAsset(
        @Path("idAsset") idAsset: String
    ): Response<UserFav>

    @DELETE("users/favorites/{idAsset}")
    suspend fun deleteFavoriteAsset(
        @Path("idAsset") idAsset: String
    ): Response<UserFav>
}