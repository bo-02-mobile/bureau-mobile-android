package com.jalasoft.bureau.data.model.asset

data class AssetLabel(
    val id: String?,
    val name: String?,
    val text: String?,
    val value: String?
)
