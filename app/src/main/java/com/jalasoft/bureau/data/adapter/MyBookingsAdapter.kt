package com.jalasoft.bureau.data.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.interfaces.MyBookingsListener
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.databinding.ItemBookingBinding
import com.jalasoft.bureau.ui.bookings.viewmodel.MyBookingsViewModel
import com.jalasoft.bureau.utils.*
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId


class MyBookingsAdapter(
    val list: List<BookingResponse.Booking>,
    var viewModel: MyBookingsViewModel,
    private var listener: MyBookingsListener,
    private var isToday: Boolean,
    private val highlightedBookId: String?,
) : RecyclerView.Adapter<MyBookingsAdapter.ListViewHolder>() {

    lateinit var context: Context
    lateinit var prefs: Prefs

    class ListViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val myBookingItemBinding = ItemBookingBinding.bind(view)
        val cardView = myBookingItemBinding.myBookingContainer
        val code = myBookingItemBinding.workstationAreaCodeTV
        val date = myBookingItemBinding.dateTV
        val type = myBookingItemBinding.assetTypeTV
        val qrButton = myBookingItemBinding.qrButton
        val qrArea = myBookingItemBinding.qrArea
        val isValidated = myBookingItemBinding.validatedTV
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        context = parent.context
        prefs = Prefs(context)
        return ListViewHolder(layoutInflater.inflate(R.layout.item_booking, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val nowDate = LocalDateTime.now()
        if (!isToday) {
            holder.qrArea.visibility = View.GONE
        }
        val item = list[position]
        viewModel.getBuildingByBuildingId(item.buildingId) { buildingTimezone ->
            val title = item.assetCode
            var isBookingValidationInRange = false
            var bookingRangeHourText = ""
            val responseDate = LocalDateTime.ofInstant(
                Instant.parse(item.startTime),
                ZoneId.systemDefault()
            )
            val bookingStartTime = LocalDateTime.ofInstant(
                Instant.parse(item.startTime),
                ZoneId.of(buildingTimezone)
            )
            val assetType = item.assetType ?: WORKSTATION_LABEL
            holder.code.text = title
            if (item.isPermanent == true) {
                holder.date.text = PERMANENT_BOOKING
            } else {
                holder.date.text = StringUtils.capitalize(item.reservationMessage ?: "")
            }
            holder.type.text = StringUtils.capitalize(assetType).replace("-", " ")
            if (item.confirmedAt != null || item.isPermanent == true) {
                holder.isValidated.text = context.resources.getString(R.string.validated)
                holder.isValidated.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.bureau_secondary_color
                    )
                )
                holder.qrButton.setImageResource(R.drawable.ic_nc_vqrcode)
                holder.qrButton.isEnabled = false
                holder.isValidated.isEnabled = false
            } else {
                val limitBeforeDeadLine = prefs.getData(SHARED_BOOKING_LIMIT_BEFORE_DEADLINE)
                    ?: NOTIFICATION_DEFAULT_RANGE
                val limitAfterDeadLine =
                    prefs.getData(SHARED_BOOKING_LIMIT_AFTER_DEADLINE) ?: NOTIFICATION_DEFAULT_RANGE
                isBookingValidationInRange = nowDate.isAfter(responseDate.minusMinutes(limitBeforeDeadLine.toLong())) && nowDate.isBefore( responseDate.plusMinutes(limitAfterDeadLine.toLong()))
                bookingRangeHourText = viewModel.formatRangeHour(bookingStartTime, limitBeforeDeadLine, limitAfterDeadLine, context)
                if ( isBookingValidationInRange ) {
                    holder.qrButton.setImageResource(R.drawable.ic_nc_qrcode)
                    holder.qrButton.isEnabled = true
                    holder.isValidated.isEnabled = true
                } else {
                    holder.isValidated.text = bookingRangeHourText
                    holder.isValidated.gravity = Gravity.CENTER
                    holder.isValidated.textSize = 12.0F
                    holder.qrButton.setImageResource(0)
                    holder.qrButton.isEnabled = false
                    holder.isValidated.isEnabled = false
                }
            }
            //NAVIGATE
            holder.isValidated.setOnClickListener {
                listener.didQrButtonPressed()
            }

            holder.qrButton.setOnClickListener {
                listener.didQrButtonPressed()
            }

            holder.cardView.setOnClickListener {
                listener.didBookingSelected(
                    BookingResponse.BookingBundle(
                        item.assetCode,
                        item.assetId,
                        item.confirmedAt,
                        item.startTime,
                        item.endTime,
                        item.id,
                        item.buildingId,
                        item.assetType,
                        isToday,
                        item.reservationMessage,
                        item.isPermanent,
                        item.areaName,
                        isBookingValidationInRange,
                        bookingRangeHourText
                    )
                )
            }

            if (list[position].id == highlightedBookId) {
                val savedColor = holder.cardView.backgroundTintList
                Handler(Looper.getMainLooper()).postDelayed({
                    holder.cardView.backgroundTintList = ColorStateList.valueOf(
                        (ContextCompat.getColor(
                            context,
                            R.color.medium_gray
                        ))
                    )
                }, VERY_SHORT)
                Handler(Looper.getMainLooper()).postDelayed({
                    holder.cardView.backgroundTintList = savedColor
                    listener.didAnimationCompleted()
                }, SHORT)
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}