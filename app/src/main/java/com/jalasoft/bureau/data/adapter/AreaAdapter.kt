package com.jalasoft.bureau.data.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.interfaces.NavigationListener
import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.databinding.AreaItemBinding

class AreaAdapter(
    var myAreas: List<Area.AreaItem>,
    private var listener: NavigationListener,
    private var buildingName: String,
    private var assets: List<Asset.AssetItem>,
    var selected: String
) :
    RecyclerView.Adapter<AreaAdapter.BuildingHolder>() {
    var context: Context? = null
    class BuildingHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val areaItemBinding = AreaItemBinding.bind(view)

        @SuppressLint("SetTextI18n")
        fun render(areaItem: Area.AreaItem, buildingName: String, sizeAvailable: Int) {

            areaItemBinding.tvAreaName.text = areaItem.name
            if (sizeAvailable >= 1) {
                val suffix = if (sizeAvailable == 1) view.context.getString(R.string.area_space)
                else view.context.getString(R.string.area_spaces)
                areaItemBinding.tvAreaCount.text = "$sizeAvailable $suffix"
                areaItemBinding.tvAreaCount.setTextColor(view.context.getColor(R.color.area_available))
            } else {
                areaItemBinding.tvAreaCount.text = view.context.getString(R.string.area_no_spaces)
                areaItemBinding.tvAreaCount.setTextColor(view.context.getColor(R.color.area_not_available))
            }
            areaItemBinding.tvAreaBuildingName.text = buildingName
        }
    }

    fun setAreasOfABuilding(areas: List<Area.AreaItem>, assets: List<Asset.AssetItem>, filter:Boolean) {
        areas.forEach {
            it.availableSpace = assets.filter { item -> item.areaId == it.id }.size
        }
        if (filter){
            this.myAreas = areas.sortedByDescending { it.availableSpace }
        } else{
            this.myAreas = areas
        }
        this.assets = assets
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BuildingHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        context = parent.context
        return BuildingHolder(layoutInflater.inflate(R.layout.area_item, parent, false))
    }

    override fun onBindViewHolder(holder: BuildingHolder, position: Int) {
        val current = myAreas[position]
        val sizeAvailable = this.assets.filter { item -> item.areaId == current.id }.size
        holder.render(current, buildingName, sizeAvailable)
        holder.areaItemBinding.cardView.setOnClickListener {
            listener.didAreaSelected(current.id.toString(), current.name.toString())
        }
        if(context?.resources?.getBoolean(R.bool.isTablet) == true){
            if (selected == current.id) {
                holder.areaItemBinding.cardInfoContainer?.setBackgroundColor(context?.resources?.getColor(R.color.main_color)
                    ?: Color.LTGRAY)
                holder.areaItemBinding.tvAreaName.setTextColor(Color.WHITE)
                holder.areaItemBinding.tvAreaBuildingName.setTextColor(context?.resources?.getColor(R.color.area_item_address_selected) ?: Color.LTGRAY)
                holder.areaItemBinding.tvAreaCount.setTextColor(Color.WHITE)
                holder.areaItemBinding.separatorLine?.visibility = View.INVISIBLE
            } else {
                holder.areaItemBinding.cardInfoContainer?.setBackgroundColor(context?.resources?.getColor(R.color.surface_high)
                    ?: Color.LTGRAY)
                holder.areaItemBinding.tvAreaName.setTextColor(context?.resources?.getColor(R.color.text_primary) ?: Color.LTGRAY)
                holder.areaItemBinding.tvAreaBuildingName.setTextColor(context?.resources?.getColor(R.color.text_secondary) ?: Color.LTGRAY)
                holder.areaItemBinding.separatorLine?.visibility = View.VISIBLE
            }
        }
    }

    override fun getItemCount(): Int = myAreas.size
}