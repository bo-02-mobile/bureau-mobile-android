package com.jalasoft.bureau.data.model.scanner

data class BookingConfirmRequestBody(
    val op: String,
    val path: String,
    val value: String
)
