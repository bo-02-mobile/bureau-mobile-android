package com.jalasoft.bureau.data.model.token

import net.openid.appauth.AuthorizationException
import net.openid.appauth.TokenResponse

class AccessTokenResponse(val resp: TokenResponse?, val ex: AuthorizationException?)