package com.jalasoft.bureau.data.adapter

import android.content.Context
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.interfaces.FavoritesListener
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.databinding.ItemFavoritesBinding
import com.jalasoft.bureau.utils.*

class FavoritesAdapter(
    var list: List<Asset.AssetItem>,
    private var listener: FavoritesListener,
    private var resources: Resources?
): RecyclerView.Adapter<FavoritesAdapter.ListViewHolder>() {


    private var context: Context? = null

    class ListViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        private val favoriteItemBinding = ItemFavoritesBinding.bind(v)
        val cardView = favoriteItemBinding.favoritesContainer
        val code = favoriteItemBinding.workstationAreaCodeTV
        val assetInfo = favoriteItemBinding.assetInfoTV
        val heartButton = favoriteItemBinding.heartButton
        val available = favoriteItemBinding.availableTV
        var assetId: String? = ""
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        context = parent.context
        return ListViewHolder(
            layoutInflater.inflate(
                R.layout.item_favorites,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val item = list[position]
        val title = item.code
        val assetType = item.properties?.type ?: WORKSTATION_LABEL
        val tempSpaceList: List<String>? = item.properties?.labels
        var tempAssetFav = ""
        tempSpaceList?.forEachIndexed { index, spaceType ->
            if (index == 0) tempAssetFav += " | ${spaceType}"
            else tempAssetFav += " - ${spaceType}"
        }
        holder.code.text = title
        holder.assetInfo.text = StringUtils.capitalize(assetType).replace("-", " ") + tempAssetFav
        holder.available.resources.getColor(R.color.main_color)
        holder.assetId = item.id

        val hasOwner = item.properties?.owner
        when {
            hasOwner != null -> {
                holder.available.text = context?.getString(R.string.favoritePermanentAsset)
                context?.let {
                    resources?.let {
                        it.getColor(R.color.text_unavailable).let { holder.available.setTextColor(it) }
                    }
                }
            }
            item.available == false -> {
                holder.available.text = context?.getString(R.string.unAvailable)
                context?.let {
                    resources?.let {
                        it.getColor(R.color.text_unavailable).let { holder.available.setTextColor(it) }
                    }
                }
            }
            else -> {
                holder.available.text = context?.getString(R.string.available)
                context?.let {
                    resources?.let {
                        it.getColor(R.color.temporal_event).let { holder.available.setTextColor(it) }
                    }
                }
            }
        }

        holder.heartButton.setOnClickListener {
            holder.heartButton.isEnabled = false
            context?.let {
                resources?.let {
                    it.getColor(R.color.medium_gray, null).let { holder.heartButton.setColorFilter(it) }
                }
            }
            val assetId = holder.assetId ?: return@setOnClickListener
            listener.didHeartPressed(assetId) {
                holder.heartButton.isEnabled = true
            }
        }

        holder.cardView.setOnClickListener {
            listener.didFavoriteSelected(item)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}
