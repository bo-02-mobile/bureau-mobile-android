package com.jalasoft.bureau.data.interfaces

import com.alamkanak.weekview.WeekViewEvent
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.utils.ErrorMessage

interface BuildingListener {
    fun didSelectBuildingItem(buildingId: String, buildingName: String, buildingTimeZone: String?)
}

interface AreaListener {
    fun didSelectAreaFromSpinner(index: Int)
}

interface BadRequest {
    fun fetchBadRequest(errorType: ErrorMessage)
}

interface NavigationListener {
    fun didAreaSelected(areaId: String, areaCode: String)
}

interface WorkstationListener {
    fun didWorkstationSelected(
        workstationCode: String,
        features: List<String>,
        workstationId: String
    )
}

interface BookingOptionsListener {
    fun didDoneButtonPressed()
}

interface MyBookingsListener {
    fun didBookingSelected(booking: BookingResponse.BookingBundle)
    fun didQrButtonPressed()
    fun didAnimationCompleted()
}

interface FavoritesListener {
    fun didFavoriteSelected(asset: Asset.AssetItem)
    fun didHeartPressed(assetId: String, onDelete: () -> Unit = {})
}

interface BookingAvailabilityListener {
    fun didBookingSuccessful(workstationCode: String)
    fun didPostBookingFailed()
}

interface BookingAvailabilityBadRequest {
    fun fetchBadRequest()
}

interface FilterSpaceListener {
    fun didSelectSpaceType(selected: Int)
    fun didSelectLabels(selected: Int)
}

interface BottomSheetDialogListener {
    fun didSelectBottomSheetDialog()
}

interface BottomSheetFromQRDialogListener {
    fun didConfirmBooking(assetId: String)
}

interface CalendarEventListener {
    fun didSelectEvent(event: WeekViewEvent)
}