package com.jalasoft.bureau.data.model.asset

import com.jalasoft.bureau.data.model.common.Owner

class Asset : ArrayList<Asset.AssetItem>() {
    data class AssetItem(
        val areaId: String?,
        val buildingId: String?,
        val code: String?,
        val createdAt: String?,
        val description: String?,
        val id: String?,
        val name: String?,
        val properties: Properties?,
        val policies: Policies?,
        val `public`: Boolean?,
        val stats: Stats?,
        val updatedAt: String?,
        val version: Int?,
        var available: Boolean?
    ) {
        data class Properties(
            val labels: List<String>,
            val owner: Owner?,
            val type: String?
        )

        data class Stats(
            val average: Int?,
            val count: Int?
        )

        data class Policies (
            val bookingWindow: Int,
            val isRecurrent: Boolean,
            val min: Int,
            val max: Int,
            val schedule: List<Schedule>
        )

        data class Schedule (
            val id: String? = null,
            val startTime: String,
            val endTime: String
        )
    }
}