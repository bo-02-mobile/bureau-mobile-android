package com.jalasoft.bureau.data.model.area

class Area : ArrayList<Area.AreaItem>() {
    data class AreaItem(
        val background: Background?,
        val buildingId: String?,
        val code: String?,
        val createdAt: String?,
        val description: String?,
        val id: String?,
        val markers: List<Marker?>?,
        val name: String?,
        val `public`: Boolean?,
        val thumbnail: Thumbnail?,
        val updatedAt: String?,
        val version: Any?,
        var availableSpace: Int?
    ) {
        data class Background(
            val id: String?,
            val url: String?
        )

        data class Marker(
            val assetId: String,
            val `public`: Boolean?,
            val x: Double,
            val y: Double
        )

        data class Thumbnail(
            val id: String?,
            val url: String?
        )
    }
}