package com.jalasoft.bureau.data.db

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.jalasoft.bureau.utils.SHARED_STORAGE

class Prefs(val context: Context) {

    private val storage: SharedPreferences? =
        context.getSharedPreferences(SHARED_STORAGE, MODE_PRIVATE)
    var editor: SharedPreferences.Editor? = storage?.edit()

    fun saveData(value: String?, key: String) {
        storage?.edit()?.putString(key, value ?: "")?.apply()
    }

    fun getData(key: String): String? {
        return storage?.getString(key, "")
    }

    fun saveBooleanData(value: Boolean, key: String) {
        storage?.edit()?.putBoolean(key, value)?.apply()
    }

    fun getBooleanData(key: String): Boolean? {
        return storage?.getBoolean(key, false)
    }

    fun clearData() {
        editor?.clear()
        editor?.commit();
    }
}