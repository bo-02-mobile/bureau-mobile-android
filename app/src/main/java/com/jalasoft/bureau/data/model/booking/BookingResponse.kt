package com.jalasoft.bureau.data.model.booking

import com.jalasoft.bureau.data.model.common.Owner
import com.jalasoft.bureau.data.model.common.RecurrenceRule

class BookingResponse : ArrayList<BookingResponse.Booking>() {
    data class Booking(
        val areaId: String,
        val areaName: String,
        val assetCode: String,
        val assetId: String,
        val assetType: String?,
        val buildingId: String,
        val confirmedAt: String?,
        val createdAt: String,
        var endTime: String,
        val id: String,
        val isPermanent: Boolean?,
        val isRecurrent: Boolean,
        val owner: Owner?,
        val recurrenceRule: RecurrenceRule?,
        var startTime: String,
        val updatedAt: String,
        val version: Int,
        val reservationMessage: String?,
        val status: Status?,
        val recurrenceId: String?,
    )

    data class BookingBundle(
        val assetCode: String,
        val assetId: String,
        val confirmedAt: String?,
        val startTime: String,
        val endTime: String,
        val bookingId: String,
        val buildingId: String,
        val assetType: String?,
        val isToday: Boolean,
        val reservationMessage: String?,
        val isPermanent: Boolean?,
        val areaName: String,
        val isBookingValidationInRange: Boolean = false,
        val bookingRangeHourText: String = ""
    )

    data class Status (
        val state: String? = null,
        val cancellationDate: String? = null,
        val reason: Any? = null
    )
}