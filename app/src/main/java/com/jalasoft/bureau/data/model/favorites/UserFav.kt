package com.jalasoft.bureau.data.model.favorites

data class UserFav(
    val id: String?,
    val externalID: String?,
    val name: String?,
    val givenName: String?,
    val familyName: String?,
    val preferredUsername: String?,
    val email: String?,
    val role: String?,
    val picture: String?,
    val emailVerified: Boolean?,
    val groups: List<String>?,
    val favoriteAssets: List<String?>,
    val createdAt: String?,
    val updatedAt: String?,
    val version: Any? = null
)