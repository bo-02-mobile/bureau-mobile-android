package com.jalasoft.bureau.data.model.asset

class AssetType : ArrayList<AssetType.AssetTypeItem>() {
    data class AssetTypeItem(
        val id: String?,
        val labels: List<String>,
        val name: String?,
        val text: String?,
        val value: String?
    )
}