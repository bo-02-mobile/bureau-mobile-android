package com.jalasoft.bureau.data.model.common

import com.google.gson.annotations.SerializedName

data class Owner(
    val email: String?,
    val id: String?,
    @SerializedName("preferredUsername") val name: String?
)