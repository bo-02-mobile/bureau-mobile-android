package com.jalasoft.bureau.data.model.scanner

import com.jalasoft.bureau.data.model.common.Owner
import com.jalasoft.bureau.data.model.common.RecurrenceRule

data class BookingConfirmResponse(
    val id: String,
    val workstationId: String,
    val ownerEmail: String,
    val ownerName: String,
    val startTime: String,
    val endTime: String,
    val isRecurrent: Boolean,
    val recurrenceRule: RecurrenceRule?,
    val isPermanent: Boolean,
    val isConfirmed: Boolean,

    val areaId: String,
    val areaName: String,
    val assetCode: String,
    val assetId: String,
    val assetType: String,
    val buildingId: String,
    val confirmedAt: String?,
    val createdAt: String,
    val owner: Owner?,
    val updatedAt: String,
    val version: Int
)
