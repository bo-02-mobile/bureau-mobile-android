package com.jalasoft.bureau.data.model.user

class Groups : ArrayList<Groups.Group>() {
    data class Group(
        val id: String? = "",
        val name: String? = "",
        val quota: Map<String, Int>? = mapOf(Pair("",0)),
        val bookForOthers: Boolean? = false,
        val recurrenceBookings: Boolean? = false,
        val createdAt: String? = "",
        val updatedAt: String? = "",
        val version: Int? = 0,
        val role: String? = ""
    )
}