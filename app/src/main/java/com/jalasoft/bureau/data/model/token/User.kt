package com.jalasoft.bureau.data.model.token

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("id") var id: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("picture") val picture: String? = null,
    @SerializedName("name") val name: String? = null,
    @SerializedName("role") val role: String? = null
)
