package com.jalasoft.bureau.data.model.booking

data class BookingConfiguration(
    val id: String?,
    val limitAfterDeadLine: Int?,
    val limitBeforeDeadLine: Int?
)