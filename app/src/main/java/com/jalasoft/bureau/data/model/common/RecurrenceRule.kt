package com.jalasoft.bureau.data.model.common

class RecurrenceRule {
    var isCustom: Boolean? = null
    var type: String? = null
    var endDate: String? = null
    var onDays: Array<String>? = null
    var onMonth: Int? = null
    var onWeek: Int? = null
    var onDate: Int? = null
    var every: Int? = null

    fun isEmpty(): Boolean {
        if (isCustom == null && type == null && endDate == null && onDays == null &&
            onMonth == null && onWeek == null && onDate == null && every == null)
                return true
        return false
    }
}