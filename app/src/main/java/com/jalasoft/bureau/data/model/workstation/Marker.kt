package com.jalasoft.bureau.data.model.workstation

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.PointF
import androidx.core.content.res.ResourcesCompat
import com.jalasoft.bureau.R
import com.jalasoft.bureau.utils.MARKER_SIZE_MULTIPLIER
import com.jalasoft.bureau.utils.SELECTED_MARKER_SIZE_MULTIPLIER

class Marker(var context: Context, x: Float, y: Float, private var isAvailable: Boolean) {

    private lateinit var bitmap: Bitmap
    private var point = PointF(x, y)

    init {
        setDefault()
    }

    fun select() {
        val vectorDrawable =
            ResourcesCompat.getDrawable(context.resources, R.drawable.ic_selected_marker, null)
        if (vectorDrawable != null) {
            val bitmapImage = Bitmap.createBitmap(
                vectorDrawable.intrinsicWidth,
                vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmapImage)
            vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
            vectorDrawable.draw(canvas)
            val w = SELECTED_MARKER_SIZE_MULTIPLIER * bitmapImage.width
            val h = SELECTED_MARKER_SIZE_MULTIPLIER * bitmapImage.height
            bitmap = Bitmap.createScaledBitmap(bitmapImage, w.toInt(), h.toInt(), true)
        }
    }

    fun setDefault() {
        if (isAvailable) {
            val vectorDrawable =
                ResourcesCompat.getDrawable(context.resources, R.drawable.ic_marker, null)
            if (vectorDrawable != null) {
                val bitmapImage = Bitmap.createBitmap(
                    vectorDrawable.intrinsicWidth,
                    vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888
                )
                val canvas = Canvas(bitmapImage)
                vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
                vectorDrawable.draw(canvas)
                val w = MARKER_SIZE_MULTIPLIER * bitmapImage.width
                val h = MARKER_SIZE_MULTIPLIER * bitmapImage.height
                bitmap = Bitmap.createScaledBitmap(bitmapImage, w.toInt(), h.toInt(), true)
            }
        } else {
            setUnavailable()
        }
    }

    fun setUnavailable() {
        val vectorDrawable =
            ResourcesCompat.getDrawable(context.resources, R.drawable.ic_unavailable_marker, null)
        if (vectorDrawable != null) {
            val bitmapImage = Bitmap.createBitmap(
                vectorDrawable.intrinsicWidth,
                vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmapImage)
            vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
            vectorDrawable.draw(canvas)
            val w = MARKER_SIZE_MULTIPLIER * bitmapImage.width
            val h = MARKER_SIZE_MULTIPLIER * bitmapImage.height
            bitmap = Bitmap.createScaledBitmap(bitmapImage, w.toInt(), h.toInt(), true)
        }
    }

    fun getBitmap(): Bitmap {
        return bitmap
    }

    fun getPoint(): PointF {
        return point
    }
}