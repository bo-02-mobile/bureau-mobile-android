package com.jalasoft.bureau.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jalasoft.bureau.databinding.FeatureItemBinding
import com.jalasoft.bureau.utils.StringUtils

class WorkstationFeaturesAdapter(val list: List<String>) :
    RecyclerView.Adapter<WorkstationFeaturesAdapter.ListViewHolder>() {

    inner class ListViewHolder(binding: FeatureItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val featureTV: TextView = binding.feature
        val dot = binding.dot
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WorkstationFeaturesAdapter.ListViewHolder {
        return ListViewHolder(
            FeatureItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val item = list[position]
        holder.featureTV.text = StringUtils.capitalize(item)
        if (position == itemCount - 1) {
            holder.dot.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}
