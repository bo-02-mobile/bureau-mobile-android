package com.jalasoft.bureau.data.model.workstation

import com.google.gson.annotations.SerializedName

class Workstation : ArrayList<Workstation.WorkstationItem>() {
    data class WorkstationItem(
        @SerializedName("areaId") val areaId: String,
        @SerializedName("code") val code: String,
        @SerializedName("description") val description: String,
        @SerializedName("features") val features: List<String>,
        @SerializedName("id") val id: String,
        @SerializedName("imageLocation") val imageLocation: Any,
        @SerializedName("kind") val kind: String,
        @SerializedName("published") val published: Boolean,
        @SerializedName("ratingAvg") val ratingAvg: Double,
        @SerializedName("ratingCount") val ratingCount: Double
    )

    data class WorkstationLayout(
        @SerializedName("id") val id: String,
        @SerializedName("areaId") val areaCode: String,
        @SerializedName("background") val background: String,
        @SerializedName("markers") val markers: List<Marker>,
    )

    data class Marker(
        @SerializedName("workstationId") val workstationId: String,
        @SerializedName("published") val published: Boolean,
        @SerializedName("x") val x: Double,
        @SerializedName("y") val y: Double
    )
}