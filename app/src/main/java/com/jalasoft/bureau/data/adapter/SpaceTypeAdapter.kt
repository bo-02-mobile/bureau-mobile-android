package com.jalasoft.bureau.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.interfaces.FilterSpaceListener
import com.jalasoft.bureau.data.model.asset.AssetType
import com.jalasoft.bureau.databinding.SpaceTypeItemBinding

class SpaceTypeAdapter(
    private var spaceTypes: List<AssetType.AssetTypeItem>,
    private var listener: FilterSpaceListener,
    var selectedIndex: Int
) : RecyclerView.Adapter<SpaceTypeAdapter.SpaceTypeHolder>() {

    class SpaceTypeHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val spaceTypeBinding = SpaceTypeItemBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpaceTypeHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return SpaceTypeHolder(layoutInflater.inflate(R.layout.space_type_item, parent, false))
    }

    override fun onBindViewHolder(holder: SpaceTypeHolder, position: Int) {
        val current = spaceTypes[position]
        holder.spaceTypeBinding.txtSpaceTypeName.text = current.name
        holder.spaceTypeBinding.spaceTypeRadioButton.isChecked = selectedIndex == position
        holder.spaceTypeBinding.spaceTypeRadioButton.setOnClickListener {
            listener.didSelectSpaceType(position)
        }
    }

    override fun getItemCount(): Int = spaceTypes.size
}