package com.jalasoft.bureau.data.model.booking

import com.jalasoft.bureau.data.model.common.RecurrenceRule

data class BookingBody(
    val endTime: String?,
    val isRecurrent: Boolean?,
    val recurrenceRule: RecurrenceRule?,
    val startTime: String?
)
