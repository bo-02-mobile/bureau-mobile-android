package com.jalasoft.bureau.data.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.interfaces.FilterSpaceListener
import com.jalasoft.bureau.data.model.asset.AssetLabel
import com.jalasoft.bureau.databinding.FeatureTypeItemBinding

class FeatureTypeAdapter(
    private val features: List<AssetLabel>,
    private val listener: FilterSpaceListener,
    var arrLabels: List<String>
) :
    RecyclerView.Adapter<FeatureTypeAdapter.FeatureTypeAdapterHolder>() {
    class FeatureTypeAdapterHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val featureTypeBinding = FeatureTypeItemBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeatureTypeAdapterHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return FeatureTypeAdapterHolder(
            layoutInflater.inflate(
                R.layout.feature_type_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: FeatureTypeAdapterHolder, position: Int) {
        val current = features[position]
        holder.featureTypeBinding.txtFeatureTypeName.text = current.name
        holder.featureTypeBinding.switchFeatureType.isChecked = arrLabels.contains(current.id)
        holder.featureTypeBinding.switchFeatureType.setOnClickListener {
            listener.didSelectLabels(position)
        }
    }

    override fun getItemCount(): Int = features.size
}