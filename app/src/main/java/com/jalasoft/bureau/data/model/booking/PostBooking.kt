package com.jalasoft.bureau.data.model.booking

import com.jalasoft.bureau.data.model.common.Owner
import com.jalasoft.bureau.data.model.common.RecurrenceRule

data class PostBooking(
    val assetId: String?,
    val endTime: String?,
    val isRecurrent: Boolean?,
    val owner: Owner?,
    val recurrenceRule: RecurrenceRule?,
    val startTime: String?,
    val status: Status?,
)

data class Status(
    val state: String?
)