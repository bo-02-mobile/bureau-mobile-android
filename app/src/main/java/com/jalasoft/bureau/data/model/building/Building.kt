package com.jalasoft.bureau.data.model.building

import com.google.gson.annotations.SerializedName

class Building : ArrayList<Building.BuildingItem>() {
    data class BuildingItem(
        @SerializedName("address") val address: String?,
        val code: String?,
        val createdAt: String?,
        val id: String?,
        var name: String?,
        val `public`: Boolean?,
        val timezone: String?,
        val updatedAt: String?,
        val version: Int?,
        var totalAreas: String?
    )
}