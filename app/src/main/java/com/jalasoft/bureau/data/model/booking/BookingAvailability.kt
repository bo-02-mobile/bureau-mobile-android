package com.jalasoft.bureau.data.model.booking

data class BookingAvailability(
    val isAvailable: Boolean?,
    val isPermanent: Boolean?,
    val lastOverlappedEndTime: String?,
    val ownersEmailList: List<String?>?,
    val ownersNameList: List<String?>?
)