package com.jalasoft.bureau.data.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jalasoft.bureau.R
import com.jalasoft.bureau.data.interfaces.BuildingListener
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.databinding.BuildingItemBinding

class BuildingAdapter(
    var buildings: List<Building.BuildingItem>,
    private val listener: BuildingListener,
    var selected: String
) : RecyclerView.Adapter<BuildingAdapter.BuildingHolder>() {
    var context: Context? = null
    class BuildingHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val buildingBinding: BuildingItemBinding = BuildingItemBinding.bind(view)
        fun render(building: Building.BuildingItem) {
            buildingBinding.tvBuildingName.text = building.name
            buildingBinding.tvBuildingAddress.text = building.address
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BuildingHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        context = parent.context
        return BuildingHolder(layoutInflater.inflate(R.layout.building_item, parent, false))
    }

    override fun onBindViewHolder(holder: BuildingHolder, position: Int) {
        this.context = context
        val current = buildings[position]
        if (selected == current.id) {
            holder.buildingBinding.cardViewItems?.setBackgroundColor(context?.resources?.getColor(R.color.selected_building)
                ?: Color.LTGRAY)
        } else {
            holder.buildingBinding.cardViewItems?.setBackgroundColor(context?.resources?.getColor(R.color.background)
                ?: Color.LTGRAY)
        }
        holder.buildingBinding.tvBuildingAreas?.text = current.totalAreas
        holder.render(current)
        holder.buildingBinding.cardView.setOnClickListener {
            listener.didSelectBuildingItem(current.id.toString(), current.name.toString(), current.timezone)
        }
    }

    override fun getItemCount(): Int = buildings.size


}