package com.jalasoft.bureau.utils

import android.content.Context
import android.net.ConnectivityManager
import com.jalasoft.bureau.R
import com.jalasoft.bureau.ui.areas.view.AreaFragment
import com.jalasoft.bureau.utils.login.AuthStateManager

//region LOGOUT
const val LOGOUT_CONFIRM = "Do you want to logout?"
const val LOGOUT_AFFIRMATIVE = "YES"
const val LOGOUT_DISMISS = "NO"
//endregion

//region SAVE STATE
const val KEY_STATE = "state"
const val STORE_NAME = "AuthState"
//endregion

//region TOOLBAR
const val EMPTY_TITLE = ""
//endregion

//region PROFILE IMAGE
const val ERROR_TAG = "ERROR"
const val BEARER = "Bearer "
//endregion

//region VERSION LABEL
const val VERSION_LABEL_TEXT = "Version Number: %s"
const val VERSION_LABEL_STAGING = " - Staging"
//endregion

//region ADAPTER UTILS
const val INITIAL_SCREEN_WIDTH = 0
const val ITEM_WIDTH_DIVIDE = 1.8
//endregion

//region BUNDLE TAGS
const val AREA_ID = "areaId"
const val AREA_CODE = "areaCode"
const val WORKSTATION_CODE = "workstationCode"
const val FEATURES_ARRAY = "features"
const val PLACE_CODE = "placeCode"
const val WORKSTATION_ID = "workstationId"
const val ASSET_ID = "assetId"
const val BOTTOM_SHEET_ASSET_DETAIL = "bottomSheetAssetDetail"
const val BOTTOM_SHEET_FAVORITE_ASSET_DETAIL = "bottomSheetFavoriteAssetDetail"
const val BOTTOM_SHEET_FILTER_PADDING = 180
const val VALIDATED_AT = "validatedAt"
const val START_TIME = "startTime"
const val END_TIME = "endTime"
const val BOOKING_ID = "bookingId"
const val BOOKING_DATE = "bookingDate"
const val BOOKING_TITLE = "bookingTitle"
const val ASSET_TYPE = "assetType"
const val RES_MESSAGE = "reservationMessage"
const val BOTTOM_SHEET_ASSET_DETAIL_TABLET = "bottomSheetAssetDetailTablet"
const val AVAILABLE = "Available"
const val UNAVAILABLE = "Unavailable"
const val FAVORITE_PERMANENT_ASSET = "Unavailable / Permanently booked"
const val IS_PERMANENT = "isPermanent"
const val AREA_NAME = "areaName"
const val IS_BOOKING_VALIDATION_IN_RANGE = "isBookingValidationInRange"
const val BOOKING_RANGE_HOUR_TEXT = "bookingRangeHourText"
//endregion

//region NOTIFICATION CHANNEL
const val CHANNEL_NAME = "BureauReminderChannel"
const val CHANNEL_DESCRIPTION = "Channel for Bureau Reminder"
const val NOTIFY_BUREAU = "notifyBureau"
const val NOTIFICATION_REQUEST_CODE = 1
const val IS_NOTIFICATION = "isNotification"
const val TITLE_NOTIFICATION = "Your booking is coming."
const val IS_TODAY = "isToday"
//endregion

//region WIDGETS
const val NO_BOOKINGS_TITLE = "No Bookings"
const val MY_BOOKINGS_TITLE = "My Bookings"
const val WIDGET_HEIGHT = 117F
const val WIDGET_WIDTH_SMALL = 129F
const val WIDGET_WIDTH_LARGE = 276F
const val WIDGET_ACTION = "WidgetButton"
//endregion

//region BUILDING
const val BUILDING_ID = "buildingId"
const val BUILDING_NAME = "buildingName"
const val UNDEFINED_TIMEZONE = "Undefined timezone"
//endregion

//region WORKSTATIONS RESULTS
const val AREA_PREFIX = "Area "
const val INITIAL_MARKER = 0
const val SCALE_MULTIPLIER = 0.8f
const val ANIMATION_DURATION = 500
const val MAX_AREA_SELECTION = 40
const val MARKER_SIZE_MULTIPLIER = 0.8
const val SELECTED_MARKER_SIZE_MULTIPLIER = 1.3
const val WORKSTATION_LABEL = "Workstation"
const val EMPTY_MAP = "There are not available spaces to book at the moment"
const val FILTERS_TEXT_NO_COUNT = "FILTER"
const val FILTERS_TEXT_COUNT ="FILTER (%d)"
const val MISMATCHED_FILTERS = "Your current filter settings does not contain results"
//endregion

//region RECURRENCE
const val RECURRENCE_OPTION_NEVER = "Never"
const val RECURRENCE_OPTION_DAILY = "Daily"
const val RECURRENCE_OPTION_WEEKLY = "Weekly"
const val RECURRENCE_OPTION_MONTHLY = "Monthly"
const val RECURRENCE_OPTION_YEARLY = "Yearly"
const val RECURRENCE_EVERY_LABEL_FORMAT = "(%s)"
const val SUNDAY = "Sunday"
const val SUNDAY_VALUE = 0
const val MONDAY = "Monday"
const val MONDAY_VALUE = 1
const val TUESDAY = "Tuesday"
const val TUESDAY_VALUE = 2
const val WEDNESDAY = "Wednesday"
const val WEDNESDAY_VALUE = 3
const val THURSDAY = "Thursday"
const val THURSDAY_VALUE = 4
const val FRIDAY = "Friday"
const val FRIDAY_VALUE = 5
const val SATURDAY = "Saturday"
const val SATURDAY_VALUE = 6
//endregion

//region DATE
const val API_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"
const val API_DATE_FORMAT_2 = "yyyy-MM-dd'T'HH:mm:ssZ"
const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"
const val LOCAL_DATE_FORMAT = "MMMM dd, YYYY"
const val LOCAL_DATE_FORMAT_WITHOUT_HOUR = "MMMM dd"
const val ONLY_HOUR = "HH"
const val NOTIFICATION_DATE = "yyyy-MM-dd'T'HH:mm"
const val TODAY_TEXT = "Today"
//endregion

//region TIME
const val START_TIME_PICKER_TAG = "TIME_START_PICKER"
const val END_TIME_PICKER_TAG = "TIME_END_PICKER"
const val LOCAL_TIME_FORMAT = "hh:mm a"
const val DATETIME_FORMAT = "E, MMM dd"
const val DATETIME_WITH_YEAR_FORMAT = "E, MMM dd yyyy"
const val FULL_DATETIME_FORMAT = "%s - %s to %s"
const val STARTHOUR_TO_ENDHOUR = "%02d:%02d - %02d:%02d"
const val TIMEZONE_UTC = "UTC-00:00"
//endregion

//region EVENTS
const val TEMPORAL_EVENT_ID = "tempId"
const val SCHEDULED_EVENT_ID = "scheduleId"
//endregion

//region QR SCANNER
const val DATE_TIME_PATTERN = "%s %s"
const val QR_DATE_PATTERN = "MMMM dd, "
const val REQ_BODY_OP = "replace"
const val REQ_BODY_PATH = "/ConfirmedAt"
//endregion

//region BOTTOM SHEET
const val FILTER_BOTTOM_SHEET = "FILTER_BOTTOM_SHEET"
const val FILTER_BOTTOM_SHEET_TABLET = "FILTER_BOTTOM_SHEET_TABLET"
const val BOOKING_OPTIONS_BOTTOM_SHEET = "BOOKING_OPTIONS_BOTTOM_SHEET"
const val BOOKING_OPTIONS_BOTTOM_SHEET_TABLET = "BOOKING_OPTIONS_BOTTOM_SHEET_TABLET"
const val RECURRENCE_OPTIONS_BOTTOM_SHEET = "RECURRENCE_OPTIONS_BOTTOM_SHEET"
const val RECURRENCE_OPTIONS_BOTTOM_SHEET_TABLET = "RECURRENCE_OPTIONS_BOTTOM_SHEET_TABLET"
//endregion

//region PICKERS
const val DAY_BOOKING = "Day Booking"
const val DATE_PICKER = "DATE_PICKER"
const val SELECT_START_TIME = "Select Start Time"
const val SELECT_END_TIME = "Select End Time"
//endregion

//region SHARED PREFERENCES
const val SHARED_STORAGE = "BureauSharedPreferences"
const val SHARED_USER_ID = "userid"
const val SHARED_USER_NAME = "username"
const val SHARED_USER_EMAIL = "useremail"
const val SHARED_USER_IMAGE = "userimage"
const val SHARED_USER_ROLE = "userrole"
const val SHARED_USER_APPEARANCE = "userAppearance"
const val SHARED_BUILDING_ID = "buildingId"
const val SHARED_BUILDING_NAME = "buildingName"
const val SHARED_BUILDING_ZONE_ID = "buildingZoneId"
const val SHARED_USER_SAVE_BUILDING = "saveBuilding"
const val RECURRENCE_BOOKING_PERMISSION = "recurrenceBookingPermission"
const val BOOK_FOR_OTHERS_PERMISSION = "bookForOthersPermission"
const val SHARED_BOOKING_LIMIT_AFTER_DEADLINE = "limitAfterDeadLine"
const val SHARED_BOOKING_LIMIT_BEFORE_DEADLINE = "limitBeforeDeadLine"
//endregion

//region MENU INDEX
const val BOOKING_TAB = 0
const val MY_BOOKING_TAB = 1
const val QR_TAB = 2
const val NO_SELECTED_TAB = -1
const val SHORTCUT_EXTRA = "destination"
//endregion

//region DELAY VALUES IN MS
const val VERY_SHORT = 300L
const val SHORT = 1000L
//endregion

//region VALIDATION RANGE
const val NOTIFICATION_RANGE: Long = 10
const val NOTIFICATION_DEFAULT_RANGE = "10"
//endregion

//region VALIDATE TIME MESSAGE
const val VALIDATE_DATE = "You can only select the date from today onwards"
const val VALIDATE_END_TIME = "Your ending time must be greater than your starting time"
const val VALIDATE_END_DATE = "Your end date must be after your start date"
const val IS_PERMANENT_MESSAGE = "This is a permanent asset assigned to you"
//endregion

//region VALIDATE TIME MESSAGE
const val PERMANENT_BOOKING = "Permanent Booking"
//endregion

//region CRASHLYTICS KEYS
const val KEY_NO_EMAIL = "no_email"
const val KEY_BUILDING_ID = "building_id"
const val KEY_AREA_ID = "area_id"
const val KEY_ASSET_ID = "asset_id"
const val KEY_DATE_VALUE = "date_filter"
const val KEY_START_TIME_VALUE = "start_time_filter"
const val KEY_END_TIME_VALUE = "end_time_filter"
const val KEY_TIMEZONE_VALUE = "time_zone_abbreviation"
const val PREFS_ERROR = "preferences_error"
const val NULL_VALUE = "null_value"
//endregion

//region IS PUBLIC
const val IS_PUBLIC = "public eq bool(true)"
//endregion

//region Book for column names
const val COL0 = "id"
const val COL1 = "name"
const val COL2 = "email"
//endregion

//region role
const val ADMINISTRATOR_ROLE = "administrator"
//endregion

class ApiUrl {
    companion object {
        const val BOOKING_BY_ID = "bookings/{bookingId}"
    }
}

class GlobalConstants {
    companion object {
        fun getUserToken(context: Context?): String {
            return (BEARER + context?.let { AuthStateManager.getInstance(it).current.accessToken })
        }

        fun hasNetwork(context: Context): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                return true
            }
            return false
        }
    }
}

//region Booking state
const val PENDING = "Pending"
const val VALIDATED = "Validated"
const val USER_CANCELLED = "UserCancelled"
const val SYSTEM_CANCELLED = "SystemCancelled"
//endregion