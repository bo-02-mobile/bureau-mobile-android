package com.jalasoft.bureau.utils.timeFilter

import java.time.LocalDate
import java.time.LocalTime

class TimeFilter(
    var date: LocalDate?,
    var start: LocalTime?,
    var end: LocalTime?,
)