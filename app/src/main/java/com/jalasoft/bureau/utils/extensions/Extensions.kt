package com.jalasoft.bureau.utils.extensions

import android.content.Context
import android.provider.SyncStateContract
import com.jalasoft.bureau.R
import com.jalasoft.bureau.utils.*
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

fun String.formatDateToTimezone(buildingTimezone: String): String {
    val formatter = DateTimeFormatter.ofPattern(LOCAL_TIME_FORMAT)
    val convertedDate = LocalDateTime.ofInstant(
        Instant.parse(this),
        ZoneId.of(buildingTimezone)
    )
    return convertedDate.format(formatter)
}

fun LocalDate.formatCalendar(context: Context): String {
    val todayString = context.getString(R.string.day_placeholder)
    val formatter = DateTimeFormatter.ofPattern(DATETIME_FORMAT, Locale.ENGLISH)
    val todayDate = LocalDateTime.now().format(formatter)
    val currDate = this.format(formatter)
    return if ( currDate == todayDate) todayString else currDate
}

fun String.formatNotificationField(field: String, buildingTimezone: String): String {
    val currTime = LocalDateTime.ofInstant(Instant.parse(this), ZoneId.of(buildingTimezone))
    val dateTime = currTime.format(DateTimeFormatter.ofPattern(API_DATE_FORMAT))
    val input = SimpleDateFormat(API_DATE_FORMAT, Locale.getDefault())
    input.timeZone = TimeZone.getTimeZone(TIMEZONE_UTC)
    val output = SimpleDateFormat(field, Locale.US)
    output.timeZone = TimeZone.getTimeZone(TIMEZONE_UTC)
    val getAbbreviate = input.parse(dateTime)    // parse input
    return output.format(getAbbreviate)    // format output
}