@file:Suppress("RedundantOverride")

package com.jalasoft.bureau.utils.extensions

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.util.AttributeSet
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import com.jalasoft.bureau.data.model.workstation.Marker


class PinView @JvmOverloads constructor(context: Context, attr: AttributeSet? = null) :
    SubsamplingScaleImageView(context, attr) {

    private val paint = Paint()
    private val vPin = PointF()
    private var pins: List<Marker>? = null


    fun setPin(sPin: List<Marker>) {
        this.pins = sPin
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        // Don't draw pin before image is ready so it doesn't move around during setup.
        if (!isReady) return

        paint.isAntiAlias = true
        pins?.forEach {
            sourceToViewCoord(it.getPoint(), vPin)
            val vX = vPin.x - it.getBitmap().width / 2
            val vY = vPin.y - it.getBitmap().height
            if (it.getBitmap().isRecycled) {
                return
            }
            canvas.drawBitmap(it.getBitmap(), vX, vY, paint)
        }
    }

    override fun performClick(): Boolean {
        return super.performClick()
    }
}