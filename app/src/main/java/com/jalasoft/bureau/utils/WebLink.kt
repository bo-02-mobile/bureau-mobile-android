package com.jalasoft.bureau.utils
import android.view.MenuItem
import com.jalasoft.bureau.databinding.ActivityMainBinding
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel

enum class WebLinkType(var assetId: String = "") {
    WEB_LINK_BOOKING_CONFIRMATION,
    WEB_LINK_DEFAULT
}

class WebLink(private val binding: ActivityMainBinding, private val url: String,private val viewModel: MainActivityViewModel) {

    private val webLinkType: WebLinkType = when {
        url.contains("/confirm") -> WebLinkType.WEB_LINK_BOOKING_CONFIRMATION
        else -> WebLinkType.WEB_LINK_DEFAULT
    }

    fun openWebLink() {
        when (webLinkType) {
            WebLinkType.WEB_LINK_BOOKING_CONFIRMATION -> {
                viewModel.webLinkType = WebLinkType.WEB_LINK_BOOKING_CONFIRMATION
                viewModel.webLinkType.assetId = StringUtils.getAssetIdFromQRUrl(url)
                val item: MenuItem = binding.bottomNavView.menu.getItem(QR_TAB)
                binding.bottomNavView.selectedItemId = item.itemId
            }
            WebLinkType.WEB_LINK_DEFAULT -> {
                // Do nothing when the url is not a booking confirmation
            }
        }
    }

}