package com.jalasoft.bureau.utils.calendar

import android.content.Context
import android.graphics.RectF
import android.view.View
import com.alamkanak.weekview.*
import com.jalasoft.bureau.data.interfaces.CalendarEventListener
import com.jalasoft.bureau.databinding.FragmentAssetDetailFavoriteBinding
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

open class BaseCalendarFavorites(
    val context: Context,
    private val baseActivityBaseBinding: FragmentAssetDetailFavoriteBinding,
    private val calendarEventListener: CalendarEventListener
) : WeekView.EventClickListener, MonthLoader.MonthChangeListener, WeekView.EventLongPressListener,
    WeekView.EmptyViewLongPressListener, WeekView.EmptyViewClickListener,
    WeekView.AddEventClickListener, WeekView.DropListener {

    private lateinit var shortDateFormat: DateFormat
    private lateinit var timeFormat: DateFormat


    fun initCalendarSettingsFirst() {
        shortDateFormat = WeekViewUtil.getWeekdayWithNumericDayAndMonthFormat(context, true)
        timeFormat = android.text.format.DateFormat.getTimeFormat(context) ?: SimpleDateFormat(
            "HH:mm",
            Locale.getDefault()
        )
        // Show a toast message about the touched event.
        baseActivityBaseBinding.weekView.eventClickListener = this
        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.
        baseActivityBaseBinding.weekView.monthChangeListener = this
        // Set long press listener for events.
        baseActivityBaseBinding.weekView.eventLongPressListener = this
        // Set long press listener for empty view
        baseActivityBaseBinding.weekView.emptyViewLongPressListener = this
        //baseActivityBaseBinding.weekView.setLimitTime(1,24)
        setupDateTimeInterpreter()
    }

    /**
     * Set up a date time interpreter which will show short date values when in week view and long
     * date values otherwise.
     */
    protected fun setupDateTimeInterpreter() {
        val calendar = Calendar.getInstance().apply {
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }
        baseActivityBaseBinding.weekView.dateTimeInterpreter = object : DateTimeInterpreter {
            override fun getFormattedTimeOfDay(hour: Int, minutes: Int): String {
                calendar.set(Calendar.HOUR_OF_DAY, hour)
                calendar.set(Calendar.MINUTE, minutes)
                return timeFormat.format(calendar.time)
            }

            override fun getFormattedWeekDayTitle(date: Calendar): String {
                return ""
            }
        }
    }

    override fun onEventClick(event: WeekViewEvent, eventRect: RectF) {
        // when an event is clicked
        calendarEventListener.didSelectEvent(event)
    }

    override fun onEventLongPress(event: WeekViewEvent, eventRect: RectF) {
        // on long pressed
    }

    override fun onEmptyViewLongPress(time: Calendar) {
        // empty long press
    }

    override fun onEmptyViewClicked(date: Calendar) {
        // click empty event
    }

    override fun onMonthChange(newYear: Int, newMonth: Int): MutableList<out WeekViewEvent>? {
        return null
    }

    override fun onAddEventClicked(startTime: Calendar, endTime: Calendar) {
        // when adding a new event clicked
    }

    override fun onDrop(view: View, date: Calendar) {
        // when dropping a event
    }

}