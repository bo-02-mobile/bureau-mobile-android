package com.jalasoft.bureau.utils

import android.content.Context
import android.icu.text.SimpleDateFormat
import android.icu.util.TimeZone
import android.net.ParseException
import android.text.TextUtils
import android.util.Patterns
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.util.PatternsCompat
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.jalasoft.bureau.R
import java.time.*
import java.time.format.DateTimeFormatter
import java.util.*

class StringUtils {
    companion object {
        fun capitalize(string: String): String {
            return string[0].uppercaseChar() + string.substring(1)
        }

        fun formatAssetType(string: String): String {
            return capitalize(string).replace("-", " ")
        }

        fun isURL(string: String): Boolean {
            return PatternsCompat.WEB_URL.matcher(string).matches()
        }

        fun getAssetIdFromQRUrl(url: String): String {
            if (!isURL(url)) return ""
            val paths = url.split("/")
            if (paths.size < 2) return ""
            return paths[paths.size - 2]
        }

        fun isValidEmail(target: CharSequence?): Boolean {
            return if (TextUtils.isEmpty(target)) {
                false
            } else {
                Patterns.EMAIL_ADDRESS.matcher(target).matches()
            }
        }
    }
}

class DateUtils {
    companion object {
        fun dateFormat(dateTime: String, dateFormat: String, field: String): String? {
            val input = SimpleDateFormat(dateFormat, Locale.getDefault())
            input.timeZone = TimeZone.getTimeZone("UTC")
            val output = SimpleDateFormat(field, Locale.US)
            output.timeZone = TimeZone.getDefault()
            try {
                val getAbbreviate = input.parse(dateTime)    // parse input
                return output.format(getAbbreviate)    // format output
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return null
        }

        fun dateFormatUTC(dateTime: String, dateFormat: String, field: String): String? {
            val input = SimpleDateFormat(dateFormat, Locale.getDefault())
            input.timeZone = TimeZone.getTimeZone("UTC")
            val output = SimpleDateFormat(field, Locale.US)
            output.timeZone = TimeZone.getTimeZone("UTC")
            try {
                val getAbbreviate = input.parse(dateTime)    // parse input
                return output.format(getAbbreviate)    // format output
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return null
        }

        fun localDateToInstant(date: LocalDateTime): Instant {
            val cal = Calendar.getInstance()
            cal.clear()
            cal.set(date.year,date.monthValue - 1,date.dayOfMonth, date.hour, date.minute, 0)
            return cal.toInstant()
        }

        fun localDateToDate(localDate: LocalDate): Date? {
            val calendar = Calendar.getInstance()
            calendar.clear()
            //assuming start of day
            calendar[localDate.getYear(), localDate.getMonthValue() - 1] = localDate.getDayOfMonth()
            return calendar.time
        }

        fun dateToCalendar(date: Date?): Calendar? {
            val c = Calendar.getInstance()
            if (date != null) {
                c.time = date
            }
            return c
        }

        // From 'dateTime' with format 'yyyy-MM-dd'T'HH:mm:ss'Z'' to LocalDateTime using 'date' as the date portion
        fun stringToLocalDateTime(date: LocalDate, dateTime: String, zoneId: ZoneId): LocalDateTime {
            val localDateTime = ZonedDateTime.parse(dateTime).withZoneSameInstant(zoneId)
            return LocalDateTime.of(date.year, date.month, date.dayOfMonth, localDateTime.hour, localDateTime.minute)
        }

        // From 'dateTime' with format 'yyyy-MM-dd'T'HH:mm' to LocalTime
        fun stringToLocalTime(dateTime: String): LocalTime {
            val localDateTime = LocalDateTime.parse(dateTime)
            return LocalTime.of(localDateTime.hour, localDateTime.minute)
        }

        fun ordinalFormOfNumber(number: Int): String {
            val remainder = number % 10
            return if (remainder == 1 && number != 11) "$number"+"st"
                else if (remainder == 2 && number != 12) "$number"+"nd"
                else if (remainder == 3 && number != 13) "$number"+"rd"
                else "$number"+"th"
        }

        fun daysOfMonth(month: Int): Int {
            return if (month == 2) return 28
                else if ((month < 8 && month % 2 == 0) || (month > 8 && month % 2 == 1)) 30
                else 31
        }

        fun hourRangeFormat(startTime: LocalDateTime, endTime: LocalDateTime): String {
            return String.format(STARTHOUR_TO_ENDHOUR, startTime.hour, startTime.minute, endTime.hour, endTime.minute)
        }

        fun isToday(dateTime: LocalDateTime?): Boolean {
            dateTime?.let { date ->
                val today = LocalDateTime.now()
                return date.year == today.year &&
                        date.monthValue == today.monthValue &&
                        date.dayOfYear == today.dayOfYear
            }
            return false
        }

        fun getTimeZoneFromZoneId(zoneId: ZoneId): java.util.TimeZone {
            return java.util.TimeZone.getTimeZone(zoneId)
        }

        fun getFormattedDate(date: LocalDate?, time: LocalTime?, zoneId: ZoneId): String {
            val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
            return ZonedDateTime.of(date, time, zoneId).format(formatter)
        }
    }
}

class DarkModeUtils {
    companion object{
        var LIGHT_MODE_OPTION: String? = null
        var DARK_MODE_OPTION: String? = null

        fun setup(context: Context) {
            LIGHT_MODE_OPTION = context.getString(R.string.light)
            DARK_MODE_OPTION = context.getString(R.string.dark)
        }

        fun darkMode(darkModePrefs: String?) {
            AppCompatDelegate.setDefaultNightMode(returnAppearanceFlag(darkModePrefs))
        }

        fun returnAppearanceFlag(darkModePrefs: String?): Int {
            return when (darkModePrefs) {
                LIGHT_MODE_OPTION -> AppCompatDelegate.MODE_NIGHT_NO
                DARK_MODE_OPTION -> AppCompatDelegate.MODE_NIGHT_YES
                else -> AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
            }
        }
    }
}

class FirebaseCrashlyticsUtils {
    companion object{

        fun setCrashlyticsKey(key: String, value: String?) {
            Firebase.crashlytics.setCustomKey(key, value?: NULL_VALUE)
        }

        fun setUserEmailId(userEmail: String?) {
            Firebase.crashlytics.setUserId(userEmail?: KEY_NO_EMAIL)
        }

        fun cleanAllKeys() {
            Firebase.crashlytics.setCustomKey(KEY_BUILDING_ID, NULL_VALUE)
            Firebase.crashlytics.setCustomKey(KEY_AREA_ID, NULL_VALUE)
            Firebase.crashlytics.setCustomKey(KEY_ASSET_ID, NULL_VALUE)
            Firebase.crashlytics.setCustomKey(KEY_DATE_VALUE, NULL_VALUE)
            Firebase.crashlytics.setCustomKey(KEY_START_TIME_VALUE, NULL_VALUE)
            Firebase.crashlytics.setCustomKey(KEY_END_TIME_VALUE, NULL_VALUE)
            Firebase.crashlytics.setCustomKey(KEY_TIMEZONE_VALUE, NULL_VALUE)
        }

        fun cleanUserId() {
            Firebase.crashlytics.setUserId(KEY_NO_EMAIL)
        }
    }
}

enum class ErrorMessage() {
    REQUEST_FAILED,
    REQUEST_FAILED_AREAS,
    REQUEST_FAILED_ASSET,
    REQUEST_FAILED_ASSET_RESULT,
    BOOKING_NOT_VALIDATED,
    CONNECTION_FAILED;

    fun message(): Pair<Int, Int> {
        return when(this) {
            REQUEST_FAILED -> {
                Pair(
                    R.string.server_error_title,
                    R.string.server_error_message
                )
            }
            REQUEST_FAILED_AREAS -> {
                Pair(
                    R.string.tittle_error_validation_booking,
                    R.string.server_error_message
                )
            }
            REQUEST_FAILED_ASSET -> {
                Pair(
                    R.string.server_asset_detail_title,
                    R.string.server_error_message
                )
            }
            REQUEST_FAILED_ASSET_RESULT -> {
                Pair(
                    R.string.server_map_title,
                    R.string.server_error_message
                )
            }
            BOOKING_NOT_VALIDATED -> {
                Pair(
                    R.string.tittle_error_validation_booking,
                    R.string.error_validation_booking
                )
            }
            CONNECTION_FAILED -> {
                Pair(
                    R.string.connection_error_title,
                    R.string.connection_error_message
                )
            }
        }
    }
}

class ApiFilterUtils {
    companion object {
        fun checkEmptyOverlappedIds(overlappedBookings: String): String? {
            return if (overlappedBookings.isEmpty()) {
                null
            } else {
                "_id nin array(${overlappedBookings})"
            }
        }
    }
}