package com.jalasoft.bureau.utils

import androidx.appcompat.app.AppCompatDelegate
import okhttp3.internal.UTC
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.junit.Assert
import org.junit.Test
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

class ConstantsTest {

    private val globalConstants = GlobalConstants()
    private val stringUtils = StringUtils()
    private val dateUtils = DateUtils()
    private val darkModeUtils = DarkModeUtils()
    private val urlQR = "http://bureau.ses-unit.com/assets/61a7821815686466584bdf77/confirm"
    private val randomUrl = "domain.com"
    private val notUrl = "string"
    private val assetID = "61a7821815686466584bdf77"

    @Test
    fun testCapitalizeFunction() {
        Assert.assertEquals("Constant", StringUtils.capitalize("constant"))
    }

    @Test
    fun testFormatAssetTypeFunction() {
        Assert.assertEquals("Hello world", StringUtils.formatAssetType("hello-world"))
    }

    @Test
    fun testIsURL() {
        Assert.assertFalse(StringUtils.isURL(notUrl))
        Assert.assertTrue(StringUtils.isURL(randomUrl))
        Assert.assertTrue(StringUtils.isURL(urlQR))
    }

    @Test
    fun testGetAssetIdFromQRUrl() {
        var actual = StringUtils.getAssetIdFromQRUrl(urlQR)
        Assert.assertEquals(assetID, actual)
        val empty = ""
        actual = StringUtils.getAssetIdFromQRUrl(notUrl)
        Assert.assertEquals(empty,actual)
        actual = StringUtils.getAssetIdFromQRUrl(randomUrl)
        Assert.assertEquals(empty,actual)
    }

    @Test
    fun testStringToLocalDateTime() {
        val date = LocalDate.of(2022, 1,28)
        val dateString = "2021-11-05T14:01:00Z"
        val expected = DateUtils.stringToLocalDateTime(date,  dateString, ZoneId.systemDefault())
        val calendar = Calendar.getInstance()
        calendar.time = DateTime(
            dateString,
            DateTimeZone.forTimeZone(TimeZone.getDefault())
        ).toDate()
        val actual = LocalDateTime.ofInstant(
            calendar.toInstant(),
            ZoneId.systemDefault()
        )
        Assert.assertEquals(expected.year, 2022)
        Assert.assertEquals(expected.monthValue, 1)
        Assert.assertEquals(expected.dayOfMonth, 28)
        Assert.assertEquals(expected.hour, actual.hour)
        Assert.assertEquals(expected.minute, actual.minute)
    }

    @Test
    fun testStringToLocalTime() {
        val dateString = "2021-11-05T14:01:00"
        val expected = DateUtils.stringToLocalTime(dateString)
        val calendar = Calendar.getInstance()
        calendar.time = DateTime(
            dateString,
            DateTimeZone.forTimeZone(java.util.TimeZone.getDefault())
        ).toDate()
        val actual = LocalDateTime.ofInstant(
            calendar.toInstant(),
            ZoneId.systemDefault()
        )
        Assert.assertEquals(expected.hour, actual.hour)
        Assert.assertEquals(expected.minute, actual.minute)
    }

    @Test
    fun testOrdinalFormOfNumber() {
        Assert.assertEquals("1st", DateUtils.ordinalFormOfNumber(1))
        Assert.assertEquals("2nd", DateUtils.ordinalFormOfNumber(2))
        Assert.assertEquals("3rd", DateUtils.ordinalFormOfNumber(3))
        Assert.assertEquals("4th", DateUtils.ordinalFormOfNumber(4))
        Assert.assertEquals("11th", DateUtils.ordinalFormOfNumber(11))
        Assert.assertEquals("12th", DateUtils.ordinalFormOfNumber(12))
        Assert.assertEquals("13th", DateUtils.ordinalFormOfNumber(13))
        Assert.assertEquals("21st", DateUtils.ordinalFormOfNumber(21))
        Assert.assertEquals("22nd", DateUtils.ordinalFormOfNumber(22))
        Assert.assertEquals("23rd", DateUtils.ordinalFormOfNumber(23))
        Assert.assertEquals("24th", DateUtils.ordinalFormOfNumber(24))
        Assert.assertEquals("31st", DateUtils.ordinalFormOfNumber(31))
    }

    @Test
    fun testDaysOfMonth() {
        Assert.assertEquals(31, DateUtils.daysOfMonth(1))
        Assert.assertEquals(28, DateUtils.daysOfMonth(2))
        Assert.assertEquals(31, DateUtils.daysOfMonth(3))
        Assert.assertEquals(30, DateUtils.daysOfMonth(4))
        Assert.assertEquals(31, DateUtils.daysOfMonth(5))
        Assert.assertEquals(30, DateUtils.daysOfMonth(6))
        Assert.assertEquals(31, DateUtils.daysOfMonth(7))
        Assert.assertEquals(31, DateUtils.daysOfMonth(8))
        Assert.assertEquals(30, DateUtils.daysOfMonth(9))
        Assert.assertEquals(31, DateUtils.daysOfMonth(10))
        Assert.assertEquals(30, DateUtils.daysOfMonth(11))
        Assert.assertEquals(31, DateUtils.daysOfMonth(12))
    }

    @Test
    fun testHourRangeFormat() {
        TimeZone.getTimeZone(UTC.toZoneId())
        val startTime = LocalDate.of(2000, 1, 1).atStartOfDay()
        val endTime = LocalDateTime.of(2000, 1, 1, 15, 30)
        val range = DateUtils.hourRangeFormat(startTime, endTime)
        Assert.assertEquals("00:00 - 15:30", range)
    }

    @Test
    fun testIsToday() {
        Assert.assertFalse(DateUtils.isToday(null))
        Assert.assertTrue(DateUtils.isToday(LocalDateTime.now()))
        Assert.assertFalse(DateUtils.isToday(LocalDateTime.now().plusDays(10)))
        Assert.assertFalse(DateUtils.isToday(LocalDateTime.now().plusDays(100)))
        Assert.assertFalse(DateUtils.isToday(LocalDateTime.now().plusDays(1000)))
    }

    @Test
    fun testDarkModeFunction() {
        val systemOption = DarkModeUtils.USE_SYSTEM_OPTION
        DarkModeUtils.darkMode(systemOption)
        Assert.assertEquals(-1, AppCompatDelegate.getDefaultNightMode())
        val lightOption = DarkModeUtils.LIGHT_MODE_OPTION
        DarkModeUtils.darkMode(lightOption)
        Assert.assertEquals(1, AppCompatDelegate.getDefaultNightMode())
        val emptyOption = ""
        DarkModeUtils.darkMode(emptyOption)
        Assert.assertEquals(-1, AppCompatDelegate.getDefaultNightMode())
        val darkOption = DarkModeUtils.DARK_MODE_OPTION
        DarkModeUtils.darkMode(darkOption)
        Assert.assertEquals(2, AppCompatDelegate.getDefaultNightMode())
        val nullOption = null
        DarkModeUtils.darkMode(nullOption)
        Assert.assertEquals(-1, AppCompatDelegate.getDefaultNightMode())
    }

    @Test
    fun testReturnAppearanceFlag() {
        Assert.assertEquals(2, DarkModeUtils.returnAppearanceFlag(DarkModeUtils.DARK_MODE_OPTION))
        Assert.assertEquals(1, DarkModeUtils.returnAppearanceFlag(DarkModeUtils.LIGHT_MODE_OPTION))
        Assert.assertEquals(-1, DarkModeUtils.returnAppearanceFlag(null))
        Assert.assertEquals(-1, DarkModeUtils.returnAppearanceFlag(DarkModeUtils.USE_SYSTEM_OPTION))
        Assert.assertEquals(-1, DarkModeUtils.returnAppearanceFlag(""))
    }

}