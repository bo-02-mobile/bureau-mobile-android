package com.jalasoft.bureau.mocks

import com.jalasoft.bureau.data.interfaces.BadRequest
import com.jalasoft.bureau.data.model.common.Owner
import com.jalasoft.bureau.data.model.common.RecurrenceRule
import com.jalasoft.bureau.data.model.scanner.BookingConfirmRequestBody
import com.jalasoft.bureau.data.model.scanner.BookingConfirmResponse
import com.jalasoft.bureau.ui.scanner.repository.IQRScannerRepository
import com.jalasoft.bureau.utils.ErrorMessage
import okhttp3.ResponseBody.Companion.toResponseBody
import retrofit2.Response
import retrofit2.Retrofit

class BookingValidationRepositoryMock: IQRScannerRepository {
    override suspend fun confirmBookingByCode(
        bookingId: String,
        requestBody: BookingConfirmRequestBody,
    ): Response<BookingConfirmResponse>? {
        val responseMock = BookingConfirmResponse(
            "123abc",
            "3CF3GD",
            "mock@mock.com",
            "mock name",
            "2021-06-21T21:00:00Z",
            "2021-06-21T22:00:00Z",
            false,
            RecurrenceRule(),
            isPermanent = false,
            isConfirmed = true,
            "60b143eeb74284b5f27ffad4",
            "Main floor",
            "CR01 - Wakanda",
            "60b12da6b74284b5f27ffabe",
            "conference-room",
            "60b12d71b74284b5f27ffabd",
            "2021-08-20T01:53:39.551Z",
            "2021-08-20T01:36:21.539Z",
            Owner("kevinviscafe@gmail.com",null,"Kevin Viscafe"),
            "2021-08-20T01:53:39Z",
            1,
        )
        if (bookingId != "3CF3GD") {
            val badResponse = "".toResponseBody(null)
            return Response.error(404, badResponse)
        }
        return Response.success(responseMock)
    }
}

class MockBadResponseListener: BadRequest {

    var fetchBadRequestCalled = false
    override fun fetchBadRequest(errorType: ErrorMessage) {
        fetchBadRequestCalled = true
    }

}
