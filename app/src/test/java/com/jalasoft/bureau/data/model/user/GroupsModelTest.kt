package com.jalasoft.bureau.data.model.user
import org.junit.Assert
import org.junit.Test

class UserModelTest {
    @Test
    fun checkGroupsPropertiesAreSet(){
        val group: Groups.Group = Groups.Group(
            "6243286a3504259aea126fae",
            "officeQA",
            mapOf(Pair("Lounge",2)),
            true,
            true,
            "2022-03-29T15:40:26.631Z",
            "2022-03-29T15:41:48.313Z",
            1
        )

        Assert.assertEquals("6243286a3504259aea126fae", group.id)
        Assert.assertEquals("officeQA", group.name)
        Assert.assertEquals(mapOf(Pair("Lounge",2)), group.quota)
        Assert.assertEquals(true, group.bookForOthers)
        Assert.assertEquals(true, group.recurrenceBookings)
        Assert.assertEquals("2022-03-29T15:40:26.631Z", group.createdAt)
        Assert.assertEquals("2022-03-29T15:41:48.313Z", group.updatedAt)
        Assert.assertEquals(1, group.version)
    }
}