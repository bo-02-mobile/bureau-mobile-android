package com.jalasoft.bureau.data.model.common

import org.junit.Assert
import org.junit.Test

class RecurrenceRuleModelTest {
    var recurrenceModel = RecurrenceRule()

    @Test
    fun checkPropertiesAreSet(){
        recurrenceModel.isCustom = true
        recurrenceModel.type = "type"
        recurrenceModel.endDate = "0001-01-01T00:00:00Z"
        recurrenceModel.onDays = arrayOf("2022-01-01T15:15:32.521Z", "2022-01-02T15:16:42.435Z")
        recurrenceModel.onMonth = 1
        recurrenceModel.onWeek = 3
        recurrenceModel.onDate = 4
        recurrenceModel.every = 6

        Assert.assertEquals(true, recurrenceModel.isCustom)
        Assert.assertEquals("type", recurrenceModel.type)
        Assert.assertEquals("0001-01-01T00:00:00Z", recurrenceModel.endDate)
        Assert.assertArrayEquals(arrayOf("2022-01-01T15:15:32.521Z", "2022-01-02T15:16:42.435Z"), recurrenceModel.onDays)
        Assert.assertEquals(1, recurrenceModel.onMonth)
        Assert.assertEquals(3, recurrenceModel.onWeek)
        Assert.assertEquals(4, recurrenceModel.onDate)
        Assert.assertEquals(6, recurrenceModel.every)
    }

    @Test
    fun checkIfIsEmpty(){
        val isEmpty = recurrenceModel.isEmpty()
        Assert.assertEquals(true, isEmpty)
    }

    @Test
    fun checkIfIsNotEmpty(){
        recurrenceModel.isCustom = true
        recurrenceModel.type = "type"
        val isNotEmpty = recurrenceModel.isEmpty()
        Assert.assertEquals(false, isNotEmpty)
    }
}