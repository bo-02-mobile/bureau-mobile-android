package com.jalasoft.bureau.data.model.workstation

import org.junit.Assert
import org.junit.Test

class WorkstationModelTest {
    @Test
    fun checkWorkstationItemPropertiesAreSet(){

        val workstationItem = Workstation.WorkstationItem(
            "61a780db15686466584bdf6f",
            "CDE1",
            "desc",
            listOf("power", "wifi"),
            "WRSKT1",
            "power",
            "UTC",
            false,
            2.0,
            3.0
        )
        Assert.assertEquals("61a780db15686466584bdf6f", workstationItem.areaId)
        Assert.assertEquals("CDE1", workstationItem.code)
        Assert.assertEquals("desc", workstationItem.description)
        Assert.assertEquals(listOf("power", "wifi"), workstationItem.features)
        Assert.assertEquals("WRSKT1", workstationItem.id)
        Assert.assertEquals("power", workstationItem.imageLocation)
        Assert.assertEquals("UTC", workstationItem.kind)
        Assert.assertEquals(false, workstationItem.published)
        Assert.assertEquals(2.0, workstationItem.ratingAvg, 1.0)
        Assert.assertEquals(3.0, workstationItem.ratingCount, 1.0)
    }

    @Test
    fun checkWorkstationLayoutPropertiesAreSet(){
        val markers = listOf(
            Workstation.Marker("LCKD", true, 1.0, 4.0),
            Workstation.Marker("LCWS", true, 1.0, 4.0)
        )
        val workstationLayout = Workstation.WorkstationLayout(
            "LCWD",
            "61a780db15686466584bdf6f",
            "BKGRND",
            markers,
        )
        Assert.assertEquals("LCWD", workstationLayout.id)
        Assert.assertEquals("61a780db15686466584bdf6f", workstationLayout.areaCode)
        Assert.assertEquals("BKGRND", workstationLayout.background)
        Assert.assertEquals(markers.size, 2)
        Assert.assertEquals("LCKD", markers[0].workstationId)
        Assert.assertEquals(true, markers[0].published)
        Assert.assertEquals(1.0, markers[0].x, 1.0)
        Assert.assertEquals(4.0, markers[0].y, 1.0)
    }
}