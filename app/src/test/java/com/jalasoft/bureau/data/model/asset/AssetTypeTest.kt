package com.jalasoft.bureau.data.model.asset

import org.junit.Assert
import org.junit.Test

class AssetTypeTest {

    val assetTypes = AssetType()

    val assetTypeAll = AssetType.AssetTypeItem("all", emptyList(),"all", "all", "all")
    val assetTypeOther = AssetType.AssetTypeItem("other", emptyList(), "other", "other", "other")

    @Test
    fun verifyAssetTypes() {
        Assert.assertEquals(0, assetTypes.size)
        assetTypes.add(assetTypeAll)
        assetTypes.add(assetTypeOther)
        Assert.assertEquals(2, assetTypes.size)
    }

    @Test
    fun verifyAssetTypeProperties() {
        Assert.assertEquals("all", assetTypeAll.id)
        Assert.assertEquals("all", assetTypeAll.name)
        Assert.assertEquals("all", assetTypeAll.text)
        Assert.assertEquals("all", assetTypeAll.value)
    }
}