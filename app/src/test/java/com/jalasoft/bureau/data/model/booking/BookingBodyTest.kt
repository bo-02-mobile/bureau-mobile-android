package com.jalasoft.bureau.data.model.booking

import org.junit.Assert
import org.junit.Test

class BookingBodyTest {

    val bookingBody = BookingBody(
        "2021-06-31T19:15:51.205Z",
        false,
        null,
        "2021-07-31T19:20:51.205Z"
    )

    @Test
    fun verifyPropertiesOfBookingBundle() {
        Assert.assertEquals("2021-06-31T19:15:51.205Z", bookingBody.endTime)
        Assert.assertEquals(false, bookingBody.isRecurrent)
        Assert.assertEquals("2021-07-31T19:20:51.205Z", bookingBody.startTime)
    }
}