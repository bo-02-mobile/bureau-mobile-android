package com.jalasoft.bureau.data.model.booking

import com.jalasoft.bureau.utils.PENDING
import org.junit.Assert
import org.junit.Test

class PostBookingTest {

    val bookingItem = PostBooking(
        "610446ee846769858d9ccf6f",
        "2021-06-31T19:15:51.205Z",
        true,
        null,
        null,
        "2021-07-31T19:20:51.205Z",
        Status(PENDING)
    )

    @Test
    fun verifyPropertiesOfBookingItem() {
        Assert.assertEquals("610446ee846769858d9ccf6f", bookingItem.assetId)
        Assert.assertEquals("2021-06-31T19:15:51.205Z", bookingItem.endTime)
        Assert.assertEquals(true, bookingItem.isRecurrent)
        Assert.assertNull(bookingItem.owner)
        Assert.assertNull(bookingItem.recurrenceRule)
        Assert.assertEquals("2021-07-31T19:20:51.205Z", bookingItem.startTime)
        Assert.assertEquals(Status(PENDING), bookingItem.status)
    }
}