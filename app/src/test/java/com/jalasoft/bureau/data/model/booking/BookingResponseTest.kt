package com.jalasoft.bureau.data.model.booking

import org.junit.Assert
import org.junit.Test

class BookingResponseTest {

    val bookingStatus = BookingResponse.Status("Pending", null, null)
    val bookingItem = BookingResponse.Booking(
        "610445e3846769858d9ccf6e",
        "NewArea3100",
        "W0031",
        "610446ee846769858d9ccf6f",
        "workstation",
        "60c11f74984d2c7a73a0da0e",
        "",
        "2021-07-29T19:15:51.205Z",
        "2021-06-31T19:15:51.205Z",
        "61044fe7846769858d9ccf75",
        false, true, null, null, "2021-07-31T19:20:51.205Z", "2021-08-30T14:15:51.205Z", 1,
        "",
        bookingStatus,
        null
    )

    val bookings = BookingResponse()

    val bookingBundle = BookingResponse.BookingBundle(
        bookingItem.assetCode,
        bookingItem.assetId,
        bookingItem.confirmedAt,
        bookingItem.startTime,
        bookingItem.endTime,
        bookingItem.id,
        bookingItem.buildingId,
        bookingItem.assetType,
        true,
        bookingItem.reservationMessage,
        bookingItem.isPermanent,
        bookingItem.areaName
    )

    @Test
    fun verifyPropertiesOfBookingBundle() {
        Assert.assertEquals("W0031", bookingBundle.assetCode)
        Assert.assertEquals("610446ee846769858d9ccf6f", bookingBundle.assetId)
        Assert.assertEquals("", bookingBundle.confirmedAt)
        Assert.assertEquals("2021-07-31T19:20:51.205Z", bookingBundle.startTime)
        Assert.assertEquals("2021-06-31T19:15:51.205Z", bookingBundle.endTime)
        Assert.assertEquals("61044fe7846769858d9ccf75", bookingBundle.bookingId)
        Assert.assertEquals("60c11f74984d2c7a73a0da0e", bookingBundle.buildingId)
        Assert.assertEquals("workstation", bookingBundle.assetType)
        Assert.assertEquals(true, bookingBundle.isToday)
        Assert.assertEquals(false, bookingBundle.isPermanent)
        Assert.assertEquals("NewArea3100", bookingBundle.areaName)
    }

    @Test
    fun verifyListOfBookings() {
        Assert.assertEquals(0, bookings.size)
        bookings.add(bookingItem)
        Assert.assertEquals(1, bookings.size)
    }

    @Test
    fun verifyIdOfBookingItem(){
        Assert.assertEquals("61044fe7846769858d9ccf75",bookingItem.id)
    }

    @Test
    fun verifyNameOfBooking(){
        Assert.assertEquals("NewArea3100",bookingItem.areaName)
    }

    @Test
    fun verifyBuildingIdOfBooking(){
        Assert.assertEquals("60c11f74984d2c7a73a0da0e",bookingItem.buildingId)
    }

    @Test
    fun verifyAreaIdOfBooking(){
        Assert.assertEquals("610445e3846769858d9ccf6e",bookingItem.areaId)
    }

    @Test
    fun assetCodeOfBooking(){
        Assert.assertEquals("W0031",bookingItem.assetCode)
    }

    @Test
    fun verifyBuildingStatusBooking(){
        Assert.assertEquals(bookingStatus,bookingItem.status)
    }
}