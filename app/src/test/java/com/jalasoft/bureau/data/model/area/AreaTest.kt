package com.jalasoft.bureau.data.model.area

import org.junit.Assert
import org.junit.Test

class AreaTest {

    val marker1 = Area.AreaItem.Marker("610446ee846769858d9ccf6f", true, 20.5, 20.5)
    val marker2 = Area.AreaItem.Marker("600446ee846769858d9ccf6f", false, 80.5, 20.5)

    val area = Area.AreaItem(
        Area.AreaItem.Background("uyz1ftb3fhpfnkktajo0",
            "https://res.cloudinary.com/dbjicvy8t/image/upload/v1638367454/uyz1ftb3fhpfnkktajo0.jpg"),
        "60a807c5dc0ba843b1ac4063",
        "3A",
        "2020-06-28T18:32:23.207Z",
        "", "60a807f1dc0ba843b1ac4064",
        listOf(marker1, marker2),
        "3A", true,
        Area.AreaItem.Thumbnail("fzejpp2fr3doyq5moott",
            "https://res.cloudinary.com/dbjicvy8t/image/upload/v1638367452/fzejpp2fr3doyq5moott.jpg"),
        "2020-06-28T14:32:23.207Z", 1, null
    )

    val areas = Area()

    @Test
    fun verifyListOfAreas() {
        Assert.assertEquals(0, areas.size)
        areas.add(area)
        Assert.assertEquals(1, areas.size)
    }

    @Test
    fun verifyBackgroundProperty() {
        Assert.assertEquals("uyz1ftb3fhpfnkktajo0", area.background?.id)
        Assert.assertEquals("https://res.cloudinary.com/dbjicvy8t/image/upload/v1638367454/uyz1ftb3fhpfnkktajo0.jpg", area.background?.url)
    }

    @Test
    fun verifyThumbnailProperty() {
        Assert.assertEquals("fzejpp2fr3doyq5moott", area.thumbnail?.id)
        Assert.assertEquals("https://res.cloudinary.com/dbjicvy8t/image/upload/v1638367452/fzejpp2fr3doyq5moott.jpg", area.thumbnail?.url)
    }

    @Test
    fun verifyMarkerProperty() {
        Assert.assertEquals("610446ee846769858d9ccf6f", area.markers?.get(0)?.assetId)
        Assert.assertEquals(20.5, area.markers?.get(1)?.y)
    }
}