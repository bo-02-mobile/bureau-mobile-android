package com.jalasoft.bureau.data.model.building

import org.junit.Assert
import org.junit.Test

class BuildingTest{

    val building = Building.BuildingItem(
        "JALA-COCHA",
        "Av Melchor Pérez de Olguín 2643",
        "60a807c5dc0ba843b1ac4063",
        "Jala Building",
        "Jala Building",
        true,
        "UTC-04",
        "2021-06-21",
        1,
        "2"
    )

    val buildings = Building()

    @Test
    fun verifyListOfBuildings() {
        Assert.assertEquals(0, buildings.size)
        buildings.add(building)
        Assert.assertEquals(1, buildings.size)
    }

    @Test
    fun verifyIdOfBuildingItem(){
        Assert.assertEquals("Jala Building",building.id)
    }

    @Test
    fun verifyCodeOfBuildingItem(){
        Assert.assertEquals("Av Melchor Pérez de Olguín 2643",building.code)
    }

    @Test
    fun verifyNameOfBuildingItem(){
        Assert.assertEquals("Jala Building",building.name)
    }

    @Test
    fun verifyAddressOfBuildingItem(){
        Assert.assertEquals("JALA-COCHA",building.address)
    }

    @Test
    fun verifyTimezoneOfBuildingItem(){
        Assert.assertEquals("UTC-04",building.timezone)
    }

    @Test
    fun verifyTotalAreasOfBuildingItem(){
        Assert.assertEquals("2",building.totalAreas)
    }
}