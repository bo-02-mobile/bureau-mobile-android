package com.jalasoft.bureau.data.model.asset

import org.junit.Assert
import org.junit.Test

class AssetLabelModelTest {
    @Test
    fun checkAssetLabelModelPropertiesAreSet(){
        val assetLabelItem = AssetLabel(
            "612d7c6e090308c71776ce21",
            "Mouse",
            "Mouse",
            "mouse"
        )

        Assert.assertEquals("612d7c6e090308c71776ce21", assetLabelItem.id)
        Assert.assertEquals("Mouse", assetLabelItem.name)
        Assert.assertEquals("Mouse", assetLabelItem.text)
        Assert.assertEquals("mouse", assetLabelItem.value)
    }
}