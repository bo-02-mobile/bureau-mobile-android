package com.jalasoft.bureau.data.model.asset

import com.jalasoft.bureau.ui.workstations.repository.AssetResultRepository
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert
import org.junit.Test
import org.mockito.kotlin.mock
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

class AssetResultRepositoryTest {

    private val mockWebServer = MockWebServer()

    private val client = OkHttpClient.Builder()
        .connectTimeout(1, TimeUnit.SECONDS)
        .readTimeout(1, TimeUnit.SECONDS)
        .writeTimeout(1, TimeUnit.SECONDS)
        .build()

    private val api = Retrofit.Builder()
        .baseUrl(mockWebServer.url("/"))
        .client(client)
        .build()

    val repository = AssetResultRepository(api)

    @Test
    fun getAreaDetailMock(){
        val response = repository.getAreaDetailMock()
        Assert.assertEquals(response.buildingId, "60a807c5dc0ba843b1ac4063")
    }

    @Test
    fun getAreaCodeDetailMock(){
        val response = repository.getAreaDetailMock()
        Assert.assertEquals("3A",response.code)
    }
}