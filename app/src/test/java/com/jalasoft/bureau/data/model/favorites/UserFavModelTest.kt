package com.jalasoft.bureau.data.model.favorites

import com.jalasoft.bureau.data.model.user.Groups
import org.junit.Assert
import org.junit.Test

class UserFavModelTest {
    @Test
    fun checkUserFavPropertiesAreSet(){
        val userFav = UserFav(
            "60b14364b74284b5f27fface",
            "esda3abe9e-a4a1-asd3245sdfv",
            "Pedro",
            "Pedro",
            "Perez",
            "Pedro Perez",
            "pedro@gmail.com",
            "administrator",
            "https://lh3.googleusercontent.com/a/AATXAJwTrrLqztPRtd1iBtaYao_LQ-6luvvdoaHkYSaC=s96-c",
            true,
            listOf("0b345364as6dfa123qwe"),
            listOf("61771fdeec33bf74b4d89a97"),
            "2021-12-29T16:17:31.992Z",
            "2022-05-04T13:55:18.328Z",
            3
        )

        Assert.assertEquals("60b14364b74284b5f27fface", userFav.id)
        Assert.assertEquals("esda3abe9e-a4a1-asd3245sdfv", userFav.externalID)
        Assert.assertEquals("Pedro", userFav.name)
        Assert.assertEquals("Pedro", userFav.givenName)
        Assert.assertEquals("Perez", userFav.familyName)
        Assert.assertEquals("Pedro Perez", userFav.preferredUsername)
        Assert.assertEquals("pedro@gmail.com", userFav.email)
        Assert.assertEquals("administrator", userFav.role)
        Assert.assertEquals("https://lh3.googleusercontent.com/a/AATXAJwTrrLqztPRtd1iBtaYao_LQ-6luvvdoaHkYSaC=s96-c", userFav.picture)
        Assert.assertEquals(true, userFav.emailVerified)
        Assert.assertEquals(listOf("0b345364as6dfa123qwe"), userFav.groups)
        Assert.assertEquals(listOf("61771fdeec33bf74b4d89a97"), userFav.favoriteAssets)
        Assert.assertEquals("2021-12-29T16:17:31.992Z", userFav.createdAt)
        Assert.assertEquals("2022-05-04T13:55:18.328Z", userFav.updatedAt)
        Assert.assertEquals(3, userFav.version)


    }
}