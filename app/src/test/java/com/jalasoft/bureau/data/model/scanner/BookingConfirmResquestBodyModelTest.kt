package com.jalasoft.bureau.data.model.scanner

import org.junit.Assert
import org.junit.Test

class BookingConfirmResquestBodyModelTest {
    @Test
    fun checkPropertiesAreSet(){
        val bookingConfirmRequestBody = BookingConfirmRequestBody("replace", "/ConfirmedAt", "2021-11-15T17:00:00Z")
        Assert.assertEquals("replace", bookingConfirmRequestBody.op)
        Assert.assertEquals("/ConfirmedAt", bookingConfirmRequestBody.path)
        Assert.assertEquals("2021-11-15T17:00:00Z", bookingConfirmRequestBody.value)
    }
}