package com.jalasoft.bureau.data.model.scanner

import com.jalasoft.bureau.data.model.common.Owner
import com.jalasoft.bureau.data.model.common.RecurrenceRule
import org.junit.Assert
import org.junit.Test

class BookingConfirmResponseModelTest {
    @Test
    fun checkPropertiesAreSet(){
        val recurrenceRule = RecurrenceRule()
        val bookingConfirmRequest = BookingConfirmResponse(
            "61a780db15686466584bdf6h",
            "ABCD",
            "Pedro@gmail.com",
            "Pedro",
            "2021-11-01T17:00:00Z",
            "2021-11-02T19:00:00Z",
            true,
            recurrenceRule,
            false,
            false,
            "61a780db15686466584bdf6f",
            "1C Cowork",
            "1C",
            "61a781cc15686466584bdf74",
            "Workstation",
            "60a807c5dc0ba843b1ac4063",
            "2021-11-01T15:00:00Z",
            "2021-11-01T14:30:00Z",
            Owner("Pedro@gmail.com", "60fec6871f9de1fa8a070583", "Pedro"),
            "2021-11-01T14:40:00Z",
            3
        )

        Assert.assertEquals("61a780db15686466584bdf6h", bookingConfirmRequest.id)
        Assert.assertEquals("ABCD", bookingConfirmRequest.workstationId)
        Assert.assertEquals("Pedro@gmail.com", bookingConfirmRequest.ownerEmail)
        Assert.assertEquals("Pedro", bookingConfirmRequest.ownerName)
        Assert.assertEquals("2021-11-01T17:00:00Z", bookingConfirmRequest.startTime)
        Assert.assertEquals("2021-11-02T19:00:00Z", bookingConfirmRequest.endTime)
        Assert.assertEquals(true, bookingConfirmRequest.isRecurrent)
        Assert.assertEquals(recurrenceRule, bookingConfirmRequest.recurrenceRule)
        Assert.assertEquals(false, bookingConfirmRequest.isPermanent)
        Assert.assertEquals(false, bookingConfirmRequest.isConfirmed)
        Assert.assertEquals("61a780db15686466584bdf6f", bookingConfirmRequest.areaId)
        Assert.assertEquals("1C Cowork", bookingConfirmRequest.areaName)
        Assert.assertEquals("1C", bookingConfirmRequest.assetCode)
        Assert.assertEquals("61a781cc15686466584bdf74", bookingConfirmRequest.assetId)
        Assert.assertEquals("Workstation", bookingConfirmRequest.assetType)
        Assert.assertEquals("60a807c5dc0ba843b1ac4063", bookingConfirmRequest.buildingId)
        Assert.assertEquals("2021-11-01T15:00:00Z", bookingConfirmRequest.confirmedAt)
        Assert.assertEquals("2021-11-01T14:30:00Z", bookingConfirmRequest.createdAt)
        Assert.assertEquals(Owner("Pedro@gmail.com", "60fec6871f9de1fa8a070583", "Pedro"), bookingConfirmRequest.owner)
        Assert.assertEquals("2021-11-01T14:40:00Z", bookingConfirmRequest.updatedAt)
        Assert.assertEquals(3, bookingConfirmRequest.version)
    }
}