package com.jalasoft.bureau.data.model.asset

import org.junit.Assert
import org.junit.Test

class AssetTest {

    private val asset = Asset.AssetItem(
        "60b13fe3b74284b5f27ffacb",
        "60a807c5dc0ba843b1ac4063",
        "1C001",
        "2021-07-26T14:28:10.294Z",
        "",
        "60b14364b74284b5f27fface",
        "1C001",
        Asset.AssetItem.Properties(listOf("Wifi", "Ethernet"), null, "workstation"),
        Asset.AssetItem.Policies(5, false, 30, 720,
            listOf(Asset.AssetItem.Schedule("Id123", "20200101", "20211212"))
        ),
        true,
        Asset.AssetItem.Stats(10, 20),
        "2021-07-26T14:28:10.294Z",
        1,
        true
    )

    private val assets = Asset()

    @Test
    fun verifyListOfAssets() {
        Assert.assertEquals(0, assets.size)
        assets.add(asset)
        Assert.assertEquals(1, assets.size)
    }

    @Test
    fun verifyPropertiesOfAsset() {
        Assert.assertEquals(2, asset.properties?.labels?.size)
        Assert.assertEquals("Ethernet", asset.properties?.labels?.get(1))
        Assert.assertNull(asset.properties?.owner)
        Assert.assertEquals("workstation", asset.properties?.type)
    }

    @Test
    fun verifyPoliciesOfAsset() {
        Assert.assertEquals(5, asset.policies?.bookingWindow)
        Assert.assertEquals(false, asset.policies?.isRecurrent)
        Assert.assertEquals("Id123",asset.policies?.schedule?.get(0)?.id)
        Assert.assertEquals(30, asset.policies?.min)
        Assert.assertEquals(720, asset.policies?.max)
    }

    @Test
    fun verifyStatsOfAsset() {
        Assert.assertEquals(10, asset.stats?.average)
        Assert.assertEquals(20, asset.stats?.count)
    }

    @Test
    fun verifyAreaIdOfAssetItem() {
        Assert.assertEquals(asset.areaId, "60b13fe3b74284b5f27ffacb")
    }

    @Test
    fun verifyBuildingIdOfAssetItem(){
        Assert.assertEquals(asset.buildingId,"60a807c5dc0ba843b1ac4063")
    }

    @Test
    fun verifyCodeOfAssetItem(){
        Assert.assertEquals(asset.code,"1C001")
    }

    @Test
    fun verifyIdOfAssetItem(){
        Assert.assertEquals(asset.id,"60b14364b74284b5f27fface")
    }

    @Test
    fun verifyNameOfAssetItem(){
        Assert.assertEquals(asset.name,"1C001")
    }

    @Test
    fun verifyAvailability(){
        Assert.assertEquals(asset.available, true)
    }
}