package com.jalasoft.bureau.data.model.common

import org.junit.Assert
import org.junit.Test

class OwnerModelTest {
    @Test
    fun checkPropertiesAreSet(){
        val owner = Owner("carlos@gmail.com","60fec6871f9de1fa8a070583","Carlos")
        Assert.assertEquals("Carlos", owner.name)
        Assert.assertEquals("carlos@gmail.com", owner.email)
        Assert.assertEquals("60fec6871f9de1fa8a070583", owner.id)
    }
}