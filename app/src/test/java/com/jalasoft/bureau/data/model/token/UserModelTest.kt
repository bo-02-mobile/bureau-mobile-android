package com.jalasoft.bureau.data.model.token

import junit.framework.Assert
import org.junit.Test

class UserModelTest {
    @Test
    fun checkPropertiesAreSet(){
        val user = User("1", "pedro@gmail.com", "https://lh3.googleusercontent.com/a/AATXAJwTrrLqztPRtd1iBtaYao_LQ-6luvvdoaHkYSaC=s96-c", "Pedro")
        Assert.assertEquals("pedro@gmail.com", user.email)
        Assert.assertEquals("https://lh3.googleusercontent.com/a/AATXAJwTrrLqztPRtd1iBtaYao_LQ-6luvvdoaHkYSaC=s96-c", user.picture)
        Assert.assertEquals("Pedro", user.name)
    }
}