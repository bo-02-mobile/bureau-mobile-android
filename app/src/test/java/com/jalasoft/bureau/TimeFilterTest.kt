package com.jalasoft.bureau

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.rules.TestCoroutineRule
import com.jalasoft.bureau.ui.bookings.repository.MainMyBookingsRepository
import com.jalasoft.bureau.ui.main.repository.MainActivityRepository
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.utils.timeFilter.TimeFilter
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import java.time.*
import java.time.temporal.ChronoUnit

@ExperimentalCoroutinesApi
class TimeFilterTest {

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: MainActivityViewModel
    private lateinit var timeFilter: TimeFilter
    private lateinit var context: Context

    @Before
    fun setUp() {
        context = Mockito.mock(Context::class.java)
        val mainMock = mock<MainActivityRepository>()
        val myBookingMock = mock<MainMyBookingsRepository>()
        val prefs = Prefs(context)
        viewModel = MainActivityViewModel(mainMock, myBookingMock, prefs)
        testCoroutineRule.runBlockingTest {
            viewModel.initDateTimeFiltersDefault()
            val date = viewModel.myDate.value
            val start = viewModel.userStartTime.value
            val end = viewModel.userEndTime.value
            timeFilter = TimeFilter(date, start, end)
        }
    }

    @Test
    fun testDateDefaultValue() {
        val expected = viewModel.myDate.value
        val actual = LocalDate.now()
        assertEquals(expected, actual)
    }

    @Test
    fun testStartTimeDefaultValue() {
        val expected = viewModel.userStartTime.value
        val actual = LocalTime.of(8, 30)
        assertEquals(expected, actual)
    }

    @Test
    fun testEndTimeDefaultValue() {
        val expected = viewModel.userEndTime.value
        val actual = LocalTime.of(18, 30)
        assertEquals(expected, actual)
    }

    @Test
    fun testPostDateValue() {
        testCoroutineRule.runBlockingTest {
            viewModel.postDate(System.currentTimeMillis())
        }
        val expected = viewModel.myDate.value
        val actual = LocalDateTime.ofInstant(
            Instant.ofEpochMilli(System.currentTimeMillis()),
            ZoneId.of("UTC")
        ).toLocalDate()
        assertEquals(expected, actual)
    }


    @Test
    fun testPostStartTimeValue() {
        val minute = 0
        val hour = 19
        viewModel.userEndTime.postValue(LocalTime.of(20, 0))
        testCoroutineRule.runBlockingTest {
            viewModel.postStartTime(hour, minute)
        }
        val localTime = LocalTime.now()
        var temporalMinute = 0
        if (minute >= 30) {
            temporalMinute = 30
        }
        var actual = LocalTime.of(hour, temporalMinute)
        if (actual.hour > hour) {
            actual = LocalTime.now().plusHours(1)
        } else {
            if (actual.hour == hour && minute > temporalMinute) {
                if (localTime.minute >= 30) {
                    actual = LocalTime.now().plusHours(1).truncatedTo(ChronoUnit.HOURS)
                } else {
                    temporalMinute = 30
                    actual = LocalTime.of(hour, temporalMinute)
                }
            }
        }
        val expected = viewModel.userStartTime.value
        assertEquals(expected, actual)
    }

    @Test
    fun testPostEndTimeValue() {
        testCoroutineRule.runBlockingTest {
            viewModel.postEndTime(23, 30)
        }
        val actual = LocalTime.of(23, 30)
        val expected = viewModel.userEndTime.value
        assertEquals(expected, actual)
    }

    @Test
    fun testTimeFilterDate() {
        assertEquals(timeFilter.date, viewModel.myDate.value)
    }

    @Test
    fun testTimeFilterStart() {
        assertEquals(timeFilter.start, viewModel.userStartTime.value)
    }

    @Test
    fun testTimeFilterEnd() {
        assertEquals(timeFilter.end, viewModel.userEndTime.value)
    }
}