@file:Suppress("SpellCheckingInspection", "SpellCheckingInspection", "SpellCheckingInspection",
    "SpellCheckingInspection", "SpellCheckingInspection", "SpellCheckingInspection",
    "SpellCheckingInspection", "SpellCheckingInspection", "SpellCheckingInspection",
    "SpellCheckingInspection")

package com.jalasoft.bureau

import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.ui.main.repository.MainRepositoryMocking
import org.junit.Assert
import org.junit.Test

class HomeUnitTest {

    private val repository = MainRepositoryMock()

    @Test
    fun getBuildingProduction(){
        val temp = repository.getBuildingsProduction()
        Assert.assertEquals(temp.size,3)
    }

    @Test
    fun getBuildingDemo(){
        val temp = repository.getBuildingsDemo()
        Assert.assertEquals(temp.size,4)
    }

    @Test
    fun getAreaBuildingByIdProduction(){
        val temp = repository.getAreaByBuildingIdProduction("BIR21ETB3KAR3GR")
        Assert.assertEquals(temp.size,1)
    }

    @Test
    fun getAreaBuildingByIdDemo(){
        val temp = repository.getAreaByBuildingIdDemo("4E6R5G46RE54")
        Assert.assertEquals(temp.size,1)
    }

}

class MainRepositoryMock: MainRepositoryMocking {
    override fun getBuildingsProduction(): List<Building.BuildingItem> {
        return listOf(
            Building.BuildingItem(
                "JALA-COCHA",
                "Av Melchor Pérez de Olguín 2643",
                "60a807c5dc0ba843b1ac4063",
                "Jala Building",
                "UTC−05=00",
                true,
                "UTC-04",
                "2021-06-21",
                1,
                "0"
            ),
            Building.BuildingItem(
                "JALA-COCHA",
                "Av Melchor Pérez de Olguín 2643",
                "60a807c5dc0ba843b1ac4063",
                "Jala Building",
                "UTC−05=00",
                true,
                "UTC-04",
                "2021-06-21",
                1,
                "0"
            ),
            Building.BuildingItem(
                "JALA-COCHA",
                "Av Melchor Pérez de Olguín 2643",
                "60a807c5dc0ba843b1ac4063",
                "Jala Building",
                "UTC−05=00",
                true,
                "UTC-04",
                "2021-06-21",
                1,
                "0"
            )
        )
    }

    override fun getBuildingsDemo(): List<Building.BuildingItem> {
        return listOf(
            Building.BuildingItem(
                "JALA-COCHA",
                "Av Melchor Pérez de Olguín 2643",
                "60a807c5dc0ba843b1ac4063",
                "Jala Building",
                "UTC−05=00",
                true,
                "UTC-04",
                "2021-06-21",
                1,
                "0"
            ),
            Building.BuildingItem(
                "JALA-COCHA",
                "Av Melchor Pérez de Olguín 2643",
                "60a807c5dc0ba843b1ac4063",
                "Jala Building",
                "UTC−05=00",
                true,
                "UTC-04",
                "2021-06-21",
                1,
                "0"
            ),
            Building.BuildingItem(
                "JALA-COCHA",
                "Av Melchor Pérez de Olguín 2643",
                "60a807c5dc0ba843b1ac4063",
                "Jala Building",
                "UTC−05=00",
                true,
                "UTC-04",
                "2021-06-21",
                1,
                "0"
            ),
            Building.BuildingItem(
                "JALA-COCHA",
                "Av Melchor Pérez de Olguín 2643",
                "60a807c5dc0ba843b1ac4063",
                "Jala Building",
                "UTC−05=00",
                true,
                "UTC-04",
                "2021-06-21",
                1,
                "0"
            )
        )
    }

    override fun getAreaByBuildingIdProduction(buildingId: String): List<Area.AreaItem> {
        return listOf(
            Area.AreaItem(
                null,
                "testArea",
                "TEST-Ar1",
                "default description1",
                "area description",
                "image",
                null,"newArea",true,null,"2021-05-12",1,0)
        )
    }

    override fun getAreaByBuildingIdDemo(buildingId: String): List<Area.AreaItem> {
        return listOf(
            Area.AreaItem(
                null,
                "testArea",
                "TEST-Ar1",
                "default description1",
                "area description",
                "image",
                null,"newArea",true,null,"2021-05-12",1, 0)
        )
    }

}


