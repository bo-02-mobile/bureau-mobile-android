package com.jalasoft.bureau

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.scanner.BookingConfirmResponse
import com.jalasoft.bureau.mocks.BookingValidationRepositoryMock
import com.jalasoft.bureau.mocks.MockBadResponseListener
import com.jalasoft.bureau.rules.TestCoroutineRule
import com.jalasoft.bureau.ui.bookings.repository.MainMyBookingsRepository
import com.jalasoft.bureau.ui.scanner.repository.IQRScannerRepository
import com.jalasoft.bureau.ui.scanner.viewmodel.QRScannerViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.mock
import retrofit2.Response

@ExperimentalCoroutinesApi
class BookingValidationUnitTest {

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var repository: IQRScannerRepository
    private lateinit var viewModel: QRScannerViewModel
    private lateinit var context: Context
    private lateinit var mockListener: MockBadResponseListener

    private val onSuccess = { _: Response<BookingConfirmResponse>, _: String ->}

    @Before
    fun setUp() {
        repository = BookingValidationRepositoryMock()
        viewModel = QRScannerViewModel(repository, mock<MainMyBookingsRepository>())
        mockListener = MockBadResponseListener()
        viewModel.badRequestListener = mockListener
        context = Mockito.mock(Context::class.java)
    }

    @Test
    fun testWorkstationIdResponse() {
        testCoroutineRule.runBlockingTest {
            viewModel.confirmBooking("3CF3GD", onSuccess)
            val expected = "3CF3GD"
            val actual = viewModel.bookingConfirmResponse.value?.workstationId
            assertEquals(expected, actual)
        }
    }

    @Test
    fun testAreaIdResponse() {
        testCoroutineRule.runBlockingTest {
            viewModel.confirmBooking("3CF3GD", onSuccess)
            val expected = "123abc"
            val actual = viewModel.bookingConfirmResponse.value?.id
            assertEquals(expected, actual)
        }
    }

    @Test
    fun testIsConfirmedResponse() {
        testCoroutineRule.runBlockingTest {
            viewModel.confirmBooking("3CF3GD", onSuccess)
            val isConfirmed = viewModel.bookingConfirmResponse.value?.isConfirmed
            if (isConfirmed != null) {
                assertTrue(isConfirmed)
            }
        }
    }

    @Test
    fun testOwnerEmailResponse() {
        testCoroutineRule.runBlockingTest {
            viewModel.confirmBooking("3CF3GD", onSuccess)
            val expected = "mock@mock.com"
            val actual = viewModel.bookingConfirmResponse.value?.ownerEmail
            assertEquals(expected, actual)
        }
    }

    @Test
    fun testOwnerNameResponse() {
        testCoroutineRule.runBlockingTest {
            viewModel.confirmBooking("3CF3GD", onSuccess)
            val expected = "mock name"
            val actual = viewModel.bookingConfirmResponse.value?.ownerName
            assertEquals(expected, actual)
        }
    }

    @Test
    fun testStartTimeResponse() {
        testCoroutineRule.runBlockingTest {
            viewModel.confirmBooking("3CF3GD", onSuccess)
            val expected = "2021-06-21T21:00:00Z"
            val actual = viewModel.bookingConfirmResponse.value?.startTime
            assertEquals(expected, actual)
        }
    }

    @Test
    fun testEndTimeResponse() {
        testCoroutineRule.runBlockingTest {
            viewModel.confirmBooking("3CF3GD", onSuccess)
            val expected = "2021-06-21T22:00:00Z"
            val actual = viewModel.bookingConfirmResponse.value?.endTime
            assertEquals(expected, actual)
        }
    }

    @Test
    fun testIsRecurrentResponse() {
        testCoroutineRule.runBlockingTest {
            viewModel.confirmBooking("3CF3GD", onSuccess)
            val isRecurrent = viewModel.bookingConfirmResponse.value?.isRecurrent
            if (isRecurrent != null) {
                assertFalse(isRecurrent)
            }
        }
    }

    @Test
    fun testRecurrenceRuleResponse() {
        testCoroutineRule.runBlockingTest {
            viewModel.confirmBooking("3CF3GD", onSuccess)
            val recurrenceRule = viewModel.bookingConfirmResponse.value?.recurrenceRule
            if (recurrenceRule != null) {
                assertTrue(recurrenceRule.isEmpty())
            }
        }
    }

    @Test
    fun testIsPermanentResponse() {
        testCoroutineRule.runBlockingTest {
            viewModel.confirmBooking("3CF3GD", onSuccess)
            val isPermanent = viewModel.bookingConfirmResponse.value?.isPermanent
            if (isPermanent != null) {
                assertFalse(isPermanent)
            }
        }
    }

    @Test
    fun testOwnerName() {
        testCoroutineRule.runBlockingTest {
            viewModel.confirmBooking("3CF3GD", onSuccess)
            val name = viewModel.bookingConfirmResponse.value?.owner?.name
            if (name != null) {
                assertEquals(name, "Kevin Viscafe")
            }
        }
    }

    @Test
    fun testOwnerEmail() {
        testCoroutineRule.runBlockingTest {
            viewModel.confirmBooking("3CF3GD", onSuccess)
            val email = viewModel.bookingConfirmResponse.value?.owner?.email
            if (email != null) {
                assertEquals(email, "kevinviscafe@gmail.com")
            }
        }
    }

    @Test
    fun testAssetCode() {
        testCoroutineRule.runBlockingTest {
            viewModel.confirmBooking("3CF3GD", onSuccess)
            val assetCode = viewModel.bookingConfirmResponse.value?.assetCode
            if (assetCode != null) {
                assertEquals(assetCode, "CR01 - Wakanda")
            }
        }
    }


    @Test
    fun testBadResponseFromApi() {
        testCoroutineRule.runBlockingTest {
            viewModel.confirmBooking("3CFw3GD", onSuccess)
            val isBadRequestCalled = mockListener.fetchBadRequestCalled
            assertTrue(isBadRequestCalled)
        }
    }


}
