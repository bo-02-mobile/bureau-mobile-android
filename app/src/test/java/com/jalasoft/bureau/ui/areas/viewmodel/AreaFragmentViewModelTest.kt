package com.jalasoft.bureau.ui.areas.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.rules.TestCoroutineRule
import com.jalasoft.bureau.ui.areas.repository.AreaProductionService
import com.jalasoft.bureau.ui.bookings.repository.MainMyBookingsRepository
import com.jalasoft.bureau.ui.bookings.repository.MyBookingsRepository
import com.jalasoft.bureau.ui.bookings.viewmodel.MyBookingsViewModel
import com.jalasoft.bureau.ui.main.repository.MainActivityRepository
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.stub
import retrofit2.Response
import retrofit2.Retrofit
import java.time.LocalDate
import java.time.LocalTime
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
class AreaFragmentViewModelTest : BeforeAllCallback, AfterAllCallback {

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)

    lateinit var areaViewModel : AreaFragmentViewModel
    lateinit var context : Context
    lateinit var mainViewModel: MainActivityViewModel

    private val buildingId = "60a807c5dc0ba843b1ac4063"
    private val assetType = "all"
    private val startTime = "2022-01-04T15:00:00Z"
    private val endTime = "2022-01-04T17:00:00Z"
    private val area = Area.AreaItem(
        Area.AreaItem.Background("uyz1ftb3fhpfnkktajo0",
            "https://res.cloudinary.com/dbjicvy8t/image/upload/v1638367454/uyz1ftb3fhpfnkktajo0.jpg"),
        "60a807c5dc0ba843b1ac4063",
        "3A",
        "2020-06-28T18:32:23.207Z",
        "", "60a807f1dc0ba843b1ac4064",
        listOf(),
        "3A", true,
        Area.AreaItem.Thumbnail("fzejpp2fr3doyq5moott",
            "https://res.cloudinary.com/dbjicvy8t/image/upload/v1638367452/fzejpp2fr3doyq5moott.jpg"),
        "2020-06-28T14:32:23.207Z", 1, null
    )

    private val schedule = Asset.AssetItem.Schedule(
        "",
        "2022-05-25T05:00:00Z",
        "2022-05-25T19:00:00Z"
    )

    private val asset = Asset.AssetItem(
        "60b13fe3b74284b5f27ffa12",
        "60a807c5dc0ba843b1ac4063",
        "1C001",
        "2020-03-26T10:28:10.294Z",
        "",
        "60b14364b74284b5f27ff903",
        "1C001", null,
        Asset.AssetItem.Policies(5, false, 30, 720, listOf(schedule)),
        true, null, "2021-10-26T14:28:10.294Z", 1, true
    )
    private val bookingItem = BookingResponse.Booking(
        "610445e3846769858d9ccf6e",
        "NewArea3100",
        "W0031",
        "610446ee846769858d9ccf6f",
        "workstation",
        "60c11f74984d2c7a73a0da0e",
        "",
        "2021-07-29T19:15:51.205Z",
        "2021-06-31T19:15:51.205Z",
        "61044fe7846769858d9ccf75",
        false, true, null, null, "2021-07-31T19:20:51.205Z", "2021-08-30T14:15:51.205Z", 1,
        "",
        BookingResponse.Status("Pending", null, null),
        null
    )

    private val mockWebServer = MockWebServer()

    private val client = OkHttpClient.Builder()
        .connectTimeout(1, TimeUnit.SECONDS)
        .readTimeout(1, TimeUnit.SECONDS)
        .writeTimeout(1, TimeUnit.SECONDS)
        .build()

    private val api = Retrofit.Builder()
        .baseUrl(mockWebServer.url("/"))
        .client(client)
        .build()

    @Before
    fun setUp() {
        val repository = AreaProductionService(api)
        areaViewModel = AreaFragmentViewModel(repository)
        context = Mockito.mock(Context::class.java)

        val mainMock = mock<MainActivityRepository>()
        val myBookingMock = mock<MainMyBookingsRepository>()
        val prefs = Prefs(context)
        mainViewModel = MainActivityViewModel(mainMock, myBookingMock, prefs)
    }

    override fun beforeAll(context: ExtensionContext?) {
        Dispatchers.setMain(testDispatcher)
    }

    override fun afterAll(context: ExtensionContext?) {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
        testScope.cleanupTestCoroutines()
    }

    @Test
    fun getAreaByBuildingId() = testDispatcher.runBlockingTest {
        val mock = mock<AreaProductionService>()
        mock.stub {
            onBlocking { getAreaByBuildingIdProduction(buildingId) }  doReturn
                    Response.success(listOf(area))
        }

        val viewModel = AreaFragmentViewModel(mock)
        viewModel.getAreaByBuildingId(buildingId)
        val expected = area
        val actual = viewModel.myAreas.value?.get(0)
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun getAssetOverlapped() = testDispatcher.runBlockingTest {
        val mock = mock<AreaProductionService>()
        mock.stub {
            onBlocking { getAssetsByBuildingId(any(), any(), any(), any()) }  doReturn
                    Response.success(listOf(asset))
        }
        val viewModel = AreaFragmentViewModel(mock)
        viewModel.getAssetOverlapped(buildingId, emptyList(), assetType, emptyList())
        val expected = asset
        val actual = viewModel.myAssets.value?.get(0)
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun getOverlappedBookings() = testDispatcher.runBlockingTest {
        val mock = mock<AreaProductionService>()
        mock.stub {
            onBlocking { getBookingsByDate(startTime, endTime) } doReturn
                    Response.success(arrayListOf(bookingItem))
        }
        val viewModel = AreaFragmentViewModel(mock)
        viewModel.getOverlappedBookings(startTime, endTime)
        val expected = bookingItem
        val actual = viewModel.overLappedBookings.value?.get(0)
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun checkWorkstationSchedulesWhenThereIsNotOverlappedSchedules() = testDispatcher.runBlockingTest {
        mainViewModel.myDate.value = LocalDate.now()
        mainViewModel.userStartTime.value = LocalTime.of(13, 0, 0)
        mainViewModel.userEndTime.value = LocalTime.of(14, 0, 0)
        val actual = areaViewModel.checkWorkstationSchedules(
            arrayListOf(asset),
            LocalTime.of(13, 0, 0),
            LocalTime.of(14, 0, 0),
            LocalDate.now(),
            mainViewModel.getBuildingZoneId()
        )
        val expected = arrayListOf(asset)
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun checkWorkstationSchedulesWhenThereIsOverlappedSchedules() = testDispatcher.runBlockingTest {
        mainViewModel.myDate.value = LocalDate.now()
        mainViewModel.userStartTime.value = LocalTime.of(1, 0, 0)
        mainViewModel.userEndTime.value = LocalTime.of(22, 0, 0)
        val actual = areaViewModel.checkWorkstationSchedules(
            arrayListOf(asset),
            LocalTime.of(1, 0, 0),
            LocalTime.of(22, 0, 0),
            LocalDate.now(),
            mainViewModel.getBuildingZoneId()
        )
        val expected = emptyList<Asset.AssetItem>()
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun checkWorkstationSchedulesWhenAssetIsOutOfBookingWindowRange() = testDispatcher.runBlockingTest {
        mainViewModel.myDate.value = LocalDate.now().plusDays(10)
        mainViewModel.userStartTime.value = LocalTime.of(13, 0, 0)
        mainViewModel.userEndTime.value = LocalTime.of(12, 0, 0)
        val actual = areaViewModel.checkWorkstationSchedules(
            arrayListOf(asset),
            LocalTime.of(13, 0, 0),
            LocalTime.of(12, 0, 0),
            LocalDate.now().plusDays(10),
            mainViewModel.getBuildingZoneId()
        )
        val expected = emptyList<Asset.AssetItem>()
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun checkWorkstationSchedulesWhenAssetIsInBookingWindowRange() = testDispatcher.runBlockingTest {
        mainViewModel.myDate.value = LocalDate.now().plusDays(2)
        mainViewModel.userStartTime.value = LocalTime.of(13, 0, 0)
        mainViewModel.userEndTime.value = LocalTime.of(12, 0, 0)
        val actual = areaViewModel.checkWorkstationSchedules(
            arrayListOf(asset),
            LocalTime.of(13, 0, 0),
            LocalTime.of(12, 0, 0),
            LocalDate.now().plusDays(2),
            mainViewModel.getBuildingZoneId()
        )
        val expected = arrayListOf(asset)
        Assert.assertEquals(expected, actual)
    }
}