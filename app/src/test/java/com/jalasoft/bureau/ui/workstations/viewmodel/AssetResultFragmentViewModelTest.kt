package com.jalasoft.bureau.ui.workstations.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.common.Owner
import com.jalasoft.bureau.rules.TestCoroutineRule
import com.jalasoft.bureau.ui.areas.repository.AreaProductionService
import com.jalasoft.bureau.ui.areas.viewmodel.AreaFragmentViewModel
import com.jalasoft.bureau.ui.bookings.repository.MainMyBookingsRepository
import com.jalasoft.bureau.ui.main.repository.MainActivityRepository
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.workstations.repository.AssetResultRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.mockito.Mockito
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.stub
import retrofit2.Response
import java.time.LocalDate
import java.time.LocalTime

@ExperimentalCoroutinesApi
class AssetResultFragmentViewModelTest : BeforeAllCallback, AfterAllCallback {

    private val marker1 = Area.AreaItem.Marker("610446ee846769858d9ccf6f", true, 20.5, 20.5)
    private val marker2 = Area.AreaItem.Marker("600446ee846769858d9ccf6f", false, 80.5, 20.5)
    private val buildingId = "60a807c5dc0ba843b1ac4063"
    private val areaId = "uyz1ftb3fhpfnkktajo0"
    private val types = "all"
    private val arrayLabels: List<String> = listOf()
    private val startTime = "2022-02-21T19:30:00Z"
    private val endTime = "2022-02-21T21:30:00Z"
    private val areaItem = Area.AreaItem(
        Area.AreaItem.Background(
            "uyz1ftb3fhpfnkktajo0",
            "https://res.cloudinary.com/dbjicvy8t/image/upload/v1638367454/uyz1ftb3fhpfnkktajo0.jpg"
        ),
        "60a807c5dc0ba843b1ac4063",
        "3A",
        "2020-06-28T18:32:23.207Z",
        "", "60a807f1dc0ba843b1ac4064",
        listOf(marker1, marker2),
        "3A", true,
        Area.AreaItem.Thumbnail(
            "fzejpp2fr3doyq5moott",
            "https://res.cloudinary.com/dbjicvy8t/image/upload/v1638367452/fzejpp2fr3doyq5moott.jpg"
        ),
        "2020-06-28T14:32:23.207Z",
        1,
        null
    )

    private val schedule = Asset.AssetItem.Schedule(
        "",
        "2022-05-25T05:00:00Z",
        "2022-05-25T19:00:00Z"
    )

    private val assetItem = Asset.AssetItem(
        "60b13fe3b74284b5f27ffacb",
        "60a807c5dc0ba843b1ac4063",
        "1C001",
        "2021-07-26T14:28:10.294Z",
        "",
        "60b14364b74284b5f27fface",
        "1C001",
        Asset.AssetItem.Properties(listOf("Wifi", "Ethernet"), null, "workstation"),
        Asset.AssetItem.Policies(5, false, 30, 720, listOf(schedule)),
        true,
        Asset.AssetItem.Stats(10, 20),
        "2021-07-26T14:28:10.294Z",
        1,
        true
    )

    private val assetItemWithOwner = Asset.AssetItem(
        "60b13fe3b74284b5f27ffacb",
        "60a807c5dc0ba843b1ac4063",
        "1C001",
        "2021-07-26T14:28:10.294Z",
        "",
        "60b14364b74284b5f27fface",
        "1C001",
        Asset.AssetItem.Properties(listOf("Wifi", "Ethernet"), Owner("", "", ""), "workstation"),
        Asset.AssetItem.Policies(5, false, 30, 720, listOf()),
        true,
        Asset.AssetItem.Stats(10, 20),
        "2021-07-26T14:28:10.294Z",
        1,
        true
    )

    private val bookingItem = BookingResponse.Booking(
        areaId= "61a780db15686466584bdf6f",
        areaName= "1C Cowork",
        assetCode= "1C001",
        assetId= "61a7812015686466584bdf70",
        assetType= "Workstation",
        buildingId= "60a807c5dc0ba843b1ac4063",
        createdAt= "2022-02-21T14:58:47.472Z",
        endTime= "2022-02-21T21:30:00Z",
        id= "6213a8a75413c7c5c111b296",
        isRecurrent= false,
        owner = null,
        recurrenceRule= null,
        startTime= "2022-02-21T19:30:00Z",
        updatedAt= "2022-02-21T14:58:47.472Z",
        confirmedAt = "",
        isPermanent = false,
        version = 0,
        reservationMessage = "",
        status = BookingResponse.Status("Pending", null, null),
        recurrenceId = null
    )

    private val overlappedBookingsString = "id(610446ee846769858d9ccf6f),id(611bbbec846769858d9cd055),id(6126b63b846769858d9cd228),id(612795b2846769858d9cd23e),id(612d9d94837a42d8c48311fe),id(612da116837a42d8c4831200)"

    private val overlappedList: List<String> = listOf("id(610446ee846769858d9ccf6f)")

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    lateinit var context: Context
    lateinit var mainViewModel: MainActivityViewModel
    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)

    override fun beforeAll(context: ExtensionContext?) {
        Dispatchers.setMain(testDispatcher)
    }

    override fun afterAll(context: ExtensionContext?) {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
        testScope.cleanupTestCoroutines()
    }

    @Before
    fun setUp() {
        context = Mockito.mock(Context::class.java)
        val mainMock = mock<MainActivityRepository>()
        val myBookingMock = mock<MainMyBookingsRepository>()
        val prefs = Prefs(context)
        mainViewModel = MainActivityViewModel(mainMock, myBookingMock, prefs)
    }

    @Test
    fun getAreaDetail() = testDispatcher.runBlockingTest {
        val areaProductionServiceMock = mock<AreaProductionService>()
        val mock = mock<AssetResultRepository>()
        mock.stub {
            onBlocking { getAreaDetailProduction(areaId) } doReturn
                    Response.success(areaItem)
        }

        val viewModel = AssetResultFragmentViewModel(mock, areaProductionServiceMock)
        viewModel.getAreaDetail(areaId)

        val assetExpected = areaItem
        Assert.assertEquals(assetExpected, viewModel.area.value)
    }

    @Test
    fun getNormalAssetByBuildingId() = testDispatcher.runBlockingTest {
        val assetRepositoryMock = mock<AssetResultRepository>()
        val mock = mock<AreaProductionService>()
        mock.stub {
            onBlocking {
                getAssetsProduction(
                    buildingId,
                    types,
                    arrayLabels
                )
            } doReturn
                    Response.success(listOf(assetItem))
        }

        val viewModel = AssetResultFragmentViewModel(assetRepositoryMock, mock)
        viewModel.areaRepository = mock
        viewModel.myOverlappedAssets = listOf(assetItem)
        viewModel.getAssetByBuildingId(buildingId, types, arrayLabels)

        val assetExpected = assetItem
        Assert.assertEquals(assetExpected, viewModel.myAssets.value?.get(0))
    }

    @Test
    fun getFixedAssetByBuildingId() = testDispatcher.runBlockingTest {
        val mock = mock<AreaProductionService>()
        val assetRepositoryMock = mock<AssetResultRepository>()
        mock.stub {
            onBlocking {
                getAssetsProduction(
                    buildingId,
                    types,
                    arrayLabels
                )
            } doReturn
                    Response.success(listOf(assetItemWithOwner))
        }
        val viewModel = AssetResultFragmentViewModel(assetRepositoryMock, mock)
        viewModel.getAssetByBuildingId(buildingId, types, arrayLabels)

        Assert.assertTrue(viewModel.myAssets.value.isNullOrEmpty())
    }

    @Test
    fun getOverlappedBookings() = testDispatcher.runBlockingTest {
        val assetRepositoryMock = mock<AreaProductionService>()
        val mock = mock<AssetResultRepository>()
        mock.stub {
            onBlocking {
                getBookingsByDate(
                    startTime,
                    endTime
                )
            } doReturn
                    Response.success(arrayListOf(bookingItem))
        }
        val viewModel = AssetResultFragmentViewModel(mock, assetRepositoryMock)
        viewModel.getOverlappedBookings(startTime, endTime)

        Assert.assertTrue(viewModel.overLappedBookings.value!!.isNotEmpty())
    }

    @Test
    fun getAssetOverlapped() = testDispatcher.runBlockingTest {
        val assetRepositoryMock = mock<AreaProductionService>()
        val mock = mock<AssetResultRepository>()
        mock.stub {
            onBlocking {
                getAssetsFromArea(
                    buildingId,
                    overlappedBookingsString,
                )
            } doReturn
                    Response.success(listOf(assetItemWithOwner))
        }

        val viewModel = AssetResultFragmentViewModel(mock, assetRepositoryMock)
        viewModel.getAssetOverlapped(buildingId, overlappedList)

        Assert.assertTrue(viewModel.myAssets.value.isNullOrEmpty())
    }

    @Test
    fun clearDataTest() = testDispatcher.runBlockingTest {
        val assetRepositoryMock = mock<AreaProductionService>()
        val mock = mock<AssetResultRepository>()
        mock.stub {
            onBlocking {
                getAssetsFromArea(
                    buildingId,
                    overlappedBookingsString,
                )
            } doReturn
                    Response.success(listOf(assetItemWithOwner))
        }
        val viewModel = AssetResultFragmentViewModel(mock, assetRepositoryMock)
        viewModel.clearData()

        Assert.assertEquals(viewModel.overLappedBookings.value?.isEmpty(), true)
        Assert.assertEquals(viewModel.myAssets.value.isNullOrEmpty(), true)
        Assert.assertEquals(viewModel.loadAsset.value, false)
    }

    @Test
    fun checkWorkstationSchedulesWhenThereIsNotOverlappedSchedules() = testDispatcher.runBlockingTest {
        val assetRepositoryMock = mock<AreaProductionService>()
        val mock = mock<AssetResultRepository>()
        mock.stub {
            onBlocking {
                getAssetsFromArea(
                    buildingId,
                    overlappedBookingsString,
                )
            } doReturn
                    Response.success(listOf(assetItemWithOwner))
        }
        val assetViewModel = AssetResultFragmentViewModel(mock, assetRepositoryMock)

        mainViewModel.myDate.value = LocalDate.now()
        mainViewModel.userStartTime.value = LocalTime.of(13, 0, 0)
        mainViewModel.userEndTime.value = LocalTime.of(14, 0, 0)
        val actual = assetViewModel.checkWorkstationSchedules(
            arrayListOf(assetItem),
            mainViewModel.userStartTime.value!!,
            mainViewModel.userEndTime.value!!,
            mainViewModel.myDate.value!!,
            mainViewModel.getBuildingZoneId()
        )
        val expected = arrayListOf(assetItem.id)
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun checkWorkstationSchedulesWhenThereIsOverlappedSchedules() = testDispatcher.runBlockingTest {
        val assetRepositoryMock = mock<AreaProductionService>()
        val mock = mock<AssetResultRepository>()
        mock.stub {
            onBlocking {
                getAssetsFromArea(
                    buildingId,
                    overlappedBookingsString,
                )
            } doReturn
                    Response.success(listOf(assetItemWithOwner))
        }
        val assetViewModel = AssetResultFragmentViewModel(mock, assetRepositoryMock)

        mainViewModel.myDate.value = LocalDate.now()
        mainViewModel.userStartTime.value = LocalTime.of(1, 0, 0)
        mainViewModel.userEndTime.value = LocalTime.of(22, 0, 0)
        val actual = assetViewModel.checkWorkstationSchedules(
            arrayListOf(assetItem),
            mainViewModel.userStartTime.value!!,
            mainViewModel.userEndTime.value!!,
            mainViewModel.myDate.value!!,
            mainViewModel.getBuildingZoneId()
        )
        val expected = emptyList<Asset.AssetItem>()
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun checkWorkstationSchedulesWhenAssetIsOutOfBookingWindowRange() = testDispatcher.runBlockingTest {
        val assetRepositoryMock = mock<AreaProductionService>()
        val mock = mock<AssetResultRepository>()
        mock.stub {
            onBlocking {
                getAssetsFromArea(
                    buildingId,
                    overlappedBookingsString,
                )
            } doReturn
                    Response.success(listOf(assetItemWithOwner))
        }
        val assetViewModel = AssetResultFragmentViewModel(mock, assetRepositoryMock)

        mainViewModel.myDate.value = LocalDate.now().plusDays(10)
        mainViewModel.userStartTime.value = LocalTime.of(13, 0, 0)
        mainViewModel.userEndTime.value = LocalTime.of(12, 0, 0)
        val actual = assetViewModel.checkWorkstationSchedules(
            arrayListOf(assetItem),
            mainViewModel.userStartTime.value!!,
            mainViewModel.userEndTime.value!!,
            mainViewModel.myDate.value!!,
            mainViewModel.getBuildingZoneId()
        )
        val expected = emptyList<Asset.AssetItem>()
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun checkWorkstationSchedulesWhenAssetIsInBookingWindowRange() = testDispatcher.runBlockingTest {
        val assetRepositoryMock = mock<AreaProductionService>()
        val mock = mock<AssetResultRepository>()
        mock.stub {
            onBlocking {
                getAssetsFromArea(
                    buildingId,
                    overlappedBookingsString,
                )
            } doReturn
                    Response.success(listOf(assetItemWithOwner))
        }
        val assetViewModel = AssetResultFragmentViewModel(mock, assetRepositoryMock)

        mainViewModel.myDate.value = LocalDate.now().plusDays(2)
        mainViewModel.userStartTime.value = LocalTime.of(13, 0, 0)
        mainViewModel.userEndTime.value = LocalTime.of(12, 0, 0)
        val actual = assetViewModel.checkWorkstationSchedules(
            arrayListOf(assetItem),
            mainViewModel.userStartTime.value!!,
            mainViewModel.userEndTime.value!!,
            mainViewModel.myDate.value!!,
            mainViewModel.getBuildingZoneId()
        )
        val expected = arrayListOf(assetItem.id)
        Assert.assertEquals(expected, actual)
    }
}