package com.jalasoft.bureau.ui.workstations.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.model.token.User
import com.jalasoft.bureau.rules.TestCoroutineRule
import com.jalasoft.bureau.ui.bookings.repository.MainMyBookingsRepository
import com.jalasoft.bureau.ui.main.repository.MainActivityRepository
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.utils.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import okhttp3.internal.UTC
import org.junit.*
import org.mockito.Mockito
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.stub
import retrofit2.Response
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId

@ExperimentalCoroutinesApi
class BookingOptionsViewModelTest {

    private val user = User(
        "1",
        "pedro@gmail.com",
        "https://lh3.googleusercontent.com/a/AATXAJwTrrLqztPRtd1iBtaYao_LQ-6luvvdoaHkYSaC=s96-c",
        "Pedro"
    )

    lateinit var optionsViewModel: BookingOptionsViewModel
    lateinit var mainViewModel: MainActivityViewModel
    lateinit var context: Context
    private val testDispatcher = TestCoroutineDispatcher()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        context = Mockito.mock(Context::class.java)
        val mainMock = mock<MainActivityRepository>()
        val myBookingMock = mock<MainMyBookingsRepository>()
        val prefs = Prefs(context)
        mainViewModel = MainActivityViewModel(mainMock, myBookingMock, prefs)
        mainViewModel.userStartTime.value = LocalTime.of(8, 30)
        mainViewModel.userEndTime.value = LocalTime.of(18, 30)
        mainViewModel.myDate.value = LocalDate.now()
        optionsViewModel = BookingOptionsViewModel(mainViewModel.myDate.value!!)
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun initialValuesTest() {
        Assert.assertEquals(1, optionsViewModel.everySelection)
        Assert.assertFalse(optionsViewModel.weekdaysSelected[0])
        Assert.assertFalse(optionsViewModel.weekdaysSelected[1])
        Assert.assertFalse(optionsViewModel.weekdaysSelected[2])
        Assert.assertFalse(optionsViewModel.weekdaysSelected[3])
        Assert.assertFalse(optionsViewModel.weekdaysSelected[4])
        Assert.assertFalse(optionsViewModel.weekdaysSelected[5])
        Assert.assertFalse(optionsViewModel.weekdaysSelected[6])
        Assert.assertEquals(1, optionsViewModel.monthSelected)
        Assert.assertEquals(1, optionsViewModel.daySelected)
        Assert.assertEquals(1, optionsViewModel.weekNumberSelected)
        Assert.assertEquals(1, optionsViewModel.weekDaySelected)
        Assert.assertTrue(optionsViewModel.onDaySelected)
    }

    @Test
    fun resetValuesTest() {
        optionsViewModel.everySelection = 15
        optionsViewModel.weekdaysSelected[2] = true
        optionsViewModel.monthSelected = 10
        optionsViewModel.daySelected = 7
        optionsViewModel.weekNumberSelected = 2
        optionsViewModel.weekDaySelected = 7
        optionsViewModel.onDaySelected = false
        optionsViewModel.resetValues()
        Assert.assertEquals(1, optionsViewModel.everySelection)
        Assert.assertTrue(optionsViewModel.weekdaysSelected[0])
        Assert.assertFalse(optionsViewModel.weekdaysSelected[1])
        Assert.assertFalse(optionsViewModel.weekdaysSelected[2])
        Assert.assertFalse(optionsViewModel.weekdaysSelected[3])
        Assert.assertFalse(optionsViewModel.weekdaysSelected[4])
        Assert.assertFalse(optionsViewModel.weekdaysSelected[5])
        Assert.assertFalse(optionsViewModel.weekdaysSelected[6])
        Assert.assertEquals(1, optionsViewModel.monthSelected)
        Assert.assertEquals(1, optionsViewModel.daySelected)
        Assert.assertEquals(1, optionsViewModel.weekNumberSelected)
        Assert.assertEquals(1, optionsViewModel.weekDaySelected)
        Assert.assertTrue(optionsViewModel.onDaySelected)
    }

    @Test
    fun getRecurrenceOptionPos() {
        Assert.assertEquals(0, optionsViewModel.getRecurrenceOptionPos())
        optionsViewModel.selection.value = RECURRENCE_OPTION_DAILY
        Assert.assertEquals(1, optionsViewModel.getRecurrenceOptionPos())
        optionsViewModel.selection.value = RECURRENCE_OPTION_WEEKLY
        Assert.assertEquals(2, optionsViewModel.getRecurrenceOptionPos())
        optionsViewModel.selection.value = RECURRENCE_OPTION_MONTHLY
        Assert.assertEquals(3, optionsViewModel.getRecurrenceOptionPos())
        optionsViewModel.selection.value = RECURRENCE_OPTION_YEARLY
        Assert.assertEquals(4, optionsViewModel.getRecurrenceOptionPos())
        optionsViewModel.selection.value = ""
        Assert.assertEquals(-1, optionsViewModel.getRecurrenceOptionPos())
        optionsViewModel.selection.value = RECURRENCE_OPTION_NEVER
        Assert.assertEquals(0, optionsViewModel.getRecurrenceOptionPos())
    }

    @Test
    fun getLimitForEveryOption() {
        Assert.assertEquals(0, optionsViewModel.getLimitForEveryOption())
        optionsViewModel.selection.value = RECURRENCE_OPTION_DAILY
        Assert.assertEquals(99, optionsViewModel.getLimitForEveryOption())
        optionsViewModel.selection.value = RECURRENCE_OPTION_WEEKLY
        Assert.assertEquals(52, optionsViewModel.getLimitForEveryOption())
        optionsViewModel.selection.value = RECURRENCE_OPTION_MONTHLY
        Assert.assertEquals(11, optionsViewModel.getLimitForEveryOption())
        optionsViewModel.selection.value = RECURRENCE_OPTION_YEARLY
        Assert.assertEquals(1, optionsViewModel.getLimitForEveryOption())
        optionsViewModel.selection.value = ""
        Assert.assertEquals(0, optionsViewModel.getLimitForEveryOption())
        optionsViewModel.selection.value = RECURRENCE_OPTION_NEVER
        Assert.assertEquals(0, optionsViewModel.getLimitForEveryOption())
    }

    @Test
    fun checkWeekdayTest() {
        optionsViewModel.resetValues()
        optionsViewModel.checkWeekday(0)
        Assert.assertTrue(optionsViewModel.weekdaysSelected[0])
        Assert.assertFalse(optionsViewModel.weekdaysSelected[1])
        optionsViewModel.checkWeekday(1)
        Assert.assertTrue(optionsViewModel.weekdaysSelected[1])
        optionsViewModel.checkWeekday(0)
        Assert.assertTrue(optionsViewModel.weekdaysSelected[1])
        Assert.assertFalse(optionsViewModel.weekdaysSelected[0])
    }

    @Test
    fun returnAlphaForWeekdayTest() {
        optionsViewModel.resetValues()
        Assert.assertEquals(1F, optionsViewModel.returnAlphaForWeekday(0))
        Assert.assertEquals(0F, optionsViewModel.returnAlphaForWeekday(1))
        optionsViewModel.weekdaysSelected[1] = true
        Assert.assertEquals(1F, optionsViewModel.returnAlphaForWeekday(1))
        optionsViewModel.resetValues()
    }

    @Test
    fun initDateTimeValuesTest() {
        optionsViewModel.initDateTimeValues(
            mainViewModel.myDate.value,
            mainViewModel.userStartTime.value,
            mainViewModel.userEndTime.value
        )
        Assert.assertEquals(optionsViewModel.tempDate, mainViewModel.myDate.value)
        Assert.assertEquals(optionsViewModel.tempStartTime, mainViewModel.userStartTime.value)
        Assert.assertEquals(optionsViewModel.tempEndTime, mainViewModel.userEndTime.value)
        Assert.assertEquals(optionsViewModel.endDate.value, mainViewModel.myDate.value?.plusMonths(1))
    }

    @Test
    fun resetDateTimeFiltersTest() {
        optionsViewModel.initDateTimeValues(
            mainViewModel.myDate.value,
            mainViewModel.userStartTime.value,
            mainViewModel.userEndTime.value
        )
        mainViewModel.myDate.postValue(optionsViewModel.tempDate?.plusDays(1))
        mainViewModel.userStartTime.postValue(optionsViewModel.tempStartTime?.plusHours(1))
        mainViewModel.userEndTime.postValue(optionsViewModel.tempEndTime?.plusHours(1))
        Assert.assertNotEquals(optionsViewModel.tempDate, mainViewModel.myDate.value)
        Assert.assertNotEquals(optionsViewModel.tempStartTime, mainViewModel.userStartTime.value)
        Assert.assertNotEquals(optionsViewModel.tempEndTime, mainViewModel.userEndTime.value)
        optionsViewModel.resetDateTimeFilters { myDate, myStartTime, myEndTime ->
            mainViewModel.myDate.value = myDate
            mainViewModel.userStartTime.value = myStartTime
            mainViewModel.userEndTime.value = myEndTime
        }
        Assert.assertEquals(optionsViewModel.tempDate, mainViewModel.myDate.value)
        Assert.assertEquals(optionsViewModel.tempStartTime, mainViewModel.userStartTime.value)
        Assert.assertEquals(optionsViewModel.tempEndTime, mainViewModel.userEndTime.value)
    }

    @Test
    fun fetchUsersTest() = testDispatcher.runBlockingTest {
        val mock = mock<MainActivityRepository>()
        mock.stub {
            onBlocking { fetchUsers() } doReturn
                    Response.success(listOf(user))
        }
        mainViewModel.repository = mock
        val bookingOptionsVM = BookingOptionsViewModel(mainViewModel.myDate.value)
        mainViewModel.fetchUsers { usersList ->
            bookingOptionsVM.usersList.postValue(usersList)
        }
        val expected = user
        val actual = bookingOptionsVM.usersList.value?.get(0)
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun postStartDateTest() = testDispatcher.runBlockingTest {
        optionsViewModel.initDateTimeValues(
            mainViewModel.myDate.value,
            mainViewModel.userStartTime.value,
            mainViewModel.userEndTime.value
        )
        var millis: Long? = LocalDate.of(2000,1,1).atStartOfDay(ZoneId.of(UTC.id)).toInstant().toEpochMilli()
        Assert.assertEquals(946684800000, millis)
        Assert.assertTrue(mainViewModel.postDate(millis!!))
        Assert.assertEquals(mainViewModel.myDate.value?.plusMonths(1), optionsViewModel.endDate.value)
        millis = optionsViewModel.endDate.value?.atStartOfDay(ZoneId.of(UTC.id))?.toInstant()?.toEpochMilli()
        Assert.assertFalse(mainViewModel.postDate(millis!!))
    }

    @Test
    fun postEndDateTest() {
        optionsViewModel.initDateTimeValues(
            mainViewModel.myDate.value,
            mainViewModel.userStartTime.value,
            mainViewModel.userEndTime.value
        )
        var millis: Long? = LocalDate.of(2000,1,1).atStartOfDay(ZoneId.of(UTC.id)).toInstant().toEpochMilli()
        Assert.assertTrue(optionsViewModel.postEndDate(millis!!, mainViewModel.myDate.value!!))
        Assert.assertEquals(mainViewModel.myDate.value?.plusMonths(1), optionsViewModel.endDate.value)
        millis = mainViewModel.myDate.value?.plusDays(2)?.atStartOfDay(ZoneId.of(UTC.id))?.toInstant()?.toEpochMilli()
        Assert.assertFalse(mainViewModel.postDate(millis!!))
        Assert.assertTrue(mainViewModel.myDate.value!! < optionsViewModel.endDate.value)
    }

    @Test
    fun updateEndDateWhenGreaterThanStartDateTest() {
        // StartDate is LocalDate.now()
        optionsViewModel.endDate.value = LocalDate.now().plusDays(1)
        val expectedEndDate = optionsViewModel.endDate.value
        optionsViewModel.updateEndDate(mainViewModel.myDate.value!!)
        Assert.assertEquals(expectedEndDate, optionsViewModel.endDate.value)
    }

    @Test
    fun updateEndDateWhenLesserThanStartDateTest() {
        // Start day is LocalDate.now()
        optionsViewModel.endDate.value = LocalDate.now().minusDays(1)
        val expectedEndDate = LocalDate.now().plusMonths(1)
        optionsViewModel.updateEndDate(mainViewModel.myDate.value!!)
        Assert.assertEquals(expectedEndDate, optionsViewModel.endDate.value)
    }
}