package com.jalasoft.bureau.ui.favorites.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.data.model.favorites.UserFav
import com.jalasoft.bureau.rules.TestCoroutineRule
import com.jalasoft.bureau.ui.bookings.repository.MainMyBookingsRepository
import com.jalasoft.bureau.ui.favorites.repository.MainFavoritesRepository
import com.jalasoft.bureau.ui.main.repository.MainActivityRepository
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.stub
import retrofit2.Response
import java.time.LocalDate
import java.time.LocalTime

@ExperimentalCoroutinesApi
class FavoritesViewModelTest : BeforeAllCallback, AfterAllCallback {

    private val assetId = "610446ee846769858d9ccf6f"
    private val startTime = "2021-07-31T19:20:51.205Z"
    private val endTime = "2021-06-31T19:15:51.205Z"

    private val schedule = Asset.AssetItem.Schedule(
        "",
        "2022-05-25T05:00:00Z",
        "2022-05-25T19:00:00Z"
    )

    private val assetItem = Asset.AssetItem(
        "60b13fe3b74284b5f27ffacb",
        "60a807c5dc0ba843b1ac4063",
        "1C001",
        "2021-07-26T14:28:10.294Z",
        "",
        "60b14364b74284b5f27fface",
        "1C001",
        Asset.AssetItem.Properties(listOf("Wifi", "Ethernet"), null, "workstation"),
        Asset.AssetItem.Policies(2, false, 30, 720, listOf(schedule)),
        true,
        Asset.AssetItem.Stats(10, 20),
        "2021-07-26T14:28:10.294Z",
        1,
        null
    )

    private val listAssetItem: List<Asset.AssetItem> = listOf(assetItem)
    private val listFavorites: List<String?> =
        listOf("610446ee846769858d9ccf6f", "610446ee846769858d9ccf6f")
    private val listAssetUserDel: List<String?> = listOf("60b14364b74284b5f27fface")
    private val listAsset: String = "id(60b14364b74284b5f27fface)"
    private val listResponseFavorites: List<String?> = listOf("60b14364b74284b5f27fface")

    private val bookingResponseItem = BookingResponse.Booking(
        "610445e3846769858d9ccf6e",
        "NewArea3100",
        "W0031",
        "610446ee846769858d9ccf6f",
        "workstation",
        "60c11f74984d2c7a73a0da0e",
        "",
        "2021-07-29T19:15:51.205Z",
        "2021-06-31T19:15:51.205Z",
        "61044fe7846769858d9ccf75",
        false,
        true,
        null,
        null,
        "2021-07-31T19:20:51.205Z",
        "2021-08-30T14:15:51.205Z",
        1,
        "",
        BookingResponse.Status("Pending", null, null),
        null
    )

    private val user = UserFav(
        "60b14364b74284b5f27fface",
        "esda3abe9e-a4a1-asd3245sdfv",
        "Pedro",
        "Pedro",
        "Perez",
        "Pedro Perez",
        "pedro@gmail.com",
        "administrator",
        "https://lh3.googleusercontent.com/a/AATXAJwTrrLqztPRtd1iBtaYao_LQ-6luvvdoaHkYSaC=s96-c",
        true,
        listOf("0b345364as6dfa123qwe"),
        listFavorites,
        "2021-12-29T16:17:31.992Z",
        "2022-05-04T13:55:18.328Z",
        3
    )
    private val userDel = UserFav(
        "60b14364b74284b5f27fface",
        "esda3abe9e-a4a1-asd3245sdfv",
        "Pedro",
        "Pedro",
        "Perez",
        "Pedro Perez",
        "pedro@gmail.com",
        "administrator",
        "https://lh3.googleusercontent.com/a/AATXAJwTrrLqztPRtd1iBtaYao_LQ-6luvvdoaHkYSaC=s96-c",
        true,
        listOf("0b345364as6dfa123qwe"),
        listOf("60b14364b74284b5f27fface"),
        "2021-12-29T16:17:31.992Z",
        "2022-05-04T13:55:18.328Z",
        3
    )

    private val building = Building.BuildingItem(
        "JALA-COCHA",
        "Av Melchor Pérez de Olguín 2643",
        "2021-06-21",
        "60a807c5dc0ba843b1ac4063",
        "Jala cochabamba",
        true,
        "UTC-00:00",
        "2021-06-21",
        1,
        "0"
    )

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    lateinit var context: Context
    lateinit var mainViewModel: MainActivityViewModel
    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)

    override fun beforeAll(context: ExtensionContext?) {
        Dispatchers.setMain(testDispatcher)
    }

    override fun afterAll(context: ExtensionContext?) {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
        testScope.cleanupTestCoroutines()
    }

    @Before
    fun setUp() {
        context = Mockito.mock(Context::class.java)
        val mainMock = mock<MainActivityRepository>()
        val myBookingMock = mock<MainMyBookingsRepository>()
        val prefs = Prefs(context)
        mainViewModel = MainActivityViewModel(mainMock, myBookingMock, prefs)
    }

    @Test
    fun getUserInfoTest() = testDispatcher.runBlockingTest {
        val mock = mock<MainFavoritesRepository>()
        mock.stub {
            onBlocking { getUserInfo() } doReturn
                    Response.success(user)
        }

        val viewModel = FavoritesViewModel(mock)
        viewModel.repository = mock
        viewModel.getUserInfo()

        val listExpected = listFavorites
        Assert.assertEquals(listExpected, viewModel.liveListFav.value)
    }

    @Test
    fun getFavoritesAssetsTest() = testDispatcher.runBlockingTest {
        val mock = mock<MainFavoritesRepository>()
        mock.stub {
            onBlocking { getFavoritesAssetsProduction(listAsset) } doReturn
                    Response.success(listAssetItem)
        }
        val viewModel = FavoritesViewModel(mock)
        viewModel.repository = mock
        mainViewModel.myBuildings.postValue(listOf(building))
        viewModel.getFavoritesAssets(listResponseFavorites, mainViewModel.myBuildings.value)
        mainViewModel.myBuildings.postValue(null)
        val listExpected = listAssetItem
        Assert.assertEquals(listExpected, viewModel.assetList.value)
    }

    @Test
    fun deleteFavoriteAssetTest() = testDispatcher.runBlockingTest {
        val mock = mock<MainFavoritesRepository>()
        mock.stub {
            onBlocking { deleteFavoriteAsset(assetId) } doReturn
                    Response.success(userDel)
        }

        val viewModel = FavoritesViewModel(mock)
        viewModel.repository = mock
        viewModel.deleteFavoriteAsset(assetId) {}

        val listExpected = listAssetUserDel
        Assert.assertEquals(listExpected, viewModel.liveListFav.value)
    }

    @Test
    fun checkOverlappingEventWhenIsOverlappedTest() = testDispatcher.runBlockingTest {
        val mock = mock<MainFavoritesRepository>()
        mock.stub {
            onBlocking { getBookingsByDate(any(), any(), any()) } doReturn
                    Response.success(arrayListOf(bookingResponseItem))
        }

        val viewModel = FavoritesViewModel(mock)
        viewModel.repository = mock
        mainViewModel.myDate.value = LocalDate.now().plusDays(1)
        viewModel.checkOverlappingEvent(
            "2022-04-18T08:30:00-0400",
            "2022-04-18T18:30:00-0400",
            listAssetItem,
            LocalTime.of(13, 0, 0),
            LocalTime.of(12, 0, 0),
            LocalDate.now().plusDays(2),
            mainViewModel.myBuildings.value
        )
        val expected = false
        Assert.assertEquals(expected, viewModel.assetListAvailable.value?.first()?.available)
    }

    @Test
    fun checkOverlappingEventWhenIsNotOverlappedTest() = testDispatcher.runBlockingTest {
        val mock = mock<MainFavoritesRepository>()
        mock.stub {
            onBlocking { getBookingsByDate(any(), any(), any()) } doReturn
                    Response.success(arrayListOf())
        }

        val viewModel = FavoritesViewModel(mock)
        mainViewModel.myBuildings.value = listOf(building)
        mainViewModel.myDate.value = LocalDate.now().plusDays(1)
        viewModel.checkOverlappingEvent(
            "2022-04-18T08:30:00-0400",
            "2022-04-18T18:30:00-0400",
            listAssetItem,
            LocalTime.now(),
            LocalTime.now(),
            LocalDate.now(),
            mainViewModel.myBuildings.value
        )
        val expected = true
        Assert.assertEquals(expected, viewModel.assetListAvailable.value?.first()?.available)
    }

    @Test
    fun checkIfAssetIsAvailableWhenAssetHasOverlappedBookingsTest() = testDispatcher.runBlockingTest {
        val mock = mock<MainFavoritesRepository>()
        val viewModel = FavoritesViewModel(mock)
        val actual = viewModel.checkIfAssetIsAvailable(
            assetItem,
            1,
            LocalTime.of(13, 0, 0),
            LocalTime.of(12, 0, 0),
            LocalDate.now().plusDays(2),
            mainViewModel.myBuildings.value
        )
        Assert.assertEquals(false, actual)
    }

    @Test
    fun checkIfAssetIsAvailableWhenAssetIsNotInBookingWindowRangeTest() = testDispatcher.runBlockingTest {
        val mock = mock<MainFavoritesRepository>()
        val viewModel = FavoritesViewModel(mock)
        mainViewModel.myDate.value = LocalDate.now().plusDays(5)
        val actual = viewModel.checkIfAssetIsAvailable(
            assetItem,
            0,
            LocalTime.of(13, 0, 0),
            LocalTime.of(12, 0, 0),
            LocalDate.now().plusDays(2),
            mainViewModel.myBuildings.value
        )
        Assert.assertEquals(false, actual)
    }

    @Test
    fun checkIfAssetIsAvailableWhenAssetIstInBookingWindowRangeTest() = testDispatcher.runBlockingTest {
        val mock = mock<MainFavoritesRepository>()
        val viewModel = FavoritesViewModel(mock)
        mainViewModel.myDate.value = LocalDate.now().plusDays(5)
        mainViewModel.userStartTime.value = LocalTime.of(11, 0, 0)
        mainViewModel.userEndTime.value = LocalTime.of(13, 0, 0)
        val actual = viewModel.checkIfAssetIsAvailable(
            assetItem,
            0,
            LocalTime.of(11, 0, 0),
            LocalTime.of(13, 0, 0),
            LocalDate.now().plusDays(5),
            mainViewModel.myBuildings.value
        )
        Assert.assertEquals(false, actual)
    }
}