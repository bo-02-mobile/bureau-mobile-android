package com.jalasoft.bureau.ui.login.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jalasoft.bureau.rules.TestCoroutineRule
import org.junit.Before
import org.junit.Rule
import org.mockito.Mockito

class LoginViewModelTest {

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    lateinit var loginViewModel: LoginViewModel
    lateinit var context: Context

    @Before
    fun setUp(){
        loginViewModel = LoginViewModel()
        context = Mockito.mock(Context::class.java)
    }
}