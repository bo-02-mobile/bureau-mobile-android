package com.jalasoft.bureau.ui.workstations.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.booking.PostBooking
import com.jalasoft.bureau.data.model.booking.Status
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.data.model.common.RecurrenceRule
import com.jalasoft.bureau.data.model.favorites.UserFav
import com.jalasoft.bureau.data.model.token.User
import com.jalasoft.bureau.rules.TestCoroutineRule
import com.jalasoft.bureau.ui.bookings.repository.MainMyBookingsRepository
import com.jalasoft.bureau.ui.main.repository.MainActivityRepository
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.workstations.repository.AssetDetailRepository
import com.jalasoft.bureau.ui.workstations.repository.AssetResultRepository
import com.jalasoft.bureau.utils.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.stub
import retrofit2.Response
import java.time.*
import java.time.format.DateTimeFormatter

@ExperimentalCoroutinesApi
class AssetDetailFragmentViewModelTest : BeforeAllCallback, AfterAllCallback {
    private val assetId = "610446ee846769858d9ccf6f"
    private val startTime = "2021-07-31T19:20:51.205Z"
    private val endTime = "2021-06-31T19:15:51.205Z"
    private val bookingId = "61044fe7846769858d9ccf75"
    private val assetItem = Asset.AssetItem(
        "60b13fe3b74284b5f27ffacb",
        "60a807c5dc0ba843b1ac4063",
        "1C001",
        "2021-07-26T14:28:10.294Z",
        "",
        "60b14364b74284b5f27fface",
        "1C001",
        Asset.AssetItem.Properties(listOf("Wifi", "Ethernet"), null, "workstation"),
        Asset.AssetItem.Policies(5, false, 30, 720, listOf()),
        true,
        Asset.AssetItem.Stats(10, 20),
        "2021-07-26T14:28:10.294Z",
        1,
        true
    )

    private val bookingResponseItem = BookingResponse.Booking(
        "610445e3846769858d9ccf6e",
        "NewArea3100",
        "W0031",
        "610446ee846769858d9ccf6f",
        "workstation",
        "60c11f74984d2c7a73a0da0e",
        "",
        "2022-04-29T23:00:00+00:00",
        "2022-04-29T23:00:00+00:00",
        "61044fe7846769858d9ccf75",
        false,
        true,
        null,
        null,
        "2022-04-29T23:00:00+00:00",
        "2022-04-29T23:00:00+00:00",
        1,
        "",
        BookingResponse.Status("Pending", null, null),
        null
    )

    private val user = User(
        "1",
        "pedro@gmail.com",
        "https://lh3.googleusercontent.com/a/AATXAJwTrrLqztPRtd1iBtaYao_LQ-6luvvdoaHkYSaC=s96-c",
        "Pedro"
    )

    // In backend, this is considered as endTime being 1 minute before startTime
    private val assetItemWithoutSchedule = Asset.AssetItem(
        "60b13fe3b74284b5f27ffacb",
        "60a807c5dc0ba843b1ac4063",
        "1C001",
        "2021-07-26T14:28:10.294Z",
        "",
        "60b14364b74284b5f27fface",
        "1C001",
        Asset.AssetItem.Properties(listOf("Wifi", "Ethernet"), null, "workstation"),
        Asset.AssetItem.Policies(5, false, 30, 720, listOf(
            Asset.AssetItem.Schedule(null, "2021-12-06T05:00:00Z", "2021-12-07T04:59:00Z")
        )),
        true,
        Asset.AssetItem.Stats(10, 20),
        "2021-07-26T14:28:10.294Z",
        1,
        true
    )

    private val assetItemWithSchedule = Asset.AssetItem(
        "60b13fe3b74284b5f27ffacb",
        "60a807c5dc0ba843b1ac4063",
        "1C001",
        "2021-07-26T14:28:10.294Z",
        "",
        "60b14364b74284b5f27fface",
        "1C001",
        Asset.AssetItem.Properties(listOf("Wifi", "Ethernet"), null, "workstation"),
        Asset.AssetItem.Policies(5, false, 30, 720, listOf(
            Asset.AssetItem.Schedule(null, "2021-11-05T14:00:00Z", "2021-11-05T16:00:00Z"),
            Asset.AssetItem.Schedule(null, "2021-11-05T20:00:00Z", "2021-11-05T23:00:00Z")
        )),
        true,
        Asset.AssetItem.Stats(10, 20),
        "2021-07-26T14:28:10.294Z",
        1,
        true
    )
    private val bookingOutsideSchedule =
        BookingResponse.Booking(
            "", "", "", "", "", "", "", "",
            LocalDateTime.of(2021, 5, 5, 5,5).toString(),
            SCHEDULED_EVENT_ID,
            false, false, null, null,
            LocalDateTime.of(2021, 5, 5, 2,5).toString(),
            "",
            1,
            "",
            BookingResponse.Status("Pending", null, null),
            null
        )
    private val bookingInsideSchedule =
        BookingResponse.Booking(
            "", "", "", "", "", "", "", "",
            LocalDateTime.of(2021, 5, 5, 23,5).toString(),
            SCHEDULED_EVENT_ID,
            false, false, null, null,
            LocalDateTime.of(2021, 5, 5, 5,5).toString(),
            "",
            1,
            "",
            BookingResponse.Status("Pending", null, null),
            null
        )

    private val listFavorites: List<String?> = listOf("610446ee846769858d9ccf6f", "610446ee846769858d9ccf6f")
    private val listAssetUserDel: List<String?> = listOf("60b14364b74284b5f27fface")
    private val optionsViewModel = BookingOptionsViewModel(LocalDate.now())
    private var isFavorited = MutableLiveData<Boolean>()
    private val userFavModel = UserFav(
        "60b14364b74284b5f27fface",
        "esda3abe9e-a4a1-asd3245sdfv",
        "Pedro",
        "Pedro",
        "Perez",
        "Pedro Perez",
        "pedro@gmail.com",
        "administrator",
        "https://lh3.googleusercontent.com/a/AATXAJwTrrLqztPRtd1iBtaYao_LQ-6luvvdoaHkYSaC=s96-c",
        true,
        listOf("0b345364as6dfa123qwe"),
        listFavorites,
        "2021-12-29T16:17:31.992Z",
        "2022-05-04T13:55:18.328Z",
        3
    )
    private val userDel = UserFav(
        "60b14364b74284b5f27fface",
        "esda3abe9e-a4a1-asd3245sdfv",
        "Pedro",
        "Pedro",
        "Perez",
        "Pedro Perez",
        "pedro@gmail.com",
        "administrator",
        "https://lh3.googleusercontent.com/a/AATXAJwTrrLqztPRtd1iBtaYao_LQ-6luvvdoaHkYSaC=s96-c",
        true,
        listOf("0b345364as6dfa123qwe"),
        listOf("60b14364b74284b5f27fface"),
        "2021-12-29T16:17:31.992Z",
        "2022-05-04T13:55:18.328Z",
        3
    )
    private val assetIdPost = "60b14364b74284b5f27f2490"
    private val userPost = UserFav(
        "60b14364b74284b5f27fface",
        "esda3abe9e-a4a1-asd3245sdfv",
        "Pedro",
        "Pedro",
        "Perez",
        "Pedro Perez",
        "pedro@gmail.com",
        "administrator",
        "https://lh3.googleusercontent.com/a/AATXAJwTrrLqztPRtd1iBtaYao_LQ-6luvvdoaHkYSaC=s96-c",
        true,
        listOf("0b345364as6dfa123qwe"),
        listOf("60b14364b74284b5f27fface", "60b14364b74284b5f27f2490"),
        "2021-12-29T16:17:31.992Z",
        "2022-05-04T13:55:18.328Z",
        3
    )

    private val buildingItem = Building.BuildingItem(
        "JALA-COCHA",
        "Av Melchor Pérez de Olguín 2643",
        "60a807c5dc0ba843b1ac4063",
        "60a807c5dc0ba843b1ac4063",
        "Jala Building",
        true,
        "UTC-04:00",
        "2021-06-21",
        1,
        "0"
    )

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    lateinit var context: Context
    lateinit var mainViewModel: MainActivityViewModel
    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)

    override fun beforeAll(context: ExtensionContext?) {
        Dispatchers.setMain(testDispatcher)
    }

    override fun afterAll(context: ExtensionContext?) {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
        testScope.cleanupTestCoroutines()
    }

    @Before
    fun setUp() {
        context = Mockito.mock(Context::class.java)
        val mainMock = mock<MainActivityRepository>()
        val myBookingMock = mock<MainMyBookingsRepository>()
        val prefs = Prefs(context)
        mainViewModel = MainActivityViewModel(mainMock, myBookingMock, prefs)
        mainViewModel.myDate.value = LocalDate.now()
        optionsViewModel.endDate.postValue(LocalDate.of(2000, 1, 1))
        optionsViewModel.everySelection = 7
        optionsViewModel.daySelected = 15
        optionsViewModel.weekDaySelected = 3
        optionsViewModel.weekNumberSelected = 5
        optionsViewModel.monthSelected = 12
        optionsViewModel.weekdaysSelected[0] = true
        optionsViewModel.weekdaysSelected[1] = true
        optionsViewModel.weekdaysSelected[5] = true
    }

    @Test
    fun getAssetByIdTest() = testDispatcher.runBlockingTest {
        val mock = mock<AssetDetailRepository>()
        mock.stub {
            onBlocking { getAssetByIdProduction(assetId) } doReturn
                    Response.success(assetItem)
        }

        val viewModel = AssetDetailFragmentViewModel(mock)
        viewModel.getAssetById(assetId)

        val assetExpected = assetItem
        Assert.assertEquals(assetExpected, viewModel.myAsset.value)
    }

    @Test
    fun getBookingByDate() = testDispatcher.runBlockingTest {
        val mock = mock<AssetDetailRepository>()
        mock.stub {
            onBlocking {
                getBookingsByDate(
                    startTime,
                    endTime,
                    assetId
                )
            } doReturn
                    Response.success(arrayListOf(bookingResponseItem))
        }

        val viewModel = AssetDetailFragmentViewModel(mock)
        viewModel.getBookingByDate(
            startTime,
            endTime,
            assetId
        )

        val bookingResponseExpected = bookingResponseItem
        Assert.assertEquals(bookingResponseExpected, viewModel.myBookings.value?.get(0))
    }


    @Test
    fun checkOverlappingEventTestWhenRepositoryResponseIsCorrect() =
        testDispatcher.runBlockingTest {
            val mock = mock<AssetDetailRepository>()
            mock.stub {
                onBlocking {
                    getBookingsByDate(
                        startTime,
                        endTime,
                        assetId
                    )
                } doReturn
                        Response.success(arrayListOf(bookingResponseItem))
            }

            val viewModel = AssetDetailFragmentViewModel(mock)
            viewModel.checkOverlappingEvent(
                startTime,
                endTime,
                assetId
            )

            val isOverlappedExpected = true
            Assert.assertEquals(isOverlappedExpected, viewModel.isOverlapped.value)
        }

    @Test
    fun checkOverlappingEventTestWhenRepositoryResponseIsEmpty() = testDispatcher.runBlockingTest {
        val mock = mock<AssetDetailRepository>()
        mock.stub {
            onBlocking {
                getBookingsByDate(
                    startTime,
                    endTime,
                    assetId
                )
            } doReturn
                    Response.success(arrayListOf())
        }

        val viewModel = AssetDetailFragmentViewModel(mock)
        viewModel.checkOverlappingEvent(
            startTime,
            endTime,
            assetId
        )

        val isOverlappedExpected = false
        Assert.assertEquals(isOverlappedExpected, viewModel.isOverlapped.value)
    }

    @Test
    fun getCurrentBookingTest() = testDispatcher.runBlockingTest {
        mainViewModel.initDateTimeFiltersDefault()
        mainViewModel.userBookFor.value = user
        val viewModel = AssetDetailFragmentViewModel(mock<AssetDetailRepository>())
        val bookingResponseItemExpected = viewModel.getCurrentBooking(
            mainViewModel.myDate.value,
            LocalTime.now(),
            LocalTime.now(),
            mainViewModel.userBookFor.value
        )

        Assert.assertEquals(bookingResponseItemExpected.owner?.email, "pedro@gmail.com")
        Assert.assertEquals(bookingResponseItemExpected.owner?.name, "Pedro")
    }

    @Test
    fun getScheduleTimesTest() {
        val viewModel = AssetDetailFragmentViewModel(mock<AssetDetailRepository>())
        mainViewModel.initDateTimeFiltersDefault()
        Assert.assertTrue(viewModel.getScheduleTimes(
            mainViewModel.myDate.value,
            mainViewModel.myBuildings.value
        ).isEmpty())
        viewModel.myAsset.postValue(assetItemWithoutSchedule)
        val emptyList = viewModel.getScheduleTimes(
            mainViewModel.myDate.value,
            mainViewModel.myBuildings.value
        )
        Assert.assertTrue(emptyList.isEmpty())
        mainViewModel.myBuildings.value = listOf(buildingItem)
        val timezone = ZoneId.of(buildingItem.timezone).normalized()
        viewModel.myAsset.postValue(assetItemWithSchedule)
        val scheduleTimes = viewModel.getScheduleTimes(
            mainViewModel.myDate.value,
            mainViewModel.myBuildings.value
        )
        val date = mainViewModel.myDate.value ?: LocalDate.now().withYear(0)
        // First item is set with time 00:00
        val startOfDay = scheduleTimes[0]
        Assert.assertEquals(startOfDay.year, date.year)
        Assert.assertEquals(startOfDay.month, date.month)
        Assert.assertEquals(startOfDay.dayOfMonth, date.dayOfMonth)
        Assert.assertEquals(startOfDay.hour, 0)
        Assert.assertEquals(startOfDay.minute, 0)
        val startTimeFirstSchedule = scheduleTimes[1]
        var expected: String = DateUtils.stringToLocalDateTime(date, "2021-11-05T14:00:00Z", timezone).toString()
        Assert.assertEquals(startTimeFirstSchedule.toString(), expected)
        val endTimeFirstSchedule = scheduleTimes[2]
        expected = DateUtils.stringToLocalDateTime(date, "2021-11-05T16:00:00Z", timezone).toString()
        Assert.assertEquals(endTimeFirstSchedule.toString(), expected)
        val startTimeSecondSchedule = scheduleTimes[3]
        expected = DateUtils.stringToLocalDateTime(date, "2021-11-05T20:00:00Z", timezone).toString()
        Assert.assertEquals(startTimeSecondSchedule.toString(), expected)
        val endTimeSecondSchedule = scheduleTimes[4]
        expected = DateUtils.stringToLocalDateTime(date, "2021-11-05T23:00:00Z", timezone).toString()
        Assert.assertEquals(endTimeSecondSchedule.toString(), expected)

        // Last item is set with time 23:59
        val endOfDay = scheduleTimes[5]
        Assert.assertEquals(endOfDay.year, date.year)
        Assert.assertEquals(endOfDay.month, date.month)
        Assert.assertEquals(endOfDay.dayOfMonth, date.dayOfMonth)
        Assert.assertEquals(endOfDay.hour, 23)
        Assert.assertEquals(endOfDay.minute, 59)
    }

    @Test
    fun getScheduleBookingsTest() {
        val viewModel = AssetDetailFragmentViewModel(mock<AssetDetailRepository>())
        mainViewModel.initDateTimeFiltersDefault()
        Assert.assertTrue(viewModel.getScheduleBookings(
            mainViewModel.myDate.value,
            mainViewModel.myBuildings.value
        ).isEmpty())
        viewModel.myAsset.postValue(assetItemWithoutSchedule)
        val emptyList = viewModel.getScheduleBookings(
            mainViewModel.myDate.value,
            mainViewModel.myBuildings.value
        )
        Assert.assertTrue(emptyList.isEmpty())
        viewModel.myAsset.postValue(assetItemWithSchedule)

        val scheduleTimes = viewModel.getScheduleTimes(
            mainViewModel.myDate.value,
            mainViewModel.myBuildings.value
        )
        viewModel.getScheduleBookings(
            mainViewModel.myDate.value,
            mainViewModel.myBuildings.value
        )
        val firstSchedule = viewModel.scheduledBookings[0]
        val secondSchedule = viewModel.scheduledBookings[1]
        val thirdSchedule = viewModel.scheduledBookings[2]
        Assert.assertEquals(firstSchedule.id, SCHEDULED_EVENT_ID)
        Assert.assertEquals(secondSchedule.id, SCHEDULED_EVENT_ID)
        Assert.assertEquals(thirdSchedule.id, SCHEDULED_EVENT_ID)

        Assert.assertEquals(firstSchedule.startTime, scheduleTimes[0].toString())
        Assert.assertEquals(secondSchedule.startTime, scheduleTimes[2].toString())
        Assert.assertEquals(thirdSchedule.startTime, scheduleTimes[4].toString())

        Assert.assertEquals(firstSchedule.endTime, scheduleTimes[1].toString())
        Assert.assertEquals(secondSchedule.endTime, scheduleTimes[3].toString())
        Assert.assertEquals(thirdSchedule.endTime, scheduleTimes[5].toString())
    }

    @Test
    fun isOverlappedWithScheduleTest() {
        val viewModel = AssetDetailFragmentViewModel(mock<AssetDetailRepository>())
        mainViewModel.initDateTimeFiltersDefault()
        Assert.assertFalse(viewModel.isOverlappedWithSchedule(
            mainViewModel.userStartTime.value,
            mainViewModel.userEndTime.value
        ))
        viewModel.scheduledBookings = listOf(bookingOutsideSchedule)
        Assert.assertFalse(viewModel.isOverlappedWithSchedule(
            mainViewModel.userStartTime.value,
            mainViewModel.userEndTime.value
        ))
        viewModel.scheduledBookings = listOf(bookingOutsideSchedule, bookingInsideSchedule)
        Assert.assertTrue(viewModel.isOverlappedWithSchedule(
            mainViewModel.userStartTime.value,
            mainViewModel.userEndTime.value
        ))
    }

    @Test
    fun postBookingTest() = testDispatcher.runBlockingTest {
        val postBookingItem = PostBooking(
            "610446ee846769858d9ccf6f",
            "2022-04-29T23:00:00+00:00",
            true,
            null,
            null,
            "2022-04-29T23:00:00+00:00",
            Status(PENDING)
        )

        val mock = mock<AssetDetailRepository>()
        mock.stub {
            onBlocking { postBooking(postBookingItem) } doReturn
                    Response.success(bookingResponseItem)
            onBlocking {
                getBookingsByDate(
                    startTime,
                    endTime,
                    assetId
                )
            } doReturn
                    Response.success(arrayListOf(bookingResponseItem))
        }
        mainViewModel.initDateTimeFiltersDefault()
        val viewModel = AssetDetailFragmentViewModel(mock)
        viewModel.postBooking(
            context,
            assetId,
            startTime,
            endTime,
            postBookingItem,
            mainViewModel.myDate.value,
            mainViewModel.userStartTime.value,
            mainViewModel.userEndTime.value,
            {},
            {},
            { _: BookingResponse.Booking, _: Context -> },
            {}
        )

        val isOverlappedExpected = true
        Assert.assertEquals(isOverlappedExpected, viewModel.isOverlapped.value)
    }

    @Test
    fun createRecurrenceRuleIsNotRecurrentTest() {
        val viewModel = AssetDetailFragmentViewModel(mock<AssetDetailRepository>())
        val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
        val rule = viewModel.createRecurrenceRule(
            RECURRENCE_OPTION_DAILY,
            false,
            optionsViewModel.endDate.value,
            mainViewModel.userEndTime.value,
            mainViewModel.getBuildingZoneId(),
            optionsViewModel.monthSelected,
            optionsViewModel.onDaySelected,
            optionsViewModel.weekNumberSelected,
            optionsViewModel.everySelection,
            optionsViewModel.daySelected,
            optionsViewModel.weekdaysSelected,
            optionsViewModel.weekDaySelected
        )
        Assert.assertNull(rule.type)
        Assert.assertEquals(false, rule.isCustom)
        Assert.assertEquals(null, rule.every)
        Assert.assertEquals(null, rule.onDate)
        Assert.assertEquals(null, rule.onMonth)
        Assert.assertEquals(null, rule.onWeek)
        Assert.assertEquals(null, rule.onDays)
        Assert.assertEquals("2000-01-01", rule.endDate?.split("T")?.get(0))
    }

    @Test
    fun createRecurrenceRuleNeverOptionTest() {
        val viewModel = AssetDetailFragmentViewModel(mock<AssetDetailRepository>())
        val optionsViewModel = BookingOptionsViewModel(mainViewModel.myDate.value)
        val rule = viewModel.createRecurrenceRule(
            RECURRENCE_OPTION_NEVER,
            true,
            mainViewModel.myDate.value,
            mainViewModel.userEndTime.value,
            mainViewModel.getBuildingZoneId(),
            optionsViewModel.monthSelected,
            optionsViewModel.onDaySelected,
            optionsViewModel.weekNumberSelected,
            optionsViewModel.everySelection,
            optionsViewModel.daySelected,
            optionsViewModel.weekdaysSelected,
            optionsViewModel.weekDaySelected
        )
        Assert.assertEquals("", rule.type)
        Assert.assertEquals(false, rule.isCustom)
        Assert.assertEquals(0, rule.every)
        Assert.assertEquals(null, rule.onDate)
        Assert.assertEquals(null, rule.onMonth)
        Assert.assertEquals(null, rule.onWeek)
        Assert.assertEquals(true, rule.onDays?.isEmpty())
    }

    @Test
    fun createRecurrenceRuleDailyOptionTest() {
        val viewModel = AssetDetailFragmentViewModel(mock<AssetDetailRepository>())
        val rule = viewModel.createRecurrenceRule(
            RECURRENCE_OPTION_DAILY,
            true,
            mainViewModel.myDate.value,
            mainViewModel.userEndTime.value,
            mainViewModel.getBuildingZoneId(),
            optionsViewModel.monthSelected,
            optionsViewModel.onDaySelected,
            optionsViewModel.weekNumberSelected,
            optionsViewModel.everySelection,
            optionsViewModel.daySelected,
            optionsViewModel.weekdaysSelected,
            optionsViewModel.weekDaySelected
        )
        Assert.assertEquals(RECURRENCE_OPTION_DAILY.lowercase(), rule.type)
        Assert.assertEquals(true, rule.isCustom)
        Assert.assertEquals(7, rule.every)
        Assert.assertEquals(null, rule.onDate)
        Assert.assertEquals(null, rule.onMonth)
        Assert.assertEquals(null, rule.onWeek)
        Assert.assertEquals(true, rule.onDays?.isEmpty())
    }

    @Test
    fun createRecurrenceRuleWeeklyOptionTest() {
        val viewModel = AssetDetailFragmentViewModel(mock<AssetDetailRepository>())
        val date = optionsViewModel.endDate.value ?: mainViewModel.myDate.value
        val rule = viewModel.createRecurrenceRule(
            RECURRENCE_OPTION_WEEKLY,
            true,
            date,
            mainViewModel.userEndTime.value,
            mainViewModel.getBuildingZoneId(),
            optionsViewModel.monthSelected,
            optionsViewModel.onDaySelected,
            optionsViewModel.weekNumberSelected,
            optionsViewModel.everySelection,
            optionsViewModel.daySelected,
            optionsViewModel.weekdaysSelected,
            optionsViewModel.weekDaySelected
        )
        Assert.assertEquals(RECURRENCE_OPTION_WEEKLY.lowercase(), rule.type)
        Assert.assertEquals(true, rule.isCustom)
        Assert.assertEquals(7, rule.every)
        Assert.assertEquals(null, rule.onDate)
        Assert.assertEquals(null, rule.onMonth)
        Assert.assertEquals(null, rule.onWeek)
        Assert.assertEquals(3, rule.onDays?.size)
        Assert.assertEquals(SUNDAY, rule.onDays?.get(0))
        Assert.assertEquals(MONDAY, rule.onDays?.get(1))
        Assert.assertEquals(FRIDAY, rule.onDays?.get(2))
    }

    @Test
    fun createRecurrenceRuleMonthlyOptionOnDayTest() {
        val viewModel = AssetDetailFragmentViewModel(mock<AssetDetailRepository>())
        optionsViewModel.onDaySelected = true
        val rule = viewModel.createRecurrenceRule(
            RECURRENCE_OPTION_MONTHLY,
            true,
            LocalDate.now(),
            mainViewModel.userEndTime.value,
            mainViewModel.getBuildingZoneId(),
            optionsViewModel.monthSelected,
            optionsViewModel.onDaySelected,
            optionsViewModel.weekNumberSelected,
            optionsViewModel.everySelection,
            optionsViewModel.daySelected,
            optionsViewModel.weekdaysSelected,
            optionsViewModel.weekDaySelected
        )
        Assert.assertEquals(RECURRENCE_OPTION_MONTHLY.lowercase(), rule.type)
        Assert.assertEquals(true, rule.isCustom)
        Assert.assertEquals(7, rule.every)
        Assert.assertEquals(15, rule.onDate)
        Assert.assertEquals(null, rule.onMonth)
        Assert.assertEquals(null, rule.onWeek)
        Assert.assertEquals(true, rule.onDays?.isEmpty())
    }

    @Test
    fun createRecurrenceRuleMonthlyOptionOnWeekTest() {
        val viewModel = AssetDetailFragmentViewModel(mock<AssetDetailRepository>())
        optionsViewModel.onDaySelected = false
        val rule = viewModel.createRecurrenceRule(
            RECURRENCE_OPTION_MONTHLY,
            true,
            LocalDate.now(),
            mainViewModel.userEndTime.value,
            mainViewModel.getBuildingZoneId(),
            optionsViewModel.monthSelected,
            optionsViewModel.onDaySelected,
            optionsViewModel.weekNumberSelected,
            optionsViewModel.everySelection,
            optionsViewModel.daySelected,
            optionsViewModel.weekdaysSelected,
            optionsViewModel.weekDaySelected
        )
        Assert.assertEquals(RECURRENCE_OPTION_MONTHLY.lowercase(), rule.type)
        Assert.assertEquals(true, rule.isCustom)
        Assert.assertEquals(7, rule.every)
        Assert.assertEquals(null, rule.onDate)
        Assert.assertEquals(null, rule.onMonth)
        Assert.assertEquals(5, rule.onWeek)
        Assert.assertEquals(1, rule.onDays?.size)
        Assert.assertEquals(WEDNESDAY, rule.onDays?.get(0))
    }

    @Test
    fun createRecurrenceRuleYearlyOptionOnDayTest() {
        val assetDetailRepositoryMock = mock<AssetDetailRepository>()
        val viewModel = AssetDetailFragmentViewModel(assetDetailRepositoryMock)
        optionsViewModel.onDaySelected = true
        val rule = viewModel.createRecurrenceRule(
            RECURRENCE_OPTION_YEARLY,
            true,
            LocalDate.now(),
            mainViewModel.userEndTime.value,
            mainViewModel.getBuildingZoneId(),
            optionsViewModel.monthSelected,
            optionsViewModel.onDaySelected,
            optionsViewModel.weekNumberSelected,
            optionsViewModel.everySelection,
            optionsViewModel.daySelected,
            optionsViewModel.weekdaysSelected,
            optionsViewModel.weekDaySelected
        )
        Assert.assertEquals(RECURRENCE_OPTION_YEARLY.lowercase(), rule.type)
        Assert.assertEquals(true, rule.isCustom)
        Assert.assertEquals(0, rule.every)
        Assert.assertEquals(15, rule.onDate)
        Assert.assertEquals(12, rule.onMonth)
        Assert.assertEquals(null, rule.onWeek)
        Assert.assertEquals(true, rule.onDays?.isEmpty())
    }

    @Test
    fun createRecurrenceRuleYearlyOptionOnWeekTest() {
        val assetDetailRepositoryMock = mock<AssetDetailRepository>()
        val viewModel = AssetDetailFragmentViewModel(assetDetailRepositoryMock)
        optionsViewModel.onDaySelected = false
        val rule = viewModel.createRecurrenceRule(
            RECURRENCE_OPTION_YEARLY,
            true,
            LocalDate.now(),
            mainViewModel.userEndTime.value,
            mainViewModel.getBuildingZoneId(),
            optionsViewModel.monthSelected,
            optionsViewModel.onDaySelected,
            optionsViewModel.weekNumberSelected,
            optionsViewModel.everySelection,
            optionsViewModel.daySelected,
            optionsViewModel.weekdaysSelected,
            optionsViewModel.weekDaySelected
        )
        Assert.assertEquals(RECURRENCE_OPTION_YEARLY.lowercase(), rule.type)
        Assert.assertEquals(true, rule.isCustom)
        Assert.assertEquals(0, rule.every)
        Assert.assertEquals(null, rule.onDate)
        Assert.assertEquals(12, rule.onMonth)
        Assert.assertEquals(5, rule.onWeek)
        Assert.assertEquals(1, rule.onDays?.size)
        Assert.assertEquals(WEDNESDAY, rule.onDays?.get(0))
    }

    @Test
    fun getArrayDaysTest() {
        val assetDetailRepositoryMock = mock<AssetDetailRepository>()
        val viewModel = AssetDetailFragmentViewModel(assetDetailRepositoryMock)
        val temp = optionsViewModel.weekdaysSelected
        optionsViewModel.weekdaysSelected = BooleanArray(7)
        Assert.assertTrue(viewModel.getArrayDays(
            RECURRENCE_OPTION_NEVER,
            optionsViewModel.weekdaysSelected,
            optionsViewModel.onDaySelected,
            optionsViewModel.weekDaySelected
        ).isEmpty())
        Assert.assertTrue(viewModel.getArrayDays(
            RECURRENCE_OPTION_DAILY,
            optionsViewModel.weekdaysSelected,
            optionsViewModel.onDaySelected,
            optionsViewModel.weekDaySelected
        ).isEmpty())
        Assert.assertTrue(viewModel.getArrayDays(
            RECURRENCE_OPTION_WEEKLY,
            optionsViewModel.weekdaysSelected,
            optionsViewModel.onDaySelected,
            optionsViewModel.weekDaySelected
        ).isEmpty())
        optionsViewModel.weekdaysSelected[0] = true
        Assert.assertTrue(viewModel.getArrayDays(
            RECURRENCE_OPTION_WEEKLY,
            optionsViewModel.weekdaysSelected,
            optionsViewModel.onDaySelected,
            optionsViewModel.weekDaySelected
        )[0] == SUNDAY)
        optionsViewModel.weekdaysSelected[0] = false
        optionsViewModel.onDaySelected = true
        Assert.assertTrue(viewModel.getArrayDays(
            RECURRENCE_OPTION_MONTHLY,
            optionsViewModel.weekdaysSelected,
            optionsViewModel.onDaySelected,
            optionsViewModel.weekDaySelected
        ).isEmpty())
        Assert.assertTrue(viewModel.getArrayDays(
            RECURRENCE_OPTION_YEARLY,
            optionsViewModel.weekdaysSelected,
            optionsViewModel.onDaySelected,
            optionsViewModel.weekDaySelected
        ).isEmpty())
        optionsViewModel.onDaySelected = false
        Assert.assertTrue(viewModel.getArrayDays(
            RECURRENCE_OPTION_MONTHLY,
            optionsViewModel.weekdaysSelected,
            optionsViewModel.onDaySelected,
            optionsViewModel.weekDaySelected
        )[0] == WEDNESDAY)
        optionsViewModel.weekDaySelected = 5
        Assert.assertTrue(viewModel.getArrayDays(
            RECURRENCE_OPTION_YEARLY,
            optionsViewModel.weekdaysSelected,
            optionsViewModel.onDaySelected,
            optionsViewModel.weekDaySelected
        )[0] == FRIDAY)
        optionsViewModel.onDaySelected = true
        optionsViewModel.weekDaySelected = 3
        optionsViewModel.weekdaysSelected = temp
    }

    @Test
    fun getWeekDayNameTest() {
        val assetDetailRepositoryMock = mock<AssetDetailRepository>()
        val viewModel = AssetDetailFragmentViewModel(assetDetailRepositoryMock)
        Assert.assertEquals(SUNDAY, viewModel.getWeekDayName(0))
        Assert.assertEquals(MONDAY, viewModel.getWeekDayName(1))
        Assert.assertEquals(TUESDAY, viewModel.getWeekDayName(2))
        Assert.assertEquals(WEDNESDAY, viewModel.getWeekDayName(3))
        Assert.assertEquals(THURSDAY, viewModel.getWeekDayName(4))
        Assert.assertEquals(FRIDAY, viewModel.getWeekDayName(5))
        Assert.assertEquals(SATURDAY, viewModel.getWeekDayName(6))
        Assert.assertEquals(SUNDAY, viewModel.getWeekDayName(7))
    }

    @Test
    fun cancelBookingTest() = testDispatcher.runBlockingTest {
        val mock = mock<AssetDetailRepository>()
        mock.stub {
            onBlocking { cancelBooking(bookingId) } doReturn
                    Response.success(bookingResponseItem)
            onBlocking {
                getBookingsByDate(
                    startTime,
                    endTime,
                    assetId
                )
            } doReturn
                    Response.success(arrayListOf())
        }
        mainViewModel.initDateTimeFiltersDefault()
        val viewModel = AssetDetailFragmentViewModel(mock)
        viewModel.stub {
            onBlocking { checkOverlappingEvent(
                startTime,
                endTime,
                assetId
            ) } doReturn viewModel.isOverlapped.postValue(false)
        }
        viewModel.cancelBooking(
            bookingId,
            startTime,
            mainViewModel.myDate.value,
            mainViewModel.userStartTime.value,
            mainViewModel.userEndTime.value,
            mainViewModel.getBuildingZoneId()
        )

        val isOverlappedExpected = false
        Assert.assertEquals(isOverlappedExpected, viewModel.isOverlapped.value)
    }

    @Test
    fun getUserInfoTest() = testDispatcher.runBlockingTest {
        val mock = mock<AssetDetailRepository>()
        mock.stub {
            onBlocking { getUserInfo() } doReturn
                    Response.success(userFavModel)
        }

        val viewModel = AssetDetailFragmentViewModel(mock)
        viewModel.getUserInfo(assetId)

        isFavorited.postValue(true)
        val valueExpected = isFavorited.value
        Assert.assertEquals(valueExpected, viewModel.isFavorite.value)
    }

    @Test
    fun deleteFavoriteAssetTest() = testDispatcher.runBlockingTest {
        val mock = mock<AssetDetailRepository>()
        mock.stub {
            onBlocking { deleteFavoriteAsset(assetId) } doReturn
                    Response.success(userDel)
        }

        val viewModel = AssetDetailFragmentViewModel(mock)
        viewModel.deleteFavoriteAsset(assetId) {}

        isFavorited.postValue(false)
        val listExpected = isFavorited.value
        Assert.assertEquals(listExpected, viewModel.isFavorite.value)
    }

    @Test
    fun postFavoriteAssetTest() = testDispatcher.runBlockingTest {
        val mock = mock<AssetDetailRepository>()
        mock.stub {
            onBlocking { postFavoriteAsset(assetIdPost) } doReturn
                    Response.success(userPost)
        }

        val viewModel = AssetDetailFragmentViewModel(mock)
        viewModel.postFavoriteAsset(assetIdPost)

        isFavorited.postValue(true)
        val listExpected = isFavorited.value
        Assert.assertEquals(listExpected, viewModel.isFavorite.value)
    }
}