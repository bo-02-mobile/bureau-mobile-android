package com.jalasoft.bureau.ui.bookings.viewmodel
import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.rules.TestCoroutineRule
import com.jalasoft.bureau.ui.bookings.repository.MainMyBookingsRepository
import com.jalasoft.bureau.ui.bookings.repository.MyBookingsProductionService
import com.jalasoft.bureau.ui.main.repository.MainActivityRepository
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.workstations.repository.EventService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.stub
import retrofit2.Response
import retrofit2.Retrofit
import java.time.ZonedDateTime
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
class MyBookingsViewModelTest : BeforeAllCallback, AfterAllCallback {

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)

    lateinit var myBookingsViewModel : MyBookingsViewModel
    lateinit var context : Context
    lateinit var repository: MainMyBookingsRepository
    lateinit var mainViewModel: MainActivityViewModel
    private val buildingId = "60a807c5dc0ba843b1ac4063"
    private val userBookingId = "61b8b1d2e55fd40dd92dcb10"
    private val assetId = "61a785af15686466584bdf83"

    private val bookingItem = BookingResponse.Booking(
        "610445e3846769858d9ccf6e",
        "NewArea3100",
        "W0031",
        "610446ee846769858d9ccf6f",
        "workstation",
        "60c11f74984d2c7a73a0da0e",
        "",
        "2021-07-29T19:15:51.205Z",
        "2021-06-31T19:15:51.205Z",
        "61044fe7846769858d9ccf75",
        false, true, null, null, "2021-07-31T19:20:51.205Z", "2021-08-30T14:15:51.205Z", 1,
        "",
        BookingResponse.Status("Pending", null, null),
        null
    )

    private val permanentBookingItem = BookingResponse.Booking(
        "610445e3846769858d9ccf6e",
        "Permanent 01",
        "W0032",
        "610446ee846769858d9ccf6f",
        "balcony",
        "60c11f74984d2c7a73a0da0e",
        "",
        "2021-07-29T19:15:51.205Z",
        "2021-06-31T19:15:51.205Z",
        "61044fe7846769858d9ccf75",
        true, true, null, null, "2021-07-31T19:20:51.205Z", "2021-08-30T14:15:51.205Z", 1,
        "",
        BookingResponse.Status("Pending", null, null),
        null
    )

    private val asset = Asset.AssetItem(
        "60b13fe3b74284b5f27ffa12",
        "60a807c5dc0ba843b1ac4063",
        "1C001",
        "2020-03-26T10:28:10.294Z",
        "",
        "60b14364b74284b5f27ff903",
        "1C001", null, null,true, null, "2021-10-26T14:28:10.294Z", 1, true
    )
    private val building = Building.BuildingItem(
        "JALA-COCHA",
        "Av Melchor Pérez de Olguín 2643",
        "60a807c5dc0ba843b1ac4063",
        "60c11f74984d2c7a73a0da0e",
        "Jala Building",
        true,
        "UTC-04",
        "2021-06-21",
        1,
        "0"
    )
    private val event = BookingResponse.Booking(
        "60a807f1dc0ba843b1ac4064", "3A", "3A013", "60b146e4b74284b5f27ffaf4",
        "workstation", "60a807c5dc0ba843b1ac4063", "", "2021-08-03T22:50:56.762Z",
        "2021-08-04T21:50:56.762Z", "6109c850846769858d9ccfa2", false, false, null, null,
        "2021-08-04T23:30:00Z", "2021-08-03T22:50:56.762Z", 1, "",
        BookingResponse.Status("Pending", null, null), null
    )

    private val mockWebServer = MockWebServer()

    private val client = OkHttpClient.Builder()
        .connectTimeout(1, TimeUnit.SECONDS)
        .readTimeout(1, TimeUnit.SECONDS)
        .writeTimeout(1, TimeUnit.SECONDS)
        .build()

    private val api = Retrofit.Builder()
        .baseUrl(mockWebServer.url("/"))
        .client(client)
        .build()

    @Before
    fun setUp() {
        context = Mockito.mock(Context::class.java)
        val bookingsProductionServiceMock = MyBookingsProductionService(api)
        val eventServiceMock = EventService(api)
        repository = MainMyBookingsRepository(bookingsProductionServiceMock, eventServiceMock)
        myBookingsViewModel = MyBookingsViewModel(repository)

        val mainMock = mock<MainActivityRepository>()
        val myBookingMock = mock<MainMyBookingsRepository>()
        val prefs = Prefs(context)
        mainViewModel = MainActivityViewModel(mainMock, myBookingMock, prefs)
    }

    override fun beforeAll(context: ExtensionContext?) {
        Dispatchers.setMain(testDispatcher)
    }

    override fun afterAll(context: ExtensionContext?) {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
        testScope.cleanupTestCoroutines()
    }

    @Test
    fun getAllBookings() = testDispatcher.runBlockingTest {
        val mock = mock<MainMyBookingsRepository>()
        mock.stub {
            onBlocking { getBookingsProduction(any(), any(), any()) }  doReturn
                    Response.success(listOf(bookingItem))
            onBlocking { getPermanentBookings(any()) }  doReturn
                    Response.success(listOf(asset))
        }
        val viewModel = MyBookingsViewModel(mock)
        viewModel.getAllBookings("", userBookingId, false, ::goToBooking, null)

        val nonPermanentExpected = arrayListOf(bookingItem)
        val permanentExpected = arrayListOf(permanentBookingItem)
        Assert.assertEquals(nonPermanentExpected, viewModel.nonPermanentBookings)
    }

    @Test
    fun getAssetDetails() = testDispatcher.runBlockingTest {
        val mock = mock<MainMyBookingsRepository>()
        mock.stub {
            onBlocking { getAssetDetailsProduction(assetId) }  doReturn
                    Response.success(asset)
        }
        val viewModel = MyBookingsViewModel(mock)
        viewModel.getAssetDetails(assetId)
        val expected = asset
        val actual = viewModel.assetDetails.value
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun getBuildingDetails() = testDispatcher.runBlockingTest {
        val mock = mock<MainMyBookingsRepository>()
        mock.stub {
            onBlocking { getBuildingDetails(buildingId) }  doReturn
                    Response.success(building)
        }
        val viewModel = MyBookingsViewModel(mock)
        viewModel.getBuildingDetails(buildingId)
        val expected = building
        val actual = viewModel.buildingDetails.value
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun deleteBooking() = testDispatcher.runBlockingTest {
        val mock = mock<MainMyBookingsRepository>()
        mock.stub {
            onBlocking { deleteBookingProduction(userBookingId) }  doReturn
                    Response.success(bookingItem)
        }
        val viewModel = MyBookingsViewModel(mock)
        myBookingsViewModel.todayBookings.postValue(listOf(bookingItem))
        viewModel.deleteBooking(context, userBookingId, ::onDelete)
        val expected = true
        val actual = myBookingsViewModel.todayBookings.value?.isEmpty()
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun getAllEvents() = testDispatcher.runBlockingTest {
        val mock = mock<MainMyBookingsRepository>()
        mock.stub {
            onBlocking { getEventsProduction(any(), any()) }  doReturn
                    Response.success(arrayListOf(bookingItem))
        }
        val viewModel = MyBookingsViewModel(mock)
        viewModel.getAllEvents(assetId, ZonedDateTime.now())
        val expected = bookingItem
        val actual = viewModel.myEvents.value?.get(0)
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun testBookingListIsEmptyReturnsTrue() {
        Assert.assertEquals(true, myBookingsViewModel.bookingListIsEmpty())
        myBookingsViewModel.todayBookings.postValue(listOf())
        myBookingsViewModel.upcomingBookings.postValue(listOf())
        Assert.assertEquals(true, myBookingsViewModel.bookingListIsEmpty())
    }

    @Test
    fun testBookingListIsEmptyReturnsFalse() {
        myBookingsViewModel.todayBookings.postValue(listOf(bookingItem))
        myBookingsViewModel.upcomingBookings.postValue(listOf(bookingItem))
        Assert.assertEquals(false, myBookingsViewModel.bookingListIsEmpty())
    }

    private fun goToBooking(booking: BookingResponse.Booking) { }
    private fun onDelete() {
        myBookingsViewModel.todayBookings.postValue(listOf())
    }

    @Test
    fun getAllBookingsAndManageBookingDataCalledFromWidget() = testDispatcher.runBlockingTest {
        val mock = mock<MainMyBookingsRepository>()
        mock.stub {
            onBlocking { getBookingsProduction(any(), any(), any()) }  doReturn
                    Response.success(listOf(bookingItem))
            onBlocking { getPermanentBookings(any()) }  doReturn
                    Response.success(listOf(asset))
        }
        val viewModel = MyBookingsViewModel(mock)
        viewModel.getAllBookings("", userBookingId, true, ::goToBooking, null)
        Assert.assertEquals(null, viewModel.upcomingBookings.value)
    }

    @Test
    fun getAllBookingsAndManageBookingDataNotCalledFromWidget() = testDispatcher.runBlockingTest {
        val mock = mock<MainMyBookingsRepository>()
        mock.stub {
            onBlocking { getBookingsProduction(any(), any(), any()) }  doReturn
                    Response.success(listOf(bookingItem))
            onBlocking { getPermanentBookings(any()) }  doReturn
                    Response.success(listOf(asset))
        }
        val viewModel = MyBookingsViewModel(mock)
        mainViewModel.myBuildings?.value = listOf(building)
        viewModel.getAllBookings("", userBookingId, false, ::goToBooking, null)
        val expected = arrayListOf(bookingItem)
        Assert.assertEquals(emptyList<BookingResponse.Booking>(), viewModel.permanentBookingsWidget)
    }
}