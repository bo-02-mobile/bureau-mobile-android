package com.jalasoft.bureau.ui.main.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.model.area.Area
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.data.model.common.Owner
import com.jalasoft.bureau.data.model.favorites.UserFav
import com.jalasoft.bureau.data.model.token.User
import com.jalasoft.bureau.data.model.user.Groups
import com.jalasoft.bureau.rules.TestCoroutineRule
import com.jalasoft.bureau.ui.bookings.repository.MainMyBookingsRepository
import com.jalasoft.bureau.ui.main.repository.MainActivityRepository
import com.jalasoft.bureau.utils.DATE_FORMAT
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.stub
import retrofit2.Response
import java.time.*
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

@ExperimentalCoroutinesApi
class MainActivityViewModelTest : BeforeAllCallback, AfterAllCallback {

    private val user = User(
        "pedro@gmail.com",
        "https://lh3.googleusercontent.com/a/AATXAJwTrrLqztPRtd1iBtaYao_LQ-6luvvdoaHkYSaC=s96-c",
        "Pedro"
    )

    private val building = Building.BuildingItem(
        "JALA-COCHA",
        "Av Melchor Pérez de Olguín 2643",
        "60a807c5dc0ba843b1ac4063",
        "Jala Building",
        "Jala Building",
        true,
        "UTC-04",
        "2021-06-21",
        1,
        "0"
    )
    private val bookingItem = BookingResponse.Booking(
        "610445e3846769858d9ccf6e",
        "NewArea3100",
        "W0031",
        "610446ee846769858d9ccf6f",
        "workstation",
        "60c11f74984d2c7a73a0da0e",
        "",
        "2021-07-29T19:15:51.205Z",
        "2021-06-31T19:15:51.205Z",
        "61044fe7846769858d9ccf75",
        false,
        true,
        null,
        null,
        "2021-07-31T19:20:51.205Z",
        "2021-08-30T14:15:51.205Z",
        1, "",
        BookingResponse.Status("Pending", null, null),
        null
    )

    private val areas = listOf<Area.AreaItem>(
        Area.AreaItem(
            Area.AreaItem.Background("uyz1ftb3fhpfnkktajo0",
                "https://res.cloudinary.com/dbjicvy8t/image/upload/v1638367454/uyz1ftb3fhpfnkktajo0.jpg"),
            "60c11f74984d2c7a73a0da0e",
            "3A",
            "2020-06-28T18:32:23.207Z",
            "", "60a807f1dc0ba843b1ac4064",
            listOf(),
            "3A", true,
            Area.AreaItem.Thumbnail("fzejpp2fr3doyq5moott",
                "https://res.cloudinary.com/dbjicvy8t/image/upload/v1638367452/fzejpp2fr3doyq5moott.jpg"),
            "2020-06-28T14:32:23.207Z", 1, null
        )
    )

    private val currentUser = UserFav(
        "60b14364b74284b5f27fface",
        "esda3abe9e-a4a1-asd3245sdfv",
        "Pedro",
        "Pedro",
        "Perez",
        "Pedro Perez",
        "pedro@gmail.com",
        "administrator",
        "https://lh3.googleusercontent.com/a/AATXAJwTrrLqztPRtd1iBtaYao_LQ-6luvvdoaHkYSaC=s96-c",
        true,
        listOf("0b345364as6dfa123qwe"),
        listOf("61a7c2d21a7fb92f9aefd8ba"),
        "2021-12-29T16:17:31.992Z",
        "2022-05-04T13:55:18.328Z",
        3
    )

    private val groups = listOf<Groups.Group>(
        Groups.Group(
            "6243286a3504259aea126fae",
            "officeQA",
            mapOf(Pair("Lounge",2)),
            true,
            true,
            "2022-03-29T15:40:26.631Z",
            "2022-03-29T15:41:48.313Z",
            1
        )
    )

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    lateinit var viewModel: MainActivityViewModel
    lateinit var context: Context
    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)

    override fun beforeAll(context: ExtensionContext?) {
        Dispatchers.setMain(testDispatcher)
    }

    override fun afterAll(context: ExtensionContext?) {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
        testScope.cleanupTestCoroutines()
    }

    @Before
    fun setUp() {
        context = Mockito.mock(Context::class.java)
        val mainMock = mock<MainActivityRepository>()
        val myBookingMock = mock<MainMyBookingsRepository>()
        val prefs = Prefs(context)
        viewModel = MainActivityViewModel(mainMock, myBookingMock, prefs)
    }

    @Test
    fun initDateTimeFiltersDefaultTest() {
        viewModel.userInfo.value = user

        viewModel.resetUserBookFor()

        Assert.assertEquals(viewModel.userInfo.value, viewModel.userBookFor.value)
    }

    @Test
    fun resetUserBookForTest() {
        viewModel.initDateTimeFiltersDefault()

        val userStartTime = LocalTime.of(8, 30)
        val userEndTime = LocalTime.of(18, 30)
        val myDate = LocalDate.now()

        Assert.assertEquals(viewModel.userStartTime.value, userStartTime)
        Assert.assertEquals(viewModel.userEndTime.value, userEndTime)
        Assert.assertEquals(viewModel.myDate.value, myDate)
    }

    @Test
    fun postDateTestWhenLocalDateIsEqualToDateInMiliseconds() {
        viewModel.initDateTimeFiltersDefault()
        val postDateResult = viewModel.postDate(System.currentTimeMillis())
        val timeZoneId = "UTC"

        val myDate = LocalDateTime.ofInstant(
            Instant.ofEpochMilli(System.currentTimeMillis()),
            ZoneId.of(timeZoneId)
        ).toLocalDate()

        val postDateResultExpected = false
        val myDateExpected = viewModel.myDate.value

        Assert.assertEquals(postDateResultExpected, postDateResult)
        Assert.assertEquals(myDateExpected, myDate)

    }

    @Test
    fun postDateTestWhenLocalDateSurpassesDateInMiliseconds() {
        viewModel.initDateTimeFiltersDefault()
        viewModel.userStartTime.value = LocalTime.now()
        val postDateResult = viewModel.postDate(1641721590469)

        val postDateResultExpected = true
        val myDateExpected = viewModel.myDate.value
        val userStartTimeExpected = viewModel.userStartTime.value
        val userEndTimeExpected = viewModel.userEndTime.value

        Assert.assertEquals(postDateResultExpected, postDateResult)
        Assert.assertEquals(myDateExpected, LocalDate.now())
        Assert.assertEquals(
            userStartTimeExpected,
            LocalTime.now().truncatedTo(ChronoUnit.HOURS).plusHours(1)
        )
        Assert.assertEquals(
            userEndTimeExpected,
            LocalTime.now().truncatedTo(ChronoUnit.HOURS).plusHours(3)
        )
    }

    @Test
    fun postStartTimeTest() {
        viewModel.initDateTimeFiltersDefault()
        viewModel.myDate.value = LocalDate.now().plusDays(1)
        viewModel.userEndTime.value = LocalTime.of(17, 30)
        val startTimeHours = 14
        val startTimeMinutes = 15
        viewModel.postStartTime(startTimeHours, startTimeMinutes)

        Assert.assertEquals(LocalTime.of(startTimeHours, startTimeMinutes), viewModel.userStartTime.value)
    }

    @Test
    fun postEndTimeTestWhenUserStartTimeHourSurpassesUserEndTimeHour() {
        val localTime = LocalTime.of(16, 0,0)
        viewModel.userStartTime.value = localTime

        val endTimeHours = 10
        val endTimeMinutes = 15
        val postEndTimeExpected = viewModel.postEndTime(endTimeHours, endTimeMinutes)
        print(viewModel.userEndTime.value)
        Assert.assertEquals(localTime, viewModel.userEndTime.value)
        Assert.assertEquals(postEndTimeExpected, true)
    }

    @Test
    fun postEndTimeTestWhenUserStartTimeHourNotSurpassesUserEndTimeHour() {
        val localTime = LocalTime.now().truncatedTo(ChronoUnit.HOURS)
        viewModel.userStartTime.value = localTime

        val endTimeHours = LocalTime.parse("23:00").hour
        val endTimeMinutes = 15
        val postEndTimeExpected = viewModel.postEndTime(endTimeHours, endTimeMinutes)

        Assert.assertEquals(endTimeHours, viewModel.userEndTime.value?.hour)
        Assert.assertEquals(postEndTimeExpected, false)
    }

    @Test
    fun getBuildingsTest() = testDispatcher.runBlockingTest {
        val mock = mock<MainActivityRepository>()
        mock.stub {
            onBlocking { getBuildingsProduction() } doReturn
                    Response.success(listOf(building))
        }
        viewModel.repository = mock
        viewModel.getBuildings()
        val expected = building
        val actual = viewModel.myBuildings.value?.get(0)
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun getBookingsTest() = testDispatcher.runBlockingTest {

        val bookingItemExpected = BookingResponse.Booking(
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            false,
            false,
            Owner("", "", ""),
            null,
            "",
            "",
            0, "",
            BookingResponse.Status("Pending", null, null),
            null
        )

        val date = LocalDate.now()
        val time = LocalTime.of(0, 0, 0)
        val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
        val dateTime =
            ZonedDateTime.of(
                date,
                time,
                ZoneId.systemDefault()
            ).format(formatter)

        val mock = mock<MainMyBookingsRepository>()
        mock.stub {
            onBlocking { getBookingsProduction(dateTime, user.email.toString(), "") } doReturn
                    Response.success(listOf(bookingItem))
        }
        viewModel.repositoryBookings = mock
        viewModel.userInfo.value = user
        viewModel.getBookings(context) { _: Context, _: BookingResponse.Booking ->}
        val bookingItemActual = bookingItemExpected
        Assert.assertEquals(bookingItemExpected, bookingItemActual)
    }

    @Test
    fun getBuildingsAreasTest() = testDispatcher.runBlockingTest {
        val mock = mock<MainActivityRepository>()
        mock.stub {
            onBlocking { getAreaByBuildingIdProduction(any()) } doReturn
                    Response.success(areas)
        }
        viewModel.repository = mock
        viewModel.getBuildingsAreas(listOf(building))
        val expected = "1"
        val actual = viewModel.buildingsAreas.value?.first()?.totalAreas
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun getCurrentUserTest() = testDispatcher.runBlockingTest {
        val mock = mock<MainActivityRepository>()
        mock.stub {
            onBlocking { getCurrentUser() } doReturn
                    Response.success(currentUser)
        }
        viewModel.repository = mock
        viewModel.getCurrentUser()
        Assert.assertEquals(viewModel.currentUser, currentUser)
    }

    @Test
    fun getUserGroupsTest() = testDispatcher.runBlockingTest {
        val mock = mock<MainActivityRepository>()
        mock.stub {
            onBlocking { getGroups(any()) } doReturn
                    Response.success(groups)
        }
        viewModel.repository = mock
        viewModel.getUserGroups(listOf("6243286a3504259aea126fae"))
        Assert.assertEquals(viewModel.groups, groups)
    }
}
