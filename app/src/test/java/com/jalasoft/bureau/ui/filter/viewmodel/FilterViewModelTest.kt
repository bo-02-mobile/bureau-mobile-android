package com.jalasoft.bureau.ui.filter.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jalasoft.bureau.data.model.asset.AssetLabel
import com.jalasoft.bureau.data.model.asset.AssetType
import com.jalasoft.bureau.rules.TestCoroutineRule
import com.jalasoft.bureau.ui.filter.repository.FilterRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.stub
import retrofit2.Response
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
class FilterViewModelTest : BeforeAllCallback, AfterAllCallback {

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)

    lateinit var filterViewModel: FilterViewModel
    lateinit var context: Context

    private val assetTypeAll = AssetType.AssetTypeItem("all", emptyList(), "All", "All", "All")
    private val assetTypeOther = AssetLabel("other", "other", "other", "other")
    private val assetTypes = arrayListOf(
        AssetType.AssetTypeItem("all", emptyList(), "All", "All", "All"),
        AssetType.AssetTypeItem(
            "612d7b9f090308c71776cde8",
            arrayListOf<String>(
                "612d7c6e090308c71776ce23",
                "61ae844810c3d9feaae0a197",
                "612d7c6e090308c71776ce21",
                "612d7c6e090308c71776ce22"
            ),
            "Conference Room",
            "Conference Room",
            "conference-room"
        )
    )
    private val assetLabels = arrayListOf<AssetLabel>(
        AssetLabel("612d7c6e090308c71776ce21", "Mouse", "Mouse", "mouse"),
        AssetLabel("612d7c6e090308c71776ce22", "Power", "Power", "power"),
        AssetLabel("61ae844810c3d9feaae0a196", "Desktop", "Desktop", "desktop")
    )

    private val mockWebServer = MockWebServer()

    private val client = OkHttpClient.Builder()
        .connectTimeout(1, TimeUnit.SECONDS)
        .readTimeout(1, TimeUnit.SECONDS)
        .writeTimeout(1, TimeUnit.SECONDS)
        .build()

    private val api = Retrofit.Builder()
        .baseUrl(mockWebServer.url("/"))
        .client(client)
        .build()

    @Before
    fun setUp() {
        val repository = FilterRepository(api)
        filterViewModel = FilterViewModel(repository)
        context = Mockito.mock(Context::class.java)
    }

    override fun beforeAll(context: ExtensionContext?) {
        Dispatchers.setMain(testDispatcher)
    }

    override fun afterAll(context: ExtensionContext?) {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
        testScope.cleanupTestCoroutines()
    }

    @Test
    fun getFilterTypes() = testDispatcher.runBlockingTest {
        val mock = mock<FilterRepository>()
        mock.stub {
            onBlocking { getFiltersTypes() } doReturn
                    Response.success(listOf(assetTypeAll))
        }
        val viewModel = FilterViewModel(mock)
        viewModel.getFiltersTypes()
        val expected = assetTypeAll
        val actual = viewModel.filterTypes.value?.get(0)
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun getFilterLabels() = testDispatcher.runBlockingTest {
        val mock = mock<FilterRepository>()
        mock.stub {
            onBlocking { getFiltersLabels() } doReturn
                    Response.success(listOf(assetTypeOther))
        }
        val viewModel = FilterViewModel(mock)
        viewModel.getFiltersLabels()
        val expected = assetTypeOther
        val actual = viewModel.filterLabels.value?.get(0)
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun testClearAllResetsProperties() {
        filterViewModel.arrLabels.value = listOf("Ethernet", "WiFi")
        filterViewModel.spaceTypeSelected.value = 1
        filterViewModel.spaceType.value = "type"
        filterViewModel.switchAvailableSpaces.value = true
        filterViewModel.clearAll()
        Assert.assertEquals(true, filterViewModel.arrLabels.value?.isEmpty())
        Assert.assertEquals(0, filterViewModel.spaceTypeSelected.value)
        Assert.assertEquals("all", filterViewModel.spaceType.value)
        Assert.assertEquals(false, filterViewModel.switchAvailableSpaces.value)
    }

    @Test
    fun testSaveFilterStateAssignsProperties() {
        filterViewModel.saveFilterState(1, "type", listOf("Ethernet", "WiFi"), true)
        Assert.assertEquals(2, filterViewModel.arrLabels.value?.size)
        Assert.assertEquals(1, filterViewModel.spaceTypeSelected.value)
        Assert.assertEquals("type", filterViewModel.spaceType.value)
        Assert.assertEquals(true, filterViewModel.switchAvailableSpaces.value)
    }

    @Test
    fun testCountFilterSelectedCountsUp() {
        filterViewModel.countFilterSelected("type", filterViewModel.arrLabels.value!!, true)
        Assert.assertEquals(2, filterViewModel.countFilterSelected.value)
    }

    @Test
    fun testGetFiltersByAssetTypeButtonALl() {
        val selectedAsset = 0
        val expected = filterViewModel.getFiltersByAssetType(assetTypes, assetLabels, selectedAsset)
        Assert.assertEquals(expected, assetLabels)
    }

    @Test
    fun testGetFiltersByAssetTypeWhenAssetSelected() {
        val selectedAsset = 1
        val expected = filterViewModel.getFiltersByAssetType(assetTypes, assetLabels, selectedAsset)
        val actual = assetLabels.take(2)
        Assert.assertEquals(expected, actual)
    }
}