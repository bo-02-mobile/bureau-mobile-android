package com.jalasoft.bureau.ui.scanner.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.model.asset.Asset
import com.jalasoft.bureau.data.model.booking.BookingResponse
import com.jalasoft.bureau.data.model.building.Building
import com.jalasoft.bureau.data.model.common.Owner
import com.jalasoft.bureau.data.model.common.RecurrenceRule
import com.jalasoft.bureau.data.model.scanner.BookingConfirmRequestBody
import com.jalasoft.bureau.data.model.scanner.BookingConfirmResponse
import com.jalasoft.bureau.data.model.token.User
import com.jalasoft.bureau.rules.TestCoroutineRule
import com.jalasoft.bureau.ui.bookings.repository.MainMyBookingsRepository
import com.jalasoft.bureau.ui.bookings.repository.MyBookingsProductionService
import com.jalasoft.bureau.ui.main.repository.MainActivityRepository
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import com.jalasoft.bureau.ui.scanner.repository.QRScannerRepository
import com.jalasoft.bureau.ui.workstations.repository.EventService
import com.jalasoft.bureau.utils.DATE_FORMAT
import com.jalasoft.bureau.utils.REQ_BODY_OP
import com.jalasoft.bureau.utils.REQ_BODY_PATH
import com.jalasoft.bureau.utils.UNDEFINED_TIMEZONE
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.stub
import retrofit2.Response
import retrofit2.Retrofit
import java.time.*
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
class QRScannerViewModelTest : BeforeAllCallback, AfterAllCallback {
    private val recurrenceRule = RecurrenceRule()
    private val bookingId = "61a780db15686466584bdf6h"
    private val workstationId = "61a7821815686466584bdf77"
    private val assetId = "61a7821815686466584bdf77"
    private val bookingConfirmResponseItem = BookingConfirmResponse(
        "61a780db15686466584bdf6h",
        "61a7821815686466584bdf77",
        "Pedro@gmail.com",
        "Pedro",
        "2021-11-01T17:00:00Z",
        "2021-11-02T19:00:00Z",
        true,
        recurrenceRule,
        false,
        false,
        "61a780db15686466584bdf6f",
        "1C Cowork",
        "1C",
        "61a7821815686466584bdf77",
        "Workstation",
        "60a807c5dc0ba843b1ac4063",
        "2021-11-01T15:00:00Z",
        "2021-11-01T14:30:00Z",
        Owner("Pedro@gmail.com", "60fec6871f9de1fa8a070583", "Pedro"),
        "2021-11-01T14:40:00Z",
        3
    )

    private val bookingResponseItem = BookingResponse.Booking(
        "610445e3846769858d9ccf6e",
        "NewArea3100",
        "W0031",
        "61a7821815686466584bdf77",
        "workstation",
        "60c11f74984d2c7a73a0da0e",
        null,
        "2021-07-29T19:15:51.205Z",
        "2021-06-31T19:15:51.205Z",
        "61a780db15686466584bdf6h",
        false,
        true,
        null,
        null,
        "2022-01-11T19:02:51.205Z",
        "2021-08-30T14:15:51.205Z",
        1,
        "",
        BookingResponse.Status("Pending", null, null),
        null
    )
    private val user = User(
        "pedro@gmail.com",
        "https://lh3.googleusercontent.com/a/AATXAJwTrrLqztPRtd1iBtaYao_LQ-6luvvdoaHkYSaC=s96-c",
        "Pedro"
    )
    private val asset = Asset.AssetItem(
        "60b13fe3b74284b5f27ffa12",
        "60a807c5dc0ba843b1ac4063",
        "1C001",
        "2020-03-26T10:28:10.294Z",
        "",
        "60b14364b74284b5f27ff903",
        "1C001", null, null,true, null, "2021-10-26T14:28:10.294Z", 1, true
    )
    private val bookingNotValidated = BookingResponse.Booking(
        "610445e3846769858d9ccf6e",
        "NewArea3100",
        "W0031",
        "61a7821815686466584bdf77",
        "workstation",
        "60c11f74984d2c7a73a0da0e",
        "",
        "2021-07-29T19:15:51.205Z",
        "2021-06-31T19:15:51.205Z",
        "61a780db15686466584bdf6h",
        false,
        true,
        null,
        null,
        "2022-01-11T19:02:51.205Z",
        "2021-08-30T14:15:51.205Z",
        1,
        "",
        BookingResponse.Status("Pending", null, null),
        null
    )
    private val bookingValidated = BookingResponse.Booking(
        "610445e3846769858d9ccf6e",
        "NewArea3100",
        "W0031",
        "61a7821815686466584bdf77",
        "workstation",
        "60c11f74984d2c7a73a0da0e",
        "2022-01-11T18:50:51.205Z",
        "2021-07-29T19:15:51.205Z",
        "2021-06-31T19:15:51.205Z",
        "61a780db15686466584bdf6h",
        false,
        true,
        null,
        null,
        "2022-01-11T19:02:51.205Z",
        "2021-08-30T14:15:51.205Z",
        1,
        "",
        BookingResponse.Status("Pending", null, null),
        null
    )
    val building = Building.BuildingItem(
        "JALA-COCHA",
        "Av Melchor Pérez de Olguín 2643",
        "60a807c5dc0ba843b1ac4063",
        "Jala Building",
        "Jala Building",
        true,
        "UTC-04",
        "2021-06-21",
        1,
        "0"
    )

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    lateinit var mainViewModel: MainActivityViewModel
    lateinit var context: Context
    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)
    private lateinit var viewModel: QRScannerViewModel

    private val mockWebServer = MockWebServer()

    private val client = OkHttpClient.Builder()
        .connectTimeout(1, TimeUnit.SECONDS)
        .readTimeout(1, TimeUnit.SECONDS)
        .writeTimeout(1, TimeUnit.SECONDS)
        .build()

    private val api = Retrofit.Builder()
        .baseUrl(mockWebServer.url("/"))
        .client(client)
        .build()

    override fun beforeAll(context: ExtensionContext?) {
        Dispatchers.setMain(testDispatcher)
    }

    override fun afterAll(context: ExtensionContext?) {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
        testScope.cleanupTestCoroutines()
    }

    @Before
    fun setUp() {
        context = Mockito.mock(Context::class.java)

        val mainMock = mock<MainActivityRepository>()
        val myBookingMock = mock<MainMyBookingsRepository>()
        val prefs = Prefs(context)
        mainViewModel = MainActivityViewModel(mainMock, myBookingMock, prefs)

        val myBookingsProductionServiceMock = MyBookingsProductionService(api)
        val eventServiceMock = EventService(api)
        val mayBookingsRepositoryMock = MainMyBookingsRepository(myBookingsProductionServiceMock, eventServiceMock)
        viewModel = QRScannerViewModel(mock<QRScannerRepository>(), mayBookingsRepositoryMock)
    }

    @Test
    fun confirmBookingTest() = testDispatcher.runBlockingTest {
        val mock = mock<QRScannerRepository>()
        mock.stub {
            onBlocking {
                confirmBookingByCode(
                    any(),
                    any()
                )
            } doReturn
                    Response.success(bookingConfirmResponseItem)
        }

        viewModel.repository = mock
        viewModel.confirmBooking(bookingId) { _: Response<BookingConfirmResponse>, _: String ->}
        val expected = bookingConfirmResponseItem
        val actual = viewModel.bookingConfirmResponse.value
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun checkIfTheBookingExistsTest() = testDispatcher.runBlockingTest {
        mainViewModel.bookingQR.value = bookingResponseItem

        val mock = mock<QRScannerRepository>()
        mock.stub {
            onBlocking {
                confirmBookingByCode(
                    any(),
                    any()
                )
            } doReturn
                    Response.success(bookingConfirmResponseItem)
        }
        viewModel.repository = mock

        val bookingResponseItem = BookingResponse.Booking(
            "610445e3846769858d9ccf6e",
            "NewArea3100",
            "W0031",
            "61a7821815686466584bdf77",
            "workstation",
            "60c11f74984d2c7a73a0da0e",
            null,
            "2021-07-29T19:15:51.205Z",
            "2021-06-31T19:15:51.205Z",
            "61a780db15686466584bdf6h",
            false,
            true,
            null,
            null,
            "2022-01-11T19:02:51.205Z",
            "2021-08-30T14:15:51.205Z",
            1,
            "",
            BookingResponse.Status("Pending", null, null),
            null
        )

        viewModel.checkIfTheBookingExists(
            context,
            bookingResponseItem,
            workstationId,
            mainViewModel.userInfo.value,
            mainViewModel.myDate.value,
            mainViewModel.userStartTime.value,
            mainViewModel.userEndTime.value,
            {_, _ ->},
            {}
        )
        val expected = bookingConfirmResponseItem
        val actual = viewModel.bookingConfirmResponse.value
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun findBookWithAssetIdTest() = testDispatcher.runBlockingTest {
        mainViewModel.userInfo.value = user
        val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
        val mock = mock<QRScannerRepository>()
        mock.stub {
            onBlocking {
                confirmBookingByCode(
                    any(),
                    any()
                )
            } doReturn
                    Response.success(bookingConfirmResponseItem)
        }

        val dateLocal = LocalDate.now()
        val time = LocalTime.of(0, 0, 0)
        val dateTime = ZonedDateTime.of(dateLocal, time, ZoneId.systemDefault()).format(formatter)
        val ownerEmail = "pedro@gmail.com"
        val mockBookingsRepository = mock<MainMyBookingsRepository>()
        mockBookingsRepository.stub {
            onBlocking { getBookingsProduction(dateTime, ownerEmail, "") } doReturn
                    Response.success(listOf(bookingResponseItem))
        }

        viewModel.repository = mock
        viewModel.repositoryBookings = mockBookingsRepository
        viewModel.findBookingWithAssetId(
            context,
            assetId,
            mainViewModel.userInfo.value,
            mainViewModel.myDate.value,
            mainViewModel.userStartTime.value,
            mainViewModel.userEndTime.value,
            { _, _ ->  },
            {}
        )
        val expected = null
        val actual = viewModel.bookingConfirmResponse.value
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun isBookingTodayTest() {
        val mock = mock<QRScannerRepository>()
        mock.stub {
            onBlocking {
                confirmBookingByCode(
                    any(),
                    any()
                )
            } doReturn
                    Response.success(bookingConfirmResponseItem)
        }
        viewModel.repository = mock
        val bookingDate = LocalDateTime.now()
        val tomorrow = LocalDateTime.now().plusDays(1)
        val yesterday = LocalDateTime.now().minusDays(1)
        Assert.assertTrue(viewModel.isBookingToday(bookingDate))
        Assert.assertFalse(viewModel.isBookingToday(tomorrow))
        Assert.assertFalse(viewModel.isBookingToday(yesterday))
    }

    @Test
    fun isBookingInValidationRangeTest() {
        val mock = mock<QRScannerRepository>()
        mock.stub {
            onBlocking {
                confirmBookingByCode(
                    any(),
                    any()
                )
            } doReturn
                    Response.success(bookingConfirmResponseItem)
        }
        viewModel.repository = mock
        val bookingDate = LocalDateTime.now()
        val laterToday = LocalDateTime.now().plusHours(5)
        val earlierToday = LocalDateTime.now().minusHours(5)
        Assert.assertTrue(viewModel.isBookingInValidationRange(bookingDate))
        Assert.assertFalse(viewModel.isBookingInValidationRange(laterToday))
        Assert.assertFalse(viewModel.isBookingInValidationRange(earlierToday))
    }

    @Test
    fun isBookingNotValidatedTest() {
        val mock = mock<QRScannerRepository>()
        mock.stub {
            onBlocking {
                confirmBookingByCode(
                    any(),
                    any()
                )
            } doReturn
                    Response.success(bookingConfirmResponseItem)
        }
        viewModel.repository = mock
        val tempBooking = bookingResponseItem
        Assert.assertTrue(viewModel.isBookingNotValidated(tempBooking))
        Assert.assertTrue(viewModel.isBookingNotValidated(bookingNotValidated))
        Assert.assertFalse(viewModel.isBookingNotValidated(bookingValidated))
    }

    @Test
    fun getTimezoneFromBuildingIdTest() {
        val mock = mock<QRScannerRepository>()
        mock.stub {
            onBlocking {
                confirmBookingByCode(
                    any(),
                    any()
                )
            } doReturn
                    Response.success(bookingConfirmResponseItem)
        }
        viewModel.repository = mock
        val expected = "UTC-04"
        var actual = viewModel.getTimezoneFromBuildingId("", mainViewModel.myBuildings.value)
        Assert.assertEquals(UNDEFINED_TIMEZONE, actual)
        actual = viewModel.getTimezoneFromBuildingId("randomID", mainViewModel.myBuildings.value)
        Assert.assertEquals(UNDEFINED_TIMEZONE, actual)
        mainViewModel.myBuildings.value = listOf(building)
        actual = viewModel.getTimezoneFromBuildingId("Jala Building", mainViewModel.myBuildings.value)
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun getAssetDetails() = testDispatcher.runBlockingTest {
        val mockQRScannerRepository = mock<QRScannerRepository>()
        mockQRScannerRepository.stub {
            onBlocking {
                confirmBookingByCode(
                    any(),
                    any()
                )
            } doReturn
                    Response.success(bookingConfirmResponseItem)
        }

        val mock = mock<MainMyBookingsRepository>()
        mock.stub {
            onBlocking { getAssetDetailsProduction(assetId) }  doReturn
                    Response.success(asset)
        }
        viewModel.repository = mockQRScannerRepository
        viewModel.repositoryBookings = mock
        viewModel.getAssetDetails(
            assetId,
            {},
            mainViewModel.myDate.value,
            mainViewModel.userStartTime.value,
            mainViewModel.userEndTime.value
        )
        val expected = asset
        val actual = viewModel.newAssetDetails.value
        Assert.assertEquals(expected, actual)
    }

    @Test
    fun testSetDateTimeFiltersQRNewBooking() {
        val mock = mock<QRScannerRepository>()
        mock.stub {
            onBlocking {
                confirmBookingByCode(
                    any(),
                    any()
                )
            } doReturn
                    Response.success(bookingConfirmResponseItem)
        }
        viewModel.repository = mock
        viewModel.setDateTimeFiltersQRNewBooking {
            mainViewModel.myDate.value = LocalDate.now()
            mainViewModel.userStartTime.value = LocalTime.now()
            mainViewModel.userEndTime.value = LocalTime.of(18, 30)
        }
        val expectedDate = LocalDate.now()
        Assert.assertEquals(expectedDate, mainViewModel.myDate.value)
        val expectedStartTime = LocalTime.now()
        // Testing for individual properties in case of seconds difference during runtime
        Assert.assertEquals(expectedStartTime.hour, mainViewModel.userStartTime.value?.hour)
        Assert.assertEquals(expectedStartTime.minute, mainViewModel.userStartTime.value?.minute)
        val expectedEndTime = LocalTime.of(18, 30)
        Assert.assertEquals(expectedEndTime, mainViewModel.userEndTime.value)
    }

    @Test
    fun testSaveCurrentDateTimeFilters() {
        val mock = mock<QRScannerRepository>()
        mock.stub {
            onBlocking {
                confirmBookingByCode(
                    any(),
                    any()
                )
            } doReturn
                    Response.success(bookingConfirmResponseItem)
        }
        viewModel.repository = mock
        mainViewModel.initDateTimeFiltersDefault()
        viewModel.saveCurrentDateTimeFilters(
            mainViewModel.myDate.value,
            mainViewModel.userStartTime.value,
            mainViewModel.userEndTime.value
        )
        val expectedDate = LocalDate.now()
        Assert.assertEquals(expectedDate, viewModel.tempMyDate)
        val expectedStartTime = LocalTime.of(8, 30)
        Assert.assertEquals(expectedStartTime, viewModel.tempMyStartTime)
        val expectedEndTime = LocalTime.of(18, 30)
        Assert.assertEquals(expectedEndTime, viewModel.tempMyEndTime)
    }

    @Test
    fun testResetDateTimeFilters() {
        val mock = mock<QRScannerRepository>()
        mock.stub {
            onBlocking {
                confirmBookingByCode(
                    any(),
                    any()
                )
            } doReturn
                    Response.success(bookingConfirmResponseItem)
        }
        viewModel.repository = mock
        mainViewModel.initDateTimeFiltersDefault()
        viewModel.saveCurrentDateTimeFilters(
            mainViewModel.myDate.value,
            mainViewModel.userStartTime.value,
            mainViewModel.userEndTime.value
        )
        viewModel.resetDateTimeFilters { _, _, _ -> }
        val expectedDate = LocalDate.now()
        Assert.assertEquals(expectedDate, mainViewModel.myDate.value)
        Assert.assertNull(viewModel.tempMyDate)
        val expectedStartTime = LocalTime.of(8, 30)
        Assert.assertEquals(expectedStartTime, mainViewModel.userStartTime.value)
        Assert.assertNull(viewModel.tempMyStartTime)
        val expectedEndTime = LocalTime.of(18, 30)
        Assert.assertEquals(expectedEndTime, mainViewModel.userEndTime.value)
        Assert.assertNull(viewModel.tempMyEndTime)
    }
}