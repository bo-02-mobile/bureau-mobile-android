package com.jalasoft.bureau

import android.content.Context
import com.jalasoft.bureau.data.db.Prefs
import com.jalasoft.bureau.data.model.workstation.Workstation
import com.jalasoft.bureau.ui.bookings.repository.MainMyBookingsRepository
import com.jalasoft.bureau.ui.main.repository.MainActivityRepository
import com.jalasoft.bureau.ui.main.viewmodel.MainActivityViewModel
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.mock

class WorkStationResultsTest {

    private lateinit var repository: WorkStationMockRepository
    private lateinit var viewModel: MainActivityViewModel
    private lateinit var context: Context

    @Before
    fun setUp() {
        context = Mockito.mock(Context::class.java)
        repository = WorkStationMockRepository()
        val mainMock = mock<MainActivityRepository>()
        val myBookingMock = mock<MainMyBookingsRepository>()
        val prefs = Prefs(context)
        viewModel = MainActivityViewModel(mainMock, myBookingMock, prefs)
    }

    @Test
    fun getWorkstationsProduction(){
        val areaId = "60a807f1dc0ba843b1ac4064"
        val temp = repository.getWorkstationsByAreaIdProduction(areaId)
        Assert.assertEquals(temp.size,3)
        temp.forEach { workstation ->
            Assert.assertEquals(workstation.areaId,areaId)
            Assert.assertEquals(workstation.code,"3A013")
            Assert.assertEquals(workstation.description,"")
            Assert.assertEquals(workstation.features.size,2)
            Assert.assertEquals(workstation.published,true)
        }
    }

    @Test
    fun getWorkstationsDemo(){
        val areaId = "602a807f1dc33ba843b14ac4064"
        val temp = repository.getWorkstationsByAreaIdDemo(areaId)
        Assert.assertEquals(temp.size,3)
        temp.forEach { workstation ->
            Assert.assertEquals(workstation.areaId,areaId)
            Assert.assertEquals(workstation.code,"3A013")
            Assert.assertEquals(workstation.description,"")
            Assert.assertEquals(workstation.published,true)
            Assert.assertEquals(workstation.features[0],"power")
        }
    }
}

class WorkStationMockRepository {
    fun getWorkstationsByAreaIdProduction(areaId: String): List<Workstation.WorkstationItem> {
        return listOf(
            Workstation.WorkstationItem(
                areaId,
                "3A013",
                "",
                listOf("power", "wifi"),
                "power",
                "wifi",
                "UTC",
                true,
                0.0,
                0.0
            ),
            Workstation.WorkstationItem(
                areaId,
                "3A013",
                "",
                listOf("power", "wifi"),
                "power",
                "wifi",
                "UTC",
                true,
                0.0,
                0.0
            ),
            Workstation.WorkstationItem(
                areaId,
                "3A013",
                "",
                listOf("power", "wifi"),
                "power",
                "wifi",
                "UTC",
                true,
                0.0,
                0.0
            ),
        )
    }
    fun getWorkstationsByAreaIdDemo(areaId: String): List<Workstation.WorkstationItem> {
        return listOf(
            Workstation.WorkstationItem(
                areaId,
                "3A013",
                "",
                listOf("power", "wifi"),
                "power",
                "wifi",
                "UTC",
                true,
                0.0,
                0.0
            ),
            Workstation.WorkstationItem(
                areaId,
                "3A013",
                "",
                listOf("power", "wifi", "ethernet"),
                "power",
                "wifi",
                "UTC",
                true,
                0.0,
                0.0
            ),
            Workstation.WorkstationItem(
                areaId,
                "3A013",
                "",
                listOf("power", "wifi", "ethernet", "tv"),
                "power",
                "wifi",
                "UTC",
                true,
                0.0,
                0.0
            ),
        )
    }
}

